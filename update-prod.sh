#!/bin/sh
cd ~/webapp
git fetch --all
git reset --hard origin/master
git pull origin master
sudo docker build -t icas:app .
sudo docker rm -f app
sudo docker run -d --name app --network micro --restart always icas:app