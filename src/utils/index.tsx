import {Spin} from 'antd'
import * as React from 'react'

export type Permission = { permission: string, name: string };
export type Rule = string;

/**
 * Tests if a string looks like a rule
 * @param rule A string formatted as a path
 */
export function isRule(rule: Rule): boolean {
	return /^((\/)|(((\/[a-zA-Z0-9\-\_]+))+))$/.test(rule);
}

/**
 * Tests if a string looks like a permission
 * @param permission A string formatted as a path with wildcards for generic names
 */
export function isPermission(permission: Permission): boolean {
	return /^((\/)|(((\/[a-zA-Z0-9\-\_]+)|(\/\*))+))$/.test(permission.permission);
}

/**
 * Test if the permission verifies the rule
 * @param rule
 * @param permission
 */
export function isPermitted(rule: Rule, permission: Permission): boolean {
	const a = rule.replace(/^\//, '').split('/');
	const comp = permission.permission.replace(/^\//, '').split('/');

	if (a.length < comp.length)
		return false;

	if (!comp[0])
		return true;

	for (let i = 0; i < comp.length; ++i) {
		if (comp[i] !== '*' && comp[i] !== a[i])
			return false;
	}
	return true;
}

/**
 * Tests if a set of permissions verifies a rule
 * @param rule
 * @param permissions
 */
export function hasPermission(rule: Rule, permissions: Permission[] = []): boolean {
	for ( const i in permissions) {
		if (isPermitted(rule, permissions[i]))
			return true;
	}
	return false;
}

export function noPermission(loading: boolean): JSX.Element {
	return <div className='no-permission'> {loading ? <Spin tip='Fetching permissions' /> : <h1>You do not have permission to view this page</h1>}</div>
}