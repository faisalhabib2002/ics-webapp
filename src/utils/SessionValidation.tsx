import Cookies from 'universal-cookie';
import {robins} from '../robins'
const {AuthRobin} = robins
const cookies = new Cookies();

export function SessionValidation(): void {
	if (!cookies.get('connect.sid') && AuthRobin.getUserInfo()) {
		AuthRobin.isLoggedIn()
	}
}