import * as React from 'react'
import {HashRouter as Router, Route, Switch, Redirect} from 'react-router-dom'
import {UsersDashboard, RolesDashboard} from  './pages'

export class UsermanagementApp extends React.Component{
	render(): JSX.Element {
		return(
			<Router>
				<Switch>
					<Redirect exact from='/usermanagement' to='/usermanagement/users'/>
					<Route exact path='/usermanagement/users' component={UsersDashboard} />
					<Route exact path='/usermanagement/roles' component={RolesDashboard} />
					<Redirect to='/' />
				</Switch>
			</Router>
		)
	}
}

export default UsermanagementApp