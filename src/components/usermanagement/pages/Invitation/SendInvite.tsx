import * as React from "react";
import { Modal } from "@simplus/siui"
import { Tooltip, Button, Icon, Form, Input, notification } from "antd"
import axios from 'axios'
import {ErrorBoundary} from '../../../../utils/ErrorBoundary'
const { TextArea } = Input;

export {InviteUser}

export interface InviteUserProps{
	id: string
}

export default class InviteUser extends React.Component<InviteUserProps, any> {
    constructor(props){
        super(props)
        this.state = {
			visible : false,
			message: '',
		}
		this.handleChange = this.handleChange.bind(this)
		this.handleInvite = this.handleInvite.bind(this)
        this.showModal = this.showModal.bind(this);
	}
	handleChange = (c) => {
		this.setState({ [c.target.name]: c.target.value })
	}
	handleInvite(e): void {
		e.preventDefault()
		const { message} = this.state
		axios.post('/users/invitation/'+this.props.id, {
			message,
		}).then( () => {
			notification.success({
				message: 'Success !',
				description: 'Invitation Sent',
			});
			this.setState({ visible: false });
		}).catch(() => {
			notification.error({
				message: 'Error !',
				description: 'Could not send invitation',
			});
		})
	}
    showModal () {
        this.setState({
			visible : true,
			name: '',
			email: '',
			message: '',
			role : ''
            
        });
    }

    onCancel = () => {
        this.setState({
            visible : false,
        });
    }

    render(){
        return (
			<ErrorBoundary>
				<span>
					<Tooltip title='Click to send invitation to change password'>
						<a onClick={this.showModal}>
							<Icon type='mail' />
						</a>
					</Tooltip>
					<Modal onClickOutside visible={this.state.visible} onCancel={this.onCancel} title="Invite User" >
					<Form style={{width: '400px', marginLeft: 'auto', marginRight:'auto'}}>
							<Form.Item label='' required= {true}>
										<TextArea name='message' rows= {6} placeholder = 'Enter Personalised Message' style = {{width: '400px'}} onChange = {this.handleChange} />
							</Form.Item>
						</Form>
						<Button type = 'primary' onClick = {this.handleInvite}>Send Invite</Button>
					</Modal>
				</span>
			</ErrorBoundary>
        );
    }
}