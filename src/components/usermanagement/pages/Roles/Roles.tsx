import * as React from 'react'
import {Tabs, TabPane, Card} from '@simplus/siui'
import Divider from 'antd/lib/divider'
import Table from 'antd/lib/table'
import Icon from 'antd/lib/icon'
import Button from 'antd/lib/button'
import Tooltip from 'antd/lib/tooltip'
import { Redirect } from 'react-router';
import {ErrorBoundary} from '../../../../utils/ErrorBoundary'

export class RolesDashboard extends React.Component {
	state = {redirect: false}

	render(): JSX.Element {
		const { Column } = Table;
		const data = [{
			role: 'Lyra SA Super User',
			permissions: 'Icas permissions, Settings, Admin users, Admin Roles, Programme Info, Dashboard: Engagement'
		}, {
			role: 'Admin',
			permissions: 'Icas permissions, Settings, Admin users, Admin Roles, Programme Info, Dashboard: Engagement'
		}, {
			role: 'Manager',
			permissions: 'Icas permissions, Settings, Admin users, Admin Roles, Programme Info, Dashboard: Engagement'
		}, {
			role: 'Employee',
			permissions: 'Icas permissions, Settings, Admin users, Admin Roles, Programme Info, Dashboard: Engagement'
		}, {
			role: 'Custodian',
			permissions: 'Icas permissions, Settings, Admin users, Admin Roles, Programme Info, Dashboard: Engagement'
		}]
		const RolesCard =
		<ErrorBoundary>
			<Card>
					<div style={{paddingLeft: '70px', paddingTop: '30px', fontWeight: 'bold'}}>
						<h3 style={{fontWeight: 'bold'}}>Roles</h3>
					</div>
					<div style={{paddingRight: '70px', paddingBottom: '60px'}}>
						<Button type='primary' style={{float: 'right'}} >New Roles <Icon type='plus' /></Button>
					</div>
					<div>
				<Table dataSource={data} rowKey={'role'} size='middle' style={{paddingLeft: '70px', paddingRight: '70px', paddingBottom: '70px'}}>
							<Column
								title='Role'
								dataIndex='role'
								key='role'
							/>
							<Column
								title='Permissions'
								dataIndex='permissions'
								key='permissions'
							/>
							<Column
									title='Action'
									key='action'
									render={() => (
										<span>
											<a href='#'>
											<Tooltip title='Click To Edit Role'>
											<Icon type='edit' />
											</Tooltip>
											</a>
											<Divider type='vertical' />
											<a href='#'>
											<Tooltip title='Click To Delete Role'>
											<Icon type='delete' />
											</Tooltip>
											</a>
										</span>
								)}
							/>
				</Table>
				</div>
			</Card>
		</ErrorBoundary>
		if (this.state.redirect) {
			this.state.redirect = false;
			return <Redirect to='/usermanagement/users'/>
		}
		return(
			<ErrorBoundary>
				<Tabs selectedDefault={1} fillContainer className= 'tab' style={{fontWeight: 'bold'}}
				active={(current) => {
					if (current !== 1)
						this.setState({redirect: true})
				}}
				>
					<TabPane label='Users' >
					</TabPane>
					<TabPane label='Roles' >
						{RolesCard}
					</TabPane>
				</Tabs>
			</ErrorBoundary>
		)
	}

}

export default RolesDashboard