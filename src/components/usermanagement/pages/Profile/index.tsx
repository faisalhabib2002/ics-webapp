import * as React from 'react'
import {HashRouter as Router, Route, Switch, Redirect} from 'react-router-dom'
import {EditProfile} from './EditProfile'
import { connectRobin } from '@simplus/robin-react';
import {robins} from '../../../../robins'

const {AuthRobin} = robins

@connectRobin([AuthRobin])
export class ProfileApp extends React.Component {
	render(): JSX.Element {
		const user = AuthRobin.getUserInfo();
		if(! user ) {
			return <div></div>
		}
		return(
			<Router>
				<Switch>
					<Redirect exact from='/user' to={`/user/profile/${user._id}`} />
					<Redirect exact path ='/user/profile' to={`/user/profile/${user._id}`}  />
					<Route path = {`/user/profile/:id`} component={EditProfile} />
					<Route exact path = {`/user/:id`} component={EditProfile} />
					<Redirect to='/' />
				</Switch>
			</Router>
		)
	}
}

export default ProfileApp