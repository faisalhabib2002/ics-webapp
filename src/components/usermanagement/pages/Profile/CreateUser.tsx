import * as React from 'react';
import Form from 'antd/lib/form'
import Input from 'antd/lib/input'
import Button from 'antd/lib/button'
import Modal from 'antd/lib/modal'
import Icon from 'antd/lib/icon'
import notification from 'antd/lib/notification'
import Select from 'antd/lib/select'
import { FormComponentProps } from 'antd/lib/form';
import {robins} from '../../../../robins'
import {ErrorBoundary} from '../../../../utils/ErrorBoundary'
const createNotification = (type) => {
	notification[type]({
		message: 'Success !',
		description: 'User Created',
	});
}
const Option = Select.Option
const {UsersRobin} = robins
interface FormProps extends FormComponentProps {
	visible: boolean,
	onCancel(): void,
	onCreate(): void,
	wrappedComponentRef?(formref: any): void
}

class CreateUserForm extends React.Component<FormProps> {

	/**
	 *  Render method
	 */
	render(): JSX.Element {
		const { visible, onCancel, onCreate } = this.props;
		const { getFieldDecorator } = this.props.form;


		return <ErrorBoundary>
			<div>
				<Modal
					visible={visible}
					title='Create a new User'
					okText='Create'
					onCancel={onCancel}
					onOk={onCreate}
					>
						<Form style={{width: '400px', marginLeft: 'auto', marginRight: 'auto'}}>
							<Form.Item label=''>
								{getFieldDecorator('name', {
									rules: [{ required: true, message: 'Enter Name of the User' }],
								})(
										<Input placeholder = 'Enter Name of User' style = {{width: '400px'}} />
								)}
							</Form.Item>
							<Form.Item label=''>
								{getFieldDecorator('email', {
									rules: [{ message: 'Enter email ID' }],
								})(
										<Input type='email' placeholder = 'Enter Email ID of User' style = {{width: '400px'}} />
								)}
							</Form.Item>
							<Form.Item label=''>
								{getFieldDecorator('password', {
									rules: [{ required: true, message: 'Enter password for user' }],
								})(
										<Input placeholder= 'Enter Password' type='password' style = {{width: '400px'}} />
								)}
							</Form.Item>
							<Form.Item label=''>
								{getFieldDecorator('status', {
									rules: [{ required: true, message: '' }],
								})(
									<Select placeholder='Select User Status' style={{width : '400px', color: '#595959'}}>
										<Option value='true'>Active</Option>
										<Option value='false'>Inactive</Option>
									</Select>
								)}
							</Form.Item>
							<Form.Item label=''>
								{getFieldDecorator('client')(
									<Select placeholder='Company' style={{width : '400px', color: '#595959'}}>
										<Option value='Barclays'>Barclays</Option>
										<Option value='Ned-Bank'>Ned Bank</Option>
										<Option value='Standard-Bank'>Standard Bank</Option>
										<Option value='Simplus'>Simplus Innovation</Option>
										<Option value='JSE'>JSE</Option>
									</Select>
								)}
							</Form.Item>
						</Form>
				</Modal>
			</div>
		</ErrorBoundary>
	}

}

export class CollectionsPage extends React.Component {
		formRef: any = null
		state = {
			visible: false,
		};
		showModal = () => {
			this.setState({ visible: true });
		}
		handleCancel = () => {
			this.setState({ visible: false });
		}
		handleCreate = () => {
			const form = this.formRef.props.form;
			form.validateFields((err, values) => {
				values.status = true;
				if (err) {
					return;
				}
				if (!err) {
						createNotification('success')
						UsersRobin.create(values)
						form.resetFields();
						this.setState({ visible: false });
					}
				});
				setTimeout(location.reload(true), 1000);
		}
		saveFormRef = (formRef) => {
			this.formRef = formRef;
		}
		render() {
			return (
				<ErrorBoundary>
					<div>
						<Button style={{float: 'right', marginRight: '10px'}} type='primary' onClick={this.showModal}>Create User <Icon type='user' /></Button>
						<CreateUserFormPage
							wrappedComponentRef={this.saveFormRef}
							visible={this.state.visible}
							onCancel={this.handleCancel}
							onCreate={this.handleCreate}
						/>
					</div>
				</ErrorBoundary>
			);
		}
	}
export const CreateUserFormPage = Form.create({

})(CreateUserForm);
export default CollectionsPage;



