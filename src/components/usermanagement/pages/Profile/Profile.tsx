import * as React from 'react'
import {TitledCard, ProfilePicture, Select} from '@simplus/siui'
import Input from 'antd/lib/input'
import Button from 'antd/lib/button'
import Icon from 'antd/lib/icon'
import notification from 'antd/lib/notification'
import {connectRobin} from '@simplus/robin-react'
import {robins} from '../../../../robins'
import {ErrorBoundary} from '../../../../utils/ErrorBoundary'
const Option = Select.Option
const onSave = (type) => {
	notification[type]({
		message: 'Success',
		description: 'Profile Updated',
	});
};
export class ProfileDashboard extends React.Component {
	render(): JSX.Element {
		const user = robins.AuthRobin.getUserInfo()

		return(
			<ErrorBoundary>
				<div style={{width: '500px', marginRight: 'auto', marginLeft: 'auto'}}>
					<TitledCard
					titleStyle={{ background : 'rgb(43, 75, 126)' }}
						margin
						rounded
						picture={<ProfilePicture url={user.picture || 'src/assets/img/profile.png'} margin size={150} rounded/>}>
						<h2 style={{textAlign : 'center', paddingBottom: '20px'}}>User Profile</h2>
							<div style={{width: '300px', marginRight: 'auto', marginLeft: 'auto'}}>
								<Input disabled placeholder='Enter User Name' defaultValue = 'ICAS_S123' prefix={<Icon type='tag-o' />} style={{paddingBottom: '15px'}}/>
								<Input key='name' placeholder='Enter User Name' defaultValue ={user.name}  prefix={<Icon type='user' />} style={{paddingBottom: '15px'}} />
								<Input key='password' placeholder='Enter Password' type= 'password'  prefix={<Icon type='mail' />} style={{paddingBottom: '15px'}}/>
								<Input key='email' placeholder='Enter User Name' defaultValue = {user.email}  prefix={<Icon type='mail' />} style={{paddingBottom: '15px'}}/>
								<Select style={{width: '100%', paddingBottom: '10px'}} placeholder='Select a Client' >
									<Option value='Barclays' >Barclays</Option>
									<Option value='Capitech'>Capitech</Option>
									<Option value='Ned-Bank'>Ned-bank</Option>
									<Option value='Simplus' >Simplus</Option>
								</Select>
								<Select style={{width: '100%'}} placeholder='Select a Role'>
									<Option value='superuser' >Lyra SA Super User</Option>
									<Option value='admin'>Admin</Option>
									<Option value='custodian'>Lyra SA Custodian</Option>
									<Option value='exco' >Exco</Option>
									<Option value='manager' >Manager</Option>
									<Option value='employee' >Employee</Option>
								</Select>

							</div>
						<div style = {{display: 'flex', justifyContent: 'space-evenly'}}>
						<Button type='primary' onClick = {() => onSave('success')} >Save Profile</Button>
						</div>
					</TitledCard>
				</div>
			</ErrorBoundary>
		)
	}
}

export default ProfileDashboard