import * as React from 'react'
import Table from 'antd/lib/table'
import Icon from 'antd/lib/icon'
import Divider from 'antd/lib/divider'
import Switch from 'antd/lib/switch'
import Tooltip from 'antd/lib/tooltip'
import notification from 'antd/lib/notification'
import Popconfirm from 'antd/lib/popconfirm'
import message from 'antd/lib/message'
import {Tabs, TabPane, Card} from '@simplus/siui'
import {connectRobin} from '@simplus/robin-react';
import {robins} from '../../../../robins'
import CreateUserForm from '../Profile/CreateUser'
import InviteUser from '../Invitation/SendInvite'
import { Link, Redirect } from 'react-router-dom';
import {ErrorBoundary} from '../../../../utils/ErrorBoundary'

const userActivated = (type) => {
	notification[type]({
		message: 'User Activated',
	});
};
const userDeactivated = (type) => {
	notification[type]({
		message: 'User Deactivated',
	});
};

const {UsersRobin} = robins;

export interface UsersDashboardOwnProps {
	myState ?: boolean
}
interface User {
	_id: string,
	name: string,
	email: string,
	role: string,
	client: string,
	status: boolean
}

@connectRobin([UsersRobin])
export class UsersDashboard extends React.Component<UsersDashboardOwnProps> {
	state = {redirect: false}

	componentDidMount(): void {
		UsersRobin.get(`users`, '/')
	}

	render(): JSX.Element {
		const UsersList = UsersRobin.getResult(`users`) ;
		const { Column } = Table;
		const data: User[] = []
		if (UsersList !== null) {
			UsersList.map(ul => {
				data.push({ _id: ul._id, name: ul.name, email: ul.email, role: ul.role, client: ul.client, status: ul.status})
			});
		}

		function onDelete(item): void {
			UsersRobin.delete(item._id, `/${item._id}`)
			notification.open({
				message: 'Success',
				description: 'User Deleted'
			});
			setTimeout(location.reload(), 1000);
		}

		function cancel(): void {
			message.error('Delete Aborted');
		}

		function onChange(checked: boolean, item): void {
			if(checked === true) {
				userActivated('success')
				UsersRobin.put(item._id, `/activate/${item._id}`, {status : true} )
			}
			if (checked === false) {
				userDeactivated('error')
				UsersRobin.put(item._id, `/deactivate/${item._id}`, {status : false})
			}
		}

		function itemStatus(item): boolean {
			if (item.status === 'true') {
				return true
			} else {
				return false
			}
		}

		const Users =
		<ErrorBoundary>
			<Card
			loading = {UsersList === null || UsersList === undefined}>
				<div style={{paddingLeft: '100px', paddingTop: '30px'}}>
					<h3 style={{fontWeight: 'bold'}}>Users</h3>
				</div>
				<div style={{paddingRight: '100px', paddingBottom: '20px'}}>
						<CreateUserForm  />
				</div>

				<div style={{paddingTop: '15px'}}>
				<Table rowKey={r => r._id} dataSource={data} style={{paddingLeft: '100px', paddingRight: '100px', paddingBottom: '70px'}}>
						<Column
							title='Full Name'
							dataIndex='name'
							key='name'
						/>
						<Column
								title='Email ID'
								dataIndex='email'
								key='email'
						/>
						<Column
								title='Role'
								dataIndex='role'
								key='role'
						/>
						<Column
								title='Client'
								dataIndex='client'
								key='client'
						/>
						<Column
								title='User Action'
								key='action'
								render={((item) => (
									<span>
											<InviteUser id={item._id}/>
												<Divider type='vertical' />
											<Tooltip title='Click To Edit User'>
												<Link to={`/user/${item._id}`} ><Icon type='edit' /></Link>
											</Tooltip>
												<Divider type='vertical' />
												<Popconfirm placement= 'rightTop' title= 'Are you sure delete this user?' onConfirm={() => {onDelete(item)}} onCancel={cancel} okText='Yes' cancelText='No'>
													<a><Icon type='delete' /></a>
												</Popconfirm>
												<Divider type='vertical' />
											<Tooltip title='Login As'>
													<Icon type='eye' />
													<Divider type='vertical' />
											</Tooltip>
											<Tooltip title='Activate/Deactivate User'>
												<Switch defaultChecked={itemStatus(item)}  onChange={(e)=> {onChange(e, item)}} />
											</Tooltip>
									</span>
							))}
						/>
				</Table>
				</div>
			</Card>
		</ErrorBoundary>
		if (this.state.redirect) {
			this.state.redirect = false;
			return <Redirect to='/usermanagement/roles'/>
		}
		return(
			<ErrorBoundary>
				<Tabs selectedDefault={0} fillContainer className= 'tab' style={{fontWeight: 'bold'}}
					active={(current) => {
						if (current !== 0)
							this.setState({redirect: true})
					}}
				>
					<TabPane label='Users' >
						{Users}
					</TabPane>
					<TabPane label='Roles' >
					</TabPane>
				</Tabs>
			</ErrorBoundary>
		)
	}
}

export default UsersDashboard