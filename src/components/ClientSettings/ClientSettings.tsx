import * as React from 'react';
import {TitledCard} from '@simplus/siui'
import ClientSettingsPage from './ClientSettingsForm'
import * as moment from 'moment';
import {connectRobin} from '@simplus/robin-react'
import {robins} from '../../robins'
import {hasPermission, noPermission} from '../../utils'
import {ErrorBoundary} from '../../utils/ErrorBoundary'
import { isArray } from 'lodash';
const {clientSettings, UsersRobin, ProductsRobin, ClientsRobin, PermissionsRobin} = robins;

export interface ClientSettingsOwnProps {

}
interface State {
	form: any;
}
@connectRobin([clientSettings, UsersRobin, ProductsRobin, ClientsRobin, PermissionsRobin])
export class ClientSettings extends React.Component<ClientSettingsOwnProps, State> {
	/**
	 *  ComponentDidMount method to initiate robin get dashboard containers request
	 */

	componentWillMount(): void {
		clientSettings.findOne((this.props as any).match.params.id)
		ClientsRobin.findOne((this.props as any).match.params.id)
		UsersRobin.find({})
		ProductsRobin.find({})
	}

	/**
	 *  Render method
	 */
	render(): JSX.Element {
		const default_settings = {"client":"","img":"","icasCustodian":"","clientCustodian":"","avgWorkingHours":8,"avgEmployeeWage":227.3,"products":[]};
		const settings:any = clientSettings.getModel() || default_settings;
		const users = UsersRobin.getCollection()
		const products = (ProductsRobin.getCollection() || [
			{product: ''},
		]).filter(product => (!!product&& (product.product!=='Absence ')))

		const client = ClientsRobin.getModel() || {};
		
		let valid_settings;
		valid_settings = isArray(settings) ? settings[0] : default_settings
		const startDate = client.FinancialYearEnd ? moment(client.FinancialYearEnd).add(1, 'day') : (settings.reportingCycle ? settings.reportingCycle : moment('Jan-01'))
		const reportingCycle = settings[0] ? moment((valid_settings.reportingCycle||'')) : startDate
		const formProps = {
			client: (this.props as any).match.params.id,
			_key: valid_settings._key,
			img: valid_settings.img,
			avgWorkingHours: valid_settings.avgWorkingHours || 8,
			avgEmployeeWage: valid_settings.avgEmployeeWage || 227.3,
			products: valid_settings.products,
			productsList: products,
			users: users,
			icasCustodian: valid_settings.icasCustodian,
			clientCustodian: valid_settings.clientCustodian,
			reportingCycle:moment(reportingCycle),
			// callbacks: valid_settings.sla.callbacks || '1',
			// callAnswerRate: valid_settings.sla.callAnswerRate || '1',
			// sessions: valid_settings.sla.sessions || '1'
		}

		const settingsPage = <ErrorBoundary>
			<div className='settings-form' style={{display: 'flex', justifyContent: 'center', flexGrow: 1}}>
				<TitledCard
					title={client.Name}
					titleStyle={{ background : 'rgb(43, 75, 126)'}}
					rounded
					loading={clientSettings.isLoading(clientSettings.ACTIONS.FIND_ONE)||ClientsRobin.isLoading(ClientsRobin.ACTIONS.FIND_ONE)}
					style={{width: '70%', margin: '2rem 0'}}
					>
						<ClientSettingsPage {...formProps} />
				</TitledCard>
			</div>
		</ErrorBoundary>
		return(
			hasPermission('/view/settings/edit-settings', PermissionsRobin.getResult('own-permissions')) ? settingsPage : noPermission(PermissionsRobin.isLoading('own-permissions'))
		)
	}
}

export default ClientSettings