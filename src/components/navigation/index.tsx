import * as React from 'react'
import {NavBar} from './NavBar'
import {HashRouter as Router, Route, Switch} from 'react-router-dom';

export const NavBarApp = () => (
	<Router>
		<Switch>
			<Route path='/login' exact component={() => <div></div>} />
			<Route path='/reset-password' exact component={() => <div></div>} />
			<Route path='/set-password/:id' exact component={() => <div></div>} />
			<Route path='/' component={NavBar} />
		</Switch>
	</Router>
)