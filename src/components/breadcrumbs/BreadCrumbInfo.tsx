import * as React from 'react'
import {connectRobin} from '@simplus/robin-react'
import {robins} from '../../robins'
const {UsersRobin, ClientsRobin} = robins;

@connectRobin([UsersRobin, ClientsRobin])
export class BreadCrumbInfo extends React.Component<{id: string}> {
	componentWillMount(): void {
		ClientsRobin.get(`${this.props.id}`, `/${this.props.id}`)
		UsersRobin.get(`list-${this.props.id}`, `/list/["${this.props.id}"]`)
	}
	render(): string {
		const id = this.props.id
		const client = ClientsRobin.getResult(`${id}`)
		const user = UsersRobin.getResult(`list-${id}`)
		let output
		if (client && client.Name) {
			output = client.Name
		} else if (user && user[id] && user[id].name) {
			output = user[id].name
		} else
			output = this.props.id
		return (output)
	}

}
export default BreadCrumbInfo;