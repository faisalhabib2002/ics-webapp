import * as React from 'react'
import {BreadcrumbRouter} from './BreadCrumbs'
import {HashRouter as Router, Route, Switch} from 'react-router-dom';
import {connectRobin} from '@simplus/robin-react'
import {robins} from '../../robins'

@connectRobin([robins.AuthRobin])
export class Breadcrumbs extends React.Component{
		render() {
			return <Router>
				{robins.AuthRobin.getUserInfo()?
					<Switch>
						<Route path='/login' exact component={() => <div></div>} />
						<Route path='/reset-password' exact component={() => <div></div>} />
						<Route path='/set-password/:id' exact component={() => <div></div>} />
						<Route path='/:id' component={BreadcrumbRouter} /> 
					</Switch>:
					<Route path='/:id' component={() => <div></div>} />
				}
			</Router>
		}
}