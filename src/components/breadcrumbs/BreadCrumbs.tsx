import * as React from 'react'
import { HashRouter as Router, Link, withRouter, RouteComponentProps } from 'react-router-dom';
import {TopMenu, TopMenuItem} from '@simplus/siui'
import Breadcrumb from 'antd/lib/breadcrumb'
import {connectRobin} from '@simplus/robin-react'
import {robins} from '../../robins'
import {BreadCrumbInfo} from './BreadCrumbInfo'
import {ErrorBoundary} from '../../utils/ErrorBoundary'
const {ClientsRobin, UsersRobin} = robins;
const breadcrumbNameMap = {
	'analysis': 'Analysis',
	'cross-client-analysis': 'Cross Client Analysis',
	'client-prioritisation': 'Client Prioritisation',
	'client': 'Client',
	'summary': 'Lyra SA Summary',
	'programmeinfo': 'Programme Info',
	'dashboard': 'Dashboards',
	'engagement': 'Engagement',
	'health': 'Health',
	'effectiveness': 'Effectiveness',
	'quality': 'Quality',
	'roi': 'ROI',
	'cost-to-serve': 'Cost to Serve',
	'prioritisation': 'Prioritisation',
	'stories': 'Stories',
	'notes': 'Notes',
	'settings': 'Settings',
	'user': 'User',
	'profile': 'Profile',
	'usermanagement': 'User Management',
	'roles': 'Roles',
	'users': 'Users',
	'workimpact': 'WorkImpact',
	'productivity': 'Productivity',
	'dod': 'Dashboard on Demand'
};

const BreadcrumbsBar = withRouter((props: RouteComponentProps<{}>) => {
	const { location } = props;
	const pathSnippets = location.pathname.split('/').filter(i => i);
	const extraBreadcrumbItems = pathSnippets.map((path, index) => {
		const url = `/${pathSnippets.slice(0, index + 1).join('/')}`;
		// const client = clientsList.find(item => item._key === path)
		// let user = ''
		// if (index > 0 && pathSnippets[index - 1] === 'set-password') {
		// 	user = { name : 'From token'} as any
		// }
		let breadcrumb = path
		breadcrumb = breadcrumbNameMap[path] ? breadcrumbNameMap[path] : <BreadCrumbInfo id={path} />
		return (
			<Breadcrumb.Item key={url}>
				<Link to={url}>
					{breadcrumb}
				</Link>
			</Breadcrumb.Item>
		);
	});
	const breadcrumbItems = [(
	<Breadcrumb.Item key='home'>
		<Link to='/'>Home</Link>
	</Breadcrumb.Item>
	)].concat(extraBreadcrumbItems);
	return (
		<ErrorBoundary>
			<TopMenu style={{
			height: '40px'
			}}>
				<TopMenuItem>
					<Breadcrumb separator='>'>
						{breadcrumbItems}
					</Breadcrumb>
				</TopMenuItem>
			</TopMenu>
		</ErrorBoundary>
	);
});

@connectRobin([ClientsRobin, UsersRobin])
export class BreadcrumbRouter extends React.Component {
	render(): JSX.Element {
		return <Router>
			<BreadcrumbsBar />
	</Router>
	}
};

export default BreadcrumbRouter;