import * as React from 'react'
// import axios from 'axios'
import {Tabs, TabPane, Select, Input, Loader} from '@simplus/siui'
import {DateRangeFilter} from '../ClientInfo/DashboardContent/utils'
import message from 'antd/lib/message'
import {connectRobin} from '@simplus/robin-react'
import {PrioritisationClientDataModel} from '../../../models'
import {robins} from '../../../robins'
import * as queryString from 'query-string'
import {hasPermission, noPermission} from '../../../utils'
import { Link, RouteComponentProps } from 'react-router-dom';
import {ErrorBoundary} from '../../../utils/ErrorBoundary'
import * as moment from 'moment';
import defaults from 'lodash/defaults'
import flatten from 'lodash/flatten'
import { random } from 'lodash'

const Option = Select.Option
const Search = Input.SearchInput

const {clientSettings, IndustryRobin, ClientsRobin, UsersRobin, AnalyticsRobin, TiersRobin, PermissionsRobin} = robins;

@connectRobin([clientSettings, UsersRobin, IndustryRobin, ClientsRobin, AnalyticsRobin, TiersRobin, PermissionsRobin])
export class ClientPrioritisation extends React.Component<RouteComponentProps<{}>> {
	state = {redirect: false, redirectPath: '', filter: '', dateFilter: 'month', KPI: [{name: '', engagement: 0, individualcases: 0, productivity: 0}]}
	location = ''
	unlisten?: () => void = undefined
	componentWillMount(): void {
		this.applyFilters()
		this.unlisten = this.props.history.listen((location, action) => {
			if (action === 'POP')
				this.applyFilters()
		});
		IndustryRobin.find({})
		clientSettings.when(clientSettings.find({})).then((data) => {
			const settings = clientSettings.getResult(data)
			const all_users: string[] = []
			settings.map( setting => {
				if (setting.icasCustodian && (all_users.indexOf(setting.icasCustodian) < 0))
					all_users.push(setting.icasCustodian)
			})
			UsersRobin.get('prioritisation', `/list/${JSON.stringify(all_users)}`)
		})
		TiersRobin.find({})
	}
	applyFilters(): void {
		if ((!this.location) || (this.location !== this.props.location.search)) {
			const startDate = this.urlState().startDate
			const endDate = this.urlState().endDate

			const options = {
				startdate: startDate,
				enddate: endDate,
				split: 'client',
				dailyworkhours: 8,
				hourlywage: 227.3
			}
			AnalyticsRobin.when(AnalyticsRobin.post('prioritisation', '/prioritization', options)).catch(err => {
				message.error('Could not load prioritisation data !')
			})
			this.location = this.props.location.search
		}
	}
	componentWillUnmount(): void {
		if (this.unlisten)
			this.unlisten();
	}

	pageRedirect(update: any, refrsh: boolean = false): void {
		const state = {
			...this.urlState(true),
			...update}
		this.props.history.push(`${this.props.location.pathname}?${queryString.stringify(state)}`)
		if (refrsh)
			setTimeout(() => {this.applyFilters()}, 100)
	}

	urlState(nodefault?: boolean): any {
		if (nodefault)
			return queryString.parse(location.hash.split('?')[1]) || {}
		return defaults(queryString.parse(location.hash.split('?')[1]) || {}, this.defaultQuery)
	}

	defaultQuery = {
		sort: 'engagement',
		order: 'dec',
		industry : '',
		segment: '',
		tier: '',
		dateRange : 'month',
		startDate : moment().startOf('month').subtract(1, 'month').format('YYYY-MM-DD'),
		endDate : moment().subtract(1, 'month').endOf('month').format('YYYY-MM-DD')
	}

	/**
	 *  Render method
	 */
	render(): JSX.Element {
		const clients = ClientsRobin.getCollection() || [];
		const Industries = IndustryRobin.getCollection();
		const TiersList = TiersRobin.getCollection();
		const prioritisation_data = flatten((AnalyticsRobin.getResult('prioritisation') || {data: {}}).data)
		const settings = clientSettings.getCollection();
		const Users = UsersRobin.getResult('prioritisation');
		// const filtered_clients = this.state.filter ? clients.filter((item) => item.Name.toLocaleLowerCase().includes(this.state.filter.toLocaleLowerCase())) : clients
		const filtered_clients = (this.state.filter ? clients.filter((item) => (item.Name || '').toLocaleLowerCase().includes(this.state.filter.toLocaleLowerCase())) : clients).filter( c => {
			return (!this.urlState().industry || this.urlState().industry === c.Industry ) &&
				(!this.urlState().segment || this.urlState().segment === c.SegmentSize ) &&
				(!this.urlState().tier || this.urlState().tier === c.Tier)
		})

		return <ErrorBoundary>
				<div className='client-prioritisation'>
					<h1 className='analysis-tab-title'>Client-Prioritisation</h1>
					<div className='clients-filters'>
						<DateRangeFilter value={this.urlState().dateRange} customOption label='DATE RANGE' style={{minWidth: 120, marginRight: '1rem'}}
							onChange={(selected) => {
								this.pageRedirect({
									dateRange : selected.value,
									startDate: moment(selected.startDate).format('YYYY-MM-DD'),
									endDate: moment(selected.endDate).format('YYYY-MM-DD')
								}, true)
							}
						}/>
						<Select
							value={this.urlState().industry}
							label='INDUSTRY'
							defaultValue=''
							style={{marginRight: '1rem', minWidth: 175}}
							onSelect={(item) => {
								this.pageRedirect({ industry : item})
							}}
							>
							<Option value=''>All Industries</Option>
							{Industries.map(item => <Option key={item.industry} value={item.industry}>{item.industry}</Option>)}
						</Select>
						<Select
							defaultValue=''
							value={this.urlState().segment}
							label='SIZE SEGMENT'
							style={{marginRight: '1rem', minWidth: 150}}
							onSelect={(item) => {
								this.pageRedirect({ segment: item })
							}}
							>
							<Option value=''>All Segments</Option>
							<Option value='Small'>Small</Option>
							<Option value='Small-Medium'>Small - Medium</Option>
							<Option value='Medium'>Medium</Option>
							<Option value='Large Enterprise'>Large Enterprise</Option>
						</Select>
						<Select
							value={this.urlState().tier}
							label='TIER'
							defaultValue=''
							style={{marginRight: '1rem', minWidth: 150}}
							onSelect={(item) => {
								this.pageRedirect({tier : item})
							}}
							>
							<Option value=''>All Tiers</Option>
							{TiersList.map(item => <Option key={item.tier} value={item.tier}>{item.tier}</Option>)}
						</Select>
						<Select
							value={this.urlState().sort}
							label='SORT BY'
							style={{marginRight: '1rem', minWidth: 200}}
							onSelect={(item) => {
								this.pageRedirect({sort : item})
							}}
							>
							<Option value='engagement'>Engagement</Option>
							<Option value='individual'>Individual case utilisation</Option>
							<Option value='productivity'>Productivity</Option>
						</Select>
						<Select
							value={this.urlState().order}
							label='ORDER'
							style={{marginRight: '1rem', minWidth: 150}}
							onSelect={(item) => {
								this.pageRedirect({order : item})
							}}
							>
							<Option value='inc'>Increasing</Option>
							<Option value='dec'>Decreasing</Option>
						</Select>
						<Search label='FILTER' placeholder='Search by name' onChange={value =>  this.setState({filter: value})}/>
					</div>
						<div className='analysis-tab-content' style={{position : 'relative'}}>
							{(ClientsRobin.isLoading(ClientsRobin.ACTIONS.FIND) || ClientsRobin.isError(ClientsRobin.ACTIONS.FIND)) ? <Loader error={ClientsRobin.isError(ClientsRobin.ACTIONS.FIND)}/> : null}
							{filtered_clients.sort( (a, b) => {
								const item_a = (prioritisation_data.find((data: PrioritisationClientDataModel) => data.key === a._key) || {})[this.urlState().sort]
								const item_b = (prioritisation_data.find((data: PrioritisationClientDataModel) => data.key === b._key) || {})[this.urlState().sort]
								return (( item_b || 0) - (item_a || 0)) * (this.urlState().order === 'inc' ? -1 : 1)
							}).map((client, index) => {
								const _clientSettings = settings.find(item => item.client === client._key);
								const custodian = (_clientSettings && _clientSettings.icasCustodian) ? ((Users && Users[_clientSettings.icasCustodian]) ? Users[_clientSettings.icasCustodian] : {name: 'None'}).name : 'None'
								const profileURL = (_clientSettings && _clientSettings.img) ? _clientSettings.img : '/src/assets/img/no_image.png'
								// const KPI = ((prioritisation_data as any) || []).filter(item =>  (item || {key: ''}).key === client.key);
								const prioritisation = prioritisation_data.find((data: PrioritisationClientDataModel) => data.key === client._key) || {}
								const KPI_engagement = `${(((prioritisation as any).engagement || 0) * 100).toFixed(2)}%`
								const KPI_individualcases =  `${(((prioritisation as any).individual || 0) * 100).toFixed(2)}%`
								const KPI_productivity =  `${(((prioritisation as any).productivity || 0) * 100).toFixed(2)}%`
								return <Link key={index} to={`/client/${client._key}/programmeinfo`} style={{display: 'flex'}}>
									<div key={index} className = 'client-card'>
										<h3>
											<div className='client-card-title'>{client.Name}</div>
											{/* <div className='client-card-industry'>{client.Industry}</div> */}
										</h3>
										<div>
											{/* {profileURL ? <img height={100} src={profileURL} style={{margin: '2rem', maxWidth: '300px'}}/>
											: <div style={{height: '100px', margin: '2rem'}}/>} */}
											<img height={100} src={profileURL} style={{margin: '2rem', maxWidth: '300px'}}/>
											<div className='client-card-content'>
												<div className='client-card-item-name'>Industry: <span className='client-card-items'>{client.Industry}</span></div>
												<div className='client-card-item-name'>Size segment: <span className='client-card-items'>{client.SegmentSize}</span></div>
												<div className='client-card-item-name'># employees: <span className='client-card-items'>{client.EmployeeCount}</span></div>
												<div className='client-card-item-name'>Tier: <span className='client-card-items'>{client.Tier}</span> </div>
												<div className='client-card-item-name'>Lyra SA Custodian: <span className='client-card-items'>{custodian}</span> </div>
											</div>
											</div>
											<div className='client-card-kpi-container' style={{position: 'relative'}}>
												{AnalyticsRobin.isLoading('prioritisation') ? <Loader/> : null }
												<div className='client-card-kpi'>
													<div className='client-card-kpi-value'>{KPI_engagement}</div>
													<div className='client-card-kpi-name'>ENGAGEMENT</div>
												</div>
												<div className='client-card-kpi'>
													<div className='client-card-kpi-value'>{KPI_individualcases}</div>
													<div className='client-card-kpi-name'>INDIVIDUAL CASES UTILISATION</div>
												</div>
												<div className='client-card-kpi' style={{borderRight: 0}}>
													<div className='client-card-kpi-value'>{KPI_productivity}</div>
													<div className='client-card-kpi-name'>PRODUCTIVITY</div>
												</div>
											</div>
									</div>
								</Link>
							}
							)}
						</div>
				</div>
			</ErrorBoundary>
	// 	return(
	// 		<ErrorBoundary>
	// 			<Tabs selectedDefault={1} fillContainer className= 'tab' style={{fontWeight: 'bold'}}
	// 			active={(current) => {
	// 				if (current !== 1)
	// 					this.props.history.push('/cross-client-analysis')
	// 			}}
	// 			>
	// 				<TabPane label='Cross-Client Analysis' disabled={!hasPermission('/view/cross-client-analysis', PermissionsRobin.getResult('own-permissions'))} />
	// 				<TabPane label='Client Prioritisation' >
	// 					{hasPermission('/view/clients-prioritization', PermissionsRobin.getResult('own-permissions')) ?  ClientPrioritisationCard : noPermission(PermissionsRobin.isLoading('own-permissions'))}
	// 				</TabPane>
	// 			</Tabs>
	// 		</ErrorBoundary>
	// 	)
	// }
	}
}
export default ClientPrioritisation;