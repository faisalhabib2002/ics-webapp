import * as React from 'react';
import {Route, Switch, Redirect} from 'react-router-dom'
import {CrossClientLayout} from './CrossClientAnalysis/CrossClientLayout'
import {ClientPrioritisation} from './ClientPrioritisation/ClientPrioritisation'
import {ClientInfo} from './ClientInfo'
import { connectRobin } from '@simplus/robin-react';
import {robins} from '../../robins'
import {TopMenuLayout} from './ClientInfo/layout/TopMenuLayout'
import { hasPermission } from '../../utils';
import { Spin } from 'antd';
import ClientSettingsApp from '../ClientSettings';
import ProfileApp from '../usermanagement/pages/Profile';

const {AuthRobin, PermissionsRobin, ClientsRobin} = robins

@connectRobin([AuthRobin, PermissionsRobin, ClientsRobin])
export class AnalysisApp extends React.Component {
	
	/**
	 *  Render method
	 */
	render(): JSX.Element {
		const user = AuthRobin.getUserInfo();
		const clients = Array.isArray(user.client) ? user.client : (user.client || '').split(',')
		const pageLink = (this.props as any).location.pathname.split('/')
		if(PermissionsRobin.isLoading('own-permissions'))
			return <Spin spinning={PermissionsRobin.isLoading('own-permissions')} />
		if(pageLink[1] === 'client')
		return (
			<Switch>
				<Route path='/client/:id' component={ClientInfo} />
				<Redirect from='/client' to='/' />
			</Switch>)
		else
			return(
				<TopMenuLayout>
					<Switch>
						{hasPermission('/view/cross-client-analysis', PermissionsRobin.getResult('own-permissions')) ?
							<Redirect exact from='/' to='/cross-client-analysis'/>
							: (hasPermission('/view/clients-prioritization', PermissionsRobin.getResult('own-permissions')) ?
								<Redirect exact from='/' to='/client-prioritisation'/> :
								<Redirect exact from='/' to={`/client/${clients[0]}`}/>)
						}
						<Route exact path='/cross-client-analysis' component={CrossClientLayout} />
						<Route exact path='/client-prioritisation' component={ClientPrioritisation} />
						<Route path='/client/:id' component={ClientInfo} />
						<Route path='/settings' component={ClientSettingsApp} />,
						<Route path='/user' component={ProfileApp} />,
						<Redirect exact to='/' />
					</Switch>
				</TopMenuLayout>
			)
	}
}

export default AnalysisApp;