import * as React from 'react'
import {TitledCard, Tabs, TabPane, Loader, Select} from '@simplus/siui'
import {PrioritisationIndustryDataModel, IndustryModel} from '../../../models'
import {DateRangeFilter} from '../ClientInfo/DashboardContent/utils'
import * as moment from 'moment'
import * as queryString from 'query-string'
import Divider from 'antd/lib/divider'
import message from 'antd/lib/message'
import {robins} from '../../../robins'
import {connectRobin} from '@simplus/robin-react'
import { Link, RouteComponentProps } from 'react-router-dom';
import defaults from 'lodash/defaults'
import flatten from 'lodash/flatten'
import {hasPermission, noPermission} from '../../../utils'
import {ErrorBoundary} from '../../../utils/ErrorBoundary'
const {ClientsRobin, AnalyticsRobin, IndustryRobin, PermissionsRobin} = robins;
const Option = Select.Option


@connectRobin([ClientsRobin, AnalyticsRobin, IndustryRobin, PermissionsRobin])
export class CrossClientLayout extends React.Component<RouteComponentProps<{}>> {
	state = {redirect: false ,  redirectPath: '', loading: false, KPI: [{name: '', engagement: 0, individualcases: 0, productivity: 0}]}
	location: string
	unlisten?: () => void = undefined
	componentWillMount(): void {
		IndustryRobin.find({})
		this.applyFilters()
		this.unlisten = this.props.history.listen((location, action) => {
			if (action === 'POP')
				this.applyFilters()
			});

	}
	applyFilters(): void {
		if ((!this.location) || (this.location !== this.props.location.search)) {
			const dateRange = queryString.parse(location.hash.split('?')[1]).dateRange || 'month'

			const startDate = queryString.parse(location.hash.split('?')[1]).startDate || moment().startOf('month').subtract(1, dateRange).format('YYYY-MM-DD');
			const endDate = queryString.parse(location.hash.split('?')[1]).endDate || moment().subtract(1, dateRange).endOf(dateRange).format('YYYY-MM-DD');

			const options = {
				startdate: startDate,
				enddate: endDate,
				split: 'industry',
				dailyworkhours: 8,
				hourlywage: 227.3
			}
			AnalyticsRobin.when(AnalyticsRobin.post('cross-client', '/prioritization', options)).catch( err => {
				message.error('Could not fetch prioritization data !')
			})

			this.location = this.props.location.search
		}
	}
	componentWillUnmount(): void {
		if (this.unlisten)
			this.unlisten();
	}

	pageRedirect(update: any, refrsh: boolean = false): void {
		const state = {
			...this.urlState(),
			...update}
		this.props.history.push(`${this.props.location.pathname}?${queryString.stringify(state)}`)
		if (refrsh)
			setTimeout(() => {this.applyFilters()}, 100)
	}

	urlState(): any {
		return defaults(queryString.parse(location.hash.split('?')[1]) || {}, this.defaultQuery)
	}

	defaultQuery = {
		sort: 'engagement',
		order: 'dec'
	}

	/**
	 *  Render method
	 */
	render(): JSX.Element {
		const clients = ClientsRobin.getCollection() || []
		const industries = IndustryRobin.getCollection() || []
		const prioritisation_data = flatten((AnalyticsRobin.getResult('cross-client') || {data: {key: ''}}).data);
		const dateRange = queryString.parse(location.hash.split('?')[1]).dateRange || 'month';

		return <ErrorBoundary>
			<div className='cross-client-layout'>
				<h1 className='analysis-tab-title'>Cross-Client Analysis</h1>
				<div className='filters'>
				<DateRangeFilter value={dateRange} customOption label='DATE RANGE' style={{minWidth: 120, marginRight: '1rem'}}
						onChange={(selected) => {
							this.pageRedirect({
								dateRange : selected.value,
								startDate: moment(selected.startDate).format('YYYY-MM-DD'),
								endDate: moment(selected.endDate).format('YYYY-MM-DD')
							}, true)
						}
						}/>
					<Select
						value={this.urlState().sort}
						label='SORT BY'
						style={{marginRight: '1rem', minWidth: 200}}
						onSelect={(item) => {
							this.pageRedirect({sort : item})
						}}
						>
						<Option value='engagement'>Engagement</Option>
						<Option value='individual'>Individual case utilisation</Option>
						<Option value='productivity'>Productivity</Option>
					</Select>
					<Select
						value={this.urlState().order}
						label='ORDER'
						style={{marginRight: '1rem', minWidth: 150}}
						onSelect={(item) => {
							this.pageRedirect({order : item})
						}}
						>
						<Option value='inc'>Increasing</Option>
						<Option value='dec'>Decreasing</Option>
					</Select>
				</div>
				<div className='analysis-tab-content'  style={{position : 'relative'}}>
					{(IndustryRobin.isLoading(IndustryRobin.ACTIONS.FIND) || IndustryRobin.isError(IndustryRobin.ACTIONS.FIND)) ? <Loader error={IndustryRobin.isError(IndustryRobin.ACTIONS.FIND)}/> : null}
					{industries.sort( (a, b) => {
						const item_a = (prioritisation_data.find((data: PrioritisationIndustryDataModel) => data.key === a.industry) || {})[this.urlState().sort]
						const item_b = (prioritisation_data.find((data: PrioritisationIndustryDataModel) => data.key === b.industry) || {})[this.urlState().sort]
						return (( item_b || 0) - (item_a || 0)) * (this.urlState().order === 'inc' ? -1 : 1)
					}).map((item: IndustryModel, index) => {
						const redirectURI = `/client-prioritisation?industry=${item.industry}`
						const companies = clients.filter(client => client.Industry === item.industry).length;
						const prioritisation = prioritisation_data.find((data: PrioritisationIndustryDataModel) => data.key === item.industry) || {}
						return <Link key={index} to = {redirectURI} style={{display: 'flex', flexBasis: '25%'}}>
								<div key={index} className = 'industry-card'>
										<div>
											<img className='industry-logo' src={`/src/assets/img/sector/${(item.industry || 'null').split(' ').join('')}.jpg`} />
										</div>
										<div>
											<div className='industry-card-body'>
											<h3 className='industry-name'>{item.industry}</h3>
											<div className='companies-count'>{companies} companies</div>
										</div>
									</div>
									<div className='client-card-kpi-container'  style={{position: 'relative'}}>
									{(AnalyticsRobin.isLoading('cross-client') || AnalyticsRobin.isError('cross-client')) ? <Loader error={AnalyticsRobin.isError('cross-client')}/> : null}
										<div className='client-card-kpi'>
											<div className='client-card-kpi-value'>{(((prioritisation as any).engagement || 0) * 100).toFixed(2)}%</div>
											<div className='client-card-kpi-name'>ENGAGEMENT</div>
										</div>
										<div className='client-card-kpi'>
											<div className='client-card-kpi-value'>{(((prioritisation as any).individual || 0) * 100).toFixed(2)}%</div>
											<div className='client-card-kpi-name'>INDIVIDUAL CASE UTILISATION</div>
										</div>
										<div className='client-card-kpi' style={{borderRight: 0}}>
											<div className='client-card-kpi-value'>{(((prioritisation as any).productivity || 0) * 100).toFixed(2)}%</div>
											<div className='client-card-kpi-name'>PRODUCTIVITY</div>
										</div>
									</div>
								</div>
								{/* <TitledCard
								key={index}
								className='industry-card'
								margin
								rounded
								titleStyle = {{backgroundImage: `url(/src/assets/img/sector/${(item.industry || 'null').split(' ').join('')}.jpg)`, backgroundColor: '#0090cd', backgroundSize: 'cover'}}>
									<div className='industry-card-body'>
										<h3 className='industry-name'>{item.industry}</h3>
										<div className='companies-count'>{companies} companies</div>
									</div>
									<div className='client-card-kpi-container'  style={{position: 'relative'}}>
									{(AnalyticsRobin.isLoading('cross-client') || AnalyticsRobin.isError('cross-client')) ? <Loader error={AnalyticsRobin.isError('cross-client')}/> : null}
										<div className='client-card-kpi'>
											<div className='client-card-kpi-value'>{(((prioritisation as any).engagement || 0) * 100).toFixed(2)}%</div>
											<div className='client-card-kpi-name'>ENGAGEMENT</div>
										</div>
										<div className='client-card-kpi'>
											<div className='client-card-kpi-value'>{(((prioritisation as any).individual || 0) * 100).toFixed(2)}%</div>
											<div className='client-card-kpi-name'>INDIVIDUAL CASE UTILISATION</div>
										</div>
										<div className='client-card-kpi' style={{borderRight: 0}}>
											<div className='client-card-kpi-value'>{(((prioritisation as any).productivity || 0) * 100).toFixed(2)}%</div>
											<div className='client-card-kpi-name'>PRODUCTIVITY</div>
										</div>
									</div>
								</TitledCard> */}
							</Link>
						}
					)}
				</div>
			</div>
		</ErrorBoundary>
		// return(
		// 	<ErrorBoundary>
		// 		<Tabs selectedDefault={0} fillContainer className= 'tab' style={{fontWeight: 'bold'}}
		// 		active={(current) => {
		// 			if (current !== 0)
		// 				this.props.history.push('/client-prioritisation/')
		// 		}}
		// 		>
		// 			<TabPane label='Cross-Client Analysis' >
		// 				{hasPermission('/view/cross-client-analysis', PermissionsRobin.getResult('own-permissions')) ? CrossClientLayoutCard :	noPermission(PermissionsRobin.isLoading('own-permissions'))}
		// 			</TabPane>
		// 			<TabPane label='Client Prioritisation' disabled={!hasPermission('/view/clients-prioritization', PermissionsRobin.getResult('own-permissions'))} />
		// 		</Tabs>
		// 	</ErrorBoundary>
		// )
	}

}
export default CrossClientLayout;