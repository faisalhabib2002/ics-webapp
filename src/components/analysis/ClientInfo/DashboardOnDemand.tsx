import * as React from "react";
import { connectRobin } from "@simplus/robin-react";
import * as moment from "moment";
import notification from "antd/lib/notification";
import { RouteComponentProps } from "react-router-dom";
import TreeSelect from "antd/lib/tree-select";
import {
  Button,
  Input,
  Popconfirm,
  Popover,
  Spin,
  Table,
  Tabs,
  Tooltip,
} from "antd";
import Icon from "antd/lib/icon";
import Modal from "antd/lib/modal";
import AntDatePicker from "antd/lib/date-picker";
import { Responsive, WidthProvider } from "react-grid-layout";
import "react-grid-layout/css/styles.css";
import "react-resizable/css/styles.css";
import DOMToImage from "dom-to-image";
import { Document, Page, PDFDownloadLink, Image } from "@react-pdf/renderer";
import Highlighter from "react-highlight-words";
const ResponsiveGridLayout = WidthProvider(Responsive);

import {
  DateRangeFilter,
  ListToTree,
  getTopActiveNodes,
} from "./DashboardContent/utils";
//Engagement Dashboards
import { EngagementAdjustedKPI } from "./DashboardContent/Engagement/EngagementAdjustedKPI";
import { KeyMetricsKPI } from "./DashboardContent/Engagement/KeyMetricsKPI";
import { UtilisationBreakdown } from "./DashboardContent/Engagement/UtilisationBreakdown";
import { IndividualCasesBreakdown } from "./DashboardContent/Engagement/IndividualCasesBreakdown";
import { GroupParticipantsBreakdown } from "./DashboardContent/Engagement/GroupParticipantsBreakdown";
import { OnlineAccessBreakdown } from "./DashboardContent/Engagement/OnlineAccessBreakdown";
import { SessionsBreakdown } from "./DashboardContent/Engagement/SessionsBreakdown";
//Health Dashboards
import { WorkImpactScoreKPI } from "./DashboardContent/Health/WorkImpactScoreKPI";
import { HighRiskFactorKPI } from "./DashboardContent/Health/HighRiskFactorKPI";
import { ProblemBreakdown } from "./DashboardContent/Health/ProblemBreakdown";
import { ProblemAssociations } from "./DashboardContent/Health/ProblemAssociations";
import { ProblemAnalysisBreakdown } from "./DashboardContent/Health/ProblemAnalysisBreakdown";
import { ProblemPrioritisation } from "./DashboardContent/Health/ProblemPrioritisation";
//Effectiveness Dashboards
import { ProductivitySavingsKPI } from "./DashboardContent/Effectiveness/ProductivitySavingsKPI";
import { CaseClosureSplit } from "./DashboardContent/Effectiveness/CaseClosureSplit";
import { ProductivityBreakdown } from "./DashboardContent/Effectiveness/ProductivityBreakdown";
//ROI Dashboards
import { ROIMetricsKPI } from "./DashboardContent/ROI/ROIMetricsKPI";
import { ProductBreakdown } from "./DashboardContent/ROI/ProductBreakdown";
//Quality Dashboards
import { FeedbackQualityMetric } from "./DashboardContent/Quality/FeedbackQualityMetric";
import { ComplaintsWordCloud } from "./DashboardContent/Quality/ComplaintsWordCloud";
import { ComplimentsWordCloud } from "./DashboardContent/Quality/ComplimentsWordCloud";
import pptxgen from "pptxgenjs";

import { robins } from "src/robins";
import { ErrorBoundary } from "src/utils/ErrorBoundary";
const {
  AnalyticsRobin,
  clientSettings,
  PermissionsRobin,
  siteLevelsRobin,
  ClientsRobin,
  DoDRobin,
  AuthRobin,
  UsersRobin,
} = robins;
import { Dashboard, DashboardFilter } from "src/components/dashboard";
import { hasPermission, noPermission } from "src/utils";
import TextArea from "antd/lib/input/TextArea";
const { TabPane } = Tabs;

const AnalyticsRobinOveride = AnalyticsRobin.overwriteResult();

@connectRobin([
  AnalyticsRobin,
  clientSettings,
  PermissionsRobin,
  siteLevelsRobin,
])
export class DashboardOnDemand extends React.Component<
  RouteComponentProps<{ id: string }>
> {
  state = {
    filter: moment("2018-01-01"),
    clientName: "",
    isLoading: false,
    benchmark: "Previous range",
    dateRange: "month" as any,
    compareBenchmark: false,
    includeDependants: true,
    customBenchmark: false,
    sitelevelBenchmark: false,
    startDate: moment()
      .startOf("month")
      .subtract(1, "month")
      .format("YYYY-MM-DD"),
    endDate: moment()
      .subtract(1, "month")
      .endOf("month")
      .format("YYYY-MM-DD"),
    benchmarkStartDate: moment()
      .startOf("month")
      .subtract(1, "month")
      .format("YYYY-MM-DD"),
    benchmarkEndDate: moment()
      .endOf("month")
      .subtract(1, "month")
      .format("YYYY-MM-DD"),
    dashboardHeight: 0,
    downloadPreview: false,
    searchText: "",
    searchedColumn: "",
    site: ["All"] as any,
    benchmarksitelevel: ["All"] as any,
    dashboard: [] as string[],
    dashboardComponents: [] as string[],
    utilizationbreakdown: ["product"],
    keymetrics: ["service"],
    groupParticipants: ["service"],
    groupParticipantsFilterOn: ["all"],
    onlineAccess: ["service"],
    sessionBreakdown: ["product"],
    sessionBreakdownFilterOn: ["all"],
    problemsplit: ["problemcluster"],
    breakdownFilterOn: [""],
    associationsplit: ["problemcluster"],
    associationcases: [""],
    associationbreakdown: [""],
    problems: [],
    problemclusters: [],
    analysisBreakdown: ["problemcluster"],
    problemAnalysisDrilldown: [""],
    analysisBreakdownCases: [""],
    problemAnalysisSplit: ["employementlvl"],
    productivity: ["product"],
    productbreakdown: ["Gains"],
    availableHandles: [],
    pdfImage: [],
    divHeight: 0,
    pdfGenerated: false,
    draggable: false,
    key: undefined,
    dodName: "",
    dodDescription: "",
    dodList: [] as any,
    dodAdminList: [] as any,
    dodNameModal: false,
    dodLoadModal: false,
    deleteLoader: false,
    shareModal: false,
    scheduleModal: false,
    newScheduleModal: false,
    schedules: [] as any,
    scheduleValue: "",
    scheduleStartDate: "",
    scheduleEndDate: "",
    scheduleFilter: "",
    editScheduleModal: false,
    scheduleToEdit: null,
    users: [],
    filteredUsers: [],
    sharedUsers: [],
    sharedLength: 0,
    pptLoader: false,
    dodLoader: false,
    positions: [
      { x: 0, y: 0, w: 4, h: 3.2, maxH: 10, i: "UtilizationKPI" },
      { x: 4, y: 0, w: 8, h: 3.2, maxH: 10, i: "EngagementKPI" },
      { x: 0, y: 4, w: 16, h: 4, maxH: 10, i: "UtilizationBreakdown" },
      { x: 0, y: 8, w: 16, h: 4, maxH: 10, i: "IndividualCasesBreakdown" },
      { x: 0, y: 12, w: 16, h: 4, maxH: 10, i: "GroupParticipantsBreakdown" },
      { x: 0, y: 16, w: 16, h: 4, maxH: 10, i: "OnlineAccessBreakdown" },
      { x: 0, y: 20, w: 16, h: 4, maxH: 10, i: "SessionBreakdown" },
      { x: 0, y: 24, w: 16, h: 3.2, maxH: 10, i: "WorkHealthKPI" },
      { x: 0, y: 28, w: 16, h: 3.2, maxH: 10, i: "RiskHealthKpi" },
      { x: 0, y: 32, w: 16, h: 4, maxH: 10, i: "ProblemBreakdown" },
      { x: 0, y: 36, w: 16, h: 4, maxH: 10, i: "ProblemAssociations" },
      { x: 0, y: 40, w: 16, h: 4, maxH: 10, i: "ProblemAnalysisBreakdown" },
      { x: 0, y: 44, w: 16, h: 4, maxH: 10, i: "ProblemPrioritisation" },
      { x: 0, y: 48, w: 4, h: 4, maxH: 10, i: "ProductivitySavingsKPI" },
      { x: 4, y: 48, w: 8, h: 4, maxH: 10, i: "CaseClosureSplit" },
      { x: 0, y: 52, w: 16, h: 4, maxH: 10, i: "ProductivityBreakdown" },
      { x: 0, y: 56, w: 16, h: 3.2, maxH: 10, i: "ROIKPI" },
      { x: 0, y: 60, w: 16, h: 4, maxH: 10, i: "ProductBreakdown" },
      { x: 0, y: 64, w: 16, h: 3.2, maxH: 10, i: "QualityKPI" },
      { x: 0, y: 68, w: 16, h: 4, maxH: 10, i: "Complaints" },
      { x: 0, y: 72, w: 16, h: 4, maxH: 10, i: "Compliments" },
    ],
  };
  unlisten?: () => void = undefined;
  componentWillMount(): void {
    const clientID = this.props.match.params.id;
    ClientsRobin.when(
      ClientsRobin.get(
        `${this.props.match.params.id}`,
        `/${this.props.match.params.id}`
      )
    )
      .then(() => {
        const clientName = (
          ClientsRobin.getResult(`${clientID}`) || { name: "" }
        ).Name;
        this.setState({ clientName });
      })
      .catch((err) => {
        notification.error({
          type: "error",
          message: "Could not load clients data !",
          description: "There was an error during the clients api execution.",
        });
      });
    siteLevelsRobin
      .when(siteLevelsRobin.findOne(this.props.match.params.id))
      .then(() => {
        clientSettings
          .when(clientSettings.findOne(this.props.match.params.id))
          .then(() => {
            const settings = clientSettings.getModel();
            if (settings[0]) {
              const reportingCycle = moment(settings[0].reportingCycle);
              if (!reportingCycle.isSame(this.state.filter, "d")) {
                this.setState({ filter: reportingCycle });
              } else
                notification.info({
                  type: "info",
                  message: "Reporting Period",
                  description: (
                    <div>
                      <div>
                        Start Date:{" "}
                        {moment(this.state.startDate).format("YYYY-MM-DD")}
                      </div>
                      <div>
                        End Date:{" "}
                        {moment(this.state.endDate).format("YYYY-MM-DD")}
                      </div>
                    </div>
                  ),
                });
            } else
              notification.info({
                type: "info",
                message: "Reporting Period",
                description: (
                  <div>
                    <div>
                      Start Date:{" "}
                      {moment(this.state.startDate).format("YYYY-MM-DD")}
                    </div>
                    <div>
                      End Date:{" "}
                      {moment(this.state.endDate).format("YYYY-MM-DD")}
                    </div>
                  </div>
                ),
              });
          })
          .catch((err) => {
            notification.error({
              type: "error",
              message: "Could not load settings data !",
              description:
                "There was an error during the settings api execution.",
            });
          });
      });
    this.getDashboard();
    this.getAllDashboard();
    this.getUsers();
  }

  getUsers = () => {
    UsersRobin.when(UsersRobin.get("allusers", ``))
      .then(() => {
        const users = UsersRobin.getResult("allusers");
        const activeUsers = users.filter((user) => user.status);
        this.setState({ users: activeUsers });
      })
      .catch((err) => {
        notification.error({
          type: "error",
          message: "Could not load users !",
          description: "There was an error during the users api execution.",
        });
      });
  };

  getFilterValue(): any {
    const benchmark = this.state.benchmark || "Previous range";
    const dateRange = this.state.dateRange || "month";

    const settings = clientSettings.getModel();
    const dailyworkhours = settings[0] ? settings[0].avgWorkingHours : 8;
    const hourlywage = settings[0]
      ? Number(settings[0].avgEmployeeWage)
      : 227.3;

    const startDate = this.state.startDate;
    const endDate = this.state.endDate;
    const benchmarkStartDate = this.state.benchmarkStartDate;
    const benchmarkEndDate = this.state.benchmarkEndDate;

    const clientID = this.props.match.params.id;
    const siteMapping = siteLevelsRobin.isLoading(
      siteLevelsRobin.ACTIONS.FIND_ONE
    )
      ? []
      : (siteLevelsRobin.getModel() || []).map((s) =>
          `clients/${clientID}` === s.branchTo ? { ...s, branchTo: null } : s
        );
    let siteLevelsAssigned = false;
    siteMapping.map((site) => {
      if (site.disabled) siteLevelsAssigned = true;
    });
    const siteTreeData = ListToTree(siteMapping);
    const nodes = [];
    getTopActiveNodes(siteTreeData, nodes);
    const defaultSites = siteLevelsAssigned ? nodes : [""];
    const sitelevels = this.state.site || defaultSites;
    const benchmarkSitelevel = this.state.benchmarksitelevel || defaultSites;
    const includeDependants = this.state.includeDependants;

    const options: any = {
      startdate: startDate,
      sitelevels: Array.isArray(sitelevels)
        ? sitelevels.filter((item) => item !== "All").length === 0
          ? defaultSites
          : sitelevels.filter((item) => item !== "All")
        : sitelevels === "All"
        ? defaultSites
        : sitelevels.split(),
      enddate: endDate,
      client: this.props.match.params.id,
      dailyworkhours: dailyworkhours,
      hourlywage: hourlywage,
      dicarddepenedent: includeDependants == true ? false : true,
      benchmarksitelevels: Array.isArray(benchmarkSitelevel)
        ? benchmarkSitelevel.filter((item) => item !== "All").length === 0
          ? defaultSites
          : benchmarkSitelevel.filter((item) => item !== "All")
        : benchmarkSitelevel === "All"
        ? defaultSites
        : benchmarkSitelevel.split(),
    };
    if (benchmark === "Previous range") {
      if (dateRange === "month") {
        options.benchmarkstartdate = moment(startDate)
          .subtract(1, dateRange)
          .format("YYYY-MM-DD");
        options.benchmarkenddate = moment(endDate)
          .subtract(1, dateRange)
          .endOf("month")
          .format("YYYY-MM-DD");
      } else if (dateRange === "YTD") {
        // YTD option should use same dates of previous year for benchmark
        options.benchmarkstartdate = moment(startDate)
          .subtract(1, "year")
          .format("YYYY-MM-DD");
        options.benchmarkenddate = moment(endDate)
          .subtract(1, "year")
          .format("YYYY-MM-DD");
      } else {
        options.benchmarkstartdate = (moment(startDate) as any)
          .subtract(1, dateRange)
          .format("YYYY-MM-DD");
        options.benchmarkenddate = (moment(endDate) as any)
          .subtract(1, dateRange)
          .format("YYYY-MM-DD");
      }
    } else if (benchmark === "custom") {
      options.benchmarkstartdate = moment(benchmarkStartDate).format(
        "YYYY-MM-DD"
      );
      options.benchmarkenddate = moment(benchmarkEndDate).format("YYYY-MM-DD");
    } else {
      options.benchmark = benchmark.split(",");
    }
    return options;
  }
  componentWillUnmount(): void {
    if (this.unlisten) this.unlisten();
  }
  componentDidUpdate(previousProps, previousState): void {
    const dashboardHeight = (
      document.getElementById("dod") || { clientHeight: 0 }
    ).clientHeight;
    if (previousState.dashboardHeight !== dashboardHeight) {
      this.setState({ dashboardHeight: dashboardHeight });
    }
    if (
      previousState.dodList !== this.state.dodList ||
      previousProps.match.params.key !== this.props.match.params.key
    ) {
      this.handleReset();
      setTimeout(() => this.handleLoad(this.props.match.params.key), 500);
    }
  }
  // Function to toggle loader
  setIsLoading = (loading: boolean) => {
    this.setState({ isLoading: loading });
  };
  customBencharkOk = (e) => {
    this.setState({
      customBenchmark: false,
    });
    this.setState({
      benchmark: "custom",
      benchmarkStartDate: moment(this.state.benchmarkStartDate).format(
        "YYYY-MM-DD"
      ),
      benchmarkEndDate: moment(this.state.benchmarkEndDate).format(
        "YYYY-MM-DD"
      ),
    });
  };

  sitelevelBencharkOk = (e) => {
    this.setState({
      sitelevelBenchmark: false,
      benchmark: "clients",
    });
  };

  handleGenerate = async () => {
    this.setState({ availableHandles: [], pptLoader: true });
    this.removeHr();
    let img: any = [];
    const parentDiv: any = document.getElementById("dod-modal");
    const footer: any = document.getElementById("footer");
    const fClone = footer.cloneNode(true);
    footer.innerHTML = "";
    const dod: any = parentDiv.childNodes[2];
    const gridLayout = dod.firstChild;
    const orgArray = Array.from(gridLayout.children);
    // hide scrollbars
    const dodDashboard = [
      ...Array.from(document.getElementsByClassName("dod-scrollbar")),
    ];
    dodDashboard.forEach((dod: any) => {
      dod.style.overflow = "hidden";
    });
    // hide scrollbars for kpis
    const kpiCharts = [
      ...Array.from(document.getElementsByClassName("si-with-background")),
    ];
    kpiCharts.forEach((kpi: any) => {
      kpi.style.overflow = "hidden";
    });

    // to calculate no. of pages
    const steps = Math.ceil(this.state.divHeight / 1200);
    let firstIndex = 0;
    let lastIndex = 0;
    for (let i = 0; i < steps; i++) {
      // set pdf page height
      gridLayout.style.height = "1700px";
      gridLayout.innerHTML = "";
      const children: any = [...orgArray];

      // to set order of charts
      children.sort(
        (a, b) =>
          a.style.transform.split(", ")[1].split("px)")[0] -
          b.style.transform.split(", ")[1].split("px)")[0]
      );

      let height = 0;
      // find first and last index for a single page
      for (let index = firstIndex; index <= children.length; index++) {
        if (children[index] && index > 1) {
          const heightTransform = Number(
            children[index].style.transform.split(",")[1].split("px")[0]
          );
          const lastHeightTransform = Number(
            children[index - 1].style.transform.split(",")[1].split("px")[0]
          );
          if (heightTransform != lastHeightTransform)
            height += Number(children[index].style.height.split("px")[0]);
        }
        lastIndex = index;
        if (height > 1550) break;
      }
      if (i === steps - 1) lastIndex = children.length;
      else if (firstIndex == lastIndex) lastIndex += 1;

      // divide components into pages
      const splice = children.slice(firstIndex, lastIndex);

      firstIndex = lastIndex;
      let widthTransform = 10;
      let heightTransform = 10;
      let widthIndex = 0;
      splice.forEach((div: any, index) => {
        const width = Number(div.style.width.split("px")[0]);
        widthTransform = div.style.transform
          .split(",")[0]
          .split("translate(")[1];
        // to check if there are multiple components in a single row
        if (width < 1000) {
          if (
            index === 0 ||
            (index === 1 &&
              Number(splice[index - 1].style.width.split("px")[0]) < 1000)
          )
            heightTransform = 10;
          else {
            heightTransform =
              heightTransform +
              (widthIndex == 0
                ? Number(splice[index - 1].style.height.split("px")[0]) + 20
                : 0);
            const lastHeightTransform = Number(
              splice[index - 1].style.transform.split(",")[1].split("px")[0]
            );
            widthIndex =
              heightTransform !== lastHeightTransform ? widthIndex + 1 : 0;
          }
        } else {
          heightTransform =
            heightTransform +
            (index === 0
              ? 0
              : Number(splice[index - 1].style.height.split("px")[0])) +
            10;
          widthIndex = 0;
        }
        div.style.transform = `translate(${widthTransform}, ${heightTransform}px)`;
        gridLayout.appendChild(div);
      });
      // set footer at last page
      if (i == steps - 1 || lastIndex === children.length) {
        // fClone.style['margin-top'] = '150px'
        parentDiv.appendChild(fClone);
      }
      if (splice.length) {
        // convert div to pdfImage
        await DOMToImage.toPng(parentDiv)
          .then((url: any) => {
            img = [...this.state.pdfImage];
            img.push(url);
            this.setState({ pdfImage: img });
          })
          .catch(function(error: any) {
            console.error(error);
          });
      }
    }
    fClone.style["margin-top"] = "0px";
    fClone.innerHTML = "";
    this.setState({ pdfGenerated: true, pptLoader: false });
  };

  convertDivToImage = async (div: HTMLElement) => {
    return await DOMToImage.toPng(div)
      .then((url: any) => {
        return url;
      })
      .catch(function(error: any) {
        console.error(error);
      });
  };

  handlePPTGenerate = async () => {
    let pptx = new pptxgen();
    this.setState({ availableHandles: [], pptLoader: true });
    this.removeHr();
    let img: any = [];
    const parentDiv: any = document.getElementById("dod-modal");
    const header: any = document.getElementById("pdf-header");
    const footer: any = document.getElementById("footer");
    const headerImg = await this.convertDivToImage(header);
    let slide = pptx.addSlide();
    console.log(headerImg);
    slide.addImage({ data: `${headerImg}`, x: 0.2, y: 0, w: 9.5, h: 0.8 });
    slide.addText(this.state.clientName, {
      x: 1.2,
      y: 2,
      color: "363636",
      fill: { color: "F1F1F1" },
      align: pptx.AlignH.center,
    });
    slide.addText(
      `Reporting Cycle: Start Date: ${this.state.startDate}, End Date: ${this.state.endDate}`,
      {
        x: 0.8,
        y: 3,
        color: "363636",
        fill: { color: "F1F1F1" },
        align: pptx.AlignH.center,
      }
    );
    footer.style.margin = "0px";
    const footerImg = await this.convertDivToImage(footer);
    slide.addImage({ data: `${footerImg}`, x: 0.2, y: 5, w: 9.5, h: 0.5 });
    header.innerHTML = "";
    footer.innerHTML = "";
    parentDiv.removeChild(parentDiv.firstChild);
    parentDiv.removeChild(parentDiv.firstChild);
    const dod: any = parentDiv.childNodes[0];
    const gridLayout = dod.firstChild;
    const orgArray = Array.from(gridLayout.children);
    // hide scrollbars
    const dodDashboard = [
      ...Array.from(document.getElementsByClassName("dod-scrollbar")),
    ];
    dodDashboard.forEach((dod: any) => {
      dod.style.overflow = "hidden";
    });
    // hide scrollbars for kpis
    const kpiCharts = [
      ...Array.from(document.getElementsByClassName("si-with-background")),
    ];
    kpiCharts.forEach((kpi: any) => {
      kpi.style.overflow = "hidden";
    });

    // to calculate no. of pages
    const steps = Math.ceil(this.state.divHeight / 400);
    orgArray.sort(
      (a: any, b: any) =>
        a.style.transform.split(", ")[1].split("px)")[0] -
        b.style.transform.split(", ")[1].split("px)")[0]
    );
    const children: any = [...orgArray];
    const slideWidth = 1920;
    const slideHeight = 1080;
    let skip = false;
    for (let i = 0; i <= steps; i++) {
      gridLayout.innerHTML = "";
      if (skip) {
        skip = false;
        continue;
      }
      if (children[i] && children[i + 1]) {
        const combinedWidth =
          Number(children[i].style.width.split("px")[0]) +
          Number(children[i + 1].style.width.split("px")[0]);
        if (combinedWidth <= 1800) {
          // If combined width fits in a slide
          // Set up the layout
          children[i].style.transform = `translate(0px, 0px)`;
          children[i + 1].style.transform = `translate(0px, 0px)`;
          gridLayout.style.maxHeight =
            Math.max(
              children[i].style.height.split("px")[0],
              children[i + 1].style.height.split("px")[0]
            ) + "px";
          gridLayout.style.maxWidth = slideWidth + "px";
          gridLayout.appendChild(children[i]);
          gridLayout.appendChild(children[i + 1]);
          const heightInPercfirst =
            (Math.min(780, children[i].style.height.split("px")[0]) /
              slideHeight) *
            100; // Height in Perncetage
          const widthInPercfirst =
            (children[i].style.width.split("px")[0] / slideWidth) * 100; // Width in Perncetage

          const heightInPercSecond =
            (Math.min(780, children[i + 1].style.height.split("px")[0]) /
              slideHeight) *
            100; // Height in Perncetage
          const widthInPercSecond =
            (children[i + 1].style.width.split("px")[0] / slideWidth) * 100; // Width in Perncetage

          // Add images to the slide
          const url1 = await this.convertDivToImage(children[i]);
          const url2 = await this.convertDivToImage(children[i + 1]);
          let slide = pptx.addSlide();
          slide.addImage({
            data: `${headerImg}`,
            x: 0.2,
            y: 0,
            w: 9.5,
            h: 0.8,
          });
          slide.addImage({
            data: `${url1}`,
            x: "10%",
            y: 1,
            w: `${widthInPercfirst}%`,
            h: `${heightInPercfirst}%`,
          }); // Adjust positioning and size
          slide.addImage({
            data: `${url2}`,
            x: `${widthInPercfirst + 10}%`,
            y: 1,
            w: `${widthInPercSecond}%`,
            h: `${heightInPercSecond}%`,
          }); // Adjust positioning and size
          slide.addImage({
            data: `${footerImg}`,
            x: 0.2,
            y: 5,
            w: 9.5,
            h: 0.5,
          });

          // Update state or do any necessary operations
          const img: any = [...this.state.pdfImage];
          img.push(url1, url2);
          this.setState({ pdfImage: img });
          skip = true;
          // Move to the next iteration
          continue;
        }
      }
      if (children[i]) {
        children[i].style.transform = `translate(0px, 0px)`;
        gridLayout.style.maxHeight = children[i].style.height;
        gridLayout.style.maxWidth = children[i].style.width;
        gridLayout.appendChild(children[i]);
        const heightInPerc =
          (Math.min(780, children[i].style.height.split("px")[0]) /
            slideHeight) *
          100; // Height in Perncetage
        const widthInPerc =
          (children[i].style.width.split("px")[0] / slideWidth) * 100; // Width in Perncetage
        const url = await this.convertDivToImage(children[i]);
        let slide = pptx.addSlide();
        slide.addImage({ data: `${headerImg}`, x: 0.2, y: 0, w: 9.5, h: 0.8 });
        const x = (100 - widthInPerc) / 2; // Center horizontally
        const y = (100 - heightInPerc) / 2; // Center vertically

        img = [...this.state.pdfImage];
        img.push(url);
        this.setState({ pdfImage: img });
        slide.addImage({
          data: `${url}`,
          x: `${x}%`,
          y: `${y}%`,
          w: `${widthInPerc}%`,
          h: `${heightInPerc}%`,
        });
        slide.addImage({ data: `${footerImg}`, x: 0.2, y: 5, w: 9.5, h: 0.5 });
      }
    }
    pptx.writeFile({
      fileName: `${this.state.clientName} Dashboard.pptx`,
      compression: true,
    });
    this.setState({
      pdfImage: [],
      pdfGenerated: false,
      downloadPreview: false,
      draggable: false,
      pptLoader: false,
    });
  };

  calculateHeight = () => {
    const div: any = document.getElementById("dod-modal");
    const divHeight = document.getElementById("dod-modal")
      ? (document.getElementById("dod-modal") as any).clientHeight
      : 0;
    for (let i = 0; i < divHeight / 1600; i++) {
      const hr = document.createElement("hr");
      hr.style.position = "absolute";
      hr.style.top = `${(i === 0 ? 2100 : 1800) * (i + 1)}px`;
      hr.style.width = "100%";
      hr.style.left = "0";
      div.appendChild(hr);
    }
    this.setState({ divHeight: divHeight });
  };

  removeHr = () => {
    const hr = Array.from(document.getElementsByTagName("hr"));
    hr.map((h) => {
      h.remove();
    });
  };

  handleDownload = () => {
    setTimeout(() => {
      this.setState({
        pdfImage: [],
        pdfGenerated: false,
        downloadPreview: false,
        draggable: false,
      });
    }, 50);
  };

  PDFDocument = () => (
    <Document>
      {this.state.pdfImage.map((img) => {
        return (
          <Page>
            <Image src={img} />
          </Page>
        );
      })}
    </Document>
  );

  generateReport = async (dashboard: string[]) => {
    // to reset positions and dashboards components when you select dashboards out of order
    const options = this.getFilterValue();
    const clientID = this.props.match.params.id;
    this.setState({ dashboardComponents: dashboard });
    const tasks: (() => Promise<any>)[] = []; // Array of API call tasks
    //Engagement Dashboard APIs
    if (dashboard.indexOf("engagement_kpi") >= 0) {
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(
            `${clientID}-EngagementKPI`,
            "/engagement/keymetrics",
            options
          )
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load engagement keymetrics data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(
            `${clientID}-UtilizationKPI`,
            "/engagement/utilization",
            options
          )
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load engagement utilisation data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    }
    if (dashboard.indexOf("utilization_breakdown") >= 0)
      this.state.utilizationbreakdown.map((_breakdown, i) => {
        tasks.push(() =>
          AnalyticsRobin.when(
            AnalyticsRobin.post(
              `${clientID}-UtilizationBreakdown-${i}`,
              "/engagement/utilization",
              {
                ...options,
                split: this.state.utilizationbreakdown[i] || "product",
              }
            )
          ).catch((err) => {
            notification.error({
              type: "error",
              message: "Could not load utilisation breakdown data !",
              description:
                "There was an error during the analytics api execution.",
            });
          })
        );
      });
    if (dashboard.indexOf("individual_cases_breakdown") >= 0)
      this.state.keymetrics.map((_metric, i) => {
        tasks.push(() =>
          AnalyticsRobin.when(
            AnalyticsRobin.post(
              `${clientID}-IndividualCasesBreakdown-${i}`,
              "/engagement/keymetrics",
              { ...options, split: this.state.keymetrics[i] || "service" }
            )
          ).catch((err) => {
            notification.error({
              type: "error",
              message: "Could not load individual cases breakdown data !",
              description:
                "There was an error during the analytics api execution.",
            });
          })
        );
      });

    if (dashboard.indexOf("group_participants_breakdown") >= 0)
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post("products_breakdown", "/engagement/keymetrics", {
            ...options,
            split: "product",
          })
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load products breakdown data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    tasks.push(() =>
      AnalyticsRobin.when(
        AnalyticsRobin.post(
          "problem_cluster_breakdown",
          "/engagement/keymetrics",
          { ...options, split: "problemcluster" }
        )
      ).catch((err) => {
        notification.error({
          type: "error",
          message: "Could not load problem cluster breakdown data !",
          description: "There was an error during the analytics api execution.",
        });
      })
    );
    this.state.groupParticipants.map((_participants, i) => {
      const additionalFilters: any = {};
      if (this.state.groupParticipantsFilterOn[i] !== "all") {
        if (_participants === "problem")
          additionalFilters.problemcluster = this.state.groupParticipantsFilterOn[
            i
          ];
        else
          additionalFilters.product = this.state.groupParticipantsFilterOn[i];
      }
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(
            `${clientID}-GroupParticipantsBreakdown-${i}`,
            "/engagement/keymetrics",
            {
              ...options,
              ...additionalFilters,
              split: this.state.groupParticipants[i] || "service",
            }
          )
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load group participants breakdown data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    });
    if (dashboard.indexOf("online_access_breakdown") >= 0)
      this.state.onlineAccess.map((_access, i) => {
        tasks.push(() =>
          AnalyticsRobin.when(
            AnalyticsRobin.post(
              `${clientID}-OnlineAccessBreakdown-${i}`,
              "/engagement/keymetrics",
              { ...options, split: this.state.onlineAccess[i] || "service" }
            )
          ).catch((err) => {
            notification.error({
              type: "error",
              message: "Could not load group participants breakdown data !",
              description:
                "There was an error during the analytics api execution.",
            });
          })
        );
      });
    if (dashboard.indexOf("sessions_breakdown") >= 0)
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(
            "sessions_breakdown_products",
            "/engagement/sessions",
            { ...options, split: "product" }
          )
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load sessions breakdown data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    this.state.sessionBreakdown.map((_session, i) => {
      const additionalFilters: any = {};
      if (this.state.sessionBreakdownFilterOn[i] !== "all") {
        additionalFilters.product = this.state.sessionBreakdownFilterOn[i];
      }
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(
            `${clientID}-SessionBreakdown-${i}`,
            "/engagement/sessions",
            {
              ...options,
              ...additionalFilters,
              split: this.state.sessionBreakdown[i] || "product",
            }
          )
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load sessions breakdown data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    });
    //Health Dashboard APIs
    if (
      dashboard.indexOf("work_health_kpi") >= 0 ||
      dashboard.indexOf("risk_health_kpi") >= 0
    )
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(
            `${clientID}-HealthKPI`,
            "/health/keymetrics",
            options
          )
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load health keymetrics data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    if (dashboard.indexOf("problem_breakdown") >= 0)
      this.state.problemsplit.map((_breakdown, i) => {
        tasks.push(() =>
          AnalyticsRobin.when(
            AnalyticsRobin.post(
              `${clientID}-ProblemBreakdown-${i}`,
              "/health/breakdown",
              {
                ...options,
                casetype: this.state.breakdownFilterOn[i] || "",
                split: this.state.problemsplit[i],
              }
            )
          ).catch((err) => {
            notification.error({
              type: "error",
              message: "Could not load problem breakdown data !",
              description:
                "There was an error during the analytics api execution.",
            });
          })
        );
      });
    if (dashboard.indexOf("problems_association_breakdown") >= 0)
      this.fetchProblemList();
    this.fetchProblemClusters();
    this.state.associationsplit.map((_breakdown, i) => {
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(
            `${clientID}-ProblemAssociations-${i}`,
            "/health/associations",
            {
              ...options,
              association: this.state.associationbreakdown[i],
              casetype: this.state.associationcases[i],
              split: this.state.associationsplit[i],
            }
          )
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load health associations data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    });
    if (dashboard.indexOf("problem_analysis_breakdown") >= 0)
      this.fetchProblemList();
    this.fetchProblemClusters();
    this.state.problemAnalysisSplit.map((_breakdown, i) => {
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(
            `${clientID}-ProblemAnalysisBreakdown-${i}`,
            "/health/breakdown",
            {
              ...options,
              problemlevel: this.state.analysisBreakdown[i],
              drilldown: this.state.problemAnalysisDrilldown[i],
              casetype: this.state.analysisBreakdownCases[i],
              split: this.state.problemAnalysisSplit[i],
            }
          )
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load health breakdown data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    });
    if (dashboard.indexOf("problem_prioritisation") >= 0)
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(
            `${clientID}-ProblemPrioritisation`,
            "/health/prioritization",
            { ...options, split: "problem" }
          )
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load health prioritization drilldown data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    //Effectiveness Dashboard APIs
    if (dashboard.indexOf("productivity_savings") >= 0) {
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(
            `${clientID}-ProductivitySavingsKPI`,
            "/effectiveness/productivity",
            options
          )
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load effectiveness keymetrics data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(
            `${clientID}-CaseClosureSplit`,
            "/effectiveness/closure",
            { ...options, split: "status" }
          )
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load effectiveness closure data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    }
    if (dashboard.indexOf("productivity_breakdown") >= 0)
      this.state.productivity.map((_breakdown, i) => {
        tasks.push(() =>
          AnalyticsRobin.when(
            AnalyticsRobin.post(
              `${clientID}-ProductivityBreakdown-${i}`,
              "/effectiveness/productivity",
              { ...options, split: this.state.productivity[i] }
            )
          ).catch((err) => {
            notification.error({
              type: "error",
              message: "Could not load effectiveness productivity data !",
              description:
                "There was an error during the analytics api execution.",
            });
          })
        );
      });
    //ROI Dashboard APIs
    if (dashboard.indexOf("roi_kpi") >= 0)
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(`${clientID}-ROIKPI`, "/roi/keymetrics", {
            ...options,
          })
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load roi keymetrics data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    if (dashboard.indexOf("product_breakdown") >= 0)
      this.state.productbreakdown.map((_breakdown, i) => {
        tasks.push(() =>
          AnalyticsRobin.when(
            AnalyticsRobin.post(
              `${clientID}-ProductBreakdown-${i}`,
              "/roi/keymetrics",
              { ...options, split: "product" }
            )
          ).catch((err) => {
            notification.error({
              type: "error",
              message: "Could not load roi products data !",
              description:
                "There was an error during the analytics api execution.",
            });
          })
        );
      });
    //Quality Dashboard APIs
    if (dashboard.indexOf("quality_kpi") >= 0)
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(
            `${clientID}-QualityKPI`,
            "/quality/sentiments",
            options
          )
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load quality keymetrics data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    if (dashboard.indexOf("compliments") >= 0)
      AnalyticsRobin.when(
        AnalyticsRobin.post(`${clientID}-Compliments`, "/quality/wordcloud", {
          wordcategory: "compliments",
          ...options,
        })
      ).catch((err) => {
        notification.error({
          type: "error",
          message: "Could not load quality wordcloud data !",
          description: "There was an error during the analytics api execution.",
        });
      });
    if (dashboard.indexOf("complaints") >= 0)
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(`${clientID}-Complaints`, "/quality/wordcloud", {
            wordcategory: "complaints",
            ...options,
          })
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load quality wordcloud data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );

    // Process tasks with concurrency control
    const CONCURRENCY_LIMIT = 3; // Adjust based on server capacity
    await this.runWithConcurrency(tasks, CONCURRENCY_LIMIT);
  };
  runWithConcurrency = async (
    tasks: (() => Promise<any>)[],
    concurrencyLimit: number
  ) => {
    this.setIsLoading(true);
    const executing: Promise<void>[] = [];

    for (const task of tasks) {
      const promise = task().finally(() => {
        executing.splice(executing.indexOf(promise), 1);
      });
      executing.push(promise);

      if (executing.length >= concurrencyLimit) {
        await Promise.race(executing);
      }
    }

    await Promise.all(executing);
    this.setIsLoading(false);
  };
  fetchProblemList = () => {
    const options = this.getFilterValue();
    AnalyticsRobin.when(
      AnalyticsRobin.post("problem_list", "/health/breakdown", {
        ...options,
        split: "problem",
      })
    )
      .then((key) => {
        const problemList = AnalyticsRobin.getResult(key).data || [];
        this.setState({
          problems: problemList.map((d) => {
            return { value: d.key, text: [d.key] };
          }),
        });
      })
      .catch((err) => {
        notification.error({
          type: "error",
          message: "Could not load problem list data !",
          description: "There was an error during the analytics api execution.",
        });
      });
  };
  fetchProblemClusters = () => {
    const options = this.getFilterValue();
    AnalyticsRobin.when(
      AnalyticsRobin.post("problem_cluster_list", "/health/breakdown", {
        ...options,
        split: "problemcluster",
      })
    )
      .then((key) => {
        const problemList = AnalyticsRobin.getResult(key).data || [];
        let associationsBreakdown = [];
        let problemAnalysisDrilldown = [];
        if (problemList.length > 0) {
          associationsBreakdown = [problemList[0].key];
          problemAnalysisDrilldown = [problemList[0].key];
        }
        this.setState({
          problemclusters: problemList.map((d) => {
            return { value: d.key, text: [d.key] };
          }),
          associationbreakdown: associationsBreakdown,
          problemAnalysisDrilldown: problemAnalysisDrilldown,
        });
      })
      .catch((err) => {
        notification.error({
          type: "error",
          message: "Could not load problem cluster list data !",
          description: "There was an error during the analytics api execution.",
        });
      });
  };
  customBencharkCancel = (e) => {
    this.setState({ customBenchmark: false });
  };
  sitelevelBencharkCancel = (e) => {
    this.setState({ sitelevelBenchmark: false, benchmarksitelevel: ["All"] });
  };

  addDuplicateGraphs = (filter) => {
    const duplicates = [...this.state[filter]];
    let index = 0;
    switch (filter) {
      case "utilizationbreakdown":
        duplicates.push("product");
        index = this.state.positions.findIndex(
          (p) => p.i === "UtilizationBreakdown"
        );
        break;
      case "keymetrics":
        duplicates.push("service");
        index = this.state.positions.findIndex(
          (p) => p.i === "IndividualCasesBreakdown"
        );
        break;
      case "groupParticipants":
        duplicates.push("service");
        const groupParticipantsFilterOn = [
          ...this.state.groupParticipantsFilterOn,
        ];
        groupParticipantsFilterOn.push("");
        index = this.state.positions.findIndex(
          (p) => p.i === "GroupParticipantsBreakdown"
        );
        this.setState({ groupParticipantsFilterOn });
        break;
      case "onlineAccess":
        duplicates.push("service");
        index = this.state.positions.findIndex(
          (p) => p.i === "OnlineAccessBreakdown"
        );
        break;
      case "sessionBreakdown":
        duplicates.push("product");
        const sessionBreakdownFilterOn = [
          ...this.state.sessionBreakdownFilterOn,
        ];
        sessionBreakdownFilterOn.push("");
        index = this.state.positions.findIndex(
          (p) => p.i === "SessionBreakdown"
        );
        this.setState({ sessionBreakdownFilterOn });
        break;
      case "problemsplit":
        duplicates.push("problemcluster");
        const breakdownFilterOn = [...this.state.breakdownFilterOn];
        breakdownFilterOn.push("");
        index = this.state.positions.findIndex(
          (p) => p.i === "ProblemBreakdown"
        );
        this.setState({ breakdownFilterOn });
        break;
      case "associationsplit":
        duplicates.push("problemcluster");
        const associationcases = [...this.state.associationcases];
        associationcases.push("");
        const associationbreakdown = [...this.state.associationbreakdown];
        associationbreakdown.push((this.state.problemclusters[0] as any).value);
        index = this.state.positions.findIndex(
          (p) => p.i === "ProblemAssociations"
        );
        this.setState({ associationcases, associationbreakdown });
        break;
      case "problemAnalysisSplit":
        duplicates.push("employementlvl");
        const analysisBreakdown = [...this.state.analysisBreakdown];
        analysisBreakdown.push("problemcluster");
        const problemAnalysisDrilldown = [
          ...this.state.problemAnalysisDrilldown,
        ];
        problemAnalysisDrilldown.push(
          (this.state.problemclusters[0] as any).value
        );
        const analysisBreakdownCases = [...this.state.analysisBreakdownCases];
        analysisBreakdownCases.push("");
        index = this.state.positions.findIndex(
          (p) => p.i === "ProblemAnalysisBreakdown"
        );
        this.setState({
          analysisBreakdown,
          problemAnalysisDrilldown,
          analysisBreakdownCases,
        });
        break;
      case "productivity":
        duplicates.push("product");
        index = this.state.positions.findIndex(
          (p) => p.i === "ProductivityBreakdown"
        );
        break;
      case "productbreakdown":
        duplicates.push("Gains");
        index = this.state.positions.findIndex(
          (p) => p.i === "ProductBreakdown"
        );
        break;
    }
    const positions = [...this.state.positions];
    for (let i = index + 1; i < positions.length; i++) {
      positions[i].y = positions[i].y + 4;
    }
    this.setState({ [filter]: duplicates, positions });
  };

  removeDuplicateGraphs = (index, filter) => {
    switch (filter) {
      case "keymetrics":
        AnalyticsRobinOveride.post(
          `${this.props.match.params.id}-IndividualCasesBreakdown-${index}`,
          "/engagement/keymetrics",
          []
        );
        break;
      case "utilizationbreakdown":
        AnalyticsRobinOveride.post(
          `${this.props.match.params.id}-UtilizationBreakdown-${index}`,
          "/engagement/utilization",
          []
        );
        break;
      case "groupParticipants":
        AnalyticsRobinOveride.post(
          `${this.props.match.params.id}-GroupParticipantsBreakdown-${index}`,
          "/engagement/keymetrics",
          []
        );
        const groupParticipantsFilterOn = [
          ...this.state.groupParticipantsFilterOn,
        ];
        groupParticipantsFilterOn.splice(index, 1);
        this.setState({ groupParticipantsFilterOn });
        break;
      case "onlineAccess":
        AnalyticsRobinOveride.post(
          `${this.props.match.params.id}-OnlineAccessBreakdown-${index}`,
          "/engagement/keymetrics",
          []
        );
        break;
      case "sessionBreakdown":
        AnalyticsRobinOveride.post(
          `${this.props.match.params.id}-SessionBreakdown-${index}`,
          "/engagement/sessions",
          []
        );
        const sessionBreakdownFilterOn = [
          ...this.state.sessionBreakdownFilterOn,
        ];
        sessionBreakdownFilterOn.splice(index, 1);
        this.setState({ sessionBreakdownFilterOn });
        break;
      case "problemsplit":
        AnalyticsRobinOveride.post(
          `${this.props.match.params.id}-ProblemBreakdown-${index}`,
          "/health/breakdown",
          []
        );
        const breakdownFilterOn = [...this.state.breakdownFilterOn];
        breakdownFilterOn.splice(index, 1);
        this.setState({ breakdownFilterOn });
        break;
      case "associationsplit":
        AnalyticsRobinOveride.post(
          `${this.props.match.params.id}-ProblemAssociations-${index}`,
          "/health/associations",
          []
        );
        const associationcases = [...this.state.associationcases];
        associationcases.splice(index, 1);
        const associationbreakdown = [...this.state.associationbreakdown];
        associationbreakdown.splice(index, 1);
        this.setState({ associationcases, associationbreakdown });
        break;
      case "problemAnalysisSplit":
        AnalyticsRobinOveride.post(
          `${this.props.match.params.id}-ProblemAnalysisBreakdown-${index}`,
          "/health/breakdown",
          []
        );
        const analysisBreakdown = [...this.state.analysisBreakdown];
        analysisBreakdown.splice(index, 1);
        const problemAnalysisDrilldown = [
          ...this.state.problemAnalysisDrilldown,
        ];
        problemAnalysisDrilldown.splice(index, 1);
        const analysisBreakdownCases = [...this.state.analysisBreakdownCases];
        analysisBreakdownCases.splice(index, 1);
        this.setState({
          analysisBreakdown,
          problemAnalysisDrilldown,
          analysisBreakdownCases,
        });
        break;
      case "productivity":
        AnalyticsRobinOveride.post(
          `${this.props.match.params.id}-ProductivityBreakdown-${index}`,
          "/effectiveness/productivity",
          []
        );
        break;
      case "productbreakdown":
        AnalyticsRobinOveride.post(
          `${this.props.match.params.id}-ProductBreakdown-${index}`,
          "/roi/keymetrics",
          []
        );
        break;
    }
    const duplicates = [...this.state[filter]];
    duplicates.splice(index, 1);
    this.setState({ [filter]: duplicates });
  };

  resetPositions = () => {
    this.setState({
      dashboardComponents: [],
      positions: [
        { x: 0, y: 0, w: 4, h: 3.2, maxH: 10, i: "UtilizationKPI" },
        { x: 4, y: 0, w: 8, h: 3.2, maxH: 10, i: "EngagementKPI" },
        { x: 0, y: 4, w: 16, h: 4, maxH: 10, i: "UtilizationBreakdown" },
        { x: 0, y: 8, w: 16, h: 4, maxH: 10, i: "IndividualCasesBreakdown" },
        { x: 0, y: 12, w: 16, h: 4, maxH: 10, i: "GroupParticipantsBreakdown" },
        { x: 0, y: 16, w: 16, h: 4, maxH: 10, i: "OnlineAccessBreakdown" },
        { x: 0, y: 20, w: 16, h: 4, maxH: 10, i: "SessionBreakdown" },
        { x: 0, y: 24, w: 16, h: 3.2, maxH: 10, i: "WorkHealthKPI" },
        { x: 0, y: 28, w: 16, h: 3.2, maxH: 10, i: "RiskHealthKpi" },
        { x: 0, y: 32, w: 16, h: 4, maxH: 10, i: "ProblemBreakdown" },
        { x: 0, y: 36, w: 16, h: 4, maxH: 10, i: "ProblemAssociations" },
        { x: 0, y: 40, w: 16, h: 4, maxH: 10, i: "ProblemAnalysisBreakdown" },
        { x: 0, y: 44, w: 16, h: 4, maxH: 10, i: "ProblemPrioritisation" },
        { x: 0, y: 48, w: 4, h: 4, maxH: 10, i: "ProductivitySavingsKPI" },
        { x: 4, y: 48, w: 8, h: 4, maxH: 10, i: "CaseClosureSplit" },
        { x: 0, y: 52, w: 16, h: 4, maxH: 10, i: "ProductivityBreakdown" },
        { x: 0, y: 56, w: 16, h: 3.2, maxH: 10, i: "ROIKPI" },
        { x: 0, y: 60, w: 16, h: 4, maxH: 10, i: "ProductBreakdown" },
        { x: 0, y: 64, w: 16, h: 3.2, maxH: 10, i: "QualityKPI" },
        { x: 0, y: 68, w: 16, h: 4, maxH: 10, i: "Complaints" },
        { x: 0, y: 72, w: 16, h: 4, maxH: 10, i: "Compliments" },
      ],
    });
  };

  onLayoutChange = (event: any) => {
    this.calculateHeight();
    const positions = [...this.state.positions];
    const updatedPositions = positions.map((p) => {
      const found = event.find((e) => e.i.startsWith(p.i));
      if (found)
        return { ...p, h: found.h, w: found.w, x: found.x, y: found.y };
      else return p;
    });
    this.setState({ positions: updatedPositions });
  };

  getDashboard = () => {
    const clientID = this.props.match.params.id;
    const user = AuthRobin.getUserInfo();
    DoDRobin.when(
      DoDRobin.get(
        "getDashboard",
        `/email/?client=${clientID}&email=${user.email}`
      )
    )
      .then(() => {
        const data = DoDRobin.getResult("getDashboard");
        this.setState({ dodList: data });
      })
      .catch((_) => {
        notification.error({
          type: "error",
          message: "Could not load dod data !",
          description: "There was an error during the dod api execution.",
        });
      });
  };
  getAllDashboard = () => {
    DoDRobin.when(DoDRobin.get("getAllDashboard", ``))
      .then(() => {
        const data = DoDRobin.getResult("getAllDashboard");
        this.setState({ dodAdminList: data });
      })
      .catch((_) => {
        notification.error({
          type: "error",
          message: "Could not load dod data !",
          description: "There was an error during the dod api execution.",
        });
      });
  };

  updateDashboard = () => {
    const user = AuthRobin.getUserInfo();
    DoDRobin.when(
      DoDRobin.put(`editDashboard`, "", {
        _key: this.state.key,
        email: user.email,
        userName: user.name,
        name: this.state.dodName,
        description: this.state.dodDescription,
        dashboard: this.state.dashboard,
        positions: this.state.positions,
        utilizationbreakdown: this.state.utilizationbreakdown,
        keymetrics: this.state.keymetrics,
        groupParticipants: this.state.groupParticipants,
        groupParticipantsFilterOn: this.state.groupParticipantsFilterOn,
        onlineAccess: this.state.onlineAccess,
        sessionBreakdown: this.state.sessionBreakdown,
        sessionBreakdownFilterOn: this.state.sessionBreakdownFilterOn,
        problemsplit: this.state.problemsplit,
        associationsplit: this.state.associationsplit,
        problemAnalysisSplit: this.state.problemAnalysisSplit,
        productivity: this.state.productivity,
        productbreakdown: this.state.productbreakdown,
        breakdownFilterOn: this.state.breakdownFilterOn,
        associationcases: this.state.associationcases,
        associationbreakdown: this.state.associationbreakdown,
        problems: this.state.problems,
        problemclusters: this.state.problemclusters,
        analysisBreakdown: this.state.analysisBreakdown,
        problemAnalysisDrilldown: this.state.problemAnalysisDrilldown,
        analysisBreakdownCases: this.state.analysisBreakdownCases,
      })
    )
      .then(() => {
        this.handleNameModalClose();
        notification.success({
          type: "success",
          message: "Success",
          description: "Dashboard Updated successfully",
        });
      })
      .catch((err) => {
        this.handleNameModalClose();
        notification.error({
          type: "error",
          message: "Could not load quality keymetrics data !",
          description: "There was an error during the analytics api execution.",
        });
      });
  };

  saveDashboard = () => {
    const user = AuthRobin.getUserInfo();
    DoDRobin.when(
      DoDRobin.post(`saveDashboard`, "", {
        name: this.state.dodName,
        description: this.state.dodDescription,
        email: user.email,
        userName: user.name,
        dashboard: this.state.dashboard,
        positions: this.state.positions,
        utilizationbreakdown: this.state.utilizationbreakdown,
        keymetrics: this.state.keymetrics,
        groupParticipants: this.state.groupParticipants,
        groupParticipantsFilterOn: this.state.groupParticipantsFilterOn,
        onlineAccess: this.state.onlineAccess,
        sessionBreakdown: this.state.sessionBreakdown,
        sessionBreakdownFilterOn: this.state.sessionBreakdownFilterOn,
        problemsplit: this.state.problemsplit,
        associationsplit: this.state.associationsplit,
        problemAnalysisSplit: this.state.problemAnalysisSplit,
        productivity: this.state.productivity,
        productbreakdown: this.state.productbreakdown,
        breakdownFilterOn: this.state.breakdownFilterOn,
        associationcases: this.state.associationcases,
        associationbreakdown: this.state.associationbreakdown,
        problems: this.state.problems,
        problemclusters: this.state.problemclusters,
        analysisBreakdown: this.state.analysisBreakdown,
        problemAnalysisDrilldown: this.state.problemAnalysisDrilldown,
        analysisBreakdownCases: this.state.analysisBreakdownCases,
      })
    )
      .then(() => {
        this.handleNameModalClose();
        notification.success({
          type: "success",
          message: "Success",
          description: "Dashboard Saved successfully",
        });
      })
      .catch((err) => {
        this.handleNameModalClose();
        notification.error({
          type: "error",
          message: "Could not load quality keymetrics data !",
          description: "There was an error during the analytics api execution.",
        });
      });
  };

  handleLoad = (key) => {
    const found: any = this.state.dodList.find((d: any) => d._key === key);
    this.setState({
      key: found._key,
      dodName: found.name,
      dodDescription: found.description,
      dashboard: found.dashboard,
      dashboardComponents: found.dashboard,
      positions: found.positions,
      utilizationbreakdown: found.utilizationbreakdown,
      keymetrics: found.keymetrics,
      groupParticipants: found.groupParticipants,
      groupParticipantsFilterOn: found.groupParticipantsFilterOn || ["all"],
      sessionBreakdownFilterOn: found.sessionBreakdownFilterOn || ["all"],
      onlineAccess: found.onlineAccess,
      sessionBreakdown: found.sessionBreakdown,
      problemsplit: found.problemsplit,
      associationsplit: found.associationsplit,
      problemAnalysisSplit: found.problemAnalysisSplit,
      productivity: found.productivity,
      productbreakdown: found.productbreakdown,
      breakdownFilterOn: found.breakdownFilterOn,
      associationcases: found.associationcases,
      associationbreakdown: found.associationbreakdown,
      problems: found.problems,
      problemclusters: found.problemclusters,
      analysisBreakdown: found.analysisBreakdown,
      problemAnalysisDrilldown: found.problemAnalysisDrilldown,
      analysisBreakdownCases: found.analysisBreakdownCases,
      dodLoadModal: false,
      sharedLength: found.total_shared.length,
    });
  };

  handleReset = () => {
    this.setState({
      benchmark: "Previous range",
      dateRange: "month" as any,
      compareBenchmark: true,
      includeDependants: true,
      customBenchmark: false,
      sitelevelBenchmark: false,
      startDate: moment()
        .startOf("month")
        .subtract(1, "month")
        .format("YYYY-MM-DD"),
      endDate: moment()
        .subtract(1, "month")
        .endOf("month")
        .format("YYYY-MM-DD"),
      benchmarkStartDate: moment()
        .startOf("month")
        .subtract(1, "month")
        .format("YYYY-MM-DD"),
      benchmarkEndDate: moment()
        .endOf("month")
        .subtract(1, "month")
        .format("YYYY-MM-DD"),
      dashboardHeight: 0,
      downloadPreview: false,
      searchText: "",
      searchedColumn: "",
      site: ["All"] as any,
      benchmarksitelevel: ["All"] as any,
      dashboard: [] as string[],
      dashboardComponents: [] as string[],
      utilizationbreakdown: ["product"],
      keymetrics: ["service"],
      groupParticipants: ["service"],
      groupParticipantsFilterOn: ["all"],
      sessionBreakdownFilterOn: ["all"],
      onlineAccess: ["service"],
      sessionBreakdown: ["product"],
      problemsplit: ["problemcluster"],
      breakdownFilterOn: [""],
      associationsplit: ["problemcluster"],
      associationcases: [""],
      analysisBreakdown: ["problemcluster"],
      analysisBreakdownCases: [""],
      problemAnalysisSplit: ["employementlvl"],
      productivity: ["product"],
      productbreakdown: ["Gains"],
      draggable: false,
      key: undefined,
      dodName: "",
      dodDescription: "",
      positions: [
        { x: 0, y: 0, w: 4, h: 3.2, maxH: 10, i: "UtilizationKPI" },
        { x: 4, y: 0, w: 8, h: 3.2, maxH: 10, i: "EngagementKPI" },
        { x: 0, y: 4, w: 16, h: 4, maxH: 10, i: "UtilizationBreakdown" },
        { x: 0, y: 8, w: 16, h: 4, maxH: 10, i: "IndividualCasesBreakdown" },
        { x: 0, y: 12, w: 16, h: 4, maxH: 10, i: "GroupParticipantsBreakdown" },
        { x: 0, y: 16, w: 16, h: 4, maxH: 10, i: "OnlineAccessBreakdown" },
        { x: 0, y: 20, w: 16, h: 4, maxH: 10, i: "SessionBreakdown" },
        { x: 0, y: 24, w: 16, h: 3.2, maxH: 10, i: "WorkHealthKPI" },
        { x: 0, y: 28, w: 16, h: 3.2, maxH: 10, i: "RiskHealthKpi" },
        { x: 0, y: 32, w: 16, h: 4, maxH: 10, i: "ProblemBreakdown" },
        { x: 0, y: 36, w: 16, h: 4, maxH: 10, i: "ProblemAssociations" },
        { x: 0, y: 40, w: 16, h: 4, maxH: 10, i: "ProblemAnalysisBreakdown" },
        { x: 0, y: 44, w: 16, h: 4, maxH: 10, i: "ProblemPrioritisation" },
        { x: 0, y: 48, w: 4, h: 4, maxH: 10, i: "ProductivitySavingsKPI" },
        { x: 4, y: 48, w: 8, h: 4, maxH: 10, i: "CaseClosureSplit" },
        { x: 0, y: 52, w: 16, h: 4, maxH: 10, i: "ProductivityBreakdown" },
        { x: 0, y: 56, w: 16, h: 3.2, maxH: 10, i: "ROIKPI" },
        { x: 0, y: 60, w: 16, h: 4, maxH: 10, i: "ProductBreakdown" },
        { x: 0, y: 64, w: 16, h: 3.2, maxH: 10, i: "QualityKPI" },
        { x: 0, y: 68, w: 16, h: 4, maxH: 10, i: "Complaints" },
        { x: 0, y: 72, w: 16, h: 4, maxH: 10, i: "Compliments" },
      ],
    });
  };

  onDelete = (key) => {
    this.setState({ deleteLoader: true });
    DoDRobin.when(DoDRobin.delete("deleteDashboard", `/${key}`))
      .then(() => {
        this.getDashboard();
        this.getAllDashboard();
        this.setState({ key: undefined, deleteLoader: false });
        notification.success({
          type: "success",
          message: "Success",
          description: "Dashboard Deleted successfully",
        });
      })
      .catch((_) => {
        this.setState({ deleteLoader: false });
        notification.error({
          type: "error",
          message: "Could not delete dod data !",
          description: "There was an error during the dod api execution.",
        });
      });
  };

  handleModalCancel = () => {
    this.getDashboard();
    this.getAllDashboard();
    this.setState({
      downloadPreview: false,
      availableHandles: [],
      pdfGenerated: false,
      draggable: false,
      pdfImage: [],
    });
  };

  shareUsers = () => {
    DoDRobin.when(
      DoDRobin.post("shareusers", "/share", {
        dashboard_key: this.state.key,
        users: this.state.sharedUsers,
      })
    )
      .then(() => {
        this.getDashboard();
        this.getAllDashboard();
        this.setState({ key: undefined, sharedUsers: [], shareModal: false });
        notification.success({
          type: "success",
          message: "Success",
          description: "Dashboard Shared successfully",
        });
      })
      .catch((_) => {
        notification.error({
          type: "error",
          message: "Could not share dod data !",
          description: "There was an error during the dod api execution.",
        });
      });
  };
  viewSchedule = (id) => {
    const clientId = this.props.match.params.id;
    DoDRobin.when(
      DoDRobin.get("schedule", `/schedule?client=${clientId}&id=${id}`)
    )
      .then(() => {
        const data = DoDRobin.getResult("schedule");
        this.setState({ schedules: data });
      })
      .catch((_) => {
        notification.error({
          type: "error",
          message: "Could not load schedules !",
          description: "There was an error during the fetching schedules.",
        });
      });
  };
  createSchedule = () => {
    const [scheduleDuration, scheduleUnit] = this.state.scheduleValue.split(
      " "
    );
    DoDRobin.when(
      DoDRobin.post("createSchedule", "/schedule", {
        dashboard_key: this.state.key,
        users: this.state.sharedUsers,
        schedule_date: this.state.scheduleStartDate,
        start_date: this.state.scheduleStartDate,
        end_date: this.state.scheduleEndDate,
        schedule_value: scheduleDuration,
        schedule_unit: scheduleUnit,
        client_id: this.props.match.params.id,
        filter_range: this.state.scheduleFilter,
      })
    )
      .then(() => {
        this.viewSchedule(this.state.key);
        this.setState({
          sharedUsers: [],
          newScheduleModal: false,
          scheduleDate: "",
          scheduleValue: "",
          scheduleFilter: "",
        });
        notification.success({
          type: "success",
          message: "Success",
          description: "Dashboard Scheduled successfully",
        });
      })
      .catch((_) => {
        notification.error({
          type: "error",
          message: "Could not schedule dod report !",
          description: "There was an error during the dod scheduling.",
        });
      });
  };
  updateSchedule = () => {
    const {
      scheduleToEdit,
      sharedUsers,
      scheduleStartDate,
      scheduleEndDate,
      scheduleValue,
      scheduleFilter,
    } = this.state;
    const [scheduleDuration, scheduleUnit] = scheduleValue.split(" ");
    DoDRobin.when(
      DoDRobin.put("updateSchedule", `/schedule/${scheduleToEdit._key}`, {
        dashboard_key: this.state.key,
        users: sharedUsers,
        schedule_date: scheduleStartDate,
        start_date: scheduleStartDate,
        end_date: scheduleEndDate,
        schedule_value: scheduleDuration,
        schedule_unit: scheduleUnit,
        client_id: this.props.match.params.id,
        filter_range: scheduleFilter,
      })
    )
      .then(() => {
        this.viewSchedule(this.state.key);
        this.setState({
          editScheduleModal: false,
          scheduleToEdit: null,
          sharedUsers: [],
          scheduleStartDate: "",
          scheduleEndDate: "",
          scheduleValue: "",
          scheduleFilter: "",
        });
        notification.success({
          type: "success",
          message: "Success",
          description: "Schedule updated successfully",
        });
      })
      .catch((_) => {
        notification.error({
          type: "error",
          message: "Could not update schedule!",
          description: "There was an error during the schedule update.",
        });
      });
  };
  deleteSchedule = (key) => {
    DoDRobin.when(DoDRobin.delete("deleteSchedule", `/schedule/${key}`))
      .then(() => {
        this.viewSchedule(this.state.key);
        notification.success({
          type: "success",
          message: "Success",
          description: "Dashboard schedule deleted successfully",
        });
      })
      .catch((_) => {
        notification.error({
          type: "error",
          message: "Could not schedule dod report !",
          description: "There was an error during the dod scheduling.",
        });
      });
  };

  handlePopOverClick = (mode: string, key?: string) => {
    const user = AuthRobin.getUserInfo();
    const companyKey = this.props.match.params.id;
    let users: any = [];
    const scheduleUsers = this.state.users
      .map((user: any) => {
        return {
          title: user.name,
          text: user.name,
          key: user.email,
          value: user.email,
          client: user.client,
          children: [],
        };
      })
      .filter((user) => user.client && user.client.includes(companyKey))
      .sort((a, b) => a.title.localeCompare(b.title));
    switch (mode) {
      case "public":
        DoDRobin.when(
          DoDRobin.put("publicshare", "", { _key: key, public: true })
        )
          .then(() => {
            this.getDashboard();
            this.getAllDashboard();
            this.setState({ key: undefined });
            notification.success({
              type: "success",
              message: "Success",
              description: "Dashboard Shared successfully",
            });
          })
          .catch((_) => {
            notification.error({
              type: "error",
              message: "Could not share dod data !",
              description: "There was an error during the dod api execution.",
            });
          });
        break;
      case "company":
        const clients = ClientsRobin.getCollection();
        const companyObj = clients.reduce((acc, item) => {
          acc[item._key] = item.Name;
          return acc;
        }, {});
        const companies = user.client
          ? user.client.split(",")
          : clients.map((c) => c._key);
        companies.forEach((c) => {
          if (!companyObj[c]) return;
          const filteredUsers: any = this.state.users
            .filter((u: any) => u.client && u.client.includes(c))
            .map((u: any) => {
              return { title: u.name, key: u.email, value: u.email };
            });
          if (filteredUsers.length)
            users.push({
              title: companyObj[c],
              key: c,
              value: c,
              children: filteredUsers,
            });
        });
        users.sort((a, b) => a.title.localeCompare(b.title));
        this.setState({ key, shareModal: true, filteredUsers: users });
        break;
      case "users":
        const allUsers = this.state.users
          .map((user: any) => {
            return {
              title: user.name,
              key: user.email,
              value: user.email,
              children: [],
            };
          })
          .filter((user) => user);
        this.setState({ key, shareModal: true, filteredUsers: allUsers });
        break;
      case "role":
        const groupedByRole = this.state.users.reduce((acc: any, obj) => {
          const { role, email, name } = obj;

          if (!acc[role]) {
            acc[role] = { title: role, key: role, value: role, children: [] };
          }

          acc[role].children.push({ title: name, key: email, value: email });

          return acc;
        }, {});

        users = Object.values(groupedByRole);
        this.setState({ key, shareModal: true, filteredUsers: users });
        break;
      case "create_schedule":
        this.setState({ newScheduleModal: true, filteredUsers: scheduleUsers });
        break;
      case "view_schedule":
        this.viewSchedule(key);
        this.setState({ key, scheduleModal: true });
        break;
      case "edit_schedule":
        const scheduleToEdit = this.state.schedules.find(
          (schedule) => schedule._key === key
        );
        this.setState({
          sharedUsers: scheduleToEdit.email,
          scheduleStartDate: scheduleToEdit.start_date,
          scheduleEndDate: scheduleToEdit.end_date,
          scheduleValue: `${scheduleToEdit.schedule_value} ${scheduleToEdit.schedule_unit}`,
          scheduleFilter: scheduleToEdit.filter_range,
        });
        this.setState({
          editScheduleModal: true,
          filteredUsers: scheduleUsers,
          scheduleToEdit,
        });
        break;
    }
  };

  handleNameModalClose = () => {
    this.setState({ dodNameModal: false, dodName: "", dodDescription: "" });
    this.getAllDashboard();
    this.getDashboard();
  };

  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            // @ts-ignore
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            this.handleSearch(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleFilterReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: (filtered) => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex] &&
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        // @ts-ignore
        setTimeout(() => this.searchInput.select());
      }
    },
    render: (text) =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text && text.toString()}
        />
      ) : (
        text
      ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleFilterReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  ref = (React as any).createRef();
  dashboardRef = (React as any).createRef();
  render(): JSX.Element {
    const user = AuthRobin.getUserInfo();
    const clientID = this.props.match.params.id;
    const clientName = (ClientsRobin.getResult(`${clientID}`) || { name: "" })
      .Name;
    // Robin results
    // Below variable is used for testing only. It has to be replaced when api is available for sitemapping
    const siteMapping = siteLevelsRobin.isLoading(
      siteLevelsRobin.ACTIONS.FIND_ONE
    )
      ? []
      : (siteLevelsRobin.getModel() || []).map((s) =>
          `clients/${clientID}` === s.branchTo ? { ...s, branchTo: null } : s
        );
    const treeData = ListToTree(siteMapping);

    const casesMapping = {
      "": "All Cases",
      riskcases: "Risk Cases",
      formalreferals: "Formal Referrals",
      severecases: "Severe Cases",
      significantcases: "Significant Cases",
      aid: "AID Cases",
    };

    const splitMapping = {
      employementlvl: "Employment Level",
      gender: "Gender",
      age: "Age",
    };

    let benchmark = this.state.benchmark;

    const dashboards = [
      {
        title: "Engagement",
        value: "engagement",
        key: "engagement",
        disabled: !hasPermission(
          "/view/dashboard/engagement",
          PermissionsRobin.getResult("own-permissions")
        ),
        children: [
          {
            title: "Engagement KPIs",
            value: "engagement_kpi",
            key: "engagement_kpi",
            disabled: !hasPermission(
              "/view/dashboard/engagement",
              PermissionsRobin.getResult("own-permissions")
            ),
          },
          {
            title: "Utilization breakdown",
            value: "utilization_breakdown",
            key: "utilization_breakdown",
            disabled: !hasPermission(
              "/view/dashboard/engagement",
              PermissionsRobin.getResult("own-permissions")
            ),
          },
          {
            title: "Individual cases breakdown",
            value: "individual_cases_breakdown",
            key: "individual_cases_breakdown",
            disabled: !hasPermission(
              "/view/dashboard/engagement",
              PermissionsRobin.getResult("own-permissions")
            ),
          },
          {
            title: "Group participants breakdown",
            value: "group_participants_breakdown",
            key: "group_participants_breakdown",
            disabled: !hasPermission(
              "/view/dashboard/engagement",
              PermissionsRobin.getResult("own-permissions")
            ),
          },
          {
            title: "Online access breakdown",
            value: "online_access_breakdown",
            key: "online_access_breakdown",
            disabled: !hasPermission(
              "/view/dashboard/engagement",
              PermissionsRobin.getResult("own-permissions")
            ),
          },
          {
            title: "Sessions breakdown",
            value: "sessions_breakdown",
            key: "sessions_breakdown",
            disabled: !hasPermission(
              "/view/dashboard/engagement",
              PermissionsRobin.getResult("own-permissions")
            ),
          },
        ],
      },
      {
        title: "Health",
        value: "health",
        key: "health",
        disabled: !hasPermission(
          "/view/dashboard/health",
          PermissionsRobin.getResult("own-permissions")
        ),
        children: [
          {
            title: "Work Health KPI",
            value: "work_health_kpi",
            key: "work_health_kpi",
            disabled: !hasPermission(
              "/view/dashboard/health",
              PermissionsRobin.getResult("own-permissions")
            ),
          },
          {
            title: "Risk Health KPIs",
            value: "risk_health_kpi",
            key: "risk_health_kpi",
            disabled: !hasPermission(
              "/view/dashboard/health",
              PermissionsRobin.getResult("own-permissions")
            ),
          },
          {
            title: "Problem breakdown",
            value: "problem_breakdown",
            key: "problem_breakdown",
            disabled: !hasPermission(
              "/view/dashboard/health",
              PermissionsRobin.getResult("own-permissions")
            ),
          },
          {
            title: "Problems association breakdown",
            value: "problems_association_breakdown",
            key: "problems_association_breakdown",
            disabled: !hasPermission(
              "/view/dashboard/health",
              PermissionsRobin.getResult("own-permissions")
            ),
          },
          {
            title: "Problem analysis breakdown",
            value: "problem_analysis_breakdown",
            key: "problem_analysis_breakdown",
            disabled: !hasPermission(
              "/view/dashboard/health",
              PermissionsRobin.getResult("own-permissions")
            ),
          },
          {
            title: "Problem prioritisation",
            value: "problem_prioritisation",
            key: "problem_prioritisation",
            disabled: !hasPermission(
              "/view/dashboard/health",
              PermissionsRobin.getResult("own-permissions")
            ),
          },
        ],
      },
      {
        title: "Effectiveness",
        value: "effectiveness",
        key: "effectiveness",
        disabled: !hasPermission(
          "/view/dashboard/effectiveness",
          PermissionsRobin.getResult("own-permissions")
        ),
        children: [
          {
            title: "Productivity Savings",
            value: "productivity_savings",
            key: "productivity_savings",
            disabled: !hasPermission(
              "/view/dashboard/effectiveness",
              PermissionsRobin.getResult("own-permissions")
            ),
          },
          {
            title: "Productivity Breakdown",
            value: "productivity_breakdown",
            key: "productivity_breakdown",
            disabled: !hasPermission(
              "/view/dashboard/effectiveness",
              PermissionsRobin.getResult("own-permissions")
            ),
          },
        ],
      },
      {
        title: "ROI",
        value: "roi",
        key: "roi",
        disabled: !hasPermission(
          "/view/dashboard/roi",
          PermissionsRobin.getResult("own-permissions")
        ),
        children: [
          {
            title: "ROI KPI",
            value: "roi_kpi",
            key: "roi_kpi",
            disabled: !hasPermission(
              "/view/dashboard/roi",
              PermissionsRobin.getResult("own-permissions")
            ),
          },
          {
            title: "Product Breakdown",
            value: "product_breakdown",
            key: "product_breakdown",
            disabled: !hasPermission(
              "/view/dashboard/roi",
              PermissionsRobin.getResult("own-permissions")
            ),
          },
        ],
      },
      {
        title: "Quality",
        value: "quality",
        key: "quality",
        disabled: !hasPermission(
          "/view/dashboard/quality",
          PermissionsRobin.getResult("own-permissions")
        ),
        children: [
          {
            title: "Feedback Quality KPI",
            value: "quality_kpi",
            key: "quality_kpi",
            disabled: !hasPermission(
              "/view/dashboard/quality",
              PermissionsRobin.getResult("own-permissions")
            ),
          },
          // {
          // 	title: 'Complaints',
          // 	value: 'complaints',
          // 	key: 'complaints',
          // },
          // {
          // 	title: 'Compliments',
          // 	value: 'compliments',
          // 	key: 'compliments',
          // }
        ],
      },
    ];
    const benchmarkFilter = Array.isArray(this.state.benchmarksitelevel)
      ? this.state.benchmarksitelevel.filter((item) => item !== "All")
      : this.state.benchmarksitelevel.split();
    if (
      benchmarkFilter &&
      benchmarkFilter.filter((item) => item !== "All").length
    ) {
      benchmark = "site";
    }
    const benchmarkMapping = {
      "Previous range": "Previous period",
      industry: "Sector",
      SegmentSize: "Size",
      clients: "Company",
      custom: "Custom range",
      site: "Site levels",
    };
    const compareBenchmark = this.state.compareBenchmark;

    const columns = [
      {
        title: "Name",
        dataIndex: "name",
        key: "name",
        ...this.getColumnSearchProps("name"),
      },
      {
        title: "Description",
        dataIndex: "description",
        key: "description",
        ...this.getColumnSearchProps("description"),
      },
      {
        title: "Shared",
        key: "shared",
        render: (record) =>
          record.public
            ? "Public"
            : (user.email === record.email || user.role === "admin") &&
              record.total_shared
            ? record.total_shared.length
            : "",
      },
      {
        title: "Scheduled",
        key: "scheduled",
        render: (record) => (record.scheduled ? record.scheduled.length : 0),
      },
      {
        title: "Created By",
        dataIndex: "userName",
        key: "userName",
        ...this.getColumnSearchProps("userName"),
      },
      {
        title: "Action",
        key: "action",
        render: (record) => (
          <div>
            {user.email === record.email && (
              <Popconfirm
                title="Are you sure you want to delete this dashboard?"
                onConfirm={() => this.onDelete(record._key)}
                onCancel={(e) => console.log(e)}
                okText="Yes"
                cancelText="No"
              >
                <Tooltip title="Delete">
                  <Button type="danger" style={{ marginRight: "10px" }}>
                    <Icon type="delete" />
                  </Button>
                </Tooltip>
              </Popconfirm>
            )}
            <Tooltip title="Load">
              <Button
                type="primary"
                style={{ marginRight: "10px" }}
                onClick={() =>
                  this.props.history.push(
                    `/client/${clientID}/dod/${record._key}`
                  )
                }
              >
                <Icon type="cloud-download" />
              </Button>
            </Tooltip>
            <Popover
              content={
                <div style={{ display: "flex", flexDirection: "column" }}>
                  {hasPermission(
                    "/view/dod/share-everyone",
                    PermissionsRobin.getResult("own-permissions")
                  ) ? (
                    <Popconfirm
                      title="Are you sure you want to share this dashboard publicly?"
                      onConfirm={() =>
                        this.handlePopOverClick("public", record._key)
                      }
                      onCancel={(e) => console.log(e)}
                      okText="Yes"
                      cancelText="No"
                    >
                      <Button style={{ marginBottom: "10px" }}>
                        Share with everyone
                      </Button>
                    </Popconfirm>
                  ) : (
                    <></>
                  )}
                  {hasPermission(
                    "/view/dod/share-company",
                    PermissionsRobin.getResult("own-permissions")
                  ) ? (
                    <Button
                      onClick={() =>
                        this.handlePopOverClick("company", record._key)
                      }
                      style={{ marginBottom: "10px" }}
                    >
                      Share within company
                    </Button>
                  ) : (
                    <></>
                  )}
                  {hasPermission(
                    "/view/dod/share-users",
                    PermissionsRobin.getResult("own-permissions")
                  ) ? (
                    <Button
                      onClick={() =>
                        this.handlePopOverClick("users", record._key)
                      }
                      style={{ marginBottom: "10px" }}
                    >
                      Share with users
                    </Button>
                  ) : (
                    <></>
                  )}
                  {hasPermission(
                    "/view/dod/share-role",
                    PermissionsRobin.getResult("own-permissions")
                  ) ? (
                    <Button
                      onClick={() =>
                        this.handlePopOverClick("role", record._key)
                      }
                      style={{ marginBottom: "10px" }}
                    >
                      Share with specific role
                    </Button>
                  ) : (
                    <></>
                  )}
                </div>
              }
            >
              {user.email === record.email && (
                <Button type="primary" style={{ marginRight: "10px" }}>
                  <Icon type="share-alt" />
                </Button>
              )}
            </Popover>
            {hasPermission(
              "/view/dod/schedule",
              PermissionsRobin.getResult("own-permissions")
            ) ? (
              <Tooltip title="Schedule">
                <Button
                  type="primary"
                  onClick={() =>
                    this.handlePopOverClick("view_schedule", record._key)
                  }
                >
                  <Icon type="clock-circle" />
                </Button>
              </Tooltip>
            ) : (
              <></>
            )}
          </div>
        ),
      },
    ];

    const dodAdminTableColumns = [
      {
        title: "Name",
        dataIndex: "name",
        key: "name",
        ...this.getColumnSearchProps("name"),
      },
      {
        title: "Description",
        dataIndex: "description",
        key: "description",
        ...this.getColumnSearchProps("description"),
      },
      {
        title: "Shared",
        key: "shared",
        render: (record) => (record.public ? "Public" : record.shared.length),
      },
      {
        title: "Created By",
        dataIndex: "userName",
        key: "userName",
        ...this.getColumnSearchProps("userName"),
      },
    ];
    const dodScheduleColumns = [
      {
        title: "Scheduled for",
        dataIndex: "email",
        key: "email",
        ...this.getColumnSearchProps("email"),
      },
      {
        title: "Frequency",
        key: "frequency",
        render: (record) =>
          `Every ${record.schedule_value > 1 ? record.schedule_value : ""} ${
            record.schedule_unit
          }`,
      },
      {
        title: "Start Date",
        key: "start_date",
        render: (record) => moment(record.start_date).format("YYYY-MM-DD"),
      },
      {
        title: "End Date",
        key: "end_date",
        render: (record) => moment(record.end_date).format("YYYY-MM-DD"),
      },
      {
        title: "Default date range",
        key: "filter_range",
        render: (record) => {
          switch (record.filter_range) {
            case "past month":
              return "Past month";
            case "past 3 months":
              return "Past 3 months";
            case "past 6 months":
              return "Past 6 months";
            case "past 1 year":
              return "Past 1 year";
          }
        },
      },
      {
        title: "Action",
        key: "action",
        render: (record) => (
          <div>
            <Tooltip title="Edit">
              <Button
                style={{ marginRight: "10px" }}
                onClick={() =>
                  this.handlePopOverClick("edit_schedule", record._key)
                }
              >
                <Icon type="edit" />
              </Button>
            </Tooltip>
            <Tooltip title="Delete">
              <Button
                type="danger"
                style={{ marginRight: "10px" }}
                onClick={() => this.deleteSchedule(record._key)}
              >
                <Icon type="delete" />
              </Button>
            </Tooltip>
          </div>
        ),
      },
    ];

    const topBar = (
      <div
        className="dashboard-top-bar"
        style={{
          position: "fixed",
          zIndex: 1,
          width: "100%",
          borderBottom: "1px solid #CCC",
          borderTop: "1px solid #CCC",
          background: "#F8FBFF",
        }}
      >
        <div
          style={{
            display: "flex",
            padding: "1rem",
            justifyContent: "space-between",
            width: "100%",
          }}
        >
          <div
            style={{ display: "flex", alignItems: "center", overflowX: "auto" }}
          >
            <div className="user-filter">
              <DashboardFilter
                width={200}
                type="tree"
                title="DASHBOARD"
                placeholder="Select Charts"
                showCheckedStrategy={TreeSelect.SHOW_CHILD}
                style={{ marginRight: "1rem" }}
                defaultValue={this.state.dashboard}
                treeData={dashboards}
                onChange={(selected) => {
                  this.resetPositions();
                  this.setState({ dashboard: selected });
                }}
              />
            </div>
            <div className="user-filter">
              <DashboardFilter
                width={200}
                type="tree"
                title="SITE"
                defaultValue={this.state.site}
                style={{ marginRight: "1rem" }}
                treeData={treeData}
                onChange={(selected) => this.setState({ site: selected })}
              />
            </div>
            <DateRangeFilter
              value={this.state.dateRange}
              customOption
              financialStartYear={moment(this.state.filter)}
              label="DATE RANGE"
              style={{ minWidth: 130, marginRight: "1rem" }}
              onChange={(selected) => {
                notification.info({
                  type: "info",
                  message: "Reporting Period",
                  description: (
                    <div>
                      <div>
                        Start Date:{" "}
                        {moment(selected.startDate).format("YYYY-MM-DD")}
                      </div>
                      <div>
                        End Date:{" "}
                        {moment(selected.endDate).format("YYYY-MM-DD")}
                      </div>
                    </div>
                  ),
                });
                this.setState({
                  dateRange: selected.value,
                  startDate: moment(selected.startDate).format("YYYY-MM-DD"),
                  endDate: moment(selected.endDate).format("YYYY-MM-DD"),
                });
              }}
            />
            <DashboardFilter
              width={150}
              type="single"
              text="Compared to"
              title="BENCHMARK"
              defaultValue={benchmark}
              options={[
                { value: "Previous range", text: "Previous period" },
                { value: "industry", text: "Sector" },
                { value: "SegmentSize", text: "Size" },
                { value: "clients", text: "Company" },
                { value: "custom", text: "Custom range" },
                { value: "site", text: "Site levels" },
              ]}
              // onChange={(selected) => selected === 'custom' ? this.setState({ customBenchmark: true }) : this.setState({ benchmark: selected })
              onChange={(selected) => {
                if (selected === "custom") {
                  this.setState({ customBenchmark: true });
                } else if (selected === "site") {
                  this.setState({ sitelevelBenchmark: true });
                } else {
                  this.setState({ benchmark: selected });
                }
              }}
            />
            <DashboardFilter
              type="switch"
              text="Display Benchmark"
              style={{ marginTop: "18px" }}
              checked={compareBenchmark}
              onChange={(selected) =>
                this.setState({ compareBenchmark: selected })
              }
            />

            <DashboardFilter
              type="switch"
              text={`Include Dependants`}
              style={{ marginTop: "18px" }}
              checked={this.state.includeDependants}
              onChange={(selected) =>
                this.setState({ includeDependants: selected })
              }
            />
          </div>
          <div style={{ display: "flex", alignItems: "center" }}>
            <Tooltip title="Clear all">
              <Button
                onClick={() => {
                  this.handleReset();
                  this.props.history.push(`/client/${clientID}/dod`);
                }}
                style={{ marginLeft: "10px" }}
              >
                <Icon type="close-circle" />
              </Button>
            </Tooltip>
            <Tooltip title="Template library">
              <Button
                onClick={() => this.setState({ dodLoadModal: true })}
                style={{ marginLeft: "10px" }}
              >
                <Icon type="folder-open" />
              </Button>
            </Tooltip>
            <Tooltip title="Preview data">
              <Button
                disabled={this.state.dashboard.length === 0}
                onClick={() => this.generateReport(this.state.dashboard)}
                style={{ marginLeft: "10px" }}
              >
                <Icon type="global" />
              </Button>
            </Tooltip>
            <Tooltip title="Build">
              <Button
                disabled={this.state.dashboard.length === 0}
                onClick={() => {
                  this.setState(
                    {
                      downloadPreview: true,
                      availableHandles: [
                        "s",
                        "w",
                        "e",
                        "n",
                        "sw",
                        "nw",
                        "se",
                        "ne",
                      ],
                      draggable: true,
                    },
                    function() {
                      setTimeout(this.calculateHeight, 2000);
                    }
                  );
                }}
                style={{ marginLeft: "10px" }}
              >
                <Icon type="layout" />
              </Button>
            </Tooltip>
          </div>
        </div>
        <Modal
          className="si-date-picker-custom"
          title={
            <span style={{ fontSize: "20px", fontWeight: 300 }}>
              Custom range
            </span>
          }
          visible={this.state.customBenchmark}
          onOk={this.customBencharkOk}
          onCancel={this.customBencharkCancel}
        >
          <div className="si-date-picker-modal-body">
            <div className="si-date-picker-row-from">
              <span style={{ width: 46 }}>From: </span>
              <AntDatePicker
                allowClear={false}
                onChange={(date: any) =>
                  this.setState({ benchmarkStartDate: date.startOf("day") })
                }
              />
            </div>
            <div className="si-date-picker-row-to">
              <span style={{ width: 46 }}>To: </span>
              <AntDatePicker
                allowClear={false}
                onChange={(date: any) =>
                  this.setState({ benchmarkEndDate: date.endOf("day") })
                }
              />
            </div>
          </div>
        </Modal>
        <Modal
          className="si-date-picker-custom"
          title={
            <span style={{ fontSize: "20px", fontWeight: 300 }}>
              Site levels{" "}
            </span>
          }
          width={400}
          visible={this.state.sitelevelBenchmark}
          onOk={this.sitelevelBencharkOk}
          onCancel={this.sitelevelBencharkCancel}
        >
          <DashboardFilter
            width={300}
            type="tree"
            title="SITE"
            defaultValue={this.state.benchmarksitelevel}
            style={{
              marginRight: "1rem",
              display: "flex",
              flexDirection: "column",
            }}
            treeData={treeData}
            onChange={(selected) =>
              this.setState({ benchmarksitelevel: selected })
            }
          />
        </Modal>
      </div>
    );

    //Engagement Dashboards
    const engagement_kpi = [
      <div
        key={`UtilizationKPI`}
        data-grid={this.state.positions.find((p) => p.i === "UtilizationKPI")}
      >
        <Dashboard className="dod-scrollbar">
          <EngagementAdjustedKPI
            key={`${clientID}-UtilizationKPI`}
            apiKey={`${clientID}-UtilizationKPI`}
            client={clientName}
            displayRelativeValues={true}
            compareBenchmark={compareBenchmark}
            benchmarkMapping={benchmarkMapping}
          />
        </Dashboard>
      </div>,
      <div
        key={`EngagementKPI`}
        data-grid={this.state.positions.find((p) => p.i === "EngagementKPI")}
      >
        <Dashboard className="dod-scrollbar">
          <KeyMetricsKPI
            key={`${clientID}-EngagementKPI`}
            apiKey={`${clientID}-EngagementKPI`}
            client={clientName}
            displayRelativeValues={false}
            compareBenchmark={compareBenchmark}
            benchmarkMapping={benchmarkMapping}
          />
        </Dashboard>
      </div>,
    ];

    const utilisation_breakdown: any = [];
    this.state.utilizationbreakdown.map((_breakdown, index) => {
      utilisation_breakdown.push(
        <div
          key={`UtilizationBreakdown-${index}`}
          data-grid={this.state.positions.find(
            (p) => p.i === "UtilizationBreakdown"
          )}
          style={{ paddingBottom: "30px" }}
        >
          <Dashboard
            key={index}
            listIndex={index}
            addDuplicateGraphs={this.addDuplicateGraphs}
            removeDuplicateGraphs={this.removeDuplicateGraphs}
            graphLength={this.state.utilizationbreakdown.length}
            downloadPreview={this.state.downloadPreview}
            filter="utilizationbreakdown"
            className="dod-dashboard dod-scrollbar"
            name="Utilization Breakdown"
          >
            <UtilisationBreakdown
              apiKey={`${clientID}-UtilizationBreakdown-${index}`}
              client={clientName}
              displayRelativeValues={false}
              compareBenchmark={compareBenchmark}
              downloadPreview={this.state.downloadPreview}
              benchmark={benchmark}
              benchmarkMapping={benchmarkMapping}
              filter={this.state.utilizationbreakdown[index]}
              showLabel={true}
              splitFilter={(selected) => {
                const keys: any = [...this.state.utilizationbreakdown];
                keys[index] = selected;
                this.setState({ utilizationbreakdown: keys });
              }}
            />
          </Dashboard>
        </div>
      );
    });

    const individual_cases_breakdown: any = [];
    this.state.keymetrics.map((_metric, indCases) => {
      individual_cases_breakdown.push(
        <div
          key={`IndividualCasesBreakdown-${indCases}`}
          data-grid={this.state.positions.find(
            (p) => p.i === "IndividualCasesBreakdown"
          )}
          style={{ paddingBottom: "30px" }}
        >
          <Dashboard
            key={indCases}
            listIndex={indCases}
            addDuplicateGraphs={this.addDuplicateGraphs}
            removeDuplicateGraphs={this.removeDuplicateGraphs}
            graphLength={this.state.keymetrics.length}
            downloadPreview={this.state.downloadPreview}
            filter="keymetrics"
            className="dod-dashboard dod-scrollbar"
            name="Individual Cases Breakdown"
          >
            <IndividualCasesBreakdown
              apiKey={`${clientID}-IndividualCasesBreakdown-${indCases}`}
              client={clientName}
              displayRelativeValues={false}
              compareBenchmark={compareBenchmark}
              benchmark={benchmark}
              benchmarkMapping={benchmarkMapping}
              downloadPreview={this.state.downloadPreview}
              filter={this.state.keymetrics[indCases]}
              showLabel={true}
              splitFilter={(selected) => {
                const keys: any = [...this.state.keymetrics];
                keys[indCases] = selected;
                this.setState({ keymetrics: keys });
              }}
            />
          </Dashboard>
        </div>
      );
    });

    const group_participants_breakdown: any = [];
    this.state.groupParticipants.map((_participants, index) => {
      group_participants_breakdown.push(
        <div
          key={`GroupParticipantsBreakdown-${index}`}
          data-grid={this.state.positions.find(
            (p) => p.i === "GroupParticipantsBreakdown"
          )}
          style={{ paddingBottom: "30px" }}
        >
          <Dashboard
            key={index}
            listIndex={index}
            addDuplicateGraphs={this.addDuplicateGraphs}
            removeDuplicateGraphs={this.removeDuplicateGraphs}
            graphLength={this.state.groupParticipants.length}
            downloadPreview={this.state.downloadPreview}
            filter="groupParticipants"
            className="dod-dashboard dod-scrollbar"
            name="Group Participants Breakdown"
          >
            <GroupParticipantsBreakdown
              apiKey={`${clientID}-GroupParticipantsBreakdown-${index}`}
              client={clientName}
              displayRelativeValues={false}
              compareBenchmark={compareBenchmark}
              benchmark={benchmark}
              benchmarkMapping={benchmarkMapping}
              downloadPreview={this.state.downloadPreview}
              filter={this.state.groupParticipants[index]}
              filterValue={this.state.groupParticipantsFilterOn[index]}
              showLabel={true}
              splitFilter={(selected) => {
                const keys: any = [...this.state.groupParticipants];
                keys[index] = selected;
                this.setState({ groupParticipants: keys });
              }}
              filterOn={(selected) => {
                const keys: any = [...this.state.groupParticipantsFilterOn];
                keys[index] = selected;
                this.setState({ groupParticipantsFilterOn: keys });
              }}
            />
          </Dashboard>
        </div>
      );
    });

    const online_access_breakdown: any = [];
    this.state.onlineAccess.map((_access, index) => {
      online_access_breakdown.push(
        <div
          key={`OnlineAccessBreakdown-${index}`}
          data-grid={this.state.positions.find(
            (p) => p.i === "OnlineAccessBreakdown"
          )}
          style={{ paddingBottom: "30px" }}
        >
          <Dashboard
            key={index}
            listIndex={index}
            addDuplicateGraphs={this.addDuplicateGraphs}
            removeDuplicateGraphs={this.removeDuplicateGraphs}
            graphLength={this.state.onlineAccess.length}
            downloadPreview={this.state.downloadPreview}
            filter="onlineAccess"
            className="dod-dashboard dod-scrollbar"
            name="Online Access Breakdown"
          >
            <OnlineAccessBreakdown
              apiKey={`${clientID}-OnlineAccessBreakdown-${index}`}
              client={clientName}
              displayRelativeValues={false}
              compareBenchmark={compareBenchmark}
              benchmark={benchmark}
              benchmarkMapping={benchmarkMapping}
              downloadPreview={this.state.downloadPreview}
              filter={this.state.onlineAccess[index]}
              showLabel={true}
              splitFilter={(selected) => {
                const keys: any = [...this.state.onlineAccess];
                keys[index] = selected;
                this.setState({ onlineAccess: keys });
              }}
            />
          </Dashboard>
        </div>
      );
    });

    const session_breakdown: any = [];
    this.state.sessionBreakdown.map((_session, index) => {
      session_breakdown.push(
        <div
          key={`SessionBreakdown-${index}`}
          data-grid={this.state.positions.find(
            (p) => p.i === "SessionBreakdown"
          )}
          style={{ paddingBottom: "30px" }}
        >
          <Dashboard
            key={index}
            listIndex={index}
            addDuplicateGraphs={this.addDuplicateGraphs}
            removeDuplicateGraphs={this.removeDuplicateGraphs}
            graphLength={this.state.sessionBreakdown.length}
            downloadPreview={this.state.downloadPreview}
            filter="sessionBreakdown"
            className="dod-dashboard dod-scrollbar"
            name="Session Breakdown"
          >
            <SessionsBreakdown
              apiKey={`${clientID}-SessionBreakdown-${index}`}
              client={clientName}
              displayRelativeValues={false}
              compareBenchmark={compareBenchmark}
              benchmark={benchmark}
              downloadPreview={this.state.downloadPreview}
              benchmarkMapping={benchmarkMapping}
              filter={this.state.sessionBreakdown[index]}
              filterValue={this.state.sessionBreakdownFilterOn[index]}
              showLabel={true}
              splitFilter={(selected) => {
                const keys: any = [...this.state.sessionBreakdown];
                keys[index] = selected;
                this.setState({ sessionBreakdown: keys });
              }}
              filterOn={(selected) => {
                const keys: any = [...this.state.sessionBreakdownFilterOn];
                keys[index] = selected;
                this.setState({ sessionBreakdownFilterOn: keys });
              }}
            />
          </Dashboard>
        </div>
      );
    });

    // Health Dashboards

    const work_health_kpi = (
      <div
        key={`WorkHealthKPI`}
        data-grid={this.state.positions.find((p) => p.i === "WorkHealthKPI")}
      >
        <Dashboard className="dod-scrollbar">
          <WorkImpactScoreKPI
            apiKey={`${clientID}-HealthKPI`}
            client={clientName}
            displayRelativeValues={false}
            compareBenchmark={compareBenchmark}
            benchmarkMapping={benchmarkMapping}
          />
        </Dashboard>
      </div>
    );

    const risk_health_kpi = (
      <div
        key={`RiskHealthKpi`}
        data-grid={this.state.positions.find((p) => p.i === "RiskHealthKpi")}
      >
        <Dashboard className="dod-scrollbar">
          <HighRiskFactorKPI
            apiKey={`${clientID}-HealthKPI`}
            client={clientName}
            displayRelativeValues={false}
            compareBenchmark={compareBenchmark}
            benchmarkMapping={benchmarkMapping}
          />
        </Dashboard>
      </div>
    );

    const problem_breakdown: any = [];
    this.state.problemsplit.map((_breakdown, index) => {
      problem_breakdown.push(
        <div
          key={`ProblemBreakdown-${index}`}
          data-grid={this.state.positions.find(
            (p) => p.i === "ProblemBreakdown"
          )}
          style={{ paddingBottom: "30px" }}
        >
          <Dashboard
            key={index}
            listIndex={index}
            addDuplicateGraphs={this.addDuplicateGraphs}
            removeDuplicateGraphs={this.removeDuplicateGraphs}
            graphLength={this.state.problemsplit.length}
            downloadPreview={this.state.downloadPreview}
            filter="problemsplit"
            className="dod-dashboard dod-scrollbar"
            name="Problem Breakdown"
          >
            <ProblemBreakdown
              apiKey={`${clientID}-ProblemBreakdown-${index}`}
              client={clientName}
              displayRelativeValues={false}
              compareBenchmark={compareBenchmark}
              benchmark={benchmark}
              downloadPreview={this.state.downloadPreview}
              benchmarkMapping={benchmarkMapping}
              splitBy={this.state.problemsplit[index]}
              casesMapping={casesMapping}
              cases={this.state.breakdownFilterOn[index]}
              showLabel={true}
              splitFilter={(selected) => {
                const keys: any = [...this.state.problemsplit];
                keys[index] = selected;
                this.setState({ problemsplit: keys });
              }}
              casesFilter={(selected) => {
                const keys: any = [...this.state.breakdownFilterOn];
                keys[index] = selected;
                this.setState({ breakdownFilterOn: keys });
              }}
            />
          </Dashboard>
        </div>
      );
    });

    const problems_association_breakdown: any = [];
    this.state.associationsplit.map((_breakdown, index) => {
      problems_association_breakdown.push(
        <div
          key={`ProblemAssociations-${index}`}
          data-grid={this.state.positions.find(
            (p) => p.i === "ProblemAssociations"
          )}
          style={{ paddingBottom: "30px" }}
        >
          <Dashboard
            key={index}
            listIndex={index}
            addDuplicateGraphs={this.addDuplicateGraphs}
            removeDuplicateGraphs={this.removeDuplicateGraphs}
            graphLength={this.state.associationsplit.length}
            downloadPreview={this.state.downloadPreview}
            filter="associationsplit"
            className="dod-dashboard dod-scrollbar"
            name="Problem Association Breakdown"
          >
            <ProblemAssociations
              apiKey={`${clientID}-ProblemAssociations-${index}`}
              client={clientName}
              displayRelativeValues={false}
              splitBy={this.state.associationsplit[index]}
              downloadPreview={this.state.downloadPreview}
              casesMapping={casesMapping}
              cases={this.state.associationcases[index]}
              showLabel={true}
              associations={
                this.state.associationsplit[index] === "problemcluster"
                  ? this.state.problemclusters
                  : this.state.problems
              }
              problembreakdown={this.state.associationbreakdown[index]}
              splitFilter={(selected) => {
                const keys: any = [...this.state.associationsplit];
                keys[index] = selected;
                const breakdownKeys: any = [...this.state.associationbreakdown];
                breakdownKeys[index] =
                  selected === "problemcluster"
                    ? (this.state.problemclusters[0] as any).value
                    : (this.state.problems[0] as any).value;
                this.setState({
                  associationsplit: keys,
                  associationbreakdown: breakdownKeys,
                });
              }}
              casesFilter={(selected) => {
                const keys: any = [...this.state.associationcases];
                keys[index] = selected;
                this.setState({ associationcases: keys });
              }}
              associationsFilter={(selected) => {
                const keys: any = [...this.state.associationbreakdown];
                keys[index] = selected;
                this.setState({ associationbreakdown: keys });
              }}
            />
          </Dashboard>
        </div>
      );
    });

    const problem_analysis_breakdown: any = [];
    this.state.problemAnalysisSplit.map((_breakdown, index) => {
      problem_analysis_breakdown.push(
        <div
          key={`ProblemAnalysisBreakdown-${index}`}
          data-grid={this.state.positions.find(
            (p) => p.i === "ProblemAnalysisBreakdown"
          )}
          style={{ paddingBottom: "30px" }}
        >
          <Dashboard
            key={index}
            listIndex={index}
            addDuplicateGraphs={this.addDuplicateGraphs}
            removeDuplicateGraphs={this.removeDuplicateGraphs}
            graphLength={this.state.problemAnalysisSplit.length}
            downloadPreview={this.state.downloadPreview}
            filter="problemAnalysisSplit"
            className="dod-dashboard dod-scrollbar"
            name="Problem Analysis Breakdown"
          >
            <ProblemAnalysisBreakdown
              apiKey={`${clientID}-ProblemAnalysisBreakdown-${index}`}
              client={clientName}
              displayRelativeValues={false}
              compareBenchmark={compareBenchmark}
              benchmark={benchmark}
              benchmarkMapping={benchmarkMapping}
              downloadPreview={this.state.downloadPreview}
              casesMapping={casesMapping}
              splitMapping={splitMapping}
              problemLevel={this.state.analysisBreakdown[index]}
              problemDrillDown={this.state.problemAnalysisDrilldown[index]}
              drillDown={
                this.state.analysisBreakdown[index] === "problemcluster"
                  ? this.state.problemclusters
                  : this.state.problems
              }
              cases={this.state.analysisBreakdownCases[index]}
              splitBy={this.state.problemAnalysisSplit[index]}
              showLabel={true}
              problemLevelFilter={(selected) => {
                const analysisKeys: any = [...this.state.analysisBreakdown];
                analysisKeys[index] = selected;
                const problemAnalysisKeys: any = [
                  ...this.state.problemAnalysisDrilldown,
                ];
                problemAnalysisKeys[index] =
                  selected === "problemcluster"
                    ? (this.state.problemclusters[0] as any).value
                    : (this.state.problems[0] as any).value;
                this.setState({
                  analysisBreakdown: analysisKeys,
                  problemAnalysisDrilldown: problemAnalysisKeys,
                });
              }}
              drillDownFilter={(selected) => {
                const keys: any = [...this.state.problemAnalysisDrilldown];
                keys[index] = selected;
                this.setState({ problemAnalysisDrilldown: keys });
              }}
              casesFilter={(selected) => {
                const keys: any = [...this.state.analysisBreakdownCases];
                keys[index] = selected;
                this.setState({ analysisBreakdownCases: keys });
              }}
              splitFilter={(selected) => {
                const keys: any = [...this.state.problemAnalysisSplit];
                keys[index] = selected;
                this.setState({ problemAnalysisSplit: keys });
              }}
            />
          </Dashboard>
        </div>
      );
    });

    const problem_prioritisation = (
      <div
        key={`ProblemPrioritisation`}
        data-grid={this.state.positions.find(
          (p) => p.i === "ProblemPrioritisation"
        )}
        style={{ paddingBottom: "30px" }}
      >
        <Dashboard
          className="dod-dashboard dod-scrollbar"
          name="Problem Prioritisation"
        >
          <ProblemPrioritisation
            apiKey={`${clientID}-ProblemPrioritisation`}
            client={clientName}
            benchmark={benchmark}
            benchmarkMapping={benchmarkMapping}
          />
        </Dashboard>
      </div>
    );

    // Effectiveness Dashboard
    const productivity_savings = [
      <div
        key={`ProductivitySavingsKPI`}
        data-grid={this.state.positions.find(
          (p) => p.i === "ProductivitySavingsKPI"
        )}
      >
        <Dashboard className="dod-scrollbar">
          <ProductivitySavingsKPI
            apiKey={`${clientID}-ProductivitySavingsKPI`}
            client={clientName}
            displayRelativeValues={false}
            compareBenchmark={compareBenchmark}
            benchmarkMapping={benchmarkMapping}
          />
        </Dashboard>
      </div>,
      <div
        key={`CaseClosureSplit`}
        data-grid={this.state.positions.find((p) => p.i === "CaseClosureSplit")}
      >
        <Dashboard className="dod-scrollbar">
          <CaseClosureSplit
            apiKey={`${clientID}-CaseClosureSplit`}
            client={clientName}
            showLabel={true}
            displayRelativeValues={false}
            compareBenchmark={compareBenchmark}
            benchmark={benchmark}
            benchmarkMapping={benchmarkMapping}
          />
        </Dashboard>
      </div>,
    ];

    const productivity_breakdown: any = [];
    this.state.productivity.map((_breakdown, index) => {
      productivity_breakdown.push(
        <div
          key={`ProductivityBreakdown-${index}`}
          data-grid={this.state.positions.find(
            (p) => p.i === "ProductivityBreakdown"
          )}
          style={{ paddingBottom: "30px" }}
        >
          <ProductivityBreakdown
            dod={{
              listIndex: index,
              addDuplicateGraphs: this.addDuplicateGraphs,
              removeDuplicateGraphs: this.removeDuplicateGraphs,
              graphLength: this.state.productivity.length,
              filter: "productivity",
              downloadPreview: this.state.downloadPreview,
            }}
            apiKey={`${clientID}-ProductivityBreakdown-${index}`}
            client={clientName}
            displayRelativeValues={false}
            compareBenchmark={compareBenchmark}
            benchmark={benchmark}
            benchmarkMapping={benchmarkMapping}
            splitBy={this.state.productivity[index]}
            showLabel={true}
            splitFilter={(selected) => {
              const keys: any = [...this.state.productivity];
              keys[index] = selected;
              this.setState({ productivity: keys });
            }}
          />
        </div>
      );
    });

    //ROI Dashboard
    const roi_kpi = (
      <div
        key={`ROIKPI`}
        data-grid={this.state.positions.find((p) => p.i === "ROIKPI")}
        style={{ paddingBottom: "30px" }}
      >
        <ROIMetricsKPI
          apiKey={`${clientID}-ROIKPI`}
          client={clientName}
          compareBenchmark={compareBenchmark}
          benchmarkMapping={benchmarkMapping}
        />
      </div>
    );

    const product_breakdown: any = [];
    this.state.productbreakdown.map((_breakdown, index) => {
      product_breakdown.push(
        <div
          key={`ProductBreakdown-${index}`}
          data-grid={this.state.positions.find(
            (p) => p.i === "ProductBreakdown"
          )}
          style={{ paddingBottom: "30px" }}
        >
          <ProductBreakdown
            dod={{
              listIndex: index,
              addDuplicateGraphs: this.addDuplicateGraphs,
              removeDuplicateGraphs: this.removeDuplicateGraphs,
              graphLength: this.state.productbreakdown.length,
              filter: "productbreakdown",
              downloadPreview: this.state.downloadPreview,
            }}
            apiKey={`${clientID}-ProductBreakdown-${index}`}
            client={clientName}
            compareBenchmark={compareBenchmark}
            benchmark={benchmark}
            benchmarkMapping={benchmarkMapping}
            showLabel={true}
            productBreakdown={this.state.productbreakdown[index]}
            splitFilter={(selected) => {
              const keys: any = [...this.state.productbreakdown];
              keys[index] = selected;
              this.setState({ productbreakdown: keys });
            }}
          />
        </div>
      );
    });

    //Quality Dashboard
    const quality_kpi = (
      <div
        key={`QualityKPI`}
        data-grid={this.state.positions.find((p) => p.i === "QualityKPI")}
        style={{ paddingBottom: "30px" }}
      >
        <FeedbackQualityMetric
          apiKey={`${clientID}-QualityKPI`}
          client={clientName}
          compareBenchmark={compareBenchmark}
          benchmarkMapping={benchmarkMapping}
        />
      </div>
    );
    //  const complaints =  <div key={`Complaints`} data-grid={this.state.positions.find(p => p.i === 'Complaints')} style={{paddingBottom: '30px'}}>

    // 	 <Dashboard className='dod-dashboard' name='Complaints'>
    // 		<ComplaintsWordCloud
    // 			client={clientName}
    // 			apiKey={`${clientID}-Complaints`} />
    // 	</Dashboard>
    // 	</div>
    // 	const compliments = <div key={`Compliments`} data-grid={this.state.positions.find(p => p.i === 'Compliments')} style={{paddingBottom: '30px'}}>
    // 	<Dashboard className='dod-dashboard' name='Compliments'>
    // 		<ComplimentsWordCloud
    // 			apiKey={`${clientID}-Compliments`}
    // 			client={clientName} />
    // 	</Dashboard>
    // 	</div>
    // Final Dashboard
    const dashboard_on_demand =
      this.state.dashboardComponents.length === 0 ? (
        <div style={{ width: "100%", marginTop: "45px", textAlign: "center" }}>
          <img src="/src/assets/img/dod_image.png" style={{ height: "70vh" }} />
        </div>
      ) : (
        <div
          ref={this.dashboardRef}
          id="dod"
          style={{ width: "100%", marginTop: "70px", marginBottom: "50px" }}
        >
          <ResponsiveGridLayout
            className="layout"
            breakpoints={{ lg: 1200 }}
            cols={{ lg: 12, md: 4, sm: 3, xs: 2, xxs: 1 }}
            rowHeight={100}
            resizeHandles={this.state.availableHandles}
            onLayoutChange={this.onLayoutChange}
            isDraggable={this.state.draggable}
            compactType={"vertical"}
          >
            {this.state.dashboardComponents.indexOf("engagement_kpi") >= 0
              ? engagement_kpi
              : null}
            {this.state.dashboardComponents.indexOf("utilization_breakdown") >=
            0
              ? utilisation_breakdown
              : null}
            {this.state.dashboardComponents.indexOf(
              "individual_cases_breakdown"
            ) >= 0
              ? individual_cases_breakdown
              : null}
            {this.state.dashboardComponents.indexOf(
              "group_participants_breakdown"
            ) >= 0
              ? group_participants_breakdown
              : null}
            {this.state.dashboardComponents.indexOf(
              "online_access_breakdown"
            ) >= 0
              ? online_access_breakdown
              : null}
            {this.state.dashboardComponents.indexOf("sessions_breakdown") >= 0
              ? session_breakdown
              : null}
            {/* {this.state.dashboardComponents.indexOf('health_kpi') >= 0 ? health_kpi : null} */}
            {this.state.dashboardComponents.indexOf("work_health_kpi") >= 0
              ? work_health_kpi
              : null}
            {this.state.dashboardComponents.indexOf("risk_health_kpi") >= 0
              ? risk_health_kpi
              : null}
            {this.state.dashboardComponents.indexOf("problem_breakdown") >= 0
              ? problem_breakdown
              : null}
            {this.state.dashboardComponents.indexOf(
              "problems_association_breakdown"
            ) >= 0
              ? problems_association_breakdown
              : null}
            {this.state.dashboardComponents.indexOf(
              "problem_analysis_breakdown"
            ) >= 0
              ? problem_analysis_breakdown
              : null}
            {this.state.dashboardComponents.indexOf("problem_prioritisation") >=
            0
              ? problem_prioritisation
              : null}
            {this.state.dashboardComponents.indexOf("productivity_savings") >= 0
              ? productivity_savings
              : null}
            {this.state.dashboardComponents.indexOf("productivity_breakdown") >=
            0
              ? productivity_breakdown
              : null}
            {this.state.dashboardComponents.indexOf("roi_kpi") >= 0
              ? roi_kpi
              : null}
            {this.state.dashboardComponents.indexOf("product_breakdown") >= 0
              ? product_breakdown
              : null}
            {this.state.dashboardComponents.indexOf("quality_kpi") >= 0
              ? quality_kpi
              : null}
            {/* {this.state.dashboardComponents.indexOf('complaints') >= 0 ? complaints : null} */}
            {/* {this.state.dashboardComponents.indexOf('compliments') >= 0 ? compliments : null} */}
          </ResponsiveGridLayout>
        </div>
      );

    const dashboard = (
      <ErrorBoundary>
        <Modal
          title={
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <div>Dashboard Creation View</div>
              <div>
                {!this.state.pdfGenerated && (
                  <Button
                    loading={this.state.pptLoader}
                    style={{ marginRight: 10 }}
                    onClick={this.handlePPTGenerate}
                  >
                    Generate Power Point
                  </Button>
                )}
                {!this.state.pdfImage.length ? (
                  <Button
                    loading={this.state.pptLoader}
                    style={{ marginRight: 10 }}
                    onClick={this.handleGenerate}
                  >
                    Generate PDF
                  </Button>
                ) : null}
                {!this.state.pdfGenerated && (
                  <Button
                    loading={this.state.pptLoader}
                    style={{ marginRight: 20 }}
                    onClick={() => this.setState({ dodNameModal: true })}
                  >
                    Save Dashboard
                  </Button>
                )}
              </div>
            </div>
          }
          style={{ backgroundColor: "white" }}
          visible={this.state.downloadPreview}
          footer={null}
          destroyOnClose
          onCancel={this.handleModalCancel}
          width={1500}
        >
          {this.state.pdfGenerated ? (
            <div style={{ textAlign: "center" }}>
              <PDFDownloadLink
                document={<this.PDFDocument />}
                fileName={`${clientName} Dashboard.pdf`}
              >
                {({ loading }) =>
                  loading ? (
                    "Loading document..."
                  ) : (
                    <Button
                      style={{ marginLeft: "110px" }}
                      size="large"
                      onClick={this.handleDownload}
                    >
                      Download Now!
                    </Button>
                  )
                }
              </PDFDownloadLink>
            </div>
          ) : (
            <div
              id="dod-modal"
              ref={this.ref}
              style={{ padding: "30px 0px 0px 30px" }}
            >
              <div id="pdf-header">
                <div className="pdf-header">
                  <img src="/src/assets/img/lyra-white.png" height={80} />
                </div>
              </div>
              <div
                id="reporting-cycle"
                style={{
                  textAlign: "center",
                  fontSize: 20,
                  position: "relative",
                  top: 40,
                }}
              >
                Dashboard Reporting Cycle: Start Date: {this.state.startDate},
                End Date: {this.state.endDate}
              </div>
              {dashboard_on_demand}
              <div
                id="footer"
                style={{
                  display: "flex",
                  margin: "10px",
                  alignItems: "center",
                }}
              >
                <img
                  src={"/src/assets/img/lyra-logo.png"}
                  alt="Lyra SA Logo.png"
                  style={{ height: "30px" }}
                  className="si-logo"
                />
                <p style={{ marginLeft: "10px" }}>
                  <b style={{ fontWeight: "bold" }}>
                    Confidentiality Disclaimer:
                  </b>
                  <br />
                  This document may contain confidential and/or privileged
                  material. This information is solely for the use of the
                  individual or entity for whom it was intended. If you are not
                  the intended recipient you may not peruse, use, disseminate,
                  distribute or copy this document or any information contained
                  thereto without breaching its confidentiality and you are
                  therefore requested to delete the original document
                  immediately as any further action or reaction thereon is
                  prohibited.
                </p>
              </div>
            </div>
          )}
        </Modal>

        <div>
          {topBar}
          {this.state.isLoading ? (
            <Spin tip="Please wait while your dashboard data is being loaded. It can take upto few minutes">
              <div
                style={{ marginTop: "30rem", width: "100%", height: "100%" }}
              />
            </Spin>
          ) : (
            <div style={{ display: "flex", width: "100%" }}>
              {dashboard_on_demand}
            </div>
          )}
        </div>
        <Modal
          title="Dashboard"
          visible={this.state.dodNameModal}
          footer={[
            <Button onClick={this.handleNameModalClose}>Cancel</Button>,
            this.state.key &&
              // to check if current user has created dashboard
              this.state.dodList.find((d) => d._key === this.state.key)
                .email === user.email && (
                <Popconfirm
                  title={`This dahsboard is shared with ${this.state.sharedLength} users. Do you want to update it?`}
                  onConfirm={this.updateDashboard}
                  onCancel={(e) => console.log(e)}
                  okText="Yes"
                  cancelText="No"
                >
                  <Button type="primary">Update</Button>
                </Popconfirm>
              ),
            <Button type="primary" onClick={this.saveDashboard}>
              Save
            </Button>,
          ]}
          destroyOnClose
          onCancel={this.handleNameModalClose}
        >
          <Input
            value={this.state.dodName}
            onChange={(e) => this.setState({ dodName: e.target.value })}
            placeholder="Name"
          />
          <TextArea
            value={this.state.dodDescription}
            onChange={(e) => this.setState({ dodDescription: e.target.value })}
            placeholder="Description"
            rows={4}
            style={{ marginTop: "10px" }}
          />
        </Modal>

        <Modal
          title="Select Dashboard"
          footer={null}
          visible={this.state.dodLoadModal}
          width={1400}
          destroyOnClose
          onCancel={() => this.setState({ dodLoadModal: false })}
        >
          <Tabs>
            <TabPane tab="User Dashboard" key="1">
              <div style={{ width: "100%", textAlign: "center" }}>
                <Table
                  loading={this.state.deleteLoader}
                  columns={columns}
                  dataSource={this.state.dodList}
                />
              </div>
            </TabPane>
            {user.role === "admin" && (
              <TabPane tab="All Dashboard" key="2">
                <div style={{ width: "100%", textAlign: "center" }}>
                  <Table
                    loading={this.state.deleteLoader}
                    columns={dodAdminTableColumns}
                    dataSource={this.state.dodAdminList}
                  />
                </div>
              </TabPane>
            )}
          </Tabs>
        </Modal>

        <Modal
          title="Share"
          visible={this.state.shareModal}
          destroyOnClose
          onCancel={() => this.setState({ shareModal: false, sharedUsers: [] })}
          footer={[
            <Button
              onClick={() =>
                this.setState({ shareModal: false, sharedUsers: [] })
              }
            >
              Cancel
            </Button>,
            <Popconfirm
              title="You are about to share this with other users! Are you sure?"
              onConfirm={this.shareUsers}
              okText="Yes"
              cancelText="No"
            >
              <Button type="primary">Share</Button>
            </Popconfirm>,
          ]}
        >
          <div className="user-filter">
            <DashboardFilter
              width={200}
              type="tree"
              title="Users"
              placeholder="Select User"
              showCheckedStrategy={TreeSelect.SHOW_CHILD}
              style={{ border: "1px solid #CCC" }}
              defaultValue={this.state.sharedUsers}
              treeData={this.state.filteredUsers}
              onChange={(selected) => {
                this.setState({ sharedUsers: selected });
              }}
            />
          </div>
        </Modal>
        <Modal
          title="Schedules"
          visible={this.state.scheduleModal}
          destroyOnClose
          width={1000}
          onCancel={() => this.setState({ scheduleModal: false })}
          footer={[
            <Button onClick={() => this.setState({ scheduleModal: false })}>
              Close
            </Button>,
            <Button onClick={() => this.handlePopOverClick("create_schedule")}>
              {" "}
              Create new schedule
            </Button>,
          ]}
        >
          <Table
            loading={DoDRobin.isLoading("schedule")}
            columns={dodScheduleColumns}
            dataSource={this.state.schedules}
          />
        </Modal>
        <Modal
          title="New schedule"
          visible={this.state.newScheduleModal}
          destroyOnClose
          onCancel={() =>
            this.setState({ newScheduleModal: false, sharedUsers: [] })
          }
          footer={[
            <Button
              onClick={() =>
                this.setState({ newScheduleModal: false, sharedUsers: [] })
              }
            >
              Cancel
            </Button>,
            <Button onClick={this.createSchedule} type="primary">
              Schedule
            </Button>,
          ]}
        >
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <DashboardFilter
              width={200}
              type="tree"
              title="Users"
              placeholder="Select User"
              showCheckedStrategy={TreeSelect.SHOW_CHILD}
              defaultValue={this.state.sharedUsers}
              treeData={this.state.filteredUsers}
              onChange={(selected) => {
                this.setState({ sharedUsers: selected });
              }}
            />
            <div style={{ marginBottom: "6px" }}>
              <span
                style={{
                  fontSize: "0.8rem",
                  color: "grey",
                  marginLeft: "0.3rem",
                  marginBottom: "0.3rem",
                }}
              >
                Start Date
              </span>
              <br />
              <AntDatePicker
                style={{ width: 200 }}
                onChange={(value) =>
                  this.setState({
                    scheduleStartDate: moment(value).format("YYYY-MM-DD"),
                  })
                }
              />
            </div>
            <div style={{ marginBottom: "6px" }}>
              <span
                style={{
                  fontSize: "0.8rem",
                  color: "grey",
                  marginLeft: "0.3rem",
                  marginBottom: "0.3rem",
                }}
              >
                End Date
              </span>
              <br />
              <AntDatePicker
                style={{ width: 200 }}
                onChange={(value) =>
                  this.setState({
                    scheduleEndDate: moment(value).format("YYYY-MM-DD"),
                  })
                }
              />
            </div>
            <DashboardFilter
              width={200}
              defaultValue={this.state.scheduleValue}
              type="single"
              title="Repeat After Every"
              placeholder="Schedule Date"
              options={[
                { text: "1 month", value: "1 month" },
                { text: "2 months", value: "2 month" },
                { text: "3 months", value: "3 month" },
                { text: "6 months", value: "6 month" },
                { text: "1 year", value: "12 month" },
              ]}
              onChange={(selected) => {
                this.setState({ scheduleValue: selected });
              }}
            />
            <DashboardFilter
              width={200}
              defaultValue={this.state.scheduleFilter}
              type="single"
              title="Date Filter"
              placeholder="Schedule Date"
              options={[
                { text: "Past month", value: "past month" },
                { text: "Past 3 months", value: "past 3 months" },
                { text: "Past 6 month", value: "past 6 months" },
                { text: "Past 1 year", value: "past 1 year" },
              ]}
              onChange={(selected) => {
                this.setState({ scheduleFilter: selected });
              }}
            />
          </div>
        </Modal>
        <Modal
          title="Edit Schedule"
          visible={this.state.editScheduleModal}
          destroyOnClose
          onCancel={() =>
            this.setState({
              editScheduleModal: false,
              scheduleToEdit: null,
              sharedUsers: [],
              scheduleStartDate: "",
              scheduleEndDate: "",
              scheduleValue: "",
              scheduleFilter: "",
            })
          }
          footer={[
            <Button
              onClick={() =>
                this.setState({
                  editScheduleModal: false,
                  scheduleToEdit: null,
                  sharedUsers: [],
                  scheduleStartDate: "",
                  scheduleEndDate: "",
                  scheduleValue: "",
                  scheduleFilter: "",
                })
              }
            >
              Cancel
            </Button>,
            <Button type="primary" onClick={this.updateSchedule}>
              Update
            </Button>,
          ]}
        >
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <DashboardFilter
              width={200}
              type="single"
              title="Users"
              placeholder="Select User"
              defaultValue={this.state.sharedUsers}
              options={this.state.filteredUsers}
              onChange={(selected) => {
                this.setState({ sharedUsers: selected });
              }}
            />
            <div style={{ marginBottom: "6px" }}>
              <span
                style={{
                  fontSize: "0.8rem",
                  color: "grey",
                  marginLeft: "0.3rem",
                  marginBottom: "0.3rem",
                }}
              >
                Start Date
              </span>
              <br />
              <AntDatePicker
                style={{ width: 200 }}
                value={moment(this.state.scheduleStartDate)}
                onChange={(value) =>
                  this.setState({
                    scheduleStartDate: moment(value).format("YYYY-MM-DD"),
                  })
                }
              />
            </div>
            <div style={{ marginBottom: "6px" }}>
              <span
                style={{
                  fontSize: "0.8rem",
                  color: "grey",
                  marginLeft: "0.3rem",
                  marginBottom: "0.3rem",
                }}
              >
                End Date
              </span>
              <br />
              <AntDatePicker
                style={{ width: 200 }}
                value={moment(this.state.scheduleEndDate)}
                onChange={(value) =>
                  this.setState({
                    scheduleEndDate: moment(value).format("YYYY-MM-DD"),
                  })
                }
              />
            </div>
            <DashboardFilter
              width={200}
              defaultValue={this.state.scheduleValue}
              type="single"
              title="Repeat After Every"
              placeholder="Schedule Date"
              options={[
                { text: "1 month", value: "1 month" },
                { text: "2 months", value: "2 month" },
                { text: "3 months", value: "3 month" },
                { text: "6 months", value: "6 month" },
                { text: "1 year", value: "12 month" },
              ]}
              onChange={(selected) => {
                this.setState({ scheduleValue: selected });
              }}
            />
            <DashboardFilter
              width={200}
              defaultValue={this.state.scheduleFilter}
              type="single"
              title="Date Filter"
              placeholder="Schedule Date"
              options={[
                { text: "Past month", value: "past month" },
                { text: "Past 3 months", value: "past 3 months" },
                { text: "Past 6 months", value: "past 6 months" },
                { text: "Past 1 year", value: "past 1 year" },
              ]}
              onChange={(selected) => {
                this.setState({ scheduleFilter: selected });
              }}
            />
          </div>
        </Modal>
      </ErrorBoundary>
    );
    return hasPermission(
      "/view/dashboard-on-demand",
      PermissionsRobin.getResult("own-permissions")
    )
      ? dashboard
      : noPermission(PermissionsRobin.isLoading("own-permissions"));
  }
}
export default DashboardOnDemand;
