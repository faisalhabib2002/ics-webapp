import * as React from 'react'
import {connectRobin} from '@simplus/robin-react'
import {robins} from '../../../../robins'
import {ProfilePicture} from '@simplus/siui'
const {UsersRobin} = robins;

@connectRobin([UsersRobin])
export class ProgrammeInfoUser extends React.Component<{clientCustodian: string, icasCustodian: string}> {
	componentWillReceiveProps(nextProps: {clientCustodian: string, icasCustodian: string}): void {
		if ((nextProps.clientCustodian !== this.props.clientCustodian) || (nextProps.icasCustodian !== this.props.icasCustodian)) {
			const users: string[] = []
			nextProps.clientCustodian ? users.push(`"${nextProps.clientCustodian}"`) : null;
			nextProps.icasCustodian ? users.push(`"${nextProps.icasCustodian}"`) : null;
			UsersRobin.get(`info-${nextProps.clientCustodian}-${nextProps.icasCustodian}`, `/list/[${users}]`)
		}
	}
	render(): any {
		const default_User = {
			picture: '/src/assets/img/sample-profile.png',
			name: 'Unassigned',
			email: 'Unassigned'
		}
		const custodians = UsersRobin.getResult(`info-${this.props.clientCustodian}-${this.props.icasCustodian}`)
		const ICASCustodian_data = (custodians && custodians[this.props.icasCustodian]) ? custodians[this.props.icasCustodian] : default_User
		const clientCustodian_data = (custodians && custodians[this.props.clientCustodian]) ? custodians[this.props.clientCustodian] : default_User
		return (
			<div className='custodian-row'>
				<div className='custodian-section'>
					<div className='custodian-title'>Lyra SA<br/>CUSTODIAN</div>
					<ProfilePicture rounded url={ICASCustodian_data.picture || default_User.picture} size={50} />
					<div className='custodian-name'>{ICASCustodian_data.name}</div>
					<div className='custodian-email'>{ICASCustodian_data.email}</div>
				</div>
				<div className='custodian-section'>
					<div className='custodian-title'>CLIENT<br/>CUSTODIAN</div>
					<ProfilePicture rounded url={clientCustodian_data.picture || default_User.picture} size={50} />
					<div className='custodian-name'>{clientCustodian_data.name}</div>
					<div className='custodian-email'>{clientCustodian_data.email}</div>
				</div>
			</div>
		)
	}
}
export default ProgrammeInfoUser;