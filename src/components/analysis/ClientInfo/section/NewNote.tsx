import * as React from 'react'
import Modal from 'antd/lib/modal'
import Form from 'antd/lib/form'
import Input from 'antd/lib/input'
import { FormComponentProps } from 'antd/lib/form';
import {ErrorBoundary} from '../../../../utils/ErrorBoundary'
import mapValues from 'lodash/mapValues'
const FormItem = Form.Item;

interface FormProps extends FormComponentProps {
	visible: boolean,
	onCancel(): void,
	onCreate(): void,
	wrappedComponentRef?(formref: any): void
}

export const NewNote = Form.create({
	mapPropsToFields : props => {
		return mapValues(props, (v) => {
			return Form.createFormField({value : v})
		})
	}
})(
	class extends React.Component<FormProps> {
		render(): JSX.Element {
		const { visible, onCancel, onCreate, form } = this.props;
		const { getFieldDecorator, getFieldValue } = form;
		const key = getFieldValue('_key')
		return (
			<ErrorBoundary>
				<Modal
					visible={visible}
					title={key ? 'Edit note' : 'Create a new note'}
					okText={key ? 'Update' : 'Create'}
					onCancel={onCancel}
					onOk={onCreate}
				>
					<Form layout='vertical'>
					<Form.Item
						style={{display: 'none'}}
					>
						{getFieldDecorator('_key', {
						})
						(
							<span hidden />
						)}
					</Form.Item>
					<FormItem label='Title'>
						{getFieldDecorator('title', {
						rules: [{ required: true, message: 'Please input the title of Note!' }],
						})(
						<Input placeholder='Note title'/>
						)}
					</FormItem>
					<FormItem label='Note'  className='form_last-form-item'>
						{getFieldDecorator('text')(<Input.TextArea placeholder='Write note content' autosize={{ minRows: 6, maxRows: 12 }} />)}
					</FormItem>
					</Form>
				</Modal>
			</ErrorBoundary>
			);
		}
	}
);