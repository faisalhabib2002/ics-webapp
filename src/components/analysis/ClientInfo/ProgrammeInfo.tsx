import * as React from 'react'
import Divider from 'antd/lib/divider'
import notification from 'antd/lib/notification'
import Modal from 'antd/lib/modal'
import Button from 'antd/lib/button'
import {TitledCard, Card, Loader} from '@simplus/siui'
import {ListLayout, ListItem} from '@simplus/macaw-business'
import {connectRobin} from '@simplus/robin-react'
import {RouteComponentProps} from 'react-router-dom'
import {robins} from '../../../robins'
import {ErrorBoundary} from '../../../utils/ErrorBoundary'
import {ProgrammeInfoUser} from './section'
import { hasPermission } from '../../../utils';

const productsJSON = require('../../../utils/products.json')

const {ClientsRobin, clientSettings, UsersRobin, ProductsRobin, PermissionsRobin} = robins;

@connectRobin([ClientsRobin, clientSettings, UsersRobin, ProductsRobin, PermissionsRobin])
export class ProgrammeInfo extends React.Component {
	state = { visible: false, product: {name: '', description: ''}, logo: ''}
	componentWillMount(): void {
		const clientID = (this.props as RouteComponentProps<{id: string}>).match.params.id
		ClientsRobin.when(ClientsRobin.findOne(clientID)).catch(err => {
			notification.error({type: 'error', message: 'Could not load client data !', description: 'There was an error during the settings api execution.'})
		})
		clientSettings.when(clientSettings.findOne(clientID)).catch(err => {
			notification.error({type: 'error', message: 'Could not load settings data !', description: 'There was an error during the settings api execution.'})
		})
		ProductsRobin.when(ProductsRobin.find({})).catch(err => {
			notification.error({type: 'error', message: 'Could not load products data !', description: 'There was an error during the products api execution.'})
		})
	}
	showModal = () => {
		this.setState({
			visible: true,
		});
	}
	handleOk = () => {
		this.setState({
			visible: false,
		});
	}
	handleCancel = () => {
		this.setState({
			visible: false,
		});
	}
	render(): JSX.Element {
		const client_data = ClientsRobin.getModel() || {
			Name: '',
			Industry: '',
			SegmentSize: '',
			EmployeeCount: '',
			Tier: '',
		}
		const settings = clientSettings.getModel() || [{
			img: '',
			custodian: '',
			products: []
		}]
		const allProducts = (ProductsRobin.getCollection() || [
			{product: ''},
		]).filter(product => (!!product&& (product.product!=='Absence ' && product.product!=='eCare' && product.product !== 'Unknown' )))

		const products = (settings[0] && settings[0].products) ? settings[0].products.filter( product => !!product && product!=='eCare' ) : []
		// Added Topic talks and Webinars to available products for all clients - Faisal 10/10/24
		if (!products.includes('Topic talks')) {
			products.push('Topic talks');
		}
		if (!products.includes('Webinars')) {
			products.push('Webinars');
		}
		
		const extraProducts = allProducts.filter(item => !(products || []).find(product => product.toLowerCase() === item.product.toLowerCase()))

		const clientCustodian = (settings[0] && settings[0].clientCustodian) ? settings[0].clientCustodian : ''
		const icasCustodian = (settings[0] && settings[0].icasCustodian) ? settings[0].icasCustodian : ''

		const img = settings[0] ? settings[0].img : ''
		return(
			<ErrorBoundary>
				<div className='programme-info'>
					<h1 className='analysis-tab-title'>Programme info</h1>
					<div style={{position: 'relative'}}>
						{(ClientsRobin.isLoading(ClientsRobin.ACTIONS.FIND_ONE) || ClientsRobin.isError(ClientsRobin.ACTIONS.FIND_ONE)) ? <Loader error={ClientsRobin.isError(ClientsRobin.ACTIONS.FIND_ONE)}/> : null}
						<div style={{display: 'flex'}}>
							<div>
								<TitledCard margin
									titleStyle={{ backgroundImage: `url(/src/assets/img/sector/${(client_data.Industry || 'null').split(' ').join('')}.jpg)`, backgroundColor : '#0090cd', backgroundSize: '100% 100%', height: '200px' }}
									rounded
									picture={img}
									className = 'client-card'>
									<h3>
										<div className='client-card-title'>{client_data.Name}</div>
									</h3>
									{hasPermission('/view/programme-info/icas', PermissionsRobin.getResult('own-permissions')) ? (
									<div className='client-card-content'>
										<div className='client-card-item-name'>Industry: <span className='client-card-items'>{client_data.Industry}</span></div>
										<div className='client-card-item-name'>Size segment: <span className='client-card-items'>{client_data.SegmentSize}</span></div>
										<div className='client-card-item-name'># employees: <span className='client-card-items'>{client_data.EmployeeCount}</span></div>
										<div className='client-card-item-name'>Tier: <span className='client-card-items'>{client_data.Tier}</span> </div>
									</div> )
									: null }
									<ProgrammeInfoUser clientCustodian={clientCustodian} icasCustodian={icasCustodian}/>
								</TitledCard>
							</div>
							<ListLayout spacebetween={10} expandable direction='vertical'>
								<ListItem style={{padding: '1rem'}}>
									<div className='product-title'>Available Products</div>
									<div className='product-section'>
									{(products || []).sort().map ( (product, index) =>
										<div key={index}
										style={{cursor: 'pointer'}}
										onClick={() => this.setState({
											product: productsJSON.find(item => item.key === product.toLowerCase()),
											logo: `/src/assets/img/Products/${(product.toLowerCase() || '').split(' ').join('').split('|').join('')}.png`,
											visible: true
										})}>
											<Card key={index} className='product-cards'>
												<div className='product-icon'>
													<img width={60} src={`/src/assets/img/Products/${(product.toLowerCase() || '').split(' ').join('').split('|').join('')}.png`}/>													
												</div>
												<div className='product-name'>{product === 'AID' ? 'OP Solutions' :product}</div>
											</Card>
										</div>
									)}
									</div>
								</ListItem>
								<ListItem style={{padding: '1rem'}}>
									<div className='product-title'>Other Products</div>
									<div className='product-section'>
									{extraProducts.sort((a, b) => {
									if(b.product.toLowerCase() < a.product.toLowerCase())
									{
										return 1;
									}else{
										return 0;
									}
									})
									.map ( (item, index) =>
										<div key={index}
										style={{cursor: 'pointer'}}
										onClick={() => this.setState({
											product: productsJSON.find(product => product.key === item.product.toLowerCase()),
											logo: `/src/assets/img/OtherProducts/${(item.product.toLowerCase() || '').split(' ').join('').split('|').join('')}.png`,
											visible: true
										})}>
											<Card className='product-cards'>
												<div className='product-icon'>
													<img width={60} src={`/src/assets/img/OtherProducts/${(item.product.toLowerCase() || '').split(' ').join('').split('|').join('')}.png`}/>
												</div>
												<div className='product-name'>{item.product === 'eCare' ? 'Online Wellness' : item.product === 'AID' ? 'OP Solutions' : item.product }</div>
											</Card>
										</div>
									)}
									</div>
								</ListItem>
							</ListLayout>
						</div>
						<Modal
							width={800}
							title={<div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
								<h3 style={{color: '#0E3A53', fontSize: '1.75rem', fontWeight: 400}}>{(this.state.product || {name: 'Not defined'}).name}</h3>
								<img height={60} src={this.state.logo}/>
							</div>}
							visible={this.state.visible}
							onOk={this.handleOk}
							onCancel={this.handleCancel}
							closable={false}
							footer={[<Button key='Got it' type='primary' onClick={this.handleOk}>Got it</Button>]}
							>
							<div className='programme-modal'dangerouslySetInnerHTML={{ __html: (this.state.product || {description: 'Not defined'}).description }} />
						</Modal>
					</div>
				</div>
			</ErrorBoundary>
		)
	}

}
export default ProgrammeInfo;