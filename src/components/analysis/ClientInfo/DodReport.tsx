import React, { Component } from "react";
import { Modal, Button, notification, Spin, DatePicker } from "antd";
import { Document, Page, PDFDownloadLink, Image } from "@react-pdf/renderer";
import {
  DateRangeFilter,
  ListToTree,
  getTopActiveNodes,
} from "./DashboardContent/utils";
import moment from "moment";
import { Responsive, WidthProvider } from "react-grid-layout";
import { robins } from "src/robins";
import { Dashboard, DashboardFilter } from "src/components/dashboard";
//Engagement Dashboards
import { EngagementAdjustedKPI } from "./DashboardContent/Engagement/EngagementAdjustedKPI";
import { KeyMetricsKPI } from "./DashboardContent/Engagement/KeyMetricsKPI";
import { UtilisationBreakdown } from "./DashboardContent/Engagement/UtilisationBreakdown";
import { IndividualCasesBreakdown } from "./DashboardContent/Engagement/IndividualCasesBreakdown";
import { GroupParticipantsBreakdown } from "./DashboardContent/Engagement/GroupParticipantsBreakdown";
import { OnlineAccessBreakdown } from "./DashboardContent/Engagement/OnlineAccessBreakdown";
import { SessionsBreakdown } from "./DashboardContent/Engagement/SessionsBreakdown";
//Health Dashboards
import { WorkImpactScoreKPI } from "./DashboardContent/Health/WorkImpactScoreKPI";
import { HighRiskFactorKPI } from "./DashboardContent/Health/HighRiskFactorKPI";
import { ProblemBreakdown } from "./DashboardContent/Health/ProblemBreakdown";
import { ProblemAssociations } from "./DashboardContent/Health/ProblemAssociations";
import { ProblemAnalysisBreakdown } from "./DashboardContent/Health/ProblemAnalysisBreakdown";
import { ProblemPrioritisation } from "./DashboardContent/Health/ProblemPrioritisation";
//Effectiveness Dashboards
import { ProductivitySavingsKPI } from "./DashboardContent/Effectiveness/ProductivitySavingsKPI";
import { CaseClosureSplit } from "./DashboardContent/Effectiveness/CaseClosureSplit";
import { ProductivityBreakdown } from "./DashboardContent/Effectiveness/ProductivityBreakdown";
//ROI Dashboards
import { ROIMetricsKPI } from "./DashboardContent/ROI/ROIMetricsKPI";
import { ProductBreakdown } from "./DashboardContent/ROI/ProductBreakdown";
//Quality Dashboards
import { FeedbackQualityMetric } from "./DashboardContent/Quality/FeedbackQualityMetric";
import { withRouter } from "react-router-dom";
import pptxgen from "pptxgenjs";
import DOMToImage from "dom-to-image";

const {
  AnalyticsRobin,
  clientSettings,
  siteLevelsRobin,
  ClientsRobin,
  DoDRobin,
} = robins;
const ResponsiveGridLayout = WidthProvider(Responsive);

class DodReport extends Component {
  state = {
    filter: moment("2018-01-01"),
    isLoading: false, // State to manage loader visibility
    clientName: "",
    benchmark: "Previous range",
    dateRange: "month" as any,
    compareBenchmark: false,
    includeDependants: true,
    customBenchmark: false,
    sitelevelBenchmark: false,
    startDate: moment()
      .startOf("month")
      .subtract(1, "month")
      .format("YYYY-MM-DD"),
    endDate: moment()
      .subtract(1, "month")
      .endOf("month")
      .format("YYYY-MM-DD"),
    benchmarkStartDate: moment()
      .startOf("month")
      .subtract(1, "month")
      .format("YYYY-MM-DD"),
    benchmarkEndDate: moment()
      .endOf("month")
      .subtract(1, "month")
      .format("YYYY-MM-DD"),
    dashboardHeight: 0,
    downloadPreview: false,
    searchText: "",
    searchedColumn: "",
    site: ["All"] as any,
    benchmarksitelevel: ["All"] as any,
    dashboard: [] as string[],
    dashboardComponents: [] as string[],
    utilizationbreakdown: ["product"],
    keymetrics: ["service"],
    groupParticipants: ["service"],
    groupParticipantsFilterOn: ["all"],
    onlineAccess: ["service"],
    sessionBreakdown: ["product"],
    sessionBreakdownFilterOn: ["all"],
    problemsplit: ["problemcluster"],
    breakdownFilterOn: [""],
    associationsplit: ["problemcluster"],
    associationcases: [""],
    associationbreakdown: [""],
    problems: [],
    problemclusters: [],
    analysisBreakdown: ["problemcluster"],
    problemAnalysisDrilldown: [""],
    analysisBreakdownCases: [""],
    problemAnalysisSplit: ["employementlvl"],
    productivity: ["product"],
    productbreakdown: ["Gains"],
    pdfImage: [],
    divHeight: 0,
    pdfGenerated: false,
    draggable: false,
    key: undefined,
    dodName: "",
    dodDescription: "",
    dodList: [] as any,
    dodAdminList: [] as any,
    dodNameModal: false,
    dodLoadModal: false,
    deleteLoader: false,
    shareModal: false,
    scheduleModal: false,
    scheduleDate: "",
    scheduleValue: "",
    users: [],
    filteredUsers: [],
    sharedUsers: [],
    sharedLength: 0,
    pptLoader: false,
    dodLoader: false,
    availableHandles: ["s", "e", "w", "n", "se", "sw", "ne", "nw"],
    positions: [
      { x: 0, y: 0, w: 4, h: 3.2, maxH: 10, i: "UtilizationKPI" },
      { x: 4, y: 0, w: 8, h: 3.2, maxH: 10, i: "EngagementKPI" },
      { x: 0, y: 4, w: 16, h: 4, maxH: 10, i: "UtilizationBreakdown" },
      { x: 0, y: 8, w: 16, h: 4, maxH: 10, i: "IndividualCasesBreakdown" },
      { x: 0, y: 12, w: 16, h: 4, maxH: 10, i: "GroupParticipantsBreakdown" },
      { x: 0, y: 16, w: 16, h: 4, maxH: 10, i: "OnlineAccessBreakdown" },
      { x: 0, y: 20, w: 16, h: 4, maxH: 10, i: "SessionBreakdown" },
      { x: 0, y: 24, w: 16, h: 3.2, maxH: 10, i: "WorkHealthKPI" },
      { x: 0, y: 28, w: 16, h: 3.2, maxH: 10, i: "RiskHealthKpi" },
      { x: 0, y: 32, w: 16, h: 4, maxH: 10, i: "ProblemBreakdown" },
      { x: 0, y: 36, w: 16, h: 4, maxH: 10, i: "ProblemAssociations" },
      { x: 0, y: 40, w: 16, h: 4, maxH: 10, i: "ProblemAnalysisBreakdown" },
      { x: 0, y: 44, w: 16, h: 4, maxH: 10, i: "ProblemPrioritisation" },
      { x: 0, y: 48, w: 4, h: 4, maxH: 10, i: "ProductivitySavingsKPI" },
      { x: 4, y: 48, w: 8, h: 4, maxH: 10, i: "CaseClosureSplit" },
      { x: 0, y: 52, w: 16, h: 4, maxH: 10, i: "ProductivityBreakdown" },
      { x: 0, y: 56, w: 16, h: 3.2, maxH: 10, i: "ROIKPI" },
      { x: 0, y: 60, w: 16, h: 4, maxH: 10, i: "ProductBreakdown" },
      { x: 0, y: 64, w: 16, h: 3.2, maxH: 10, i: "QualityKPI" },
      { x: 0, y: 68, w: 16, h: 4, maxH: 10, i: "Complaints" },
      { x: 0, y: 72, w: 16, h: 4, maxH: 10, i: "Compliments" },
    ],
    customeDate: false,
  };

  removeHr = () => {
    const hr = Array.from(document.getElementsByTagName("hr"));
    hr.map((h) => {
      h.remove();
    });
  };

  convertDivToImage = async (div: HTMLElement) => {
    return await DOMToImage.toPng(div)
      .then((url: any) => {
        return url;
      })
      .catch(function(error: any) {
        console.error(error);
      });
  };
  // Function to toggle loader
  setIsLoading = (loading: boolean) => {
    this.setState({ isLoading: loading });
  };

  handlePPTGenerate = async () => {
    let pptx = new pptxgen();
    this.setState({ availableHandles: [], pptLoader: true });
    this.removeHr();
    let img: any = [];
    const parentDiv: any = document.getElementById("dod-modal");
    const header: any = document.getElementById("pdf-header");
    const footer: any = document.getElementById("footer");
    const headerImg = await this.convertDivToImage(header);
    let slide = pptx.addSlide();
    slide.addImage({ data: `${headerImg}`, x: 0.2, y: 0, w: 9.5, h: 0.8 });
    slide.addText(this.state.clientName, {
      x: 1.2,
      y: 2,
      color: "363636",
      fill: { color: "F1F1F1" },
      align: pptx.AlignH.center,
    });
    slide.addText(
      `Reporting Cycle: Start Date: ${this.state.startDate}, End Date: ${this.state.endDate}`,
      {
        x: 0.8,
        y: 3,
        color: "363636",
        fill: { color: "F1F1F1" },
        align: pptx.AlignH.center,
      }
    );
    footer.style.margin = "0px";
    const footerImg = await this.convertDivToImage(footer);
    slide.addImage({ data: `${footerImg}`, x: 0.2, y: 5, w: 9.5, h: 0.5 });
    header.innerHTML = "";
    footer.innerHTML = "";
    parentDiv.removeChild(parentDiv.firstChild);
    parentDiv.removeChild(parentDiv.firstChild);
    const dod: any = parentDiv.childNodes[0];
    const gridLayout = dod.firstChild;
    const orgArray = Array.from(gridLayout.children);
    // hide scrollbars
    const dodDashboard = [
      ...Array.from(document.getElementsByClassName("dod-scrollbar")),
    ];
    dodDashboard.forEach((dod: any) => {
      dod.style.overflow = "hidden";
    });
    // hide scrollbars for kpis
    const kpiCharts = [
      ...Array.from(document.getElementsByClassName("si-with-background")),
    ];
    kpiCharts.forEach((kpi: any) => {
      kpi.style.overflow = "hidden";
    });

    // to calculate no. of pages
    const steps = Math.ceil(this.state.divHeight / 400);
    orgArray.sort(
      (a: any, b: any) =>
        a.style.transform.split(", ")[1].split("px)")[0] -
        b.style.transform.split(", ")[1].split("px)")[0]
    );
    const children: any = [...orgArray];
    const slideWidth = 1920;
    const slideHeight = 1080;
    let skip = false;
    for (let i = 0; i <= steps; i++) {
      gridLayout.innerHTML = "";
      if (skip) {
        skip = false;
        continue;
      }
      if (children[i] && children[i + 1]) {
        const combinedWidth =
          Number(children[i].style.width.split("px")[0]) +
          Number(children[i + 1].style.width.split("px")[0]);
        if (combinedWidth <= 1800) {
          // If combined width fits in a slide
          // Set up the layout
          children[i].style.transform = `translate(0px, 0px)`;
          children[i + 1].style.transform = `translate(0px, 0px)`;
          gridLayout.style.maxHeight =
            Math.max(
              children[i].style.height.split("px")[0],
              children[i + 1].style.height.split("px")[0]
            ) + "px";
          gridLayout.style.maxWidth = slideWidth + "px";
          gridLayout.appendChild(children[i]);
          gridLayout.appendChild(children[i + 1]);
          const heightInPercfirst =
            (Math.min(780, children[i].style.height.split("px")[0]) /
              slideHeight) *
            100; // Height in Perncetage
          const widthInPercfirst =
            (children[i].style.width.split("px")[0] / slideWidth) * 100; // Width in Perncetage

          const heightInPercSecond =
            (Math.min(780, children[i + 1].style.height.split("px")[0]) /
              slideHeight) *
            100; // Height in Perncetage
          const widthInPercSecond =
            (children[i + 1].style.width.split("px")[0] / slideWidth) * 100; // Width in Perncetage

          // Add images to the slide
          const url1 = await this.convertDivToImage(children[i]);
          const url2 = await this.convertDivToImage(children[i + 1]);
          let slide = pptx.addSlide();
          slide.addImage({
            data: `${headerImg}`,
            x: 0.2,
            y: 0,
            w: 9.5,
            h: 0.8,
          });
          slide.addImage({
            data: `${url1}`,
            x: "10%",
            y: 1,
            w: `${widthInPercfirst}%`,
            h: `${heightInPercfirst}%`,
          }); // Adjust positioning and size
          slide.addImage({
            data: `${url2}`,
            x: `${widthInPercfirst + 10}%`,
            y: 1,
            w: `${widthInPercSecond}%`,
            h: `${heightInPercSecond}%`,
          }); // Adjust positioning and size
          slide.addImage({
            data: `${footerImg}`,
            x: 0.2,
            y: 5,
            w: 9.5,
            h: 0.5,
          });

          // Update state or do any necessary operations
          const img: any = [...this.state.pdfImage];
          img.push(url1, url2);
          this.setState({ pdfImage: img });
          skip = true;
          // Move to the next iteration
          continue;
        }
      }
      if (children[i]) {
        children[i].style.transform = `translate(0px, 0px)`;
        gridLayout.style.maxHeight = children[i].style.height;
        gridLayout.style.maxWidth = children[i].style.width;
        gridLayout.appendChild(children[i]);
        const heightInPerc =
          (Math.min(780, children[i].style.height.split("px")[0]) /
            slideHeight) *
          100; // Height in Perncetage
        const widthInPerc =
          (children[i].style.width.split("px")[0] / slideWidth) * 100; // Width in Perncetage
        const url = await this.convertDivToImage(children[i]);
        let slide = pptx.addSlide();
        slide.addImage({ data: `${headerImg}`, x: 0.2, y: 0, w: 9.5, h: 0.8 });
        const x = (100 - widthInPerc) / 2; // Center horizontally
        const y = (100 - heightInPerc) / 2; // Center vertically

        img = [...this.state.pdfImage];
        img.push(url);
        this.setState({ pdfImage: img });
        slide.addImage({
          data: `${url}`,
          x: `${x}%`,
          y: `${y}%`,
          w: `${widthInPerc}%`,
          h: `${heightInPerc}%`,
        });
        slide.addImage({ data: `${footerImg}`, x: 0.2, y: 5, w: 9.5, h: 0.5 });
      }
    }
    pptx.writeFile({
      fileName: `${this.state.clientName} Dashboard.pptx`,
      compression: true,
    });
    this.setState({
      pdfImage: [],
      pdfGenerated: false,
      downloadPreview: false,
      draggable: false,
      pptLoader: false,
    });
  };

  handleGenerate = async () => {
    this.setState({ availableHandles: [], pptLoader: true });
    this.removeHr();
    let img: any = [];
    const parentDiv: any = document.getElementById("dod-modal");
    const footer: any = document.getElementById("footer");
    const fClone = footer.cloneNode(true);
    footer.innerHTML = "";
    const dod: any = parentDiv.childNodes[2];
    const gridLayout = dod.firstChild;
    const orgArray = Array.from(gridLayout.children);
    // hide scrollbars
    const dodDashboard = [
      ...Array.from(document.getElementsByClassName("dod-scrollbar")),
    ];
    dodDashboard.forEach((dod: any) => {
      dod.style.overflow = "hidden";
    });
    // hide scrollbars for kpis
    const kpiCharts = [
      ...Array.from(document.getElementsByClassName("si-with-background")),
    ];
    kpiCharts.forEach((kpi: any) => {
      kpi.style.overflow = "hidden";
    });

    // to calculate no. of pages
    const steps = Math.ceil(this.state.divHeight / 1200);
    let firstIndex = 0;
    let lastIndex = 0;
    for (let i = 0; i < steps; i++) {
      // set pdf page height
      gridLayout.style.height = "1700px";
      gridLayout.innerHTML = "";
      const children: any = [...orgArray];

      // to set order of charts
      children.sort(
        (a, b) =>
          a.style.transform.split(", ")[1].split("px)")[0] -
          b.style.transform.split(", ")[1].split("px)")[0]
      );

      let height = 0;
      // find first and last index for a single page
      for (let index = firstIndex; index <= children.length; index++) {
        if (children[index] && index > 1) {
          const heightTransform = Number(
            children[index].style.transform.split(",")[1].split("px")[0]
          );
          const lastHeightTransform = Number(
            children[index - 1].style.transform.split(",")[1].split("px")[0]
          );
          if (heightTransform != lastHeightTransform)
            height += Number(children[index].style.height.split("px")[0]);
        }
        lastIndex = index;
        if (height > 1550) break;
      }
      if (i === steps - 1) lastIndex = children.length;
      else if (firstIndex == lastIndex) lastIndex += 1;

      // divide components into pages
      const splice = children.slice(firstIndex, lastIndex);

      firstIndex = lastIndex;
      let widthTransform = 10;
      let heightTransform = 10;
      let widthIndex = 0;
      splice.forEach((div: any, index) => {
        const width = Number(div.style.width.split("px")[0]);
        widthTransform = div.style.transform
          .split(",")[0]
          .split("translate(")[1];
        // to check if there are multiple components in a single row
        if (width < 1000) {
          if (
            index === 0 ||
            (index === 1 &&
              Number(splice[index - 1].style.width.split("px")[0]) < 1000)
          )
            heightTransform = 10;
          else {
            heightTransform =
              heightTransform +
              (widthIndex == 0
                ? Number(splice[index - 1].style.height.split("px")[0]) + 20
                : 0);
            const lastHeightTransform = Number(
              splice[index - 1].style.transform.split(",")[1].split("px")[0]
            );
            widthIndex =
              heightTransform !== lastHeightTransform ? widthIndex + 1 : 0;
          }
        } else {
          heightTransform =
            heightTransform +
            (index === 0
              ? 0
              : Number(splice[index - 1].style.height.split("px")[0])) +
            10;
          widthIndex = 0;
        }
        div.style.transform = `translate(${widthTransform}, ${heightTransform}px)`;
        gridLayout.appendChild(div);
      });
      // set footer at last page
      if (i == steps - 1 || lastIndex === children.length) {
        // fClone.style['margin-top'] = '150px'
        parentDiv.appendChild(fClone);
      }
      if (splice.length) {
        // convert div to pdfImage
        await DOMToImage.toPng(parentDiv)
          .then((url: any) => {
            img = [...this.state.pdfImage];
            img.push(url);
            this.setState({ pdfImage: img });
          })
          .catch(function(error: any) {
            console.error(error);
          });
      }
    }
    fClone.style["margin-top"] = "0px";
    fClone.innerHTML = "";
    this.setState({ pdfGenerated: true, pptLoader: false });
    this.forceUpdate();
  };

  handleModalCancel = (clientID, key) => {
    this.props.history.push(`/client/${clientID}/dod/${key}`);
  };

  componentDidMount(): void {
    const clientID = this.props.match.params.id;
    const key = this.props.match.params.key;
    const startDate = this.props.match.params.startDate;
    const endDate = this.props.match.params.endDate;
    this.setState({ startDate, endDate });
    ClientsRobin.when(ClientsRobin.get(`${clientID}`, `/${clientID}`))
      .then(() => {
        const clientName = (
          ClientsRobin.getResult(`${clientID}`) || { name: "" }
        ).Name;
        this.setState({ clientName });
      })
      .catch((err) => {
        notification.error({
          type: "error",
          message: "Could not load clients data !",
          description: "There was an error during the clients api execution.",
        });
      });
    siteLevelsRobin.when(siteLevelsRobin.findOne(clientID)).then(() => {
      clientSettings
        .when(clientSettings.findOne(clientID))
        .then(() => {
          DoDRobin.when(DoDRobin.get(`${key}-report`, `/report/${key}`))
            .then(() => {
              const data = DoDRobin.getResult(`${key}-report`);
              if (data) {
                this.setState({
                  key: data._key,
                  dodName: data.name,
                  dodDescription: data.description,
                  dashboard: data.dashboard,
                  dashboardComponents: data.dashboard,
                  positions: data.positions,
                  utilizationbreakdown: data.utilizationbreakdown,
                  keymetrics: data.keymetrics,
                  groupParticipants: data.groupParticipants,
                  groupParticipantsFilterOn: data.groupParticipantsFilterOn || [
                    "all",
                  ],
                  sessionBreakdownFilterOn: data.sessionBreakdownFilterOn || [
                    "all",
                  ],
                  onlineAccess: data.onlineAccess,
                  sessionBreakdown: data.sessionBreakdown,
                  problemsplit: data.problemsplit,
                  associationsplit: data.associationsplit,
                  problemAnalysisSplit: data.problemAnalysisSplit,
                  productivity: data.productivity,
                  productbreakdown: data.productbreakdown,
                  breakdownFilterOn: data.breakdownFilterOn || [""],
                  associationcases: data.associationcases || [""],
                  associationbreakdown: data.associationbreakdown || [""],
                  problems: data.problems || [],
                  problemclusters: data.problemclusters || [],
                  analysisBreakdown: data.analysisBreakdown || [
                    "problemcluster",
                  ],
                  problemAnalysisDrilldown: data.problemAnalysisDrilldown || [
                    "",
                  ],
                  analysisBreakdownCases: data.analysisBreakdownCases || [""],
                });
                this.generateReport(data.dashboard);
              }
            })
            .catch((err) => {
              notification.error({
                type: "error",
                message: "Could not load dod data !",
                description: "There was an error during the dod api execution.",
              });
            });
          const settings = clientSettings.getModel();
          if (settings[0]) {
            const reportingCycle = moment(settings[0].reportingCycle);
            if (!reportingCycle.isSame(this.state.filter, "d")) {
              this.setState({ filter: reportingCycle });
            } else
              notification.info({
                type: "info",
                message: "Reporting Period",
                description: (
                  <div>
                    <div>
                      Start Date:{" "}
                      {moment(this.state.startDate).format("YYYY-MM-DD")}
                    </div>
                    <div>
                      End Date:{" "}
                      {moment(this.state.endDate).format("YYYY-MM-DD")}
                    </div>
                  </div>
                ),
              });
          } else
            notification.info({
              type: "info",
              message: "Reporting Period",
              description: (
                <div>
                  <div>
                    Start Date:{" "}
                    {moment(this.state.startDate).format("YYYY-MM-DD")}
                  </div>
                  <div>
                    End Date: {moment(this.state.endDate).format("YYYY-MM-DD")}
                  </div>
                </div>
              ),
            });
        })
        .catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load settings data !",
            description:
              "There was an error during the settings api execution.",
          });
        });
    });
  }

  componentDidUpdate(
    prevProps: Readonly<{}>,
    prevState,
    snapshot?: undefined
  ): void {
    // if (this.state.startDate !== prevState.startDate || this.state.endDate !== prevState.endDate ) {
    //     this.generateReport(this.state.dashboardComponents)
    // }
    // if (this.props.location.search !== prevProps.location.search) {
    //     const searchParams = new URLSearchParams(this.props.location.search);
    //     if (searchParams.get('startDate') && searchParams.get('endDate')) {
    //         this.setState(
    //             {
    //                 startDate: searchParams.get('startDate'),
    //                 endDate: searchParams.get('endDate')
    //             },
    //             () => {
    //                 this.generateReport(this.state.dashboardComponents);
    //             }
    //         );
    //     }
    // }

    if (
      this.props.match.params.startDate !== prevProps.match.params.startDate ||
      this.props.match.params.endDate !== prevProps.match.params.endDate
    ) {
      this.setState(
        {
          startDate: this.props.match.params.startDate,
          endDate: this.props.match.params.endDate,
        },
        () => {
          this.generateReport(this.state.dashboardComponents);
        }
      );
    }
  }

  getFilterValue(): any {
    const searchParams = new URLSearchParams(this.props.location.search);

    const benchmark = this.state.benchmark || "Previous range";
    const dateRange = this.state.dateRange || "month";

    const settings = clientSettings.getModel();
    const dailyworkhours = settings[0] ? settings[0].avgWorkingHours : 8;
    const hourlywage = settings[0]
      ? Number(settings[0].avgEmployeeWage)
      : 227.3;
    const startDate = this.state.startDate || searchParams.get("startDate");
    const endDate = this.state.endDate || searchParams.get("endDate");
    const benchmarkStartDate = this.state.benchmarkStartDate;
    const benchmarkEndDate = this.state.benchmarkEndDate;

    const clientID = this.props.match.params.id;
    const siteMapping = siteLevelsRobin.isLoading(
      siteLevelsRobin.ACTIONS.FIND_ONE
    )
      ? []
      : (siteLevelsRobin.getModel() || []).map((s) =>
          `clients/${clientID}` === s.branchTo ? { ...s, branchTo: null } : s
        );
    let siteLevelsAssigned = false;
    siteMapping.map((site) => {
      if (site.disabled) siteLevelsAssigned = true;
    });
    const siteTreeData = ListToTree(siteMapping);
    const nodes = [];
    getTopActiveNodes(siteTreeData, nodes);
    const defaultSites = siteLevelsAssigned ? nodes : [""];
    const sitelevels = this.state.site || defaultSites;
    const benchmarkSitelevel = this.state.benchmarksitelevel || defaultSites;
    const includeDependants = this.state.includeDependants;

    const options: any = {
      startdate: startDate,
      sitelevels: Array.isArray(sitelevels)
        ? sitelevels.filter((item) => item !== "All").length === 0
          ? defaultSites
          : sitelevels.filter((item) => item !== "All")
        : sitelevels === "All"
        ? defaultSites
        : sitelevels.split(),
      enddate: endDate,
      client: this.props.match.params.id,
      dailyworkhours: dailyworkhours,
      hourlywage: hourlywage,
      dicarddepenedent: includeDependants == true ? false : true,
      benchmarksitelevels: Array.isArray(benchmarkSitelevel)
        ? benchmarkSitelevel.filter((item) => item !== "All").length === 0
          ? defaultSites
          : benchmarkSitelevel.filter((item) => item !== "All")
        : benchmarkSitelevel === "All"
        ? defaultSites
        : benchmarkSitelevel.split(),
    };
    if (benchmark === "Previous range") {
      if (dateRange === "month") {
        options.benchmarkstartdate = moment(startDate)
          .subtract(1, dateRange)
          .format("YYYY-MM-DD");
        options.benchmarkenddate = moment(endDate)
          .subtract(1, dateRange)
          .endOf("month")
          .format("YYYY-MM-DD");
      } else if (dateRange === "YTD") {
        // YTD option should use same dates of previous year for benchmark
        options.benchmarkstartdate = moment(startDate)
          .subtract(1, "year")
          .format("YYYY-MM-DD");
        options.benchmarkenddate = moment(endDate)
          .subtract(1, "year")
          .format("YYYY-MM-DD");
      } else {
        options.benchmarkstartdate = (moment(startDate) as any)
          .subtract(1, dateRange)
          .format("YYYY-MM-DD");
        options.benchmarkenddate = (moment(endDate) as any)
          .subtract(1, dateRange)
          .format("YYYY-MM-DD");
      }
    } else if (benchmark === "custom") {
      options.benchmarkstartdate = moment(benchmarkStartDate).format(
        "YYYY-MM-DD"
      );
      options.benchmarkenddate = moment(benchmarkEndDate).format("YYYY-MM-DD");
    } else {
      options.benchmark = benchmark.split(",");
    }
    return options;
  }

  generateReport = async (dashboard: string[]) => {
    // to reset positions and dashboards components when you select dashboards out of order
    const options = this.getFilterValue();
    const clientID = this.props.match.params.id;
    this.setState({ dashboardComponents: dashboard });
    const tasks: (() => Promise<any>)[] = []; // Array of API call tasks
    //Engagement Dashboard APIs
    if (dashboard.indexOf("engagement_kpi") >= 0) {
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(
            `${clientID}-EngagementKPI`,
            "/engagement/keymetrics",
            options
          )
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load engagement keymetrics data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(
            `${clientID}-UtilizationKPI`,
            "/engagement/utilization",
            options
          )
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load engagement utilisation data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    }
    if (dashboard.indexOf("utilization_breakdown") >= 0)
      this.state.utilizationbreakdown.map((_breakdown, i) => {
        tasks.push(() =>
          AnalyticsRobin.when(
            AnalyticsRobin.post(
              `${clientID}-UtilizationBreakdown-${i}`,
              "/engagement/utilization",
              {
                ...options,
                split: this.state.utilizationbreakdown[i] || "product",
              }
            )
          ).catch((err) => {
            notification.error({
              type: "error",
              message: "Could not load utilisation breakdown data !",
              description:
                "There was an error during the analytics api execution.",
            });
          })
        );
      });
    if (dashboard.indexOf("individual_cases_breakdown") >= 0)
      this.state.keymetrics.map((_metric, i) => {
        tasks.push(() =>
          AnalyticsRobin.when(
            AnalyticsRobin.post(
              `${clientID}-IndividualCasesBreakdown-${i}`,
              "/engagement/keymetrics",
              { ...options, split: this.state.keymetrics[i] || "service" }
            )
          ).catch((err) => {
            notification.error({
              type: "error",
              message: "Could not load individual cases breakdown data !",
              description:
                "There was an error during the analytics api execution.",
            });
          })
        );
      });

    if (dashboard.indexOf("group_participants_breakdown") >= 0)
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post("products_breakdown", "/engagement/keymetrics", {
            ...options,
            split: "product",
          })
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load products breakdown data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    tasks.push(() =>
      AnalyticsRobin.when(
        AnalyticsRobin.post(
          "problem_cluster_breakdown",
          "/engagement/keymetrics",
          { ...options, split: "problemcluster" }
        )
      ).catch((err) => {
        notification.error({
          type: "error",
          message: "Could not load problem cluster breakdown data !",
          description: "There was an error during the analytics api execution.",
        });
      })
    );
    this.state.groupParticipants.map((_participants, i) => {
      const additionalFilters: any = {};
      if (this.state.groupParticipantsFilterOn[i] !== "all") {
        if (_participants === "problem")
          additionalFilters.problemcluster = this.state.groupParticipantsFilterOn[
            i
          ];
        else
          additionalFilters.product = this.state.groupParticipantsFilterOn[i];
      }
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(
            `${clientID}-GroupParticipantsBreakdown-${i}`,
            "/engagement/keymetrics",
            {
              ...options,
              ...additionalFilters,
              split: this.state.groupParticipants[i] || "service",
            }
          )
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load group participants breakdown data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    });
    if (dashboard.indexOf("online_access_breakdown") >= 0)
      this.state.onlineAccess.map((_access, i) => {
        tasks.push(() =>
          AnalyticsRobin.when(
            AnalyticsRobin.post(
              `${clientID}-OnlineAccessBreakdown-${i}`,
              "/engagement/keymetrics",
              { ...options, split: this.state.onlineAccess[i] || "service" }
            )
          ).catch((err) => {
            notification.error({
              type: "error",
              message: "Could not load group participants breakdown data !",
              description:
                "There was an error during the analytics api execution.",
            });
          })
        );
      });
    if (dashboard.indexOf("sessions_breakdown") >= 0)
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(
            "sessions_breakdown_products",
            "/engagement/sessions",
            { ...options, split: "product" }
          )
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load sessions breakdown data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    this.state.sessionBreakdown.map((_session, i) => {
      const additionalFilters: any = {};
      if (this.state.sessionBreakdownFilterOn[i] !== "all") {
        additionalFilters.product = this.state.sessionBreakdownFilterOn[i];
      }
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(
            `${clientID}-SessionBreakdown-${i}`,
            "/engagement/sessions",
            {
              ...options,
              ...additionalFilters,
              split: this.state.sessionBreakdown[i] || "product",
            }
          )
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load sessions breakdown data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    });
    //Health Dashboard APIs
    if (
      dashboard.indexOf("work_health_kpi") >= 0 ||
      dashboard.indexOf("risk_health_kpi") >= 0
    )
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(
            `${clientID}-HealthKPI`,
            "/health/keymetrics",
            options
          )
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load health keymetrics data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    if (dashboard.indexOf("problem_breakdown") >= 0)
      this.state.problemsplit.map((_breakdown, i) => {
        tasks.push(() =>
          AnalyticsRobin.when(
            AnalyticsRobin.post(
              `${clientID}-ProblemBreakdown-${i}`,
              "/health/breakdown",
              {
                ...options,
                casetype: this.state.breakdownFilterOn[i] || "",
                split: this.state.problemsplit[i],
              }
            )
          ).catch((err) => {
            notification.error({
              type: "error",
              message: "Could not load problem breakdown data !",
              description:
                "There was an error during the analytics api execution.",
            });
          })
        );
      });
    if (dashboard.indexOf("problems_association_breakdown") >= 0)
      this.fetchProblemList();
    this.fetchProblemClusters();
    this.state.associationsplit.map((_breakdown, i) => {
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(
            `${clientID}-ProblemAssociations-${i}`,
            "/health/associations",
            {
              ...options,
              association: this.state.associationbreakdown[i],
              casetype: this.state.associationcases[i],
              split: this.state.associationsplit[i],
            }
          )
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load health associations data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    });
    if (dashboard.indexOf("problem_analysis_breakdown") >= 0)
      this.fetchProblemList();
    this.fetchProblemClusters();
    this.state.problemAnalysisSplit.map((_breakdown, i) => {
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(
            `${clientID}-ProblemAnalysisBreakdown-${i}`,
            "/health/breakdown",
            {
              ...options,
              problemlevel: this.state.analysisBreakdown[i],
              drilldown: this.state.problemAnalysisDrilldown[i],
              casetype: this.state.analysisBreakdownCases[i],
              split: this.state.problemAnalysisSplit[i],
            }
          )
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load health breakdown data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    });
    if (dashboard.indexOf("problem_prioritisation") >= 0)
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(
            `${clientID}-ProblemPrioritisation`,
            "/health/prioritization",
            { ...options, split: "problem" }
          )
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load health prioritization drilldown data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    //Effectiveness Dashboard APIs
    if (dashboard.indexOf("productivity_savings") >= 0) {
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(
            `${clientID}-ProductivitySavingsKPI`,
            "/effectiveness/productivity",
            options
          )
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load effectiveness keymetrics data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(
            `${clientID}-CaseClosureSplit`,
            "/effectiveness/closure",
            { ...options, split: "status" }
          )
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load effectiveness closure data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    }
    if (dashboard.indexOf("productivity_breakdown") >= 0)
      this.state.productivity.map((_breakdown, i) => {
        tasks.push(() =>
          AnalyticsRobin.when(
            AnalyticsRobin.post(
              `${clientID}-ProductivityBreakdown-${i}`,
              "/effectiveness/productivity",
              { ...options, split: this.state.productivity[i] }
            )
          ).catch((err) => {
            notification.error({
              type: "error",
              message: "Could not load effectiveness productivity data !",
              description:
                "There was an error during the analytics api execution.",
            });
          })
        );
      });
    //ROI Dashboard APIs
    if (dashboard.indexOf("roi_kpi") >= 0)
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(`${clientID}-ROIKPI`, "/roi/keymetrics", {
            ...options,
          })
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load roi keymetrics data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    if (dashboard.indexOf("product_breakdown") >= 0)
      this.state.productbreakdown.map((_breakdown, i) => {
        tasks.push(() =>
          AnalyticsRobin.when(
            AnalyticsRobin.post(
              `${clientID}-ProductBreakdown-${i}`,
              "/roi/keymetrics",
              { ...options, split: "product" }
            )
          ).catch((err) => {
            notification.error({
              type: "error",
              message: "Could not load roi products data !",
              description:
                "There was an error during the analytics api execution.",
            });
          })
        );
      });
    //Quality Dashboard APIs
    if (dashboard.indexOf("quality_kpi") >= 0)
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(
            `${clientID}-QualityKPI`,
            "/quality/sentiments",
            options
          )
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load quality keymetrics data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    if (dashboard.indexOf("compliments") >= 0)
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(`${clientID}-Compliments`, "/quality/wordcloud", {
            wordcategory: "compliments",
            ...options,
          })
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load quality wordcloud data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );
    if (dashboard.indexOf("complaints") >= 0)
      tasks.push(() =>
        AnalyticsRobin.when(
          AnalyticsRobin.post(`${clientID}-Complaints`, "/quality/wordcloud", {
            wordcategory: "complaints",
            ...options,
          })
        ).catch((err) => {
          notification.error({
            type: "error",
            message: "Could not load quality wordcloud data !",
            description:
              "There was an error during the analytics api execution.",
          });
        })
      );

    // Process tasks with concurrency control
    const CONCURRENCY_LIMIT = 3; // Adjust based on server capacity
    await this.runWithConcurrency(tasks, CONCURRENCY_LIMIT);
  };
  // Helper function to control concurrency
  runWithConcurrency = async (
    tasks: (() => Promise<any>)[],
    concurrencyLimit: number
  ) => {
    this.setIsLoading(true);
    const executing: Promise<void>[] = [];

    for (const task of tasks) {
      const promise = task().finally(() => {
        executing.splice(executing.indexOf(promise), 1);
      });
      executing.push(promise);

      if (executing.length >= concurrencyLimit) {
        await Promise.race(executing);
      }
    }

    await Promise.all(executing);
    this.setIsLoading(false);
  };
  fetchProblemList = () => {
    const options = this.getFilterValue();
    AnalyticsRobin.when(
      AnalyticsRobin.post("problem_list", "/health/breakdown", {
        ...options,
        split: "problem",
      })
    )
      .then((key) => {
        const problemList = AnalyticsRobin.getResult(key).data || [];
        this.setState({
          problems: problemList.map((d) => {
            return { value: d.key, text: [d.key] };
          }),
        });
      })
      .catch((err) => {
        notification.error({
          type: "error",
          message: "Could not load problem list data !",
          description: "There was an error during the analytics api execution.",
        });
      });
  };
  fetchProblemClusters = () => {
    const options = this.getFilterValue();
    AnalyticsRobin.when(
      AnalyticsRobin.post("problem_cluster_list", "/health/breakdown", {
        ...options,
        split: "problemcluster",
      })
    )
      .then((key) => {
        const problemList = AnalyticsRobin.getResult(key).data || [];
        let associationsBreakdown = [];
        let problemAnalysisDrilldown = [];
        if (problemList.length > 0) {
          associationsBreakdown = [problemList[0].key];
          problemAnalysisDrilldown = [problemList[0].key];
        }
        this.setState({
          problemclusters: problemList.map((d) => {
            return { value: d.key, text: [d.key] };
          }),
          associationbreakdown: associationsBreakdown,
          problemAnalysisDrilldown: problemAnalysisDrilldown,
        });
      })
      .catch((err) => {
        notification.error({
          type: "error",
          message: "Could not load problem cluster list data !",
          description: "There was an error during the analytics api execution.",
        });
      });
  };
  customBencharkCancel = (e) => {
    this.setState({ customBenchmark: false });
  };
  sitelevelBencharkCancel = (e) => {
    this.setState({ sitelevelBenchmark: false, benchmarksitelevel: ["All"] });
  };
  calculateHeight = () => {
    const div: any = document.getElementById("dod-modal");
    const divHeight = document.getElementById("dod-modal")
      ? (document.getElementById("dod-modal") as any).clientHeight
      : 0;
    for (let i = 0; i < divHeight / 1600; i++) {
      const hr = document.createElement("hr");
      hr.style.position = "absolute";
      hr.style.top = `${(i === 0 ? 2100 : 1800) * (i + 1)}px`;
      hr.style.width = "100%";
      hr.style.left = "0";
      div.appendChild(hr);
    }
    this.setState({ divHeight: divHeight });
  };
  onLayoutChange = (event: any) => {
    this.calculateHeight();
    const positions = [...this.state.positions];
    const updatedPositions = positions.map((p) => {
      const found = event.find((e) => e.i.startsWith(p.i));
      if (found)
        return { ...p, h: found.h, w: found.w, x: found.x, y: found.y };
      else return p;
    });
    this.setState({ positions: updatedPositions });
  };
  PDFDocument = () => (
    <Document>
      {this.state.pdfImage.map((img) => {
        return (
          <Page>
            <Image src={img} />
          </Page>
        );
      })}
    </Document>
  );

  dashboardRef = (React as any).createRef();
  render() {
    const { pdfGenerated, pptLoader, startDate, endDate } = this.state;
    const clientID = this.props.match.params.id;
    const key = this.props.match.params.key;
    const clientName = (ClientsRobin.getResult(`${clientID}`) || { name: "" })
      .Name;
    const compareBenchmark = this.state.compareBenchmark;
    let benchmark = this.state.benchmark;
    const benchmarkMapping = {
      "Previous range": "Previous period",
      industry: "Sector",
      SegmentSize: "Size",
      clients: "Company",
      custom: "Custom range",
      site: "Site levels",
    };
    const casesMapping = {
      "": "All Cases",
      riskcases: "Risk Cases",
      formalreferals: "Formal Referrals",
      severecases: "Severe Cases",
      significantcases: "Significant Cases",
      aid: "AID Cases",
    };

    const splitMapping = {
      employementlvl: "Employment Level",
      gender: "Gender",
      age: "Age",
    };

    //Engagement Dashboards
    const engagement_kpi = [
      <div
        key={`UtilizationKPI`}
        data-grid={this.state.positions.find((p) => p.i === "UtilizationKPI")}
      >
        <Dashboard className="dod-scrollbar">
          <EngagementAdjustedKPI
            key={`${clientID}-UtilizationKPI`}
            apiKey={`${clientID}-UtilizationKPI`}
            client={clientName}
            displayRelativeValues={false}
            compareBenchmark={compareBenchmark}
            benchmarkMapping={benchmarkMapping}
          />
        </Dashboard>
      </div>,
      <div
        key={`EngagementKPI`}
        data-grid={this.state.positions.find((p) => p.i === "EngagementKPI")}
      >
        <Dashboard className="dod-scrollbar">
          <KeyMetricsKPI
            key={`${clientID}-EngagementKPI`}
            apiKey={`${clientID}-EngagementKPI`}
            client={clientName}
            displayRelativeValues={false}
            compareBenchmark={compareBenchmark}
            benchmarkMapping={benchmarkMapping}
          />
        </Dashboard>
      </div>,
    ];

    const utilisation_breakdown: any = [];
    this.state.utilizationbreakdown.map((_breakdown, index) => {
      utilisation_breakdown.push(
        <div
          key={`UtilizationBreakdown-${index}`}
          data-grid={this.state.positions.find(
            (p) => p.i === "UtilizationBreakdown"
          )}
          style={{ paddingBottom: "30px" }}
        >
          <Dashboard
            key={index}
            listIndex={index}
            graphLength={this.state.utilizationbreakdown.length}
            downloadPreview={this.state.downloadPreview}
            filter="utilizationbreakdown"
            className="dod-dashboard dod-scrollbar"
            name="Utilization Breakdown"
          >
            <UtilisationBreakdown
              apiKey={`${clientID}-UtilizationBreakdown-${index}`}
              client={clientName}
              displayRelativeValues={false}
              compareBenchmark={compareBenchmark}
              downloadPreview={this.state.downloadPreview}
              benchmark={benchmark}
              benchmarkMapping={benchmarkMapping}
              filter={this.state.utilizationbreakdown[index]}
              showLabel={true}
              splitFilter={(selected) => {
                const keys: any = [...this.state.utilizationbreakdown];
                keys[index] = selected;
                this.setState({ utilizationbreakdown: keys });
              }}
            />
          </Dashboard>
        </div>
      );
    });

    const individual_cases_breakdown: any = [];
    this.state.keymetrics.map((_metric, indCases) => {
      individual_cases_breakdown.push(
        <div
          key={`IndividualCasesBreakdown-${indCases}`}
          data-grid={this.state.positions.find(
            (p) => p.i === "IndividualCasesBreakdown"
          )}
          style={{ paddingBottom: "30px" }}
        >
          <Dashboard
            key={indCases}
            listIndex={indCases}
            graphLength={this.state.keymetrics.length}
            downloadPreview={this.state.downloadPreview}
            filter="keymetrics"
            className="dod-dashboard dod-scrollbar"
            name="Individual Cases Breakdown"
          >
            <IndividualCasesBreakdown
              apiKey={`${clientID}-IndividualCasesBreakdown-${indCases}`}
              client={clientName}
              displayRelativeValues={false}
              compareBenchmark={compareBenchmark}
              benchmark={benchmark}
              benchmarkMapping={benchmarkMapping}
              downloadPreview={this.state.downloadPreview}
              filter={this.state.keymetrics[indCases]}
              showLabel={true}
              splitFilter={(selected) => {
                const keys: any = [...this.state.keymetrics];
                keys[indCases] = selected;
                this.setState({ keymetrics: keys });
              }}
            />
          </Dashboard>
        </div>
      );
    });

    const group_participants_breakdown: any = [];
    this.state.groupParticipants.map((_participants, index) => {
      group_participants_breakdown.push(
        <div
          key={`GroupParticipantsBreakdown-${index}`}
          data-grid={this.state.positions.find(
            (p) => p.i === "GroupParticipantsBreakdown"
          )}
          style={{ paddingBottom: "30px" }}
        >
          <Dashboard
            key={index}
            listIndex={index}
            graphLength={this.state.groupParticipants.length}
            downloadPreview={this.state.downloadPreview}
            filter="groupParticipants"
            className="dod-dashboard dod-scrollbar"
            name="Group Participants Breakdown"
          >
            <GroupParticipantsBreakdown
              apiKey={`${clientID}-GroupParticipantsBreakdown-${index}`}
              client={clientName}
              displayRelativeValues={false}
              compareBenchmark={compareBenchmark}
              benchmark={benchmark}
              benchmarkMapping={benchmarkMapping}
              downloadPreview={this.state.downloadPreview}
              filter={this.state.groupParticipants[index]}
              filterValue={this.state.groupParticipantsFilterOn[index]}
              showLabel={true}
              splitFilter={(selected) => {
                const keys: any = [...this.state.groupParticipants];
                keys[index] = selected;
                this.setState({ groupParticipants: keys });
              }}
              filterOn={(selected) => {
                const keys: any = [...this.state.groupParticipantsFilterOn];
                keys[index] = selected;
                this.setState({ groupParticipantsFilterOn: keys });
              }}
            />
          </Dashboard>
        </div>
      );
    });

    const online_access_breakdown: any = [];
    this.state.onlineAccess.map((_access, index) => {
      online_access_breakdown.push(
        <div
          key={`OnlineAccessBreakdown-${index}`}
          data-grid={this.state.positions.find(
            (p) => p.i === "OnlineAccessBreakdown"
          )}
          style={{ paddingBottom: "30px" }}
        >
          <Dashboard
            key={index}
            listIndex={index}
            graphLength={this.state.onlineAccess.length}
            downloadPreview={this.state.downloadPreview}
            filter="onlineAccess"
            className="dod-dashboard dod-scrollbar"
            name="Online Access Breakdown"
          >
            <OnlineAccessBreakdown
              apiKey={`${clientID}-OnlineAccessBreakdown-${index}`}
              client={clientName}
              displayRelativeValues={false}
              compareBenchmark={compareBenchmark}
              benchmark={benchmark}
              benchmarkMapping={benchmarkMapping}
              downloadPreview={this.state.downloadPreview}
              filter={this.state.onlineAccess[index]}
              showLabel={true}
              splitFilter={(selected) => {
                const keys: any = [...this.state.onlineAccess];
                keys[index] = selected;
                this.setState({ onlineAccess: keys });
              }}
            />
          </Dashboard>
        </div>
      );
    });

    const session_breakdown: any = [];
    this.state.sessionBreakdown.map((_session, index) => {
      session_breakdown.push(
        <div
          key={`SessionBreakdown-${index}`}
          data-grid={this.state.positions.find(
            (p) => p.i === "SessionBreakdown"
          )}
          style={{ paddingBottom: "30px" }}
        >
          <Dashboard
            key={index}
            listIndex={index}
            graphLength={this.state.sessionBreakdown.length}
            downloadPreview={this.state.downloadPreview}
            filter="sessionBreakdown"
            className="dod-dashboard dod-scrollbar"
            name="Session Breakdown"
          >
            <SessionsBreakdown
              apiKey={`${clientID}-SessionBreakdown-${index}`}
              client={clientName}
              displayRelativeValues={false}
              compareBenchmark={compareBenchmark}
              benchmark={benchmark}
              downloadPreview={this.state.downloadPreview}
              benchmarkMapping={benchmarkMapping}
              filter={this.state.sessionBreakdown[index]}
              filterValue={this.state.sessionBreakdownFilterOn[index]}
              showLabel={true}
              splitFilter={(selected) => {
                const keys: any = [...this.state.sessionBreakdown];
                keys[index] = selected;
                this.setState({ sessionBreakdown: keys });
              }}
              filterOn={(selected) => {
                const keys: any = [...this.state.sessionBreakdownFilterOn];
                keys[index] = selected;
                this.setState({ sessionBreakdownFilterOn: keys });
              }}
            />
          </Dashboard>
        </div>
      );
    });

    // Health Dashboards

    const work_health_kpi = (
      <div
        key={`WorkHealthKPI`}
        data-grid={this.state.positions.find((p) => p.i === "WorkHealthKPI")}
      >
        <Dashboard className="dod-scrollbar">
          <WorkImpactScoreKPI
            apiKey={`${clientID}-HealthKPI`}
            client={clientName}
            displayRelativeValues={false}
            compareBenchmark={compareBenchmark}
            benchmarkMapping={benchmarkMapping}
          />
        </Dashboard>
      </div>
    );

    const risk_health_kpi = (
      <div
        key={`RiskHealthKpi`}
        data-grid={this.state.positions.find((p) => p.i === "RiskHealthKpi")}
      >
        <Dashboard className="dod-scrollbar">
          <HighRiskFactorKPI
            apiKey={`${clientID}-HealthKPI`}
            client={clientName}
            displayRelativeValues={false}
            compareBenchmark={compareBenchmark}
            benchmarkMapping={benchmarkMapping}
          />
        </Dashboard>
      </div>
    );

    const problem_breakdown: any = [];
    this.state.problemsplit.map((_breakdown, index) => {
      problem_breakdown.push(
        <div
          key={`ProblemBreakdown-${index}`}
          data-grid={this.state.positions.find(
            (p) => p.i === "ProblemBreakdown"
          )}
          style={{ paddingBottom: "30px" }}
        >
          <Dashboard
            key={index}
            listIndex={index}
            graphLength={this.state.problemsplit.length}
            downloadPreview={this.state.downloadPreview}
            filter="problemsplit"
            className="dod-dashboard dod-scrollbar"
            name="Problem Breakdown"
          >
            <ProblemBreakdown
              apiKey={`${clientID}-ProblemBreakdown-${index}`}
              client={clientName}
              displayRelativeValues={false}
              compareBenchmark={compareBenchmark}
              benchmark={benchmark}
              downloadPreview={this.state.downloadPreview}
              benchmarkMapping={benchmarkMapping}
              splitBy={this.state.problemsplit[index]}
              casesMapping={casesMapping}
              cases={this.state.breakdownFilterOn[index]}
              showLabel={true}
              splitFilter={(selected) => {
                const keys: any = [...this.state.problemsplit];
                keys[index] = selected;
                this.setState({ problemsplit: keys });
              }}
              casesFilter={(selected) => {
                const keys: any = [...this.state.breakdownFilterOn];
                keys[index] = selected;
                this.setState({ breakdownFilterOn: keys });
              }}
            />
          </Dashboard>
        </div>
      );
    });

    const problems_association_breakdown: any = [];
    this.state.associationsplit.map((_breakdown, index) => {
      problems_association_breakdown.push(
        <div
          key={`ProblemAssociations-${index}`}
          data-grid={this.state.positions.find(
            (p) => p.i === "ProblemAssociations"
          )}
          style={{ paddingBottom: "30px" }}
        >
          <Dashboard
            key={index}
            listIndex={index}
            graphLength={this.state.associationsplit.length}
            downloadPreview={this.state.downloadPreview}
            filter="associationsplit"
            className="dod-dashboard dod-scrollbar"
            name="Problem Association Breakdown"
          >
            <ProblemAssociations
              apiKey={`${clientID}-ProblemAssociations-${index}`}
              client={clientName}
              displayRelativeValues={false}
              splitBy={this.state.associationsplit[index]}
              downloadPreview={this.state.downloadPreview}
              casesMapping={casesMapping}
              cases={this.state.associationcases[index]}
              showLabel={true}
              associations={
                this.state.associationsplit[index] === "problemcluster"
                  ? this.state.problemclusters
                  : this.state.problems
              }
              problembreakdown={this.state.associationbreakdown[index]}
              splitFilter={(selected) => {
                const keys: any = [...this.state.associationsplit];
                keys[index] = selected;
                const breakdownKeys: any = [...this.state.associationbreakdown];
                breakdownKeys[index] =
                  selected === "problemcluster"
                    ? (this.state.problemclusters[0] as any).value
                    : (this.state.problems[0] as any).value;
                this.setState({
                  associationsplit: keys,
                  associationbreakdown: breakdownKeys,
                });
              }}
              casesFilter={(selected) => {
                const keys: any = [...this.state.associationcases];
                keys[index] = selected;
                this.setState({ associationcases: keys });
              }}
              associationsFilter={(selected) => {
                const keys: any = [...this.state.associationbreakdown];
                keys[index] = selected;
                this.setState({ associationbreakdown: keys });
              }}
            />
          </Dashboard>
        </div>
      );
    });

    const problem_analysis_breakdown: any = [];
    this.state.problemAnalysisSplit.map((_breakdown, index) => {
      problem_analysis_breakdown.push(
        <div
          key={`ProblemAnalysisBreakdown-${index}`}
          data-grid={this.state.positions.find(
            (p) => p.i === "ProblemAnalysisBreakdown"
          )}
          style={{ paddingBottom: "30px" }}
        >
          <Dashboard
            key={index}
            listIndex={index}
            graphLength={this.state.problemAnalysisSplit.length}
            downloadPreview={this.state.downloadPreview}
            filter="problemAnalysisSplit"
            className="dod-dashboard dod-scrollbar"
            name="Problem Analysis Breakdown"
          >
            <ProblemAnalysisBreakdown
              apiKey={`${clientID}-ProblemAnalysisBreakdown-${index}`}
              client={clientName}
              displayRelativeValues={false}
              compareBenchmark={compareBenchmark}
              benchmark={benchmark}
              benchmarkMapping={benchmarkMapping}
              downloadPreview={this.state.downloadPreview}
              casesMapping={casesMapping}
              splitMapping={splitMapping}
              problemLevel={this.state.analysisBreakdown[index]}
              problemDrillDown={this.state.problemAnalysisDrilldown[index]}
              drillDown={
                this.state.analysisBreakdown[index] === "problemcluster"
                  ? this.state.problemclusters
                  : this.state.problems
              }
              cases={this.state.analysisBreakdownCases[index]}
              splitBy={this.state.problemAnalysisSplit[index]}
              showLabel={true}
              problemLevelFilter={(selected) => {
                const analysisKeys: any = [...this.state.analysisBreakdown];
                analysisKeys[index] = selected;
                const problemAnalysisKeys: any = [
                  ...this.state.problemAnalysisDrilldown,
                ];
                problemAnalysisKeys[index] =
                  selected === "problemcluster"
                    ? (this.state.problemclusters[0] as any).value
                    : (this.state.problems[0] as any).value;
                this.setState({
                  analysisBreakdown: analysisKeys,
                  problemAnalysisDrilldown: problemAnalysisKeys,
                });
              }}
              drillDownFilter={(selected) => {
                const keys: any = [...this.state.problemAnalysisDrilldown];
                keys[index] = selected;
                this.setState({ problemAnalysisDrilldown: keys });
              }}
              casesFilter={(selected) => {
                const keys: any = [...this.state.analysisBreakdownCases];
                keys[index] = selected;
                this.setState({ analysisBreakdownCases: keys });
              }}
              splitFilter={(selected) => {
                const keys: any = [...this.state.problemAnalysisSplit];
                keys[index] = selected;
                this.setState({ problemAnalysisSplit: keys });
              }}
            />
          </Dashboard>
        </div>
      );
    });

    const problem_prioritisation = (
      <div
        key={`ProblemPrioritisation`}
        data-grid={this.state.positions.find(
          (p) => p.i === "ProblemPrioritisation"
        )}
        style={{ paddingBottom: "30px" }}
      >
        <Dashboard
          className="dod-dashboard dod-scrollbar"
          name="Problem Prioritisation"
        >
          <ProblemPrioritisation
            apiKey={`${clientID}-ProblemPrioritisation`}
            client={clientName}
            benchmark={benchmark}
            benchmarkMapping={benchmarkMapping}
          />
        </Dashboard>
      </div>
    );

    // Effectiveness Dashboard
    const productivity_savings = [
      <div
        key={`ProductivitySavingsKPI`}
        data-grid={this.state.positions.find(
          (p) => p.i === "ProductivitySavingsKPI"
        )}
      >
        <Dashboard className="dod-scrollbar">
          <ProductivitySavingsKPI
            apiKey={`${clientID}-ProductivitySavingsKPI`}
            client={clientName}
            displayRelativeValues={false}
            compareBenchmark={compareBenchmark}
            benchmarkMapping={benchmarkMapping}
          />
        </Dashboard>
      </div>,
      <div
        key={`CaseClosureSplit`}
        data-grid={this.state.positions.find((p) => p.i === "CaseClosureSplit")}
      >
        <Dashboard className="dod-scrollbar">
          <CaseClosureSplit
            apiKey={`${clientID}-CaseClosureSplit`}
            client={clientName}
            showLabel={true}
            displayRelativeValues={false}
            compareBenchmark={compareBenchmark}
            benchmark={benchmark}
            benchmarkMapping={benchmarkMapping}
          />
        </Dashboard>
      </div>,
    ];

    const productivity_breakdown: any = [];
    this.state.productivity.map((_breakdown, index) => {
      productivity_breakdown.push(
        <div
          key={`ProductivityBreakdown-${index}`}
          data-grid={this.state.positions.find(
            (p) => p.i === "ProductivityBreakdown"
          )}
          style={{ paddingBottom: "30px" }}
        >
          <ProductivityBreakdown
            dod={{
              listIndex: index,
              graphLength: this.state.productivity.length,
              filter: "productivity",
              downloadPreview: this.state.downloadPreview,
            }}
            apiKey={`${clientID}-ProductivityBreakdown-${index}`}
            client={clientName}
            displayRelativeValues={false}
            compareBenchmark={compareBenchmark}
            benchmark={benchmark}
            benchmarkMapping={benchmarkMapping}
            splitBy={this.state.productivity[index]}
            showLabel={true}
            splitFilter={(selected) => {
              const keys: any = [...this.state.productivity];
              keys[index] = selected;
              this.setState({ productivity: keys });
            }}
          />
        </div>
      );
    });

    //ROI Dashboard
    const roi_kpi = (
      <div
        key={`ROIKPI`}
        data-grid={this.state.positions.find((p) => p.i === "ROIKPI")}
        style={{ paddingBottom: "30px" }}
      >
        <ROIMetricsKPI
          apiKey={`${clientID}-ROIKPI`}
          client={clientName}
          compareBenchmark={compareBenchmark}
          benchmarkMapping={benchmarkMapping}
        />
      </div>
    );

    const product_breakdown: any = [];
    this.state.productbreakdown.map((_breakdown, index) => {
      product_breakdown.push(
        <div
          key={`ProductBreakdown-${index}`}
          data-grid={this.state.positions.find(
            (p) => p.i === "ProductBreakdown"
          )}
          style={{ paddingBottom: "30px" }}
        >
          <ProductBreakdown
            dod={{
              listIndex: index,
              graphLength: this.state.productbreakdown.length,
              filter: "productbreakdown",
              downloadPreview: this.state.downloadPreview,
            }}
            apiKey={`${clientID}-ProductBreakdown-${index}`}
            client={clientName}
            compareBenchmark={compareBenchmark}
            benchmark={benchmark}
            benchmarkMapping={benchmarkMapping}
            showLabel={true}
            productBreakdown={this.state.productbreakdown[index]}
            splitFilter={(selected) => {
              const keys: any = [...this.state.productbreakdown];
              keys[index] = selected;
              this.setState({ productbreakdown: keys });
            }}
          />
        </div>
      );
    });

    //Quality Dashboard
    const quality_kpi = (
      <div
        key={`QualityKPI`}
        data-grid={this.state.positions.find((p) => p.i === "QualityKPI")}
        style={{ paddingBottom: "30px" }}
      >
        <FeedbackQualityMetric
          apiKey={`${clientID}-QualityKPI`}
          client={clientName}
          compareBenchmark={compareBenchmark}
          benchmarkMapping={benchmarkMapping}
        />
      </div>
    );
    const dashboard_on_demand =
      this.state.dashboardComponents.length === 0 ? (
        <div style={{ width: "100%", marginTop: "45px", textAlign: "center" }}>
          <img src="/src/assets/img/dod_image.png" style={{ height: "70vh" }} />
        </div>
      ) : (
        <div
          ref={this.dashboardRef}
          id="dod"
          style={{ width: "100%", marginTop: "70px", marginBottom: "50px" }}
        >
          <ResponsiveGridLayout
            className="layout"
            breakpoints={{ lg: 1200 }}
            cols={{ lg: 12, md: 4, sm: 3, xs: 2, xxs: 1 }}
            rowHeight={100}
            resizeHandles={this.state.availableHandles}
            onLayoutChange={this.onLayoutChange}
            isDraggable={this.state.draggable}
            compactType={"vertical"}
          >
            {this.state.dashboardComponents.indexOf("engagement_kpi") >= 0
              ? engagement_kpi
              : null}
            {this.state.dashboardComponents.indexOf("utilization_breakdown") >=
            0
              ? utilisation_breakdown
              : null}
            {this.state.dashboardComponents.indexOf(
              "individual_cases_breakdown"
            ) >= 0
              ? individual_cases_breakdown
              : null}
            {this.state.dashboardComponents.indexOf(
              "group_participants_breakdown"
            ) >= 0
              ? group_participants_breakdown
              : null}
            {this.state.dashboardComponents.indexOf(
              "online_access_breakdown"
            ) >= 0
              ? online_access_breakdown
              : null}
            {this.state.dashboardComponents.indexOf("sessions_breakdown") >= 0
              ? session_breakdown
              : null}
            {/* {this.state.dashboardComponents.indexOf('health_kpi') >= 0 ? health_kpi : null} */}
            {this.state.dashboardComponents.indexOf("work_health_kpi") >= 0
              ? work_health_kpi
              : null}
            {this.state.dashboardComponents.indexOf("risk_health_kpi") >= 0
              ? risk_health_kpi
              : null}
            {this.state.dashboardComponents.indexOf("problem_breakdown") >= 0
              ? problem_breakdown
              : null}
            {this.state.dashboardComponents.indexOf(
              "problems_association_breakdown"
            ) >= 0
              ? problems_association_breakdown
              : null}
            {this.state.dashboardComponents.indexOf(
              "problem_analysis_breakdown"
            ) >= 0
              ? problem_analysis_breakdown
              : null}
            {this.state.dashboardComponents.indexOf("problem_prioritisation") >=
            0
              ? problem_prioritisation
              : null}
            {this.state.dashboardComponents.indexOf("productivity_savings") >= 0
              ? productivity_savings
              : null}
            {this.state.dashboardComponents.indexOf("productivity_breakdown") >=
            0
              ? productivity_breakdown
              : null}
            {this.state.dashboardComponents.indexOf("roi_kpi") >= 0
              ? roi_kpi
              : null}
            {this.state.dashboardComponents.indexOf("product_breakdown") >= 0
              ? product_breakdown
              : null}
            {this.state.dashboardComponents.indexOf("quality_kpi") >= 0
              ? quality_kpi
              : null}
            {/* {this.state.dashboardComponents.indexOf('complaints') >= 0 ? complaints : null} */}
            {/* {this.state.dashboardComponents.indexOf('compliments') >= 0 ? compliments : null} */}
          </ResponsiveGridLayout>
        </div>
      );

    return (
      <>
        <Modal
          title={
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <div>Dashboard Report</div>
              <div>
                <Button
                  onClick={() => {
                    const start = moment()
                      .startOf("month")
                      .subtract(1, "month")
                      .format("YYYY-MM-DD");
                    const end = moment()
                      .subtract(1, "month")
                      .endOf("month")
                      .format("YYYY-MM-DD");
                    this.props.history.push(
                      `/dod/${clientID}/report/${key}/${start}/${end}`
                    );
                  }}
                >
                  Past Month
                </Button>
                <Button
                  style={{ marginLeft: 10 }}
                  onClick={() => {
                    const start = moment()
                      .startOf("month")
                      .subtract(3, "month")
                      .format("YYYY-MM-DD");
                    const end = moment()
                      .subtract(1, "month")
                      .endOf("month")
                      .format("YYYY-MM-DD");
                    this.props.history.push(
                      `/dod/${clientID}/report/${key}/${start}/${end}`
                    );
                  }}
                >
                  Past 3 month
                </Button>
                <Button
                  style={{ marginLeft: 10 }}
                  onClick={() => {
                    const start = moment()
                      .startOf("month")
                      .subtract(6, "month")
                      .format("YYYY-MM-DD");
                    const end = moment()
                      .subtract(1, "month")
                      .endOf("month")
                      .format("YYYY-MM-DD");
                    this.props.history.push(
                      `/dod/${clientID}/report/${key}/${start}/${end}`
                    );
                  }}
                >
                  Past 6 month
                </Button>
                <Button
                  style={{ marginLeft: 10 }}
                  onClick={() => {
                    const start = moment()
                      .startOf("year")
                      .subtract(1, "year")
                      .format("YYYY-MM-DD");
                    const end = moment()
                      .subtract(1, "year")
                      .endOf("year")
                      .format("YYYY-MM-DD");
                    this.props.history.push(
                      `/dod/${clientID}/report/${key}/${start}/${end}`
                    );
                  }}
                >
                  Past Year
                </Button>
                <Button
                  style={{ marginLeft: 10 }}
                  onClick={() => {
                    this.setState({ customeDate: true });
                  }}
                >
                  Custom
                </Button>
              </div>
              <div style={{ marginRight: "3rem" }}>
                <Button
                  onClick={() => window.location.reload()}
                  style={{ marginRight: 10 }}
                >
                  Refresh
                </Button>
                {!pdfGenerated && (
                  <Button
                    loading={pptLoader}
                    style={{ marginRight: 10 }}
                    onClick={this.handlePPTGenerate}
                  >
                    Generate Power Point
                  </Button>
                )}
                {!this.state.pdfImage.length ? (
                  <Button
                    loading={pptLoader}
                    style={{ marginRight: 10 }}
                    onClick={this.handleGenerate}
                  >
                    Generate PDF
                  </Button>
                ) : null}
                <Button
                  onClick={() => {
                    this.props.history.push(`/client/${clientID}/dod/${key}`);
                  }}
                >
                  Advance Filters
                </Button>
              </div>
            </div>
          }
          style={{ backgroundColor: "white" }}
          visible={true}
          footer={null}
          destroyOnClose
          onCancel={() => this.handleModalCancel(clientID, key)}
          width={1500}
        >
          {pdfGenerated ? (
            <div style={{ textAlign: "center" }}>
              <PDFDownloadLink
                document={<this.PDFDocument />}
                fileName={`Dashboard-Report.pdf`}
              >
                {({ loading }) =>
                  loading ? (
                    "Loading document..."
                  ) : (
                    <Button style={{ marginLeft: "110px" }} size="large">
                      Download Now!
                    </Button>
                  )
                }
              </PDFDownloadLink>
            </div>
          ) : (
            <div id="dod-modal" style={{ padding: "30px 0px 0px 30px" }}>
              <div id="pdf-header">
                <div className="pdf-header">
                  <img src="/src/assets/img/lyra-white.png" height={80} />
                </div>
              </div>
              <div
                id="reporting-cycle"
                style={{
                  textAlign: "center",
                  fontSize: 20,
                  position: "relative",
                  top: 40,
                }}
              >
                Dashboard Reporting Cycle: Start Date: {startDate}, End Date:{" "}
                {endDate}
              </div>
              <Spin
                spinning={this.state.isLoading}
                tip="Please wait while your report is being generated. It can take upto few minutes"
              >
                {dashboard_on_demand}
              </Spin>
              <div
                id="footer"
                style={{
                  display: "flex",
                  margin: "10px",
                  alignItems: "center",
                }}
              >
                <img
                  src={"/src/assets/img/lyra-logo.png"}
                  alt="Lyra SA Logo.png"
                  style={{ height: "30px" }}
                  className="si-logo"
                />
                <p style={{ marginLeft: "10px" }}>
                  <b style={{ fontWeight: "bold" }}>
                    Confidentiality Disclaimer:
                  </b>
                  <br />
                  This document may contain confidential and/or privileged
                  material. This information is solely for the use of the
                  individual or entity for whom it was intended. If you are not
                  the intended recipient you may not peruse, use, disseminate,
                  distribute or copy this document or any information contained
                  thereto without breaching its confidentiality and you are
                  therefore requested to delete the original document
                  immediately as any further action or reaction thereon is
                  prohibited.
                </p>
              </div>
            </div>
          )}
        </Modal>
        <Modal
          className="si-date-picker-custom"
          title={
            <span style={{ fontSize: "20px", fontWeight: 300 }}>
              Custom range
            </span>
          }
          width={400}
          visible={this.state.customeDate}
          onOk={() => {
            this.props.history.push(
              `/dod/${clientID}/report/${key}/${this.state.startDate}/${this.state.endDate}`
            );
            this.setState({ customeDate: false });
          }}
          onCancel={() => this.setState({ customeDate: false })}
        >
          <div className="si-date-picker-modal-body">
            <div className="si-date-picker-row-from">
              <span style={{ width: 46 }}>From: </span>
              <DatePicker
                value={moment(this.state.startDate)}
                allowClear={false}
                onChange={(date) =>
                  this.setState({
                    startDate: date.startOf("day").format("YYYY-MM-DD"),
                  })
                }
              />
            </div>
            <div className="si-date-picker-row-to">
              <span style={{ width: 46 }}>To: </span>
              <DatePicker
                value={moment(this.state.endDate)}
                allowClear={false}
                onChange={(date) =>
                  this.setState({
                    endDate: date.endOf("day").format("YYYY-MM-DD"),
                  })
                }
              />
            </div>
          </div>
        </Modal>
      </>
    );
  }
}

export default withRouter(DodReport);
