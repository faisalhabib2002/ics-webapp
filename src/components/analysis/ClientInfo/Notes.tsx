import * as React from 'react'
import * as moment from 'moment'
import {Card, Button, Select, ProfilePicture, Loader} from '@simplus/siui'
import Divider from 'antd/lib/divider'
import notification from 'antd/lib/notification'
import Icon from 'antd/lib/icon'
import Popconfirm from 'antd/lib/popconfirm'
import {connectRobin} from '@simplus/robin-react'
import {robins} from '../../../robins'
import * as queryString from 'query-string'
import omit from 'lodash/omit'
import values from 'lodash/values'
import defaults from 'lodash/defaults'
import { DateRangeFilter} from '../ClientInfo/DashboardContent/utils'
import {hasPermission, noPermission} from '../../../utils'
import {ErrorBoundary} from '../../../utils/ErrorBoundary'
import { RouteComponentProps  } from 'react-router-dom';
import {NewNote} from './section'
const Option = Select.Option;

const {UsersRobin, AuthRobin, StoriesRobin, PermissionsRobin} = robins;

@connectRobin([UsersRobin, AuthRobin, StoriesRobin, PermissionsRobin])
export class Notes extends React.Component<RouteComponentProps<{id: string}>> {
	state = {redirect: false, visible: false, redirectPath: '', note: {}, edit: false, filter: moment('2018-01-01'), loading: false}
	formRef: any = null
	user = AuthRobin.getUserInfo()
	showModal = () => {
		this.setState({ note: {}, visible: true });
	}
	handleCancel = () => {
		this.setState({ visible: false });
	}
	createNotification = (type, title, msg) => {
		notification[type]({
			message: title,
			description: msg,
		});
	}
	handleCreate = () => {
		const form = this.formRef.props.form;
		form.validateFields((err, values) => {
			if (err) {
				return;
			}
			if (!err) {
				this.setState({ loading: true, visible: false });
				const id = values[`_key`];
				if (id) {
					let final_values = {...values, id , date: moment().toISOString()}
					final_values = omit(final_values, 'id' , '_key')
					StoriesRobin.when(StoriesRobin.update(id, {
						client: this.props.match.params.id,
						...final_values
					} as any))
					.then(() => { this.createNotification('success', 'Success!', 'Note successfully updated'); this.setState({ loading: false }); this.applyFilters()})
					.catch(() => { this.createNotification('error', 'Error!', 'There was an error when updating note'); this.setState({ loading: false }); })
				} else {
					const final_values = {...values, user: this.user._id, client: this.props.match.params.id, date: moment().toISOString()}
					StoriesRobin.when(StoriesRobin.create(final_values))
					.then(() => { this.createNotification('success', 'Success!', 'Note successfully created'); this.setState({ loading: false }); this.applyFilters()})
					.catch(() => { this.createNotification('error', 'Error!', 'There was an error when creating note'); this.setState({ loading: false }); })
				}
				form.resetFields();
			}
		});
	}
	handleDelete = (key: string) => {
		this.setState({loading: true})
		StoriesRobin.when(StoriesRobin.delete(key, `/${key}`, {
			params: {
				client: this.props.match.params.id
			}
		}) as any)
		.then(() => { this.createNotification('success', 'Success!', 'Note successfully deleted'); this.setState({ loading: false }); this.applyFilters()})
		.catch(() => { this.createNotification('error', 'Error!', 'There was an error when deleting note'); this.setState({ loading: false }); })
	}
	saveFormRef = (formRef) => {
		this.formRef = formRef;
	}
	unlisten?: () => void = undefined
	componentWillMount(): void {
		this.applyFilters()
		this.unlisten = this.props.history.listen((location, action) => {
			if (action === 'POP')
				this.applyFilters()
		});
	}
	applyFilters(): void {
		const client = this.props.match.params.id

		const startdate = queryString.parse(location.hash.split('?')[1]).startDate || moment().startOf('year').toISOString();
		const enddate = queryString.parse(location.hash.split('?')[1]).endDate || moment().toISOString();

		const options = {startdate, enddate, client}
		StoriesRobin.when(StoriesRobin.find({...options})).then((data) => {
			const stories_data = StoriesRobin.getResult(data)
			const all_users: string[] = []
			stories_data.map( note => {
				if (note.user && (all_users.indexOf(note.user) < 0))
					all_users.push(note.user)
			})
			if (all_users.length > 0)
				UsersRobin.get(`${this.props.match.params.id}-stories`, `/list/${JSON.stringify(all_users)}`)
		})

	}
	pageRedirect(update: any, refrsh: boolean = false): void {
		const state = {
			...this.urlState(),
			...update}
		this.props.history.push(`${this.props.location.pathname}?${queryString.stringify(state)}`)
		if (refrsh)
			this.applyFilters()
	}
	urlState(): any {
		return queryString.parse(location.hash.split('?')[1])
	}

	componentWillUnmount(): void {
		if (this.unlisten)
			this.unlisten();
	}
	render(): JSX.Element {
		const clientID = this.props.match.params.id
		const { dateRange, writer } = defaults(this.urlState(), {
			dateRange: 'YTD',
			writer: 'All'
		})
		const users =  UsersRobin.getResult(`${clientID}-stories`);
		const stories_data = StoriesRobin.getCollection()

		const NotesPage = <ErrorBoundary>
			<div className='stories'>
				<div className='stories-top-bar'>
					<h1 className='analysis-tab-title'>Notes</h1>
					{hasPermission('/view/notes/add-note', PermissionsRobin.getResult('own-permissions')) ?
					<Button className='new-story' onClick={this.showModal}>New Note</Button>
					: null}
				</div>
				<NewNote
					{...this.state.note}
					wrappedComponentRef={this.saveFormRef}
					visible={this.state.visible}
					onCancel={this.handleCancel}
					onCreate={this.handleCreate}/>
				<div className='stories-filters'>
					<DateRangeFilter
						value={dateRange}
						customOption
						label='DATE RANGE'
						style={{minWidth: 130, marginRight: '1rem'}}
							onChange={(selected) => this.pageRedirect({
								dateRange: selected.value,
								startDate: moment(selected.startDate).startOf('day').toISOString(),
								endDate: moment(selected.endDate).endOf('day').toISOString(),
								writer: 'All'
							}, true)
						}/>
					<Select
						value={writer}
						label='WRITTEN BY'
						showSearch
						style={{ minWidth: 150 }}
						optionFilterProp='children'
						filterOption={(input, option) => option.props.children!.toString().toLowerCase().indexOf(input.toLowerCase()) >= 0}
						onChange={(selected) => this.pageRedirect({writer: selected}, true)}>
						<Option value='All'>All users</Option>
						{stories_data.length > 0 ?
						values(users).map(user => <Option key={user._id} value={user._id}>{user.name}</Option>)
						: null
					}
					</Select>
				</div>
				<div style={{position: 'relative'}}>
					{(StoriesRobin.isLoading(StoriesRobin.ACTIONS.FIND) || StoriesRobin.isError(StoriesRobin.ACTIONS.FIND) || this.state.loading) ? <Loader error={StoriesRobin.isError(StoriesRobin.ACTIONS.FIND)}/> : null}
					<div className='stories-content'>
					{
						stories_data.length > 0 ?
						stories_data.filter(item => writer === 'All' ? true : item.user === writer).map(note => {
							const user_data = (users && users[note.user]) ? users[note.user] : {picture: '/src/assets/img/sample-profile.png', name: ''}

							// To handle when user info is set, but picture is not. Show default picture
							if (!user_data.picture || user_data.picture === 'null')
								user_data.picture = '/src/assets/img/sample-profile.png'

							return <Card className='stories-card' key={note._key}>
								<div style={{display: 'flex', justifyContent: 'space-between'}}>
									<div className='stories-title'>{note.title}</div>
									{(note.user === this.user._id) ? <div>
										{hasPermission('/view/notes/edit-note', PermissionsRobin.getResult('own-permissions')) ?
											<Icon
												style={{fontSize: '2rem', marginRight: '1rem', color: '#0090cd', cursor: 'pointer'}}
												type='edit'
												onClick= {() => this.setState({note, visible: true})}
											/> : null
										}
										{hasPermission('/view/notes/delete-note', PermissionsRobin.getResult('own-permissions')) ?
										<Popconfirm title='Are you sure you want to delete this note?' onConfirm={() => this.handleDelete(note._key)} okText='Yes' cancelText='No'>
											<Icon
												style={{fontSize: '2rem', color: '#BB4C49', cursor: 'pointer'}}
												type='close-circle-o'
											/>
										</Popconfirm> : null
										}
									</div>
									: null}
								</div>
								<Divider />
								<div className='stories-text'>{note.text}</div>
								<Divider />
								<div className='stories-footer'>
									<ProfilePicture url={user_data.picture} size={50} rounded/>
									<div className='stories-user-info'>
										<div className='stories-user-name'>{user_data.name}</div>
										<div className='stories-date'>on {moment(note.date).format('MMMM-DD-YYYY')}</div>
									</div>
								</div>
							</Card>
						}) :
						<Card className='stories-card'>No notes to display</Card>
					}
				</div>
				</div>
			</div>
		</ErrorBoundary>
		return(
			hasPermission('/view/notes/view-note', PermissionsRobin.getResult('own-permissions')) ? NotesPage : noPermission(PermissionsRobin.isLoading('own-permissions'))
		)
	}

}
export default Notes;