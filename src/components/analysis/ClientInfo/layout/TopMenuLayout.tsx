import * as React from 'react';

import {Link, NavLink} from 'react-router-dom';
import {connectRobin} from '@simplus/robin-react'
import {robins} from '../../../../robins'
import {withRouter, RouteComponentProps} from 'react-router-dom'
import {HorizontalNavBar, HorizontalNavBarWrapper, HorizontalNavBarItem, Profile} from '@simplus/siui';
import { hasPermission } from '../../../../utils';
import {ErrorBoundary} from '../../../../utils/ErrorBoundary'
import {Breadcrumbs} from 'src/components/breadcrumbs'
import { Dropdown, Icon, Menu, message } from 'antd';
import {resetCookieConsentValue} from "react-cookie-consent";
const {PermissionsRobin, AuthRobin, ClientsRobin} = robins;
const Item = HorizontalNavBarItem;

const { SubMenu } = Menu;

@connectRobin([PermissionsRobin, AuthRobin, ClientsRobin])
class TopMenu extends React.Component<RouteComponentProps<{id: string}>> {
	state = {
		collapsed: false,
	  };
	
	  toggleCollapsed = () => {
		this.setState({
		  collapsed: !this.state.collapsed,
		});
	  };

	  componentDidMount(): void {
		ClientsRobin.when(ClientsRobin.find({})).catch( err => {
			message.error('Could not fetch client data !')
		});
	}
	render(): JSX.Element {
		let menu
		const pageLink = this.props.location.pathname.split('/')
		if(pageLink[1] === 'client')
			menu = <div style={{display: 'flex'}}>
				{hasPermission('/view/programme-info/client', PermissionsRobin.getResult('own-permissions')) || hasPermission('/view/programme-info', PermissionsRobin.getResult('own-permissions')) ?
				<NavLink activeClassName='active' to={`/client/${this.props.match.params.id}/programmeinfo`}><Item>PROGRAMME INFO</Item></NavLink>
				: null }
				<NavLink activeClassName='active' to={`/client/${this.props.match.params.id}/dashboard/`}><Item>DASHBOARD</Item></NavLink>
				{hasPermission('/view/prioritization', PermissionsRobin.getResult('own-permissions')) ?
				<NavLink activeClassName='active' to={`/client/${this.props.match.params.id}/prioritisation`}><Item>PRIORITISATION</Item></NavLink>
				: null }
				{/* {hasPermission('/view/notes/view-note', PermissionsRobin.getResult('own-permissions')) ?
				<NavLink activeClassName='active' to={`/client/${this.props.match.params.id}/notes`}><Item>NOTES</Item></NavLink>
				: null } */}
				{hasPermission('/view/dashboard-on-demand', PermissionsRobin.getResult('own-permissions')) ?
				<NavLink activeClassName='active' to={`/client/${this.props.match.params.id}/dod`}><Item>Dashboard on Demand</Item></NavLink>
				: null }
			</div>
			
		else
			menu = <div style={{display: 'flex'}}>
			{hasPermission('/view/cross-client-analysis', PermissionsRobin.getResult('own-permissions'))?
			<NavLink activeClassName='active' to={`/cross-client-analysis`}><Item>Cross client analysis</Item></NavLink>
			: null }
			{hasPermission('/view/clients-prioritization', PermissionsRobin.getResult('own-permissions')) ?
			<NavLink activeClassName='active' to={`/client-prioritisation`}><Item>Client Prioritisation</Item></NavLink>
			: null }
			</div>
		
		const user = AuthRobin.getUserInfo()
		const clients_data = ClientsRobin.getCollection() || [];
		if (!user) {
			return <div></div>
		}
		const clients = Array.isArray(user.client) ? user.client : (user.client || '').split(',')

		const picture = user.picture === 'null' ? null : user.picture
		const rightMenu = (
			<Menu style={{width: '200px'}}>
				<Menu.Item disabled>
					<p style = {{float: 'left', paddingLeft: '10px', paddingTop: '10px'}}>{user.name}</p>
				</Menu.Item>
				{hasPermission('/view/user/view-profile', PermissionsRobin.getResult('own-permissions')) ?
				<Menu.Item>
					{/* <Link to='/user'> */}
					<a href='#/user'>
						<Icon type='user' /> &nbsp; &nbsp; My Profile &nbsp; &nbsp;
						</a>
					{/* </Link> */}
				</Menu.Item>
				: null }
				{(hasPermission('/view/usermanagement/navigate/users', PermissionsRobin.getResult('own-permissions')) || hasPermission('/view/usermanagement/navigate/roles', PermissionsRobin.getResult('own-permissions'))) ?
				<Menu.Item>
					<a href='/security'>
						<Icon type='usergroup-add' /> &nbsp; &nbsp; User Management &nbsp; &nbsp;
					</a>
				</Menu.Item>
				: null}
				{hasPermission('/view/settings/view-settings', PermissionsRobin.getResult('own-permissions')) ?
				<Menu.Item>
					<Link to='/settings'>
						<Icon type='setting'/> &nbsp; &nbsp; Settings &nbsp; &nbsp;
					</Link>
				</Menu.Item>
				: null }
				{clients.length > 1 ?
				<SubMenu
				key="companies"
				title={
					<span>
					<Icon type="retweet" /> &nbsp; &nbsp; Companies
					</span>
				}
				>
					{clients.map(client => {
						const c = clients_data.find(item => item._key === client)
						return <Menu.Item key={client}>
							<Link to={`/client/${client}/`}>{(c||{Name: ''}).Name}</Link>
							</Menu.Item>
					})}
				</SubMenu>
				: null}
				<Menu.Item>
					<div onClick={() => {
						resetCookieConsentValue()
						AuthRobin.when(AuthRobin.logout()).then(() => {
						(this.props as any).history.push('/logout')
						location.reload();
					})
				}}>
						<Icon type='logout'/> &nbsp; &nbsp; Log-Out &nbsp; &nbsp;
					</div>
				</Menu.Item>
			</Menu>
			);
		
		
		return <ErrorBoundary>
				<HorizontalNavBarWrapper
				style={{flex : '1 1 auto'}}
				flexContainer
				menu={<HorizontalNavBar>
					{/*hasPermission('/view/icas-summary/view-summary', PermissionsRobin.getResult('own-permissions')) ?
					<NavLink activeClassName='active' to={`/client-prioritisation/${this.props.match.params.id}/summary`}><Item>Lyra SA SUMMARY</Item></NavLink>
					: null*/}
					<div style={{display: 'flex'}}>
						<div style={{display: 'flex', alignItems: 'center', marginLeft: '25px', marginRight: '25px'}}>
							<img height={40} src='/src/assets/img/lyra-white.png' />
						</div>
					{menu}
					</div>
					<div style={{marginRight: '25px'}}>
						<Dropdown overlay={rightMenu}>
							<a className='ant-dropdown-link'>
							<Profile
								picture={{ rounded : true, url : picture || '/src/assets/img/sample-profile.png', size : 45}}
								name=''
								role=''/>
							</a>
						</Dropdown>
					</div>
				</HorizontalNavBar>}
			>
				<Breadcrumbs />
				<div style={{marginTop: '40px'}}>{this.props.children}</div>
			</HorizontalNavBarWrapper>
			
		</ErrorBoundary>
	}
}
export const TopMenuLayout = withRouter<RouteComponentProps<{id: string}>>(
	TopMenu
)