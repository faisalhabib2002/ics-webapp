import * as React from 'react'
import {
	TableChart
} from 'src/components/dashboard'
import { Download } from '../DashboardContent/utils'
import {connectRobin} from '@simplus/robin-react'
import * as moment from 'moment'
import {robins} from 'src/robins'
import get from 'lodash/get'
import Modal from 'antd/lib/modal'
import {ErrorBoundary} from 'src/utils/ErrorBoundary'

const {AnalyticsRobin} = robins;

interface Props {
	loading: boolean,
	metric: string | undefined,
	client: string,
	department: string,
	startDate: string,
	endDate: string,
	dateRange: string,
	history: any,
	location: any,
	match: any
}

@connectRobin([AnalyticsRobin])
export class DrilldownTable extends React.Component<Props> {

	render(): JSX.Element {
		const drillDown_data = get(AnalyticsRobin.getResult('prioritization_drill_down'), 'data', []);
		const drillDown_result = AnalyticsRobin.getResult('prioritization_drill_down')

		const metric_mapping = {
			engagement: 'Engagement',
			workimpact: 'Work impact',
			productivity : 'Productivity'
		}

		let unit = ''
		let factor = 1
		if (this.props.metric === 'engagement' || this.props.metric === 'productivity') {
			unit = '%'
			factor = 100
		}

		const drilldownTable = <TableChart
			loading={this.props.loading}
			columns={[
				{Header : 'Description', accessor: 'key'},
				{Header : metric_mapping[this.props.metric as string], accessor: 'current'},
				{Header : 'Percentage Change', accessor: 'delta'},
			]}
			data={drillDown_data.map(item => {
				return {...item, current: `${(Math.round(item.current * factor * 100) / 100)}${unit}`, delta: `${(Math.round(item.delta * factor * 100) / 100)}${unit}`}
			})}
		/>
		const drilldown_excel_columns = [
			'Company',
			'Department',
			'Range',
			'Description',
			metric_mapping[this.props.metric as string],
			'Percentage Change'
		]

		const drilldown_excel_data = drillDown_data.map(item => {
			return {
				'client': this.props.client,
				'department': this.props.department,
				'range': `${drillDown_result.startdate} to ${drillDown_result.enddate}`,
				'key': item.key,
				'value': `${(Math.round(item.current * factor * 100) / 100)}${unit}`,
				'delta': `${(Math.round(item.delta * factor * 100) / 100)}${unit}`
			}
		})
		return <ErrorBoundary>
			<Modal
				visible={!!this.props.metric}
				title={<div style={{display: 'flex', justifyContent: 'space-between', marginRight: '1rem'}}>
					<span>Attribution analysis ({this.props.department})</span>
					{/* <Download
					columns={drilldown_excel_columns}
					data={drilldown_excel_data}
					chart={drilldownTable}
					name={`Priotitisation Drilldown`}/> */}
					</div>}
				footer={null}
				onCancel={() => {
					this.props.history.push(`${this.props.location.pathname.replace(`/${this.props.match.params.department}/${this.props.match.params.key}`, '')}?dateRange=${encodeURI(this.props.dateRange)}&endDate=${encodeURI(moment(this.props.endDate).format('YYYY-MM-DD'))}&startDate=${encodeURI(moment(this.props.startDate).format('YYYY-MM-DD'))}`)
				}}
			>
				{drilldownTable}
			</Modal>
		</ErrorBoundary>
	}
}