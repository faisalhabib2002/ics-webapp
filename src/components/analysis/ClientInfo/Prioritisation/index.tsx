import * as React from 'react'
import * as moment from 'moment'

import {Loader} from '@simplus/siui'
import {DashboardFilter} from 'src/components/dashboard'
import defaults from 'lodash/defaults'
import get from 'lodash/get'
import * as queryString from 'query-string'
import { RouteComponentProps } from 'react-router-dom';
import {connectRobin} from '@simplus/robin-react'
import {robins} from 'src/robins'
import Divider from 'antd/lib/divider'
import notification from 'antd/lib/notification'
import {hasPermission, noPermission} from 'src/utils'
import {ErrorBoundary} from 'src/utils/ErrorBoundary'
import { ListToTree, DateRangeFilter, Download, percentage, numberFormater, getTopActiveNodes} from '../DashboardContent/utils'
import {PrioritisationTable} from './PrioritisationTable'
import {DrilldownTable} from './DrilldownTable'
const {clientSettings, AnalyticsRobin, ClientsRobin, siteLevelsRobin, PermissionsRobin} = robins;

@connectRobin([clientSettings, AnalyticsRobin, ClientsRobin, siteLevelsRobin, PermissionsRobin])
export class Prioritisation extends React.Component<RouteComponentProps<{id: string, department?: string, key?: string}>> {
	state = {redirect: false, redirectPath: '', financialYearStart: moment('2018-01-01'), noSettingFlag: false, loading: false}
	unlisten?: () => void = undefined
	componentWillMount(): void {
		this.setState({loading: true})
		ClientsRobin.get(`${this.props.match.params.id}`, `/${this.props.match.params.id}`)
		siteLevelsRobin.when(siteLevelsRobin.findOne(this.props.match.params.id)).then(() => {
			clientSettings.when(clientSettings.findOne(this.props.match.params.id)).then(() => {
				const settings = clientSettings.getModel();
				if (settings[0]) {
					const reportingCycle = moment(settings[0].reportingCycle)
					if (!reportingCycle.isSame(this.state.financialYearStart, 'd')) {
						this.setState({financialYearStart: reportingCycle})
					}
				}
				this.applyFilters()
			}).catch( err => {
				notification.error({type: 'error', message: 'Could not load settings data !', description: 'There was an error during the settings api execution.'})
			})
			this.unlisten = this.props.history.listen((location, action) => {
				if (action === 'POP')
					this.applyFilters()
			});
		})
	}

	applyFilters(): void {
		const dateRange = queryString.parse(location.hash.split('?')[1]).dateRange || 'month'


		const startDate = queryString.parse(location.hash.split('?')[1]).startDate || moment().startOf('month').subtract(1, dateRange).format('YYYY-MM-DD');
		const endDate = queryString.parse(location.hash.split('?')[1]).endDate || moment().subtract(1, dateRange).endOf(dateRange).format('YYYY-MM-DD');
		
		const clientID = this.props.match.params.id
		const siteMapping = siteLevelsRobin.isLoading(siteLevelsRobin.ACTIONS.FIND_ONE) ? [] : (siteLevelsRobin.getModel() || []).map(s => `clients/${clientID}` === s.branchTo ? ({...s, branchTo: null}) : s )
		let siteLevelsAssigned = false;
		siteMapping.map(site => {if(site.disabled) siteLevelsAssigned = true })
		const siteTreeData = ListToTree(siteMapping)
		const nodes = [];
		getTopActiveNodes(siteTreeData, nodes)

		const defaultSites = siteLevelsAssigned ? nodes : ['']
		const sitelevels = queryString.parse(location.hash.split('?')[1]).site || defaultSites


		const options: any = {
			startdate : startDate,
			enddate : endDate,
			client : this.props.match.params.id,
			sitelevels: Array.isArray(sitelevels) ? sitelevels.filter(item => item !== 'All') : sitelevels === 'All' ? defaultSites : sitelevels.split(),
			dailyworkhours: 8,
			split : 'sitelevel',
			benchmarkstartdate: moment(startDate).subtract(1, dateRange).format('YYYY-MM-DD'),
			benchmarkenddate: moment(endDate).subtract(1, dateRange).format('YYYY-MM-DD')
		}

		// YTD option should use same dates of previous year for benchmark
		if (dateRange === 'YTD') {
			options.benchmarkstartdate = moment(startDate).subtract(1, 'year').format('YYYY-MM-DD')
			options.benchmarkenddate = moment(endDate).subtract(1, 'year').format('YYYY-MM-DD')
		}

		this.setState({loading: false})
		AnalyticsRobin.when(AnalyticsRobin.post('prioritization_table', '/attribution/summary',  {...options})).catch( err => {
			notification.error({type: 'error', message: 'Could not load attribution summary data !', description: 'There was an error during the attribution summary api execution.'})
		})
	}

	componentWillUnmount(): void {
		if (this.unlisten)
			this.unlisten();
	}

	pageRedirect(update: any, refrsh: boolean = false): void {
		const state = {
			...this.urlState(),
			...update}
		this.props.history.push(`${this.props.location.pathname}?${queryString.stringify(state)}`)
		if (refrsh)
			this.applyFilters()
	}

	urlState(): any {
		return queryString.parse(location.hash.split('?')[1])
	}

	render(): JSX.Element {
		const clientID = this.props.match.params.id
		const clientName = (ClientsRobin.getResult(`${clientID}`) || {name: ''}).Name

		const result = AnalyticsRobin.getResult('prioritization_table') || {data: []}
		const clientInfo = ClientsRobin.getResult(`${this.props.match.params.department}`) || {Name: ''}
		const { dateRange, site, startDate, endDate} = defaults(this.urlState(), {
			dateRange : 'month',
			startDate : moment().startOf('month').subtract(1, 'month').format('YYYY-MM-DD'),
			endDate : moment().subtract(1, 'month').endOf('month').format('YYYY-MM-DD'),
			site: 'All',
		})
		// Temp solution to redirect with site
		let sites = '';
		const test_sites = Array.isArray(site) ? site : site.split()
		test_sites.forEach(item => sites += `&site=${item}`);
		const data = get(AnalyticsRobin.getResult('prioritization_table'), 'data', []);
		const max_engagement = Math.max.apply(Math, data.map(function(o) { return o.engagment}))
		const min_engagement = Math.min.apply(Math, data.map(function(o) { return o.engagment}))
		const max_workimpact = Math.max.apply(Math, data.map(function(o) { return o.workimpact; }))
		const min_workimpact = Math.min.apply(Math, data.map(function(o) { return o.workimpact; }))
		const max_productivity = Math.max.apply(Math, data.map(function(o) { return o.productivity; }))
		const min_productivity = Math.min.apply(Math, data.map(function(o) { return o.productivity; }))

		const excel_columns = [
			'Company',
			'Department',
			'Range',
			'Engagement',
			'Work Impact Score',
			'Productivity Savings'
		]

		const excel_data = data.map(item => {
			return {
				'client': clientName,
				'department': item.name,
				'range': `${result.startdate} to ${result.enddate}`,
				'engagement': percentage(item.engagment * 100),
				'workImpact': numberFormater(item.workimpact),
				'productivity': percentage(item.productivity * 100)
			}
		})


		const siteMapping = siteLevelsRobin.isLoading(siteLevelsRobin.ACTIONS.FIND_ONE) ? [] : (siteLevelsRobin.getModel() || []).map(s => `clients/${clientID}` === s.branchTo ? ({...s, branchTo: null}) : s )
		const treeData = ListToTree(siteMapping)


		const prioritisationPage = <ErrorBoundary>
			<div className='proritisation'>
			<DrilldownTable
				client={clientName}
				department={clientInfo.Name}
				loading = {AnalyticsRobin.isLoading('prioritization_drill_down')}
				metric = {this.props.match.params.key}
				startDate={startDate}
				endDate={endDate}
				dateRange={dateRange}
				history={this.props.history}
				location={this.props.location}
				match={this.props.match}
			/>
			<h1 className='analysis-tab-title'>Prioritisation</h1>
				<div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
					<div className='proritisation-filters' style={{position: 'relative'}}>
					{(clientSettings.isLoading(clientSettings.ACTIONS.FIND_ONE) || clientSettings.isError(clientSettings.ACTIONS.FIND_ONE)) ? <Loader error={clientSettings.isError(clientSettings.ACTIONS.FIND_ONE)}/> : null}
						<DateRangeFilter
							value={dateRange}
							customOption
							financialStartYear={moment(this.state.financialYearStart)}
							label='DATE RANGE'
							style={{marginRight: '1rem', minWidth: 130}}
							onChange={(selected) => this.pageRedirect({
								dateRange: selected.value,
								startDate: moment(selected.startDate).format('YYYY-MM-DD'),
								endDate: moment(selected.endDate).format('YYYY-MM-DD')
							}, true)
							}>
						</DateRangeFilter>
						<DashboardFilter
							width={200}
							type='tree'
							title='SITE'
							defaultValue={site}
							style={{marginRight: '1rem'}}
							treeData={treeData}
							onChange={(selected) => this.pageRedirect({site: selected}, true)
						}/>
					</div>
					<Download
						columns={excel_columns}
						data={excel_data}
						chart={<PrioritisationTable 
							id={this.props.match.params.id}
							pathname={this.props.location.pathname}
							financialYearStart = {this.state.financialYearStart}
							loading = {AnalyticsRobin.isLoading('prioritization_table')}
							data = {data || []}
							downloadable
							max_engagement = {max_engagement}
							min_engagement = {min_engagement}
							max_workimpact= {max_workimpact}
							min_workimpact= {min_workimpact}
							max_productivity= {max_productivity}
							min_productivity= {min_productivity}
							dateRange= {dateRange}
							startDate= {startDate}
							endDate= {endDate}
						/>}
						name={`Priotitisation - ${clientName}`}/>
				</div>
				<PrioritisationTable
					id={this.props.match.params.id}
					pathname={this.props.location.pathname}
					loading = {AnalyticsRobin.isLoading('prioritization_table')}
					data = {data || []}
					financialYearStart = {this.state.financialYearStart}
					max_engagement = {max_engagement}
					min_engagement = {min_engagement}
					max_workimpact= {max_workimpact}
					min_workimpact= {min_workimpact}
					max_productivity= {max_productivity}
					min_productivity= {min_productivity}
					dateRange= {dateRange}
					startDate= {startDate}
					endDate= {endDate}
				/>
			</div>
		</ErrorBoundary>
		return(
			hasPermission('/view/prioritization', PermissionsRobin.getResult('own-permissions')) ? prioritisationPage : noPermission(PermissionsRobin.isLoading('own-permissions'))
		)
	}
}
export default Prioritisation;