import * as React from 'react'
import * as moment from 'moment'
import notification from 'antd/lib/notification'
import {Link} from 'react-router-dom'
import {robins} from 'src/robins'
import {connectRobin} from '@simplus/robin-react'
import {
	numberFormater,
	percentage,
	getPastQuarter,
	ColorScale
} from '../DashboardContent/utils'
import Icon from 'antd/lib/icon'
import {
	TableChart
} from 'src/components/dashboard'
const {AnalyticsRobin, clientSettings} = robins

interface Props {
	id: string,
	pathname: string,
	loading: boolean,
	data: any[],
	downloadable?: boolean,
	max_engagement: number,
	min_engagement: number,
	max_workimpact: number,
	min_workimpact: number,
	max_productivity: number,
	min_productivity: number,
	financialYearStart: moment.Moment,
	dateRange: string,
	startDate: string,
	endDate: string,
}

@connectRobin([AnalyticsRobin, clientSettings])
export class PrioritisationTable extends React.Component<Props> {
	
	drillDown(kpi: string, key: string, startDate: string, endDate: string, dateRange: string, id: string): void {

		const settings = clientSettings.getModel();
		const dailyworkhours = settings[0] ? settings[0].avgWorkingHours : 8
		const hourlywage = settings[0] ? Number(settings[0].avgEmployeeWage) : 227.3

		const options: any = {
			startdate : startDate,
			enddate : endDate,
			client : id,
			dailyworkhours: dailyworkhours,
			hourlywage: hourlywage,
			branch: key,
			benchmarkstartdate: moment(startDate).subtract(1, dateRange).format('YYYY-MM-DD'),
			benchmarkenddate: moment(endDate).subtract(1, dateRange).format('YYYY-MM-DD')
		}
		AnalyticsRobin.when(AnalyticsRobin.post('prioritization_drill_down', `/attribution/${kpi}`,  {...options})).catch( err => {
			notification.error({type: 'error', message: 'Could not load attribution data !', description: 'There was an error during the attribution drilldown api execution.'})
		})
	}

	render(): JSX.Element {
		return <TableChart
		sortable={true}
		loading={this.props.loading}
		columns={[
			{Header : 'Department', accessor: 'name'},
			{Header : 'Engagement', accessor: 'engagment',
				Cell: props => {
					return <div style={{justifyContent : this.props.downloadable ? 'center' : 'left', display: 'flex'}}> <div style={{width: '4rem', textAlign: 'center', backgroundColor: ColorScale(props.row.engagment, this.props.max_engagement, this.props.min_engagement)}}>{percentage(props.row.engagment * 100) }</div>
						{this.props.downloadable ? null :
						<div>
							<Link to={`/client/${this.props.id}/dashboard/engagement?dateRange=${encodeURI(this.props.dateRange)}&endDate=${encodeURI(moment(this.props.endDate).format('YYYY-MM-DD'))}&startDate=${encodeURI(moment(this.props.startDate).format('YYYY-MM-DD'))}&site=${encodeURI(`clients/${props.row._original.key}`)}`}>See dashboard</Link> |
							<Link onClick={() => {
								this.drillDown('engagement', props.row._original.key, this.props.startDate, this.props.endDate, this.props.dateRange, this.props.id)
							}} to={`${this.props.pathname}/${props.row._original.key}/engagement?dateRange=${encodeURI(this.props.dateRange)}&endDate=${encodeURI(moment(this.props.endDate).format('YYYY-MM-DD'))}&startDate=${encodeURI(moment(this.props.startDate).format('YYYY-MM-DD'))}`}> drill down <Icon type='filter'/></Link>
						</div>}
						</div>
				}},
			{Header : 'Work impact score', accessor: 'workimpact',
				Cell: props => {
				return <div style={{justifyContent : this.props.downloadable ? 'center' : 'left', display: 'flex'}}> <div style={{width: '4rem', textAlign: 'center', backgroundColor: ColorScale(props.row.workimpact, this.props.max_workimpact, this.props.min_workimpact)}}>{ numberFormater(props.row.workimpact)}</div>
					{this.props.downloadable ? null :
					<div>
						<Link to={`/client/${this.props.id}/dashboard/health?dateRange=${encodeURI(this.props.dateRange)}&endDate=${encodeURI(moment(this.props.endDate).format('YYYY-MM-DD'))}&startDate=${encodeURI(moment(this.props.startDate).format('YYYY-MM-DD'))}&site=${encodeURI(`clients/${props.row._original.key}`)}`}>See dashboard</Link> |
						<Link onClick={() => {
							this.drillDown('workimpact', props.row._original.key, this.props.startDate, this.props.endDate, this.props.dateRange, this.props.id)
						}} to={`${this.props.pathname}/${props.row._original.key}/workimpact?dateRange=${encodeURI(this.props.dateRange)}&endDate=${encodeURI(moment(this.props.endDate).format('YYYY-MM-DD'))}&startDate=${encodeURI(moment(this.props.startDate).format('YYYY-MM-DD'))}`}> drill down <Icon type='filter'/></Link>
						</div>
					}
					</div>
				}},
				{Header : 'Productivity savings', accessor: 'productivity',
					Cell: props => {
						return <div style={{justifyContent : this.props.downloadable ? 'center' : 'left', display: 'flex'}}> <div style={{width: '4rem', textAlign: 'center', backgroundColor: ColorScale(props.row.productivity, this.props.max_productivity, this.props.min_productivity)}}>{percentage(props.row.productivity * 100)}</div>
						{this.props.downloadable ? null :
							<div>
								<Link to={`/client/${this.props.id}/dashboard/effectiveness?dateRange=${encodeURI(this.props.dateRange === 'month' ? 'quarter' : this.props.dateRange)}&endDate=${encodeURI(moment(this.props.dateRange === 'month' ? getPastQuarter(this.props.financialYearStart).endDate : this.props.endDate).format('YYYY-MM-DD'))}&startDate=${encodeURI(moment(this.props.dateRange === 'month' ? getPastQuarter(this.props.financialYearStart).startDate : this.props.startDate).format('YYYY-MM-DD'))}&site=${encodeURI(`clients/${props.row._original.key}`)}`}>See dashboard</Link> |
								<Link onClick={() => {
									this.drillDown('productivity', props.row._original.key, this.props.startDate, this.props.endDate, this.props.dateRange, this.props.id)
								}} to={`${this.props.pathname}/${props.row._original.key}/productivity?dateRange=${encodeURI(this.props.dateRange)}&endDate=${encodeURI(moment(this.props.endDate).format('YYYY-MM-DD'))}&startDate=${encodeURI(moment(this.props.startDate).format('YYYY-MM-DD'))}`}> drill down <Icon type='filter'/></Link>
							</div>
						}
						</div>
			}},
		]}
		data={
			this.props.data || []
		}
	/>
	}
}