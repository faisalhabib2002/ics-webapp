import * as React from 'react'
import {
	DashboardContainer,
	DashboardTab,
	Dashboard,
	DashboardFilter,
	DashboardSection,
	KpiChart
} from 'src/components/dashboard'
import * as moment from 'moment'
import * as queryString from 'query-string'
import defaults from 'lodash/defaults'
import {Tabs, TabPane, Loader} from '@simplus/siui'
import notification from 'antd/lib/notification'
import {connectRobin} from '@simplus/robin-react'
import { RouteComponentProps } from 'react-router-dom'
import {robins} from 'src/robins'
import {DateRangeFilter, extractKpis, relative, numberFormater, percentage, timeConverter, ListToTree} from './utils'
import {hasPermission, noPermission} from '../../../../utils'
import {ErrorBoundary} from '../../../../utils/ErrorBoundary'
const {clientSettings, AnalyticsRobin, PermissionsRobin} = robins

@connectRobin([clientSettings, AnalyticsRobin, PermissionsRobin])

export class CostToServe extends React.Component <RouteComponentProps<{id: string}>> {
	state = {filter: moment('2018-01-01'), compareBenchmark: true}
	unlisten?: () => void = undefined
	componentWillMount(): void {
		clientSettings.when(clientSettings.findOne(this.props.match.params.id)).then(() => {
			const settings = clientSettings.getModel();
			if (settings[0]) {
				const reportingCycle = moment(settings[0].reportingCycle)
				if (!reportingCycle.isSame(this.state.filter, 'd')) {
					this.setState({filter: reportingCycle})
				}
			}
			this.applyFilters()
		}).catch( err => {
			notification.error({type: 'error', message: 'Could not load settings data !', description: 'There was an error during the settings api execution.'})
		})
		this.unlisten = this.props.history.listen((location, action) => {
			if (action === 'POP')
				this.applyFilters()
		});
	}
	applyFilters(): void {
		const benchmark = queryString.parse(location.hash.split('?')[1]).benchmark || 'Previous range'
		const dateRange = queryString.parse(location.hash.split('?')[1]).dateRange || 'month'
		const startDate = queryString.parse(location.hash.split('?')[1]).startDate || moment().startOf('month').subtract(1, dateRange).format('YYYY-MM-DD');
		const endDate = queryString.parse(location.hash.split('?')[1]).endDate || moment().subtract(1, dateRange).endOf(dateRange).format('YYYY-MM-DD');
		const sitelevels = queryString.parse(location.hash.split('?')[1]).site || ['']

		const options: any = {
			startdate : startDate,
			enddate : endDate,
			client : this.props.match.params.id,
			sitelevels: Array.isArray(sitelevels) ? sitelevels : sitelevels.split(),
		}
		if (benchmark === 'Previous range') {
			options.benchmarkstartdate = moment(startDate).subtract(1, dateRange).format('YYYY-MM-DD')
			options.benchmarkenddate = dateRange === 'month' ?
				moment(endDate).subtract(1, dateRange).endOf('month').format('YYYY-MM-DD') :
				moment(endDate).subtract(1, dateRange).format('YYYY-MM-DD')
		} else {
			options.benchmark = benchmark.split(',')
		}
		AnalyticsRobin.when(AnalyticsRobin.post('cost_callmetrics', '/cost/callmetrics', {...options})).catch( err => {
			notification.error({type: 'error', message: 'Could not load cost to serve call metrics data !', description: 'There was an error during the analytics api execution.'})
		})
		AnalyticsRobin.when(AnalyticsRobin.post('cost_tatmetrics', '/cost/tatmetrics', {...options})).catch( err => {
			notification.error({type: 'error', message: 'Could not load cost to serve tat metrics data !', description: 'There was an error during the analytics api execution.'})
		})
		AnalyticsRobin.when(AnalyticsRobin.post('cost_casemetrics', '/cost/casemetrics', {...options})).catch( err => {
			notification.error({type: 'error', message: 'Could not load cost to serve case metrics data !', description: 'There was an error during the analytics api execution.'})
		})
		AnalyticsRobin.when(AnalyticsRobin.post('cost_sessionmetrics', '/cost/sessionmetrics', {...options})).catch( err => {
			notification.error({type: 'error', message: 'Could not load cost to serve session metrics data !', description: 'There was an error during the analytics api execution.'})
		})
	}
	componentWillUnmount(): void {
		if (this.unlisten)
		this.unlisten();
	}
	pageRedirect(update: any, refresh: boolean = false): void {
		const state = {
			...this.urlState(),
			...update}
		this.props.history.push(`${this.props.location.pathname}?${queryString.stringify(state)}`)
		if (refresh)
			this.applyFilters()
	}
	urlState(): any {
		return queryString.parse(location.hash.split('?')[1])
	}
	render(): JSX.Element {
		const clientID = this.props.match.params.id
		const settings = clientSettings.getModel() || [{siteMapping: []}]
		// Query Strings
		const { dateRange, site, benchmark, tab} = defaults(this.urlState(), {
			dateRange : 'month',
			site: 'All',
			benchmark: 'Previous range',
			tab: '0/.0'
		})
		const compareBenchmark = this.state.compareBenchmark
		// Below variable is used for testing only. It has to be replaced when api is available for sitemapping
		const siteMapping = [
			{
				id: 'Sub1',
				name: 'Sub1',
				level: '2',
				branchTo: 'S1'
			},
			{
				id: 'Sub3',
				name: 'Sub 3',
				level: '1',
				branchTo: 'S2'
			},
			{
				id: 'S1',
				name: 'sitelevel1',
				level: '1',
				branchTo: ''
			},
			{
				id: 'S2',
				name: 'siteLevel2',
				level: '1',
				branchTo: ''
			},
			{
				id: 'Sub2',
				name: 'Sub 2',
				level: '1',
				branchTo: 'S2'
			},
			{
				id: 'S3',
				name: 'Site 3',
				level: '1',
				branchTo: ''
			}
		]
		const mapping = ((settings as any).length > 0) ? settings[0].siteMapping : [{siteMapping: []}]
		const treeData = ListToTree(siteMapping, mapping)

		const kpis_callmetrics = extractKpis(['callincomming', 'avereferral', 'callduration', 'callback', 'callbackrate', 'callbackresponse'], (AnalyticsRobin.getResult('cost_callmetrics')||[])[0])
		const kpis_tatmetrics = extractKpis(['avesession', 'avereferral', 'aveaffialiate', 'firstsession', 'gtitotal', 'riskcase'], (AnalyticsRobin.getResult('cost_tatmetrics')||[])[0])
		const kpis_casemetrics = extractKpis(['total', 'ratio', 'resolution', 'avereferral'], (AnalyticsRobin.getResult('cost_casemetrics')||[])[0])
		const kpis_sessionmetrics = extractKpis(['Total', 'Scheduled', 'NoShow', 'Attended', 'Late'], (AnalyticsRobin.getResult('cost_sessionmetrics')||[])[0], true)
		const costToServeDashboard = (
			<DashboardContainer style={{backgroundColor: '#F9FBFF', padding: '1rem', position: 'relative'}}>
				{(clientSettings.isLoading(clientSettings.ACTIONS.FIND_ONE)||clientSettings.isError(clientSettings.ACTIONS.FIND_ONE)) ? <Loader error={clientSettings.isError(clientSettings.ACTIONS.FIND_ONE)}/> : null}
				<div className='dashboard-top-bar'>
					<div style={{display: 'flex', padding: '1rem'}}>
					<DashboardFilter
							width={130}
							type='tree'
							title='SITE'
							defaultValue={site}
							style={{marginRight: '1rem'}}
							treeData={treeData}
							onChange={(selected) => this.pageRedirect({site: selected}, true)
						}/>
						<DateRangeFilter value={dateRange} customOption financialStartYear={moment(this.state.filter)} label='DATE RANGE' style={{minWidth: 130, marginRight: '1rem'}}
							onChange={(selected) => this.pageRedirect({
								dateRange: selected.value,
								startDate: moment(selected.startDate).format('YYYY-MM-DD'),
								endDate: moment(selected.endDate).format('YYYY-MM-DD')
							}, true)
						}/>
						<DashboardFilter
							width={150}
							type='single'
							text='Compared to'
							title='BENCHMARK'
							defaultValue={benchmark}
							options={[{value: 'Previous range', text: 'Previous period'}, {value: 'industry', text: 'Sector'},{value: 'clients', text: 'Company'}, {value: 'SegmentSize', text: 'Size'}]}
							onChange={(selected) => this.pageRedirect({benchmark: selected}, true)
						}/>
						<DashboardFilter
							type='switch'
							text='Display Benchmark'
							checked={compareBenchmark}
							onChange={(selected) => this.setState({compareBenchmark: selected})
						}/>
					</div>
				</div>
				<Dashboard
					structure={{
						items : [1, 1],
						size : 1
					}}>
					<DashboardSection defaultActiveTab={tab}
						onTabChange={(selected) => this.pageRedirect({tab: selected})}
						section={{
							tabTitles : ['TAT Metrics', 'Case Metrics', 'Session Metrics', 'Call Metrics'],
							tabTooltips: [
								'TAT Metrics Tooltip..',
								'Case Metrics Tooltip..',
								'Session Metrics Tooltip..',
								'Call Metrics Tooltip..'
							],
						title : 'SLA Metrics Monitoring',
						tooltip : 'Tooltip..'
					}}>
						<DashboardTab>
							<KpiChart
								kpis={[
									{
										title : 'Ave. Session (F2F) TAT',
										loading: AnalyticsRobin.isLoading('cost_tatmetrics'),
										error: AnalyticsRobin.isError('cost_tatmetrics'),
										data : timeConverter(kpis_tatmetrics.avesession.value),
										footer: compareBenchmark ? `${relative(kpis_tatmetrics.avesession.value, kpis_tatmetrics.avesession.benchmark)} vs SLA (${timeConverter(kpis_tatmetrics.avesession.benchmark)})` : ''
									},
									{
										title : 'Ave. Referral TAT',
										loading: AnalyticsRobin.isLoading('cost_tatmetrics'),
										error: AnalyticsRobin.isError('cost_tatmetrics'),
										data : timeConverter(kpis_tatmetrics.avereferral.value),
										footer: compareBenchmark ? `${relative(kpis_tatmetrics.avereferral.value, kpis_tatmetrics.avereferral.benchmark)} Vs Benchmark (${timeConverter(kpis_tatmetrics.avereferral.benchmark)})` : ''
									},
									{
										title : 'Ave. Affiliate TAT',
										loading: AnalyticsRobin.isLoading('cost_tatmetrics'),
										error: AnalyticsRobin.isError('cost_tatmetrics'),
										data : timeConverter(kpis_tatmetrics.aveaffialiate.value),
										footer: compareBenchmark ? `${relative(kpis_tatmetrics.aveaffialiate.value, kpis_tatmetrics.aveaffialiate.benchmark)} Vs Benchmark (${timeConverter(kpis_tatmetrics.aveaffialiate.benchmark)})` : ''
									},
								]}
							/>
							<div style={{borderTop: '1px solid #CCC'}} />
							<KpiChart
								kpis={[
									{
										title : 'First Session TAT',
										loading: AnalyticsRobin.isLoading('cost_tatmetrics'),
										error: AnalyticsRobin.isError('cost_tatmetrics'),
										data : timeConverter(kpis_tatmetrics.firstsession.value),
										footer: compareBenchmark ? `${relative(kpis_tatmetrics.firstsession.value, kpis_tatmetrics.firstsession.benchmark)} Vs Benchmark (${timeConverter(kpis_tatmetrics.firstsession.benchmark)})` : ''
									},
									{
										title : 'Ave. GTI Total TAT',
										loading: AnalyticsRobin.isLoading('cost_tatmetrics'),
										error: AnalyticsRobin.isError('cost_tatmetrics'),
										data : timeConverter(kpis_tatmetrics.gtitotal.value),
										footer: compareBenchmark ? `${relative(kpis_tatmetrics.gtitotal.value, kpis_tatmetrics.gtitotal.benchmark)} Vs Benchmark (${timeConverter(kpis_tatmetrics.gtitotal.benchmark)})` : ''
									},
									{
										title : 'Ave. Risk Case Total TAT',
										loading: AnalyticsRobin.isLoading('cost_tatmetrics'),
										error: AnalyticsRobin.isError('cost_tatmetrics'),
										data : timeConverter(kpis_tatmetrics.riskcase.value),
										footer: compareBenchmark ? `${relative(kpis_tatmetrics.riskcase.value, kpis_tatmetrics.riskcase.benchmark)} Vs Benchmark (${timeConverter(kpis_tatmetrics.riskcase.benchmark)})` : ''
									},
								]}
							/>
						</DashboardTab>
						<DashboardTab>
							<KpiChart
								kpis={[
									{
										title : 'Total No. Cases Created',
										loading: AnalyticsRobin.isLoading('cost_casemetrics'),
										error: AnalyticsRobin.isError('cost_casemetrics'),
										data : numberFormater(kpis_casemetrics.total.value),
										footer: compareBenchmark ? `${relative(kpis_casemetrics.avereferral.value, kpis_casemetrics.avereferral.benchmark)} Vs Benchmark (${numberFormater(kpis_casemetrics.avereferral.benchmark)})` : ''
									},
									{
										title : 'Call to Case Ratio',
										loading: AnalyticsRobin.isLoading('cost_casemetrics'),
										error: AnalyticsRobin.isError('cost_casemetrics'),
										data : percentage(kpis_casemetrics.ratio.value * 100),
										footer: compareBenchmark ? `${relative(kpis_casemetrics.ratio.value, kpis_casemetrics.ratio.benchmark)} Vs Benchmark (${percentage(kpis_casemetrics.ratio.benchmark * 100)})` : ''
									}
								]}
							/>
							<div style={{borderTop: '1px solid #CCC'}} />
							<KpiChart
								kpis={[
									{
										title : 'Ave. Case Resolution Time',
										loading: AnalyticsRobin.isLoading('cost_casemetrics'),
										error: AnalyticsRobin.isError('cost_casemetrics'),
										data : timeConverter(kpis_casemetrics.resolution.value),
										footer: compareBenchmark ? `${relative(kpis_casemetrics.resolution.value, kpis_casemetrics.resolution.benchmark)} Vs Benchmark (${timeConverter(kpis_casemetrics.resolution.benchmark)})` : ''
									},
									{
										title : 'Ave. Referral Rate',
										loading: AnalyticsRobin.isLoading('cost_casemetrics'),
										error: AnalyticsRobin.isError('cost_casemetrics'),
										data : percentage(kpis_casemetrics.avereferral.value * 100),
										footer: compareBenchmark ? `${relative(kpis_casemetrics.avereferral.value, kpis_casemetrics.avereferral.benchmark)} Vs Benchmark (${percentage(kpis_casemetrics.avereferral.benchmark * 100)})` : ''
									}
								]}
							/>
						</DashboardTab>
						<DashboardTab>
							<KpiChart
								kpis={[
									{
										title : 'Total No. of Sessions (F2F)',
										loading: AnalyticsRobin.isLoading('cost_sessionmetrics'),
										error: AnalyticsRobin.isError('cost_sessionmetrics'),
										data : numberFormater(kpis_sessionmetrics.Total.value/100),
										footer: compareBenchmark ? `${relative(kpis_sessionmetrics.Total.value/100, kpis_sessionmetrics.Total.benchmark/100)} Vs Benchmark (${numberFormater(kpis_sessionmetrics.Total.benchmark/100)})` : ''
									},
									{
										title : 'Ave. Sessions Scheduled',
										loading: AnalyticsRobin.isLoading('cost_sessionmetrics'),
										error: AnalyticsRobin.isError('cost_sessionmetrics'),
										data : percentage(kpis_sessionmetrics.Scheduled.value),
										footer: compareBenchmark ? `${relative(kpis_sessionmetrics.Scheduled.value, kpis_sessionmetrics.Scheduled.benchmark)} Vs Benchmark (${percentage(kpis_sessionmetrics.Scheduled.benchmark)})` : ''
									},
									{
										title : 'Ave. Sessions Attended',
										loading: AnalyticsRobin.isLoading('cost_sessionmetrics'),
										error: AnalyticsRobin.isError('cost_sessionmetrics'),
										data : percentage(kpis_sessionmetrics.Attended.value),
										footer: compareBenchmark ? `${relative(kpis_sessionmetrics.Attended.value, kpis_sessionmetrics.Attended.benchmark)} Vs Benchmark(${percentage(kpis_sessionmetrics.Attended.benchmark)})` : ''
									},
								]}
							/>
							<div style={{borderTop: '1px solid #CCC'}} />
							<KpiChart
								kpis={[
									{
										title : 'Ave. Sessions `No Show`',
										loading: AnalyticsRobin.isLoading('cost_sessionmetrics'),
										error: AnalyticsRobin.isError('cost_sessionmetrics'),
										data : percentage(kpis_sessionmetrics.NoShow.value),
										footer: compareBenchmark ? `${relative(kpis_sessionmetrics.NoShow.value, kpis_sessionmetrics.NoShow.benchmark)} Vs Benchmark (${percentage(kpis_sessionmetrics.NoShow.benchmark)})` : ''
									},
									{
										title : 'Ave. Sessions Late/Cancelled',
										loading: AnalyticsRobin.isLoading('cost_sessionmetrics'),
										error: AnalyticsRobin.isError('cost_sessionmetrics'),
										data : percentage(kpis_sessionmetrics.Late.value),
										footer: compareBenchmark ? `${relative(kpis_sessionmetrics.Late.value, kpis_sessionmetrics.Late.benchmark)} Vs Benchmark (${percentage(kpis_sessionmetrics.Late.benchmark)})` : ''
									},
									{
										title: '',
										data: ''
									}
								]}
							/>
						</DashboardTab>
						<DashboardTab>
							<KpiChart
								kpis={[
									{
										title : 'Total No. Calls (Incoming)',
										loading: AnalyticsRobin.isLoading('cost_callmetrics'),
										error: AnalyticsRobin.isError('cost_callmetrics'),
										data : numberFormater(kpis_callmetrics.callincomming.value),
										footer: compareBenchmark ? `${relative(kpis_callmetrics.callincomming.value, kpis_callmetrics.callincomming.benchmark)} Vs Benchmark (${numberFormater(kpis_callmetrics.callincomming.benchmark)})` : ''
									},
									{
										title : 'Call Answer Rate',
										loading: AnalyticsRobin.isLoading('cost_callmetrics'),
										error: AnalyticsRobin.isError('cost_callmetrics'),
										data : 	percentage(kpis_callmetrics.avereferral.value),
										footer: compareBenchmark ? `${relative(kpis_callmetrics.avereferral.value, kpis_callmetrics.avereferral.benchmark)} Vs Benchmark (${percentage(kpis_callmetrics.avereferral.benchmark)})` : ''
									},
									{
										title : 'Ave. Call Duration',
										loading: AnalyticsRobin.isLoading('cost_callmetrics'),
										error: AnalyticsRobin.isError('cost_callmetrics'),
										data : timeConverter(kpis_callmetrics.callduration.value),
										footer: compareBenchmark ? `${relative(kpis_callmetrics.callbackrate.value, kpis_callmetrics.callbackrate.benchmark)} Vs Benchmark (${timeConverter(kpis_callmetrics.callbackrate.benchmark)})` : ''
									},
								]}
							/>
							<div style={{borderTop: '1px solid #CCC'}} />
							<KpiChart
								kpis={[
									{
										title : 'Total No. Call Back Requests',
										loading: AnalyticsRobin.isLoading('cost_callmetrics'),
										error: AnalyticsRobin.isError('cost_callmetrics'),
										data : percentage(kpis_callmetrics.callback.value),
										footer: compareBenchmark ? `${relative(kpis_callmetrics.callback.value, kpis_callmetrics.callback.benchmark)} Vs Benchmark (${percentage(kpis_callmetrics.callback.benchmark)})` : ''
									},
									{
										title : 'Call Back Rate',
										loading: AnalyticsRobin.isLoading('cost_callmetrics'),
										error: AnalyticsRobin.isError('cost_callmetrics'),
										data : percentage(kpis_callmetrics.callbackrate.value),
										footer: compareBenchmark ? `${relative(kpis_callmetrics.callbackrate.value, kpis_callmetrics.callbackrate.benchmark)} Vs Benchmark (${percentage(kpis_callmetrics.callbackrate.benchmark)})` : ''
									},
									{
										title : 'Ave. Call Back Response Time',
										loading: AnalyticsRobin.isLoading('cost_callmetrics'),
										error: AnalyticsRobin.isError('cost_callmetrics'),
										data : timeConverter(kpis_callmetrics.callbackresponse.value),
										footer: compareBenchmark ? `${relative(kpis_callmetrics.callbackresponse.value, kpis_callmetrics.callbackresponse.benchmark)} Vs Benchmark (${timeConverter (kpis_callmetrics.callbackresponse.benchmark)})` : ''
									},
								]}
							/>
						</DashboardTab>
					</DashboardSection>
				</Dashboard>
			</DashboardContainer>
		)
		return(
			<ErrorBoundary>
				<Tabs selectedDefault={5} fillContainer className= 'tab' style={{fontWeight: 'bold'}}
				active={(current) => {
					switch (current) {
						case 0:
							this.props.history.push(`/client-prioritisation/${clientID}/dashboard/engagement`)
							break;
						case 1:
							this.props.history.push(`/client-prioritisation/${clientID}/dashboard/health`)
							break;
						case 2:
							this.props.history.push(`/client-prioritisation/${clientID}/dashboard/effectiveness`)
							break;
						case 3:
							this.props.history.push(`/client-prioritisation/${clientID}/dashboard/quality`)
							break;
						case 4:
							this.props.history.push(`/client-prioritisation/${clientID}/dashboard/roi`)
							break;
					}
				}}
				>
					<TabPane label='Engagement' disabled={!hasPermission('/view/dashboard/engagement', PermissionsRobin.getResult('own-permissions')) }/>
					<TabPane label='Health' disabled={!hasPermission('/view/dashboard/health', PermissionsRobin.getResult('own-permissions'))}/>
					<TabPane label='Effectiveness' disabled={!hasPermission('/view/dashboard/effectiveness', PermissionsRobin.getResult('own-permissions'))}/>
					<TabPane label='ROI' disabled={!hasPermission('/view/dashboard/roi', PermissionsRobin.getResult('own-permissions'))}/>
					<TabPane label='Quality' disabled={!hasPermission('/view/dashboard/quality', PermissionsRobin.getResult('own-permissions'))}/>
					<TabPane label='CostToServe'>
						{hasPermission('/view/dashboard/cost-to-serve', PermissionsRobin.getResult('own-permissions')) ? costToServeDashboard : noPermission(PermissionsRobin.isLoading('own-permissions'))}
					</TabPane>
				</Tabs>
			</ErrorBoundary>
		)
	}

}
export default CostToServe;