import * as moment from 'moment'
export interface DatesObject {
	startDate: moment.Moment
	endDate: moment.Moment
}
export function getPastMonth(financialYearStartDate: moment.Moment = moment('2001-01-01')): DatesObject {
	const currentDate = moment()
	const currentYearStartDate = moment(financialYearStartDate).year(currentDate.year())
	if (currentYearStartDate.isAfter(currentDate))
		currentYearStartDate.year(currentDate.year() - 1)
	let pastMonth = moment(currentYearStartDate).subtract(1, 'month')
	let output
	for (let i = 0; i <= 12 ; i++) {
		const currentMonth = moment(currentYearStartDate).add(i, 'months')
		if (currentDate.isBetween(pastMonth, currentMonth)) {
			output =  {
				startDate: moment(pastMonth).subtract(1, 'M'),
				endDate: pastMonth.subtract(1, 'day').endOf('day')
			}
		}
		pastMonth = moment(currentMonth)
	}
	return output
}
export function getPastQuarter(financialYearStartDate: moment.Moment = moment('2001-01-01')): DatesObject {
	const currentDate = moment()
	const currentYearStartDate = moment(financialYearStartDate).year(
		currentDate.year()
	);
	const financial_start_date = moment(financialYearStartDate).date();

	if (currentYearStartDate.isAfter(currentDate))
	currentYearStartDate.year(currentDate.year() - 1);

	const Q1_end_date =
	financial_start_date >
		moment(currentYearStartDate)
		.add(1, 'quarter')
		.endOf('month')
		.date() || financial_start_date === 1
		? moment(currentYearStartDate)
			.subtract(1, 'day')
			.add(1, 'quarter')
			.endOf('month')
		: moment(currentYearStartDate)
			.add(1, 'quarter')
			.date(financial_start_date)
			.subtract(1, 'day');
	const Q2_end_date =
	financial_start_date >
		moment(Q1_end_date)
		.add(1, 'quarter')
		.endOf('month')
		.date() || financial_start_date === 1
		? moment(Q1_end_date)
			.add(1, 'quarter')
			.endOf('month')
		: moment(Q1_end_date)
			.add(1, 'quarter')
			.date(financial_start_date)
			.subtract(1, 'day');

	const Q3_end_date =
	financial_start_date >
		moment(Q2_end_date)
		.add(1, 'quarter')
		.endOf('month')
		.date() || financial_start_date === 1
		? moment(Q2_end_date)
			.add(1, 'quarter')
			.endOf('month')
		: moment(Q2_end_date)
			.add(1, 'quarter')
			.date(financial_start_date)
			.subtract(1, 'day');

	const past_Quarter = {
	startDate: moment(currentYearStartDate)
		.subtract(1, 'quarter')
		.startOf('day'),
	endDate: moment(currentYearStartDate)
		.subtract(1, 'day')
		.endOf('day')
	};
	const Q1 = {
	startDate: currentYearStartDate.startOf('day'),
	endDate: Q1_end_date
	};
	const Q2 = {
	startDate: moment(Q1_end_date)
		.add(1, 'day')
		.startOf('day'),
	endDate: Q2_end_date.endOf('day')
	};
	const Q3 = {
	startDate: moment(Q2_end_date)
		.add(1, 'day')
		.startOf('day'),
	endDate: Q3_end_date.endOf('day')
	};

	if (currentDate.isSameOrBefore(Q1.endDate)) return past_Quarter;
	else if (currentDate.isBetween(Q1.endDate, Q2.endDate)) return Q1;
	else if (currentDate.isBetween(Q2.endDate, Q3.endDate)) return Q2;
	else return Q3;
}
export function getPastYear(financialYearStartDate: moment.Moment = moment('2001-01-01')): DatesObject {
	const currentDate = moment()
	const currentYearStartDate = moment(financialYearStartDate).year(currentDate.year())
	if (currentYearStartDate.isAfter(currentDate))
		currentYearStartDate.year(currentDate.year() - 1)

	const pastYear = {startDate: moment(currentYearStartDate).subtract(1, 'year'), endDate: currentYearStartDate.subtract(1, 'day').endOf('day')}
	return pastYear
}
export function getPreviousMonth(financialYearStartDate: moment.Moment = moment('2001-01-01')): DatesObject {
	const pastMonth = getPastMonth(financialYearStartDate)
	const prevMonth = {startDate: pastMonth.startDate.subtract(1, 'month'), endDate: pastMonth.endDate.subtract(1, 'month').subtract(1, 'day').endOf('day')}
	return prevMonth
}
export function getPreviousQuarter(financialYearStartDate: moment.Moment = moment('2001-01-01')): DatesObject {
	const pastQuarter = getPastQuarter(financialYearStartDate)
	const prevQuarter = {startDate: pastQuarter.startDate.subtract(1, 'quarter'), endDate: pastQuarter.endDate.subtract(1, 'quarter').subtract(1, 'day').endOf('day')}
	return prevQuarter
}
export function getPreviousYear(financialYearStartDate: moment.Moment = moment('2001-01-01')): DatesObject {
	const pastYear = getPastYear(financialYearStartDate)
	const prevYear = {startDate: pastYear.startDate.subtract(1, 'year'), endDate: pastYear.endDate.subtract(1, 'year').subtract(1, 'day').endOf('day')}
	return prevYear
}