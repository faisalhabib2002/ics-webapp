import * as React from 'react'
import {ErrorBoundary} from 'src/utils/ErrorBoundary'
import {Dropdown, Button, Menu, Icon, Modal} from 'antd'
import domtoimage from 'dom-to-image';
import fileDownload from 'js-file-download';

import * as FileSaver from 'file-saver'
import XLSX from 'sheetjs-style'


export interface ExcelExportProps {
	name: string;
	columns: string[],
	data: Object[],
	chart?: JSX.Element,
	chartWidth ?: string,
	handleTrend?: any
}

export class Download extends React.Component<ExcelExportProps> {
	state = {visible: false}


	downloadChart = (filename: string) => {
		const ref = this
		domtoimage.toBlob(document.getElementById('download-preview'))
			.then(function (blob): void {
				fileDownload(blob, `${filename}.png`);
				ref.setState({visible: false})
			});
	}

	handleDownload = () => {
		const final_data = this.props.data.filter(item => item['value'] !== 0)
		const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
		const fileExtension = '.xlsx';
		const ws = XLSX.utils.json_to_sheet(final_data)
		const wb = {Sheets: { 'data': ws}, SheetNames: ['data']}
		const excelBuffer = XLSX.write(wb, {bookType: 'xlsx', type: 'array'})
		const data = new Blob([excelBuffer], {type: fileType})
		FileSaver.saveAs(data, this.props.name + fileExtension)
	}

	render(): JSX.Element {

		const menu = (
			<Menu className='download-menu'>
				<Menu.Item key='1'>
					<div className='download-menu-item' onClick={() => this.handleDownload()}>Download underlying data</div>
				</Menu.Item>
				{this.props.chart ? <Menu.Item key='2'><div className='download-menu-item' onClick={() => this.setState({visible: true})}>Download chart</div></Menu.Item> : null}
				{/* {this.props.handleTrend ? <Menu.Item key='3'><div className='download-menu-item' onClick={() => this.props.handleTrend()}>Engagement Trend</div></Menu.Item> : null} */}
			</Menu>
		);
		return <ErrorBoundary>
			{this.props.chart ? <Modal
					title='Preview'
					visible={this.state.visible}
					width={this.props.chartWidth ? this.props.chartWidth : '99%'}
					destroyOnClose
					onOk={() => this.downloadChart(this.props.name)}
					onCancel={() => this.setState({visible: false})}
				>
				<div id='download-preview' style={{background: 'white', paddingTop: 20}}>
					{React.cloneElement(this.props.chart, {showLabel: true})}
				</div>
			</Modal>
			: null}
			<Dropdown overlay={menu}>
				<Button className='download-icon'><Icon type='ellipsis' />
				</Button>
			</Dropdown>
			</ErrorBoundary>
	}
}