export type NumberFormater = (n: number, formater?: NumberFormater) => string

export function currency(n: number, formater: NumberFormater = numberFormater): string {
	return `R${formater(n)}`
}

export function percentage(n: number, formater : NumberFormater = numberFormater): string {
	return `${formater(n)}%`
}
export function round(n: number, decimal: number): number {
	const factor = Math.pow(10, decimal);
	return Math.round(n * factor) / factor;
}
export function numberFormater(n: number): string {
	if (isNaN(n)) return '0'
	return `${round(n, 1)}`
}

export function shortNumber(n:  number, formater: NumberFormater = numberFormater, decimal: number = 0): string {
	if (isNaN(n)) return '0'

	let rounded = round(n, decimal)
	let sufix = ''
	if (Math.abs(rounded) / 1000 > 1) {
		rounded = round(rounded / 1000, decimal)
		sufix = 'K'
	}
	if (Math.abs(rounded) / 1000 > 1) {
		rounded = round(rounded / 1000, decimal)
		sufix = 'M'
	}
	if (Math.abs(rounded) / 1000 > 1) {
		rounded = round(rounded / 1000, decimal)
		sufix = 'B'
	}
	return `${formater(rounded)}${sufix}`
}

export function relative(n: number, benchmark: number, formater: NumberFormater = numberFormater): string {
	if (isNaN(n) || isNaN(benchmark)) return '0%'
	if (n === 0) return '0'
	const delta = (n / benchmark) * 100 - 100
	return `${delta >= 0 ? '+' : ''}${percentage(delta, formater)}`
}

export function percentageRelative(n: number, benchmark: number, formater: NumberFormater = numberFormater): string {
	const delta = (n - benchmark) * 100
	return `${delta >= 0 ? '+' : ''}${percentage(delta, formater)}`
}

export function timeConverter(n: number): string {
	if (isNaN(n)) return '0m'
	const days = (n / (60 * 24))
	const rdays = Math.floor(days)
	const hours = (days - rdays) * 24;
	const rhours = Math.floor(hours);
	const minutes = (hours - rhours) * 60;
	const rminutes = Math.round(minutes);
	let output: string = ''
	if (rdays > 0)
		output += `${rdays}d`
	if (rhours > 0)
		output += `${rhours}h`
	output += `${rminutes}m`
	return output
}
export function capitalizeFirstLetter(input: string): string {
	return input.charAt(0).toUpperCase() + input.slice(1);
}