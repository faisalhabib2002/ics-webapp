export function ColorScale(value: number, max: number, min: number): string {
	const center = (max + min) / 2
	if (center === value)
		return 'white'
	else {
	const distance = Math.abs(center - value) / (max - center)
	if (value < center)
		return(`rgba(255,0,0,${distance})`)
	else
		return(`rgba(0,255,0,${distance})`)
	}
}