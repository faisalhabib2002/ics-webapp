export function ListToTree(list: any): any {
	const roots = [], map = {}
	let node, i;
	for (i = 0; i < list.length; i += 1) {
		map[list[i].id] = i; // initialize the map
		list[i].children = []; // initialize the children
	}
	for (i = 0; i < list.length; i += 1) {
		node = list[i];
		if (node.branchTo === null && node.active !== 'false') {
			(roots as any).push({
				value: node.id,
				key: node.id,
				title: node.name,
				disabled: node.disabled,
				children: node.children
			});
		} else if (node.branchTo && node.active !== 'false' && (map[node.branchTo] >= 0) && list[map[node.branchTo]]) { // Check if current node is a child of another and if the parent exists in the list
			list[map[node.branchTo]].children.push({
				value: node.id,
				key: node.id,
				title: node.name,
				disabled: node.disabled,
				children: node.children
			});
		}
	}
	return cutTreeRecursive(roots);
}

export function cutTreeRecursive(tree) {
	if(Array.isArray(tree)) {
		return tree.map( t => cutTreeRecursive(t)).filter( t => !!t)
	} else if (tree.children && tree.children.length > 0) {
		tree.children = cutTreeRecursive(tree.children).filter( t => !!t)
		if( tree.children.length === 0 && tree.disabled) {
			return null
		} else {
			return tree
		}
	} else if (tree.disabled) {
		return null
	} else {
		return tree
	}
}

export function getTopActiveNodes(tree, nodes) {
    tree.forEach(t => {
        // Find active children in case of disabled parent
        if (!t.disabled) {
            nodes.push(t.value);
        } else {
            getTopActiveNodes(t.children, nodes)
        }
    });
}