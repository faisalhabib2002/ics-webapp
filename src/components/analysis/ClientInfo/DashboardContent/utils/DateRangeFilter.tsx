import * as React from 'react'
import {Select} from '@simplus/siui'
import Modal from 'antd/lib/modal'
import DatePicker from 'antd/lib/date-picker'
import * as moment from 'moment'
import * as queryString from 'query-string'
import {getPastMonth, getPastQuarter, getPastYear} from '../utils'
const Option = Select.Option

interface ReturnProps {
	startDate: moment.Moment
	endDate: moment.Moment
	value: 'month' | 'quarter' | 'year' | 'custom date'
}
interface State {
	visible: boolean
	startDate: moment.Moment
	endDate: moment.Moment
}
export interface DateRangeFilterProps {
	customOption?: boolean
	hideMonth?: boolean
	financialStartYear?: moment.Moment
	label ?: string
	value: 'month' | 'quarter' | 'year' | 'YTD' | 'custom date'
	style: React.CSSProperties
	onChange (values: ReturnProps): void
}
export class DateRangeFilter extends React.Component<DateRangeFilterProps, State> {
	constructor(props: DateRangeFilterProps) {
		super(props);
		this.state = {
			visible: false,
			startDate: queryString.parse(location.hash.split('?')[1]).startDate || moment().startOf('month').subtract(1, 'month').format('YYYY-MM-DD'),
			endDate: queryString.parse(location.hash.split('?')[1]).endDate || moment().subtract(1, 'month').endOf('month').format('YYYY-MM-DD')
		}
	}

	componentWillReceiveProps(nextProps: DateRangeFilterProps): void {
		if (nextProps.financialStartYear && (!moment(nextProps.financialStartYear).isSame(this.props.financialStartYear))) {
			this.props.onChange(DateRangeFilter.getDates(nextProps.value, nextProps))
		}
	}
	showModal = () => {
		this.setState({visible: true});
	}
	handleOk = (e) => {
		this.props.onChange({endDate: this.state.endDate, startDate: this.state.startDate, value: 'custom date'});
		this.setState({
			visible: false
		});
	}
	handleCancel = (e) => {
		this.setState({visible: false});
	}

	static getDates = (value, props) => {
		let endDate
		let startDate
		switch (value) {
			case 'month':
				startDate = getPastMonth().startDate
				endDate = getPastMonth().endDate
				break;
			case 'quarter':
				startDate = getPastQuarter(props.financialStartYear).startDate
				endDate = getPastQuarter(props.financialStartYear).endDate
				break;
			case 'year':
				startDate = getPastYear(props.financialStartYear).startDate
				endDate = getPastYear(props.financialStartYear).endDate
				break;
			case 'YTD':
				startDate = moment().startOf('year')
				endDate = moment()
				break;
			case 'custom date':
				startDate = queryString.parse(location.hash.split('?')[1]).startDate || getPastMonth().startDate,
				endDate =  queryString.parse(location.hash.split('?')[1]).endDate || getPastMonth().endDate
				break;
		}
		return {startDate, endDate, value}
	}
	render(): JSX.Element {
		return <div>
			<Select
				value={this.props.value}
				label={this.props.label}
				style={this.props.style}
				onSelect={(value: string) => {
					value === 'custom date' ?
						this.setState({visible: true})
						: this.props.onChange(DateRangeFilter.getDates(value, this.props))}
				}>
				{this.props.hideMonth ? null :<Option value='month'>Past Month</Option>}
				<Option value='quarter'>Past Quarter</Option>
				<Option value='year'>Past Year</Option>
				<Option value='YTD'>Year to Date</Option>
				{this.props.customOption ? <Option value='custom date'>Custom Range</Option> : null}
			</Select>
			<Modal
				className='si-date-picker-custom'
				title={<span style={{fontSize: '20px', fontWeight: 300}}>Custom range</span>}
				width={400}
				visible={this.state.visible}
				onOk={this.handleOk}
				onCancel={this.handleCancel}>
				<div className='si-date-picker-modal-body'>
					<div className='si-date-picker-row-from'>
						<span style={{width: 46}}>From: </span>
						<DatePicker value={moment(this.state.startDate)} allowClear={false} onChange={(date) => this.setState({startDate: date.startOf('day')})} />
					</div>
					<div className='si-date-picker-row-to'>
						<span style={{width: 46}}>To: </span>
						<DatePicker value={moment(this.state.endDate)} allowClear={false} onChange={(date) => this.setState({endDate: date.endOf('day')})}/>
					</div>
				</div>
			</Modal>
	</div>
	}
}