import * as React from 'react'
import {robins} from 'src/robins'
import {connectRobin} from '@simplus/robin-react'
import get from 'lodash/get'
import {
	numberFormater,
	relative,
	Download
} from '../utils'
import {
	DashboardSection,
	DashboardTab,
	KpiChart
} from 'src/components/dashboard'
const {AnalyticsRobin} = robins

interface Props {
	client: string,
	compareBenchmark: boolean,
	benchmarkMapping: any
	apiKey?: string
	showDownload?: boolean
}
@connectRobin([AnalyticsRobin])
export class FeedbackQualityMetric extends React.Component<Props> {
	render(): JSX.Element {

		const result = AnalyticsRobin.getResult(this.props.apiKey || 'quality_keymetrics') || {data: {}}
		const kpis = get(result, 'data', {})
		const excel_columns = [
			'Site levels',
			'Company',
			'Period',
			'Metric',
			'Actual',
			'Benchmark',
			'Benchmark value'
		]

		const key_metrics = ['nps', 'sentiments', 'complaints', 'compliments']
		const key_metrics_mapping = {
			'nps': 'NPS',
			'sentiments': 'Sentiment score',
			'complaints': 'Complaints',
			'compliments': 'Compliments'
		}

		const excel_data = key_metrics.map( (metric) => {
			const output = {
				client: this.props.client,
				period: `${result.startdate} to ${result.enddate}`,
				metric: key_metrics_mapping[metric],
				value: metric === 'sentiments' ? numberFormater((kpis[`${metric}`] * 100) || 0) : numberFormater(kpis[`${metric}`] || 0),
				benchmark: result.benchmark ? this.props.benchmarkMapping[result.benchmark[0]] || result.benchmark[0] : `${result.benchmarkstartdate} to ${result.benchmarkenddate}`,
				benchmark_value: metric === 'sentiments' ? numberFormater((kpis[`${metric}benchmark`] * 100) || 0) : numberFormater(kpis[`${metric}benchmark`] || 0)
			}
			return output
		})

		const chart = <KpiChart
			kpis={[
				{
					title: 'NPS',
					loading: AnalyticsRobin.isLoading(this.props.apiKey || 'quality_keymetrics'),
					error: AnalyticsRobin.isError(this.props.apiKey || 'quality_keymetrics'),
					data : numberFormater(kpis.nps),
					footer: this.props.compareBenchmark ? kpis.npsbenchmark ? `${relative(kpis.nps, kpis.npsbenchmark)} Vs Benchmark (${numberFormater(kpis.npsbenchmark)})` : 'NO BENCHMARK' : ''
				}, {
					title: 'Sentiment score',
					loading: AnalyticsRobin.isLoading(this.props.apiKey || 'quality_keymetrics'),
					error: AnalyticsRobin.isError(this.props.apiKey || 'quality_keymetrics'),
					data : numberFormater(kpis.sentiments * 100),
					footer: this.props.compareBenchmark ? kpis.sentimentsbenchmark ? `${relative(kpis.sentiments, kpis.sentimentsbenchmark)} Vs Benchmark (${numberFormater(kpis.sentimentsbenchmark * 100)})` : 'NO BENCHMARK' : ''
				}, {
					title: 'Complaints',
					loading: AnalyticsRobin.isLoading(this.props.apiKey || 'quality_keymetrics'),
					error: AnalyticsRobin.isError(this.props.apiKey || 'quality_keymetrics'),
					data : numberFormater(kpis.complaints),
					footer: this.props.compareBenchmark ? kpis.complaintsbenchmark ? `${relative(kpis.complaints, kpis.complaintsbenchmark)} Vs Benchmark (${numberFormater(kpis.complaintsbenchmark)})` : 'NO BENCHMARK' : ''
				}, {
					title : 'Compliments',
					loading: AnalyticsRobin.isLoading(this.props.apiKey || 'quality_keymetrics'),
					error: AnalyticsRobin.isError(this.props.apiKey || 'quality_keymetrics'),
					data : numberFormater(kpis.compliments),
					footer: this.props.compareBenchmark ? kpis.complimentsbenchmark ? `${relative(kpis.compliments, kpis.complimentsbenchmark)} Vs Benchmark (${numberFormater(kpis.complimentsbenchmark)})` : 'NO BENCHMARK' : ''
				}
			]}
		/>
		return <DashboardSection
			download={this.props.showDownload ? <Download
				columns={excel_columns}
				data={excel_data}
				chart={chart}
				name={`${this.props.client} - Quality - Feedback metrics`}/> : null}
			section={{
				title : 'Feedback quality metrics',
				tooltip : 'Service quality measures'
			}}>
			<DashboardTab>
				{chart}
			</DashboardTab>
		</DashboardSection>
	}
}