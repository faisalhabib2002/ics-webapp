import * as React from 'react'
import {
	DashboardContainer,
	Dashboard,
	DashboardFilter,
	DashboardSection,
} from 'src/components/dashboard'
import * as moment from 'moment'
import * as queryString from 'query-string'
import defaults from 'lodash/defaults'
import {Loader} from '@simplus/siui'
import { Icon, Tabs } from 'antd'
const { TabPane } = Tabs;
import {connectRobin} from '@simplus/robin-react'
import { RouteComponentProps } from 'react-router-dom'
import notification from 'antd/lib/notification'
import Modal from 'antd/lib/modal'
import AntDatePicker from 'antd/lib/date-picker'
import {robins} from 'src/robins'
import { DateRangeFilter, ListToTree, getTopActiveNodes } from '../utils';
import {hasPermission, noPermission} from 'src/utils'
import {ErrorBoundary} from 'src/utils/ErrorBoundary'
import {FeedbackQualityMetric} from './FeedbackQualityMetric'
import {ComplaintsWordCloud} from './ComplaintsWordCloud'
import {ComplimentsWordCloud} from './ComplimentsWordCloud'
const {clientSettings, AnalyticsRobin, PermissionsRobin, siteLevelsRobin, ClientsRobin} = robins

@connectRobin([clientSettings, AnalyticsRobin, PermissionsRobin, siteLevelsRobin, ClientsRobin])
export class Quality extends React.Component<RouteComponentProps<{id: string}>> {
	state = {filter: moment('2018-01-01'), compareBenchmark:  true, customBenchmark:  false,  sitelevelBenchmark: false, benchmarkStartDate: moment(), benchmarkEndDate: moment()}
	unlisten?: () => void = undefined
	componentWillMount(): void {
		const dateRange = queryString.parse(location.hash.split('?')[1]).dateRange || 'month'
		const startDate = queryString.parse(location.hash.split('?')[1]).startDate || moment().startOf('month').subtract(1, dateRange).format('YYYY-MM-DD');
		const endDate = queryString.parse(location.hash.split('?')[1]).endDate || moment().subtract(1, dateRange).endOf(dateRange).format('YYYY-MM-DD');
		
		ClientsRobin.get(`${this.props.match.params.id}`, `/${this.props.match.params.id}`)
		siteLevelsRobin.when(siteLevelsRobin.findOne(this.props.match.params.id)).then(() => {
			clientSettings.when(clientSettings.findOne(this.props.match.params.id)).then(() => {
				const settings = clientSettings.getModel();
				if (settings[0]) {
					const reportingCycle = moment(settings[0].reportingCycle)
					if (!reportingCycle.isSame(this.state.filter, 'd')) {
						this.setState({filter: reportingCycle})
					}
					else notification.info({ type:'info', message: 'Reporting Period', description: <div><div>Start Date: {moment(startDate).format('YYYY-MM-DD')}</div><div>End Date: {moment(endDate).format('YYYY-MM-DD')}</div></div> })
				} else notification.info({ type:'info', message: 'Reporting Period', description: <div><div>Start Date: {moment(startDate).format('YYYY-MM-DD')}</div><div>End Date: {moment(endDate).format('YYYY-MM-DD')}</div></div> })
				this.applyFilters()
			}).catch( err => {
				notification.error({type: 'error', message: 'Could not load settings data !', description: 'There was an error during the settings api execution.'})
			})
			this.unlisten = this.props.history.listen((location, action) => {
				if (action === 'POP')
					this.applyFilters()
			});
		})
	}
	applyFilters(): void {
		const benchmark = queryString.parse(location.hash.split('?')[1]).benchmark || 'Previous range'
		const dateRange = queryString.parse(location.hash.split('?')[1]).dateRange || 'month'

		const startDate = queryString.parse(location.hash.split('?')[1]).startDate || moment().startOf('month').subtract(1, dateRange).format('YYYY-MM-DD');
		const endDate = queryString.parse(location.hash.split('?')[1]).endDate || moment().subtract(1, dateRange).endOf(dateRange).format('YYYY-MM-DD');
		const benchmarkStartDate = queryString.parse(location.hash.split('?')[1]).benchmarkStartDate || moment().startOf('month').subtract(1, dateRange).format('YYYY-MM-DD');
		const benchmarkEndDate = queryString.parse(location.hash.split('?')[1]).benchmarkEndDate || moment().endOf('month').subtract(1, dateRange).format('YYYY-MM-DD');

		const clientID = this.props.match.params.id
		const siteMapping = siteLevelsRobin.isLoading(siteLevelsRobin.ACTIONS.FIND_ONE) ? [] : (siteLevelsRobin.getModel() || []).map(s => `clients/${clientID}` === s.branchTo ? ({...s, branchTo: null}) : s )
		let siteLevelsAssigned = false;
		siteMapping.map(site => {if(site.disabled) siteLevelsAssigned = true })
		const siteTreeData = ListToTree(siteMapping)
		const nodes = [];
		getTopActiveNodes(siteTreeData, nodes)

		const defaultSites = siteLevelsAssigned ? nodes : ['']
		const sitelevels = queryString.parse(location.hash.split('?')[1]).site || defaultSites
		const benchmarksitelevels = queryString.parse(location.hash.split('?')[1]).benchmarksitelevel || defaultSites


		const options: any = {
			startdate : startDate,
			enddate : endDate,
			sitelevels: Array.isArray(sitelevels) ? sitelevels.filter(item => item !== 'All') : sitelevels === 'All' ? defaultSites : sitelevels.split(),
			client : this.props.match.params.id,
			benchmarksitelevels: Array.isArray(benchmarksitelevels) ? benchmarksitelevels.filter(item => item !== 'All') : benchmarksitelevels === 'All' ? defaultSites : benchmarksitelevels.split()
		}
		if (benchmark === 'Previous range') {
			if (dateRange === 'month') {
				options.benchmarkstartdate = moment(startDate).subtract(1, dateRange).format('YYYY-MM-DD')
				options.benchmarkenddate = moment(endDate).subtract(1, dateRange).endOf('month').format('YYYY-MM-DD')
			} else if (dateRange === 'YTD') { // YTD option should use same dates of previous year for benchmark
				options.benchmarkstartdate = moment(startDate).subtract(1, 'year').format('YYYY-MM-DD')
				options.benchmarkenddate = moment(endDate).subtract(1, 'year').format('YYYY-MM-DD')
			} else {
				options.benchmarkstartdate = moment(startDate).subtract(1, dateRange).format('YYYY-MM-DD')
				options.benchmarkenddate = moment(endDate).subtract(1, dateRange).format('YYYY-MM-DD')
			}
		} else if (benchmark === 'custom') {
			options.benchmarkstartdate = moment(benchmarkStartDate).format('YYYY-MM-DD')
			options.benchmarkenddate = moment(benchmarkEndDate).format('YYYY-MM-DD')
		} else {
			options.benchmark = benchmark.split(',')
		}
		AnalyticsRobin.when(AnalyticsRobin.post('quality_keymetrics', '/quality/sentiments', options)).catch( err => {
			notification.error({type: 'error', message: 'Could not load quality keymetrics data !', description: 'There was an error during the analytics api execution.'})
		})
		AnalyticsRobin.when(AnalyticsRobin.post('quality_wordcloud_compliments', '/quality/wordcloud', {wordcategory : 'compliments',...options})).catch( err => {
			notification.error({type: 'error', message: 'Could not load quality wordcloud data !', description: 'There was an error during the analytics api execution.'})
		})
		AnalyticsRobin.when(AnalyticsRobin.post('quality_wordcloud_complaints', '/quality/wordcloud', {wordcategory : 'complaints',...options})).catch( err => {
			notification.error({type: 'error', message: 'Could not load quality wordcloud data !', description: 'There was an error during the analytics api execution.'})
		})
	}
	componentWillUnmount(): void {
		if (this.unlisten)
		this.unlisten();
	}
	customBencharkOk = (e) => {
		this.setState({
			customBenchmark: false,
		});
		this.pageRedirect({
			benchmark: 'custom',
			benchmarkStartDate: moment(this.state.benchmarkStartDate).format('YYYY-MM-DD'),
			benchmarkEndDate: moment(this.state.benchmarkEndDate).format('YYYY-MM-DD')
		}, true)
	}

	sitelevelBencharkOk = (e) => {
		this.setState({
			sitelevelBenchmark: false,
		});
		this.pageRedirect({
			benchmark: 'clients'
		}, true)
	}
	customBencharkCancel = (e) => {
		this.setState({customBenchmark: false});
	}

	sitelevelBencharkCancel = (e) => {
		this.setState({sitelevelBenchmark: false});
	}
	pageRedirect(update: any, refresh: boolean = false): void {
		const state = {
			...this.urlState(),
			...update}
		this.props.history.push(`${this.props.location.pathname}?${queryString.stringify(state)}`)
		if (refresh)
			this.applyFilters()
	}
	urlState(): any {
		return queryString.parse(location.hash.split('?')[1])
	}
	render(): JSX.Element {
		const clientID = this.props.match.params.id
		const clientName = (ClientsRobin.getResult(`${clientID}`) || {name: ''}).Name
		// Query Strings
		let { dateRange, site, benchmark, comparison, tab,  benchmarksitelevel} = defaults(this.urlState(), {
			dateRange : 'month',
			site: 'All',
			benchmarksitelevel: ['All'],
			benchmark: 'Previous range',
			comparison: 'absolute',
			tab: '0/.0'
		})
		const benchmarkFilter = Array.isArray(benchmarksitelevel) ? benchmarksitelevel.filter(item => item !== 'All') :  benchmarksitelevel.split()
		if(benchmarkFilter && benchmarkFilter.filter(item => item !== 'All').length) {
			benchmark = 'site'
		}
		const displayRelativeValues = comparison === 'relative'


		const siteMapping = siteLevelsRobin.isLoading(siteLevelsRobin.ACTIONS.FIND_ONE) ? [] : (siteLevelsRobin.getModel() || []).map(s => `clients/${clientID}` === s.branchTo ? ({...s, branchTo: null}) : s )
		const treeData = ListToTree(siteMapping)

		const compareBenchmark = this.state.compareBenchmark && !displayRelativeValues
		const benchmarkMapping = {
			'Previous range': 'Previous period',
			'industry': 'Sector',
			'SegmentSize': 'Size',
			'clients': 'Company',
			'custom': 'Custom range',
			'site': 'Site levels'
		}

		const qualityDashboard = (
			<DashboardContainer style={{padding: '1rem', position: 'relative'}}>
				{(clientSettings.isLoading(clientSettings.ACTIONS.FIND_ONE) || siteLevelsRobin.isLoading(siteLevelsRobin.ACTIONS.FIND_ONE) || clientSettings.isError(clientSettings.ACTIONS.FIND_ONE) || siteLevelsRobin.isError(siteLevelsRobin.ACTIONS.FIND_ONE)) ? <Loader error={clientSettings.isError(clientSettings.ACTIONS.FIND_ONE) || siteLevelsRobin.isError(siteLevelsRobin.ACTIONS.FIND_ONE)}/> : null}
				<div className='dashboard-top-bar'>
					<div style={{display: 'flex', padding: '1rem', alignItems:'flex-end'}}>
					<DashboardFilter
							width={200}
							type='tree'
							title='SITE'
							defaultValue={site}
							style={{marginRight: '1rem', display: 'flex', flexDirection: 'column'}}
							treeData={treeData}
							onChange={(selected) => this.pageRedirect({site: selected}, true)
						}/>
						<DateRangeFilter value={dateRange} customOption financialStartYear={moment(this.state.filter)} label='DATE RANGE' style={{minWidth: 130, marginRight: '1rem'}}
							onChange={(selected) => {
								notification.info({ type:'info', message: 'Reporting Period', description: <div><div>Start Date: {moment(selected.startDate).format('YYYY-MM-DD')}</div><div>End Date: {moment(selected.endDate).format('YYYY-MM-DD')}</div></div> })
									this.pageRedirect({
									dateRange: selected.value,
									startDate: moment(selected.startDate).format('YYYY-MM-DD'),
									endDate: moment(selected.endDate).format('YYYY-MM-DD')
								}, true)
							}
						}/>
						<DashboardFilter
							width={150}
							type='single'
							text='Compared to'
							title='BENCHMARK'
							defaultValue={benchmark}
							options={[{value: 'Previous range', text: 'Previous period'}, {value: 'industry', text: 'Sector'}, {value: 'SegmentSize', text: 'Size'},{value: 'clients', text: 'Company'}, {value: 'custom', text: 'Custom range'},{value: 'site', text: 'Site levels'}]}
							onChange={(selected) => {
								if (selected === 'custom') {
									this.pageRedirect({benchmarksitelevel: undefined}, false)
									this.setState({ customBenchmark: true });
								} else if (selected === 'site') {
									this.setState({ sitelevelBenchmark: true });
								} else {
									this.pageRedirect({benchmarksitelevel: undefined}, false)
									this.pageRedirect({ benchmark: selected }, true);
								}
							}
							}/>
						<DashboardFilter
							type='switch'
							text='Display Benchmark'
							checked={compareBenchmark}
							onChange={(selected) => this.setState({compareBenchmark: selected})
							}/>
					</div>
				</div>
				<Modal
					className='si-date-picker-custom'
					title={<span style={{fontSize: '20px', fontWeight: 300}}>Custom range</span>}
					width={400}
					visible={this.state.customBenchmark}
					onOk={this.customBencharkOk}
					onCancel={this.customBencharkCancel}>
					<div className='si-date-picker-modal-body'>
						<div className='si-date-picker-row-from'>
							<span style={{width: 46}}>From: </span>
							<AntDatePicker allowClear={false} onChange={(date) => this.setState({benchmarkStartDate: date.startOf('day')})} />
						</div>
						<div className='si-date-picker-row-to'>
							<span style={{width: 46}}>To: </span>
							<AntDatePicker allowClear={false} onChange={(date) => this.setState({benchmarkEndDate: date.endOf('day')})}/>
						</div>
					</div>
				</Modal>
				<Modal
					className='si-date-picker-custom'
					title={<span style={{fontSize: '20px', fontWeight: 300}}>Site levels </span>}
					width={400}
					visible={this.state.sitelevelBenchmark}
					onOk={this.sitelevelBencharkOk}
					onCancel={this.sitelevelBencharkCancel}>
						<DashboardFilter
							width={300}
							type='tree'
							title='SITE'
							defaultValue={benchmarksitelevel}
							style={{marginRight: '1rem', display: 'flex', flexDirection: 'column'}}
							treeData={treeData}
							onChange={(selected) => this.pageRedirect({benchmarksitelevel: selected}, false)
						}/>
				</Modal>
				<Dashboard
					structure={{
						items : [4, 4],
						size : 4
					}}>
						<FeedbackQualityMetric
						client={clientName}
						showDownload={true}
						compareBenchmark={compareBenchmark}
						benchmarkMapping={benchmarkMapping}/>
					<DashboardSection
					defaultActiveTab={tab}
					onTabChange={(selected) => this.pageRedirect({tab: selected})}
					section={{
						tabTitles : ['Complaints', 'Compliments'],
						tabTooltips : [
							'Nature of complaints',
							'Nature of compliments'
						],
						title : 'Feedback word clouds',
						tooltip : 'Qualitative customer feedback'
					}}>
						<ComplaintsWordCloud 
							client={clientName}
							showDownload={true}/>
						<ComplimentsWordCloud 
							client={clientName}
							showDownload={true}/>
					</DashboardSection>
				</Dashboard>
			</DashboardContainer>
		)
		return(
			<ErrorBoundary>
				<div className='dashboard-nav'>
				<Tabs activeKey="4"
					onChange={(current) => {
						switch (current) {
							case "0":
								this.props.history.push(`/client/${clientID}/dashboard/engagement`)
								break;
							case "1":
								this.props.history.push(`/client/${clientID}/dashboard/health`)
							break;
							case "2":
								this.props.history.push(`/client/${clientID}/dashboard/effectiveness`)
							case "3":
									this.props.history.push(`/client/${clientID}/dashboard/roi`)
							break;
						}
					}}
					>
						<TabPane tab={<span><Icon type="usergroup-add" />Engagement</span>}key="0" disabled={!hasPermission('/view/dashboard/engagement', PermissionsRobin.getResult('own-permissions'))}/>
						<TabPane tab={<span><Icon type="heart" />Health</span>} key="1" disabled={!hasPermission('/view/dashboard/health', PermissionsRobin.getResult('own-permissions'))}/>
						<TabPane tab={<span><Icon type="dashboard" />Effectiveness</span>} key="2" disabled={!hasPermission('/view/dashboard/effectiveness', PermissionsRobin.getResult('own-permissions'))}/>
						<TabPane tab={<span><Icon type="line-chart" />ROI</span>} key="3" disabled={!hasPermission('/view/dashboard/roi', PermissionsRobin.getResult('own-permissions'))}/>
						<TabPane tab={<span><Icon type="like" />Quality</span>} key="4">
							{hasPermission('/view/dashboard/quality', PermissionsRobin.getResult('own-permissions')) ? qualityDashboard : noPermission(PermissionsRobin.isLoading('own-permissions'))}
						</TabPane>
					</Tabs>
				</div>
			</ErrorBoundary>
		)
	}

}
export default Quality;