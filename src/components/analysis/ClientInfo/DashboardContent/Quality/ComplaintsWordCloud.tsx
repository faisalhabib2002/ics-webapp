import * as React from 'react'
import {robins} from 'src/robins'
import {connectRobin} from '@simplus/robin-react'
import get from 'lodash/get'
import {
	Download
} from '../utils'
import {
	DashboardTab,
	WordCloud
} from 'src/components/dashboard'
const {AnalyticsRobin} = robins

interface Props {
	client: string,
	apiKey?: string
	showDownload?: boolean
}
@connectRobin([AnalyticsRobin])
export class ComplaintsWordCloud extends React.Component<Props> {
	render(): JSX.Element {
		const result = AnalyticsRobin.getResult(this.props.apiKey || 'quality_wordcloud_complaints') || {data: []}
		const data = get(result, 'data', [])

		const chart = <WordCloud words={data} />
		const excel_columns = [
			'Site levels',
			'Company',
			'Period',
			'Complaint',
			'Count'
		]

		const excel_data = (data).sort((a: any, b: any) => b.score - a.score).map(item => {
			const output  = {
				client: this.props.client,
				period: `${result.startdate} to ${result.enddate}`,
				name: item.word,
				value: item[`count`] || 0
			}
			return output
		})
		return <DashboardTab>
			<div style={{display: 'flex', justifyContent: 'flex-end', marginBottom: '0.5rem', alignItems: 'center'}}>
				{this.props.showDownload ? <Download
					columns={excel_columns}
					data={excel_data}
					chart={chart}
					name={`${this.props.client} - Quality - Complaints`}/>
					: null}
			</div>
			{chart}
		</DashboardTab>
	}
}