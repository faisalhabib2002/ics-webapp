import * as React from 'react'
import {robins} from 'src/robins'
import {connectRobin} from '@simplus/robin-react'
import get from 'lodash/get'
import {
	numberFormater,
	shortNumber,
	relative,
	currency,
	Download
} from '../utils'
import {
	DashboardSection,
	DashboardTab,
	KpiChart
} from 'src/components/dashboard'
const {AnalyticsRobin} = robins

interface Props {
	client: string,
	compareBenchmark: boolean,
	benchmarkMapping: any
	apiKey?: string
	showDownload?: boolean
}
@connectRobin([AnalyticsRobin])
export class ROIMetricsKPI extends React.Component<Props> {
	render(): JSX.Element {

		const result = AnalyticsRobin.getResult(this.props.apiKey || 'roi_keymetrics') || {data: {}}
		const keyMetrics = get(result, 'data', {})

		const excel_columns = [
			'Site levels',
			'Company',
			'Period',
			'Monetary Gains',
			'Contract value',
			'ROI',
			'Benchmark',
			'Benchmark Monetary Gains',
			'Benchmark ROI'
		]

		const excel_data = [{
			client: this.props.client,
			period: `${result.startdate} to ${result.enddate}`,
			gains: shortNumber(keyMetrics.gains, currency, 1),
			contract_value: shortNumber(keyMetrics.contractvalue, currency, 1),
			roi: numberFormater(keyMetrics.roi),
			benchmark: result.benchmark ? this.props.benchmarkMapping[result.benchmark[0]] || result.benchmark[0] : `${result.benchmarkstartdate} to ${result.benchmarkenddate}`,
			benchmark_gains: shortNumber(keyMetrics.gainsbenchmark, currency, 1),
			benchmark_roi: numberFormater(keyMetrics.roibenchmark)
		}]

		const chart = <KpiChart
			kpis={[
				{
					title : 'Monetary gains',
					data : shortNumber(keyMetrics.gains, currency, 1),
					loading: AnalyticsRobin.isLoading(this.props.apiKey || 'roi_keymetrics'),
					error: AnalyticsRobin.isError(this.props.apiKey || 'roi_keymetrics'),
					footer: this.props.compareBenchmark ? keyMetrics.gainsbenchmark ? `${relative(keyMetrics.gains, keyMetrics.gainsbenchmark)} Vs Benchmark (${shortNumber(keyMetrics.gainsbenchmark, currency, 1)})` : 'NO BENCHMARK' : ''
				}, {
					title: 'Contract value',
					data : shortNumber(keyMetrics.contractvalue, currency, 1),
					loading: AnalyticsRobin.isLoading(this.props.apiKey || 'roi_keymetrics'),
					error: AnalyticsRobin.isError(this.props.apiKey || 'roi_keymetrics'),
				}, {
					title : 'ROI',
					data : numberFormater(keyMetrics.roi),
					loading: AnalyticsRobin.isLoading(this.props.apiKey || 'roi_keymetrics'),
					error: AnalyticsRobin.isError(this.props.apiKey || 'roi_keymetrics'),
					footer: this.props.compareBenchmark ? keyMetrics.roibenchmark ? `${relative(keyMetrics.roi, keyMetrics.roibenchmark)} Vs Benchmark (${numberFormater(keyMetrics.roibenchmark)})` : 'NO BENCHMARK' : ''
				}
			]}
		/>
		return <DashboardSection
			download={this.props.showDownload ? <Download
				columns={excel_columns}
				data={excel_data}
				chart={chart}
				name={`${this.props.client} - ROI - Investment`}/> : null}
			section={{
			title : 'Return on investment metrics',
			tooltip : 'Monetary gains due to intervention'
		}}>
			<DashboardTab>
				{chart}
			</DashboardTab>
		</DashboardSection>
	}
}