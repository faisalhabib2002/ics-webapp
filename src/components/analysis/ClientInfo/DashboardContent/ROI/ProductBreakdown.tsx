import * as React from 'react'
import {robins} from 'src/robins'
import {connectRobin} from '@simplus/robin-react'
import get from 'lodash/get'
import {
	Download,
	shortNumber,
	currency,
	numberFormater
} from '../utils'
import {
	DashboardSection,
	DashboardTab,
	DashboardFilter,
	BarChart
} from 'src/components/dashboard'
const {AnalyticsRobin} = robins

interface Props {
	client: string,
	compareBenchmark: boolean,
	benchmark: string,
	benchmarkMapping: any,
	productBreakdown: string,
	splitFilter(filter: string): void
	apiKey?: string
	showDownload?: boolean
	showLabel?: boolean
	dod?: any
}
@connectRobin([AnalyticsRobin])
export class ProductBreakdown extends React.Component<Props> {
	render(): JSX.Element {
		const result = AnalyticsRobin.getResult(this.props.apiKey || 'roi_products') || {data: []}
		const splitByProducts_orig = get(result, 'data', [])
		const splitByProducts = splitByProducts_orig.map( o => ({...o}))

		const chart = <BarChart
		horizontal={true}
		bars={this.props.compareBenchmark ? [this.props.productBreakdown, `${this.props.benchmarkMapping[this.props.benchmark]} - ${this.props.productBreakdown} benchmark` ] : [this.props.productBreakdown]}
		loading={AnalyticsRobin.isLoading(this.props.apiKey || 'roi_products')}
		error={AnalyticsRobin.isError(this.props.apiKey || 'roi_products')}
		config={{showLegend: this.props.compareBenchmark}}
		showLabel={this.props.showLabel}
		tooltip={(data) => shortNumber(data)}
		data={splitByProducts.map( k => {
			return { x : k.key,
				Gains: k.gains,
				'Previous period - Gains benchmark': k.gainsbenchmark,
				'Sector - Gains benchmark': k.gainsbenchmark,
				'Size - Gains benchmark': k.gainsbenchmark,
				'Company - Gains benchmark': k.gainsbenchmark,
				'Site levels - Gains benchmark': k.gainsbenchmark,
				'Custom range - Gains benchmark': k.gainsbenchmark,
				'ROI': k.roi,
				'Previous period - ROI benchmark': k.roibenchmark,
				'Sector - ROI benchmark': k.roibenchmark,
				'Size - ROI benchmark': k.roibenchmark,
				'Company - ROI benchmark': k.roibenchmark,
				'Site levels - ROI benchmark': k.roibenchmark,
				'Custom range - ROI benchmark': k.roibenchmark
			}
		})}
	/>
		const excel_columns = [
			'Site levels',
			'Company',
			'Period',
			'Split by',
			'Monetary Gains',
			'Contract value',
			'ROI',
			'Benchmark',
			'Benchmark Monetary Gains',
			'Benchmark ROI'
		]

		const excel_data = splitByProducts_orig.sort((a: any, b: any) => {
			if (this.props.productBreakdown === 'Gains')
				return b.gains - a.gains
			else
				return b.roi - a.roi
		}).map(item => {
			const output  = {
				client: this.props.client,
				period: `${result.startdate} to ${result.enddate}`,
				name: item.key,
				gains: shortNumber(item[`gains`], currency, 1),
				contract:  shortNumber(item[`contractvalue`], currency, 1),
				roi: (item[`roi`] || 0).toFixed(2),
				benchmark: result.benchmark ? this.props.benchmarkMapping[result.benchmark[0]] || result.benchmark[0] : `${result.benchmarkstartdate} to ${result.benchmarkenddate}`,
				benchmark_gains: shortNumber(item[`gainsbenchmark`], currency, 1),
				benchmark_roi: (item[`roibenchmark`] || 0).toFixed(2)
			}
			return output
		})
		return <DashboardSection
				download={this.props.showDownload ? <Download
					columns={excel_columns}
					data={excel_data}
					chart={chart}
					name={`${this.props.client} - ROI - Product breakdown`}/> : null}
				section={{
				title : 'Product breakdown',
				tooltip : 'Monetary gains by product'
			}}
			dod={this.props.dod}>
			<DashboardTab>
				<div style={{display: 'flex', alignItems: 'center'}}>
					<DashboardFilter width={150}
					type='single'
					defaultValue={this.props.productBreakdown}
					downloadPreview={this.props.dod && this.props.dod.downloadPreview}
					options={[
						{value: 'Gains', text: 'Monetary Gains'},
						{value: 'ROI', text: 'ROI'}]}
					onChange={(selected) => this.props.splitFilter(selected)
					}/>
					<span style={{marginLeft: '1rem'}}> split by product</span>
				</div>
				{chart}
			</DashboardTab>
		</DashboardSection>
	}
}