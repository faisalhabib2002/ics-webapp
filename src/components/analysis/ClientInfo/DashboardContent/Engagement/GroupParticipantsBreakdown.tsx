import * as React from 'react'
import {robins} from 'src/robins'
import {connectRobin} from '@simplus/robin-react'
import get from 'lodash/get'
import keys from 'lodash/keys'
import {
	capitalizeFirstLetter,
	Download
} from '../utils'
import {
	DashboardFilter,
	DashboardTab,
	BarChart
} from 'src/components/dashboard'
const {AnalyticsRobin} = robins

interface Props {
	client: string,
	displayRelativeValues: boolean,
	filter: string,
	filterValue: string,
	compareBenchmark: boolean,
	benchmark: string,
	benchmarkMapping: any,
	splitFilter(filter: string): void,
	filterOn(filter: string): void,
	apiKey?: string,
	height?: number
	showDownload?: boolean
	showLabel?: boolean
	downloadPreview?: boolean
	loading?: boolean
}
@connectRobin([AnalyticsRobin])
export class GroupParticipantsBreakdown extends React.Component<Props> {
	render(): JSX.Element {
		const keymetrics_breakdown_result = AnalyticsRobin.getResult(this.props.apiKey || 'group_breakdown') || {data: []}
		const products_breakdown_result = (AnalyticsRobin.getResult('products_breakdown') || {data: []}).data
		const problem_cluster_breakdown_result = (AnalyticsRobin.getResult('problem_cluster_breakdown') || {data: []}).data
		const filterList = [{value: 'all', text: 'All'}]
		const keymetrics_data_orig = get(keymetrics_breakdown_result, 'data', [])
		const keymetrics_data = keymetrics_data_orig.map( o => ({...o}))

		// console.log()
		for (const item of problem_cluster_breakdown_result) {
			if (this.props.filter === 'problem' && item.groupparticipants > 0) {
			  filterList.push({value: item.key, text: item.key});
			}
		  }
		  for (const item of products_breakdown_result) {
			if (this.props.filter === 'service' && item.groupparticipants > 0) {
			  filterList.push({value: item.key, text: item.key});
			}
		  }
		if (this.props.displayRelativeValues) {
			[keymetrics_data].forEach( o => {
				const os = Array.isArray(o) ? o : [o]
				os.forEach( e => {
					keys(e).filter( k => /relative$/.test(k)).map( k => k.replace(/relative$/, '')).forEach( k => {
						e[k] = e[`${k}relative`] * 100
					})
				})
			})
		}

		const group_participants_excel_data = (keymetrics_data_orig).sort((a: any, b: any) => b.groupparticipants - a.groupparticipants).map(item => {
			const output  = {
				client: this.props.client,
				period: `${keymetrics_breakdown_result.startdate} to ${keymetrics_breakdown_result.enddate}`,
				name: item.key,
				value: item[`groupparticipants`] || 0,
				benchmark: keymetrics_breakdown_result.benchmark ? this.props.benchmarkMapping[keymetrics_breakdown_result.benchmark[0]] || keymetrics_breakdown_result.benchmark[0]: `${keymetrics_breakdown_result.benchmarkstartdate} to ${keymetrics_breakdown_result.benchmarkenddate}`,
				benchmark_value: item[`groupparticipantsbenchmark`] || 0
			}
			return output
		})

		const group_participants_chart = <BarChart
			loading={AnalyticsRobin.isLoading(this.props.apiKey || 'group_breakdown') || this.props.loading}
			error={AnalyticsRobin.isError(this.props.apiKey || 'group_breakdown')}
			bars={this.props.compareBenchmark ? ['Actual', `${this.props.benchmarkMapping[this.props.benchmark]} benchmark`] : ['Actual']}
			config={{showLegend: this.props.compareBenchmark}}
			minHeight={this.props.height}
			showLabel={this.props.showLabel}
			data={keymetrics_data.sort((a, b) => b.groupparticipants - a.groupparticipants).map( o => {
				return {
					x: o.key || 'Uncategorised',
					'Actual': o.groupparticipantsrelative * 100,
					'Actual_absolute': o.groupparticipants,
					'Previous period benchmark':  o.groupparticipantsbenchmarkrelative * 100,
					'Previous period benchmark_absolute': o.groupparticipantsbenchmark,
					'Sector benchmark': o.groupparticipantsbenchmarkrelative * 100,
					'Sector benchmark_absolute': o.groupparticipantsbenchmark,
					'Company benchmark': o.groupparticipantsbenchmarkrelative * 100,
					'Site levels benchmark': o.groupparticipantsbenchmarkrelative * 100,
					'Site levels benchmark_absolute': o.groupparticipantsbenchmark,
					'Company benchmark_absolute': o.groupparticipantsbenchmark,
					'Size benchmark': o.groupparticipantsbenchmarkrelative * 100,
					'Size benchmark_absolute': o.groupparticipantsbenchmark,
					'Custom range benchmark': o.groupparticipantsbenchmarkrelative * 100,
					'Custom range benchmark_absolute': o.groupparticipantsbenchmark,
				}
			})}
			hideEmptyBars
			yaxisUnits={'%'}
			relativeToTotal={!this.props.displayRelativeValues}
			/>
		// Keymetrics Section
		const keymetrics_excel_columns = [
			'Company',
			'Period',
			capitalizeFirstLetter(this.props.filter),
			'Group participants',
			'Benchmark',
			'Benchmark value'
		]

		return <DashboardTab>
		<div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
			<div style={{display: 'flex'}}>
				<DashboardFilter
					width={140}
					type='single'
					text='Split by'
					defaultValue={this.props.filter}
					downloadPreview={this.props.downloadPreview}
					options={[
						{value: 'service', text: 'Service'},
						{value: 'product', text: 'Products'},
						{value: 'employement', text: 'Employment'},
						{value: 'problemcluster', text: 'Problem Cluster'},
						{value: 'problem', text: 'Problem'},
						{value: 'protocol', text: 'Protocol'},
						{value: 'origin', text: 'Origin'},
						{value: 'severity', text: 'Severity'}]}
						onChange={(selected) => this.props.splitFilter(selected)}/>
				{(this.props.filter === 'service' || this.props.filter === 'problem') ? 
					<DashboardFilter
						width={140}
						type='single'
						text={this.props.filter === 'service'  ? 'Product': 'Problem Cluster'}
						defaultValue={this.props.filterValue}
						downloadPreview={this.props.downloadPreview}
						options={filterList}
						onChange={(selected) => this.props.filterOn(selected)}/>
				:null}
			</div>
			{this.props.showDownload ? <Download
				columns={keymetrics_excel_columns}
				data={group_participants_excel_data}
				chart={group_participants_chart}
				name={`${this.props.client} - Engagement - Group participants breakdown`}
			/>
			:null}
			
		</div>
		{group_participants_chart}
	</DashboardTab>
	}
}