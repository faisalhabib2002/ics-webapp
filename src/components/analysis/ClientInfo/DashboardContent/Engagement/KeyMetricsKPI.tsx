import * as React from 'react'
import {robins} from 'src/robins'
import {connectRobin} from '@simplus/robin-react'
import get from 'lodash/get'
import keys from 'lodash/keys'
import {
	percentage,
	relative,
	numberFormater,
	Download
} from '../utils'
import {
	DashboardSection,
	DashboardTab,
	KpiChart
} from 'src/components/dashboard'
import { Tooltip } from 'antd'
const {AnalyticsRobin} = robins

interface Props {
	client: string,
	displayRelativeValues: boolean,
	compareBenchmark: boolean,
	showDownload?: boolean
	apiKey?: string,
	benchmarkMapping: any,
	handleTrend?: any

}
@connectRobin([AnalyticsRobin])
export class KeyMetricsKPI extends React.Component<Props> {
	render(): JSX.Element {
		const key_metrics_kpi_result = AnalyticsRobin.getResult(this.props.apiKey || 'engagement_keymetrics') || {data: {}}
		const keyMetrics_orig = get(key_metrics_kpi_result, 'data', {})
		const keyMetrics = {...keyMetrics_orig}
		if (this.props.displayRelativeValues) {
			[keyMetrics].forEach( o => {
				const os = Array.isArray(o) ? o : [o]
				os.forEach( e => {
					keys(e).filter( k => /relative$/.test(k)).map( k => k.replace(/relative$/, '')).forEach( k => {
						e[k] = e[`${k}relative`] * 100
					})
				})
			})
		}

		const tooltip = 'Online Access users redirected to EAP services will only reflect in the Individual case count metric. The Online product services engaged by these redirected users will be visible in the below Key metrics breakdown -> Online Access section. '
		const numberFormaterConditional = this.props.displayRelativeValues ? percentage : numberFormater
		const key_metrics_kpi = <KpiChart
				config={{}}
				kpis={[
					{
						loading: AnalyticsRobin.isLoading(this.props.apiKey || 'engagement_keymetrics'),
						error: AnalyticsRobin.isError(this.props.apiKey || 'engagement_keymetrics'),
						title : 'Individual cases',
						data : numberFormaterConditional(keyMetrics.individualcases),
						footer: this.props.compareBenchmark ? keyMetrics.individualcasesbenchmark ? `${relative(keyMetrics.individualcases, keyMetrics.individualcasesbenchmark)} Vs Benchmark (${numberFormater(keyMetrics.individualcasesbenchmark)})` : 'NO BENCHMARK' : ''
					},
					{
						loading: AnalyticsRobin.isLoading(this.props.apiKey || 'engagement_keymetrics'),
						error: AnalyticsRobin.isError(this.props.apiKey || 'engagement_keymetrics'),
						title : 'Group Participants',
						data : numberFormaterConditional(keyMetrics.groupparticipants),
						footer : this.props.compareBenchmark ? keyMetrics.groupparticipantsbenchmark ? `${relative(keyMetrics.groupparticipants , keyMetrics.groupparticipantsbenchmark)} Vs Benchmark (${numberFormater(keyMetrics.groupparticipantsbenchmark)})` : 'NO BENCHMARK' : ''
					},
					{
						loading: AnalyticsRobin.isLoading(this.props.apiKey || 'engagement_keymetrics'),
						error: AnalyticsRobin.isError(this.props.apiKey || 'engagement_keymetrics'),
						title : 'Online Access',
						data : <Tooltip title={tooltip}>{numberFormaterConditional(keyMetrics.onlineaccess)}</Tooltip>,
						footer : this.props.compareBenchmark ? keyMetrics.onlineaccessbenchmark ? `${relative(keyMetrics.onlineaccess, keyMetrics.onlineaccessbenchmark)} Vs Benchmark (${numberFormater(keyMetrics.onlineaccessbenchmark)})` : 'NO BENCHMARK' : ''
					}
				]}
				/>
			const key_metrics_kpi_excel_columns = [
				'Company',
				'Period',
				'Individual cases',
				'Group participants',
				'Online access',
				'Benchmark',
				'Benchmark individual cases',
				'Benchmark group participant',
				'Benchmark online access',
			]

			const key_metrics_kpi_excel_data = [{
				client: this.props.client,
				period: `${key_metrics_kpi_result.startdate} to ${key_metrics_kpi_result.enddate}`,
				individual_cases: numberFormater(keyMetrics_orig.individualcases),
				group_participants: numberFormater(keyMetrics_orig.groupparticipants),
				online_access: numberFormater(keyMetrics_orig.onlineaccess),
				benchmark: key_metrics_kpi_result.benchmark ? this.props.benchmarkMapping[key_metrics_kpi_result.benchmark[0]] || key_metrics_kpi_result.benchmark[0]: `${key_metrics_kpi_result.benchmarkstartdate} to ${key_metrics_kpi_result.benchmarkenddate}`,
				individual_cases_benchmark: numberFormater(keyMetrics_orig.individualcasesbenchmark),
				group_participants_benchmark: numberFormater(keyMetrics_orig.groupparticipantsbenchmark),
				online_access_benchmark: numberFormater(keyMetrics_orig.onlineaccessbenchmark),
			}]
		return <DashboardSection
			download={this.props.showDownload ? <Download
				columns={key_metrics_kpi_excel_columns}
				data={key_metrics_kpi_excel_data}
				chart={key_metrics_kpi}
				chartWidth='75%'
				name={`${this.props.client} - Engagement - Keymetrics`}
				handleTrend={this.props.handleTrend}/> : null}
			section={{
				title : 'Key usage metrics',
				tooltip : 'Top line engagement tracking metrics'
			}}>
			<DashboardTab>
				{key_metrics_kpi}
			</DashboardTab>
		</DashboardSection>
	}
}