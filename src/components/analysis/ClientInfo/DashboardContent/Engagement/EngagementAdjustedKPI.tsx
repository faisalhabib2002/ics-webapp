import * as React from 'react'
import {robins} from 'src/robins'
import {connectRobin} from '@simplus/robin-react'
import get from 'lodash/get'
import keys from 'lodash/keys'
import {
	percentage,
	relative,
	percentageRelative,
	Download
} from '../utils'
import {
	DashboardSection,
	DashboardTab,
	KpiChart
} from 'src/components/dashboard'
const {AnalyticsRobin} = robins

interface Props {
	client: string,
	displayRelativeValues: boolean,
	compareBenchmark: boolean,
	benchmarkMapping: any,
	apiKey?: string,
	showDownload?: boolean
}
@connectRobin([AnalyticsRobin])
export class EngagementAdjustedKPI extends React.Component<Props> {
	render(): JSX.Element {
		const utilization_kpi_result = AnalyticsRobin.getResult(this.props.apiKey || 'engagement_utilization') || {data: {}}
		const engagement_utilization_orig = get(utilization_kpi_result, 'data', {})
		const engagement_utilization = {...engagement_utilization_orig}

		if (this.props.displayRelativeValues) {
			[engagement_utilization].forEach( o => {
				const os = Array.isArray(o) ? o : [o]
				os.forEach( e => {
					keys(e).filter( k => /relative$/.test(k)).map( k => k.replace(/relative$/, '')).forEach( k => {
						e[k] = e[`${k}relative`] * 100
					})
				})
			})
		}
		const utilization_kpi = <KpiChart
			kpis={[{
				loading: AnalyticsRobin.isLoading(this.props.apiKey || 'engagement_utilization'),
				error: AnalyticsRobin.isError(this.props.apiKey || 'engagement_utilization'),
				title: 'Adjusted Engagement',
				data : percentage(engagement_utilization.utilization * (this.props.displayRelativeValues ? 1 : 100)),
				footer: this.props.compareBenchmark ? engagement_utilization.utilizationbenchmark ? `${percentageRelative(engagement_utilization.utilization, engagement_utilization.utilizationbenchmark)} Vs Benchmark (${percentage(engagement_utilization.utilizationbenchmark * 100)})` : 'NO BENCHMARK' : ''
			}]}
			/>
		const utilization_kpi_excel_columns = [
			'Company',
			'Period',
			'Actual',
			'Benchmark',
			'Benchmark value',
			'Site levels'
		]
		const utilization_kpi_excel_data = [{
				client: this.props.client,
				period: `${utilization_kpi_result.startdate} to ${utilization_kpi_result.enddate}`,
				value:  percentage(engagement_utilization.utilization * (this.props.displayRelativeValues ? 1 : 100)),
				benchmark: utilization_kpi_result.benchmark ? this.props.benchmarkMapping[utilization_kpi_result.benchmark[0]] || utilization_kpi_result.benchmark[0] : `${utilization_kpi_result.benchmarkstartdate} to ${utilization_kpi_result.benchmarkenddate}`,
				benchmark_value: percentage(engagement_utilization.utilizationbenchmark * (this.props.displayRelativeValues ? 1 : 100))
			}]

		return <DashboardSection
			bluetheme
			download = {this.props.showDownload ? <Download
				columns={utilization_kpi_excel_columns}
				data={utilization_kpi_excel_data}
				chart={utilization_kpi}
				chartWidth='25%'
				name={`${this.props.client} - Engagement - Engagement Adjusted` }
				/>: null}
			section={{
				title : 'Engagement',
				tooltip : 'A 10% adjustment to account for an individual accessing multiple services'
			}}>
			<DashboardTab>
				{utilization_kpi}
			</DashboardTab>
		</DashboardSection>
	}
}