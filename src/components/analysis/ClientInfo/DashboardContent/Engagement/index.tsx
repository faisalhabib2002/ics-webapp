import * as React from 'react'
import {connectRobin} from '@simplus/robin-react'
import * as moment from 'moment'
import notification from 'antd/lib/notification'
import Modal from 'antd/lib/modal'
import AntDatePicker from 'antd/lib/date-picker'
import { Button, Icon, Tabs } from 'antd'
const { TabPane } = Tabs;
import {Loader} from '@simplus/siui'
import { RouteComponentProps } from 'react-router-dom';
import defaults from 'lodash/defaults'
import * as queryString from 'query-string'
import {
	DateRangeFilter,
	ListToTree,
	getTopActiveNodes
} from '../utils'

import {robins} from 'src/robins'
import {hasPermission, noPermission} from 'src/utils'
import {ErrorBoundary} from 'src/utils/ErrorBoundary'
const {AnalyticsRobin, clientSettings, PermissionsRobin, siteLevelsRobin, ClientsRobin, AnalyticsNewRobin} = robins
import {
	DashboardContainer,
	Dashboard,
	DashboardSection,
	DashboardFilter
} from 'src/components/dashboard'
import {EngagementAdjustedKPI} from './EngagementAdjustedKPI'
import {EngagementTrend} from './EngagementTrend'
import {KeyMetricsKPI} from './KeyMetricsKPI'
import {UtilisationBreakdown} from './UtilisationBreakdown'
import {IndividualCasesBreakdown} from './IndividualCasesBreakdown'
import {GroupParticipantsBreakdown} from './GroupParticipantsBreakdown'
import {OnlineAccessBreakdown} from './OnlineAccessBreakdown'
import {SessionsBreakdown} from './SessionsBreakdown'

@connectRobin([AnalyticsRobin, clientSettings, PermissionsRobin, siteLevelsRobin])
export class Engagement extends React.Component<RouteComponentProps<{id: string}>> {
	state = {
		filter: moment('2018-01-01'), 
		compareBenchmark:  true, 
		customBenchmark:  false, 
		sitelevelBenchmark: false, 
		benchmarkStartDate: moment(), 
		benchmarkEndDate: moment(), 
		downloadPreview: false, 
		engagementTrendModal: false,
		utilisationBreakdownLoader: false,
		individualBreakdownLoader: false,
		groupBreakdownLoader: false,
		onlineAccessBreakdownLoader: false,
		sessionsBreakdownLoader: false
	}
	unlisten?: () => void = undefined
	componentWillMount(): void {
		const dateRange = queryString.parse(location.hash.split('?')[1]).dateRange || 'month'
		const startDate = queryString.parse(location.hash.split('?')[1]).startDate || moment().startOf('month').subtract(1, dateRange).format('YYYY-MM-DD');
		const endDate = queryString.parse(location.hash.split('?')[1]).endDate || moment().subtract(1, dateRange).endOf(dateRange).format('YYYY-MM-DD');

		ClientsRobin.get(`${this.props.match.params.id}`, `/${this.props.match.params.id}`)
		siteLevelsRobin.when(siteLevelsRobin.findOne(this.props.match.params.id)).then(() => {
			clientSettings.when(clientSettings.findOne(this.props.match.params.id)).then(() => {
				const settings = clientSettings.getModel();
				if (settings[0]) {
					const reportingCycle = moment(settings[0].reportingCycle)
					if (!reportingCycle.isSame(this.state.filter, 'd')) {
						this.setState({filter: reportingCycle})
					}
					else notification.info({ type:'info', message: 'Reporting Period', description: <div><div>Start Date: {moment(startDate).format('YYYY-MM-DD')}</div><div>End Date: {moment(endDate).format('YYYY-MM-DD')}</div></div> })
				} else notification.info({ type:'info', message: 'Reporting Period', description: <div><div>Start Date: {moment(startDate).format('YYYY-MM-DD')}</div><div>End Date: {moment(endDate).format('YYYY-MM-DD')}</div></div> })
				this.applyFilters()
			}).catch( err => {
				notification.error({type: 'error', message: 'Could not load settings data !', description: 'There was an error during the settings api execution.'})
			})
			this.unlisten = this.props.history.listen((location, action) => {
				if (action === 'POP')
					this.applyFilters()
			});
		})
		
	}
	applyFilters(): void {
		const benchmark = queryString.parse(location.hash.split('?')[1]).benchmark || 'Previous range'
		const dateRange = queryString.parse(location.hash.split('?')[1]).dateRange || 'month'


		const settings = clientSettings.getModel();
		const dailyworkhours = settings[0] ? settings[0].avgWorkingHours : 8
		const hourlywage = settings[0] ? Number(settings[0].avgEmployeeWage) : 227.3

		const startDate = queryString.parse(location.hash.split('?')[1]).startDate || moment().startOf('month').subtract(1, dateRange).format('YYYY-MM-DD');
		const endDate = queryString.parse(location.hash.split('?')[1]).endDate || moment().subtract(1, dateRange).endOf(dateRange).format('YYYY-MM-DD');
		const benchmarkStartDate = queryString.parse(location.hash.split('?')[1]).benchmarkStartDate || moment().startOf('month').subtract(1, dateRange).format('YYYY-MM-DD');
		const benchmarkEndDate = queryString.parse(location.hash.split('?')[1]).benchmarkEndDate || moment().endOf('month').subtract(1, dateRange).format('YYYY-MM-DD');
		const keymetricsFilter = queryString.parse(location.hash.split('?')[1]).keymetricsFilter;
		const sessionsFilter = queryString.parse(location.hash.split('?')[1]).sessionsFilter;
		const groupKeymetrics = queryString.parse(location.hash.split('?')[1]).groupKeymetrics;
		
		const clientID = this.props.match.params.id
		const siteMapping = siteLevelsRobin.isLoading(siteLevelsRobin.ACTIONS.FIND_ONE) ? [] : (siteLevelsRobin.getModel() || []).map(s => `clients/${clientID}` === s.branchTo ? ({...s, branchTo: null}) : s )
		let siteLevelsAssigned = false;
		siteMapping.map(site => {if(site.disabled) siteLevelsAssigned = true })
		const siteTreeData = ListToTree(siteMapping)
		const nodes = [];
		getTopActiveNodes(siteTreeData, nodes)
		const defaultSites = siteLevelsAssigned ? nodes : ['']
		const sitelevels = queryString.parse(location.hash.split('?')[1]).site || defaultSites
		const benchmarksitelevels = queryString.parse(location.hash.split('?')[1]).benchmarksitelevel || defaultSites
		const includeDependants = queryString.parse(location.hash.split('?')[1]).dependants || 'true'
		 
		this.setState({
				utilisationBreakdownLoader: true,
				individualBreakdownLoader: true,
				groupBreakdownLoader: true,
				onlineAccessBreakdownLoader: true,
				sessionsBreakdownLoader: true
		})
		const options: any = {
			startdate : startDate,
			sitelevels: Array.isArray(sitelevels) ? sitelevels.filter(item => item !== 'All') : sitelevels === 'All' ? defaultSites : sitelevels.split(),
			enddate : endDate,
			client : this.props.match.params.id,
			dailyworkhours: dailyworkhours,
			hourlywage: hourlywage,
			dicarddepenedent: includeDependants == 'true' ? false: true,
			benchmarksitelevels: Array.isArray(benchmarksitelevels) ? benchmarksitelevels.filter(item => item !== 'All') : benchmarksitelevels === 'All' ? defaultSites : benchmarksitelevels.split(),
			
		}
		const groupFilterOptions: any = {
				
		}
		if(keymetricsFilter !== 'all')
		{	
			if(groupKeymetrics ==='problem')
				groupFilterOptions.problemcluster = keymetricsFilter
			else
				groupFilterOptions.product = keymetricsFilter
		}
		const sessionsFilterOptions: any = {
				
		}
		if(sessionsFilter !== 'all')
		{	
			sessionsFilterOptions.product = sessionsFilter
		}
		const newAnalyticsParams: any = {
			startDate : startDate,
			endDate : endDate,
			client : `clients/${this.props.match.params.id}`
		}
		if (benchmark === 'Previous range') {
			if (dateRange === 'month') {
				options.benchmarkstartdate = moment(startDate).subtract(1, dateRange).format('YYYY-MM-DD')
				options.benchmarkenddate = moment(endDate).subtract(1, dateRange).endOf('month').format('YYYY-MM-DD')
			} else if (dateRange === 'YTD') { // YTD option should use same dates of previous year for benchmark
				options.benchmarkstartdate = moment(startDate).subtract(1, 'year').format('YYYY-MM-DD')
				options.benchmarkenddate = moment(endDate).subtract(1, 'year').format('YYYY-MM-DD')
			} else {
				options.benchmarkstartdate = moment(startDate).subtract(1, dateRange).format('YYYY-MM-DD')
				options.benchmarkenddate = moment(endDate).subtract(1, dateRange).format('YYYY-MM-DD')
			}
		} else if (benchmark === 'custom') {
			options.benchmarkstartdate = moment(benchmarkStartDate).format('YYYY-MM-DD')
			options.benchmarkenddate = moment(benchmarkEndDate).format('YYYY-MM-DD')
		} else {
			options.benchmark = benchmark.split(',')
		}
			AnalyticsRobin.when(AnalyticsRobin.post('engagement_keymetrics', '/engagement/keymetrics', options)).catch( err => {
				notification.error({type: 'error', message: 'Could not load engagement keymetrics data !', description: 'There was an error during the analytics api execution.'})
			}).finally( () => {
				this.setState({
				individualBreakdownLoader: false,
				groupBreakdownLoader: false,
				onlineAccessBreakdownLoader: false
				})
				AnalyticsRobin.when(AnalyticsRobin.post('engagement_breakdown', '/engagement/keymetrics', {...options, split : queryString.parse(location.hash.split('?')[1]).keymetrics || 'service'})).catch( err => {
					notification.error({type: 'error', message: 'Could not load engagement breakdown data !', description: 'There was an error during the analytics api execution.'})
				})
				AnalyticsRobin.when(AnalyticsRobin.post('group_breakdown', '/engagement/keymetrics', {...options, ...groupFilterOptions, split : queryString.parse(location.hash.split('?')[1]).groupKeymetrics || 'service'})).catch( err => {
					notification.error({type: 'error', message: 'Could not load engagement breakdown data !', description: 'There was an error during the analytics api execution.'})
				}).finally( () => {
					AnalyticsRobin.when(AnalyticsRobin.post('products_breakdown', '/engagement/keymetrics', {...options, split : 'product'})).catch( err => {
						notification.error({type: 'error', message: 'Could not load products breakdown data !', description: 'There was an error during the analytics api execution.'})
					})
					AnalyticsRobin.when(AnalyticsRobin.post('problem_cluster_breakdown', '/engagement/keymetrics', {...options, split : 'problemcluster'})).catch( err => {
						notification.error({type: 'error', message: 'Could not load problem cluster breakdown data !', description: 'There was an error during the analytics api execution.'})
					})
				})
				AnalyticsRobin.when(AnalyticsRobin.post('online_access_breakdown', '/engagement/keymetrics', {...options, split : queryString.parse(location.hash.split('?')[1]).onlineAccessKeymetrics || 'service'})).catch( err => {
					notification.error({type: 'error', message: 'Could not load engagement breakdown data !', description: 'There was an error during the analytics api execution.'})
				})
			}) 
			AnalyticsRobin.when(AnalyticsRobin.post('engagement_utilization', '/engagement/utilization', options)).catch( err => {
				notification.error({type: 'error', message: 'Could not load engagement utilisation data !', description: 'There was an error during the analytics api execution.'})
			}).finally(() => {
				this.setState({
					utilisationBreakdownLoader: false
				})
				AnalyticsRobin.when(AnalyticsRobin.post('utilization_breakdown', '/engagement/utilization', {...options, split : queryString.parse(location.hash.split('?')[1]).utilizationbreakdown || 'product'})).catch( err => {
					notification.error({type: 'error', message: 'Could not load utilisation breakdown data !', description: 'There was an error during the analytics api execution.'})
				})
			})
			AnalyticsRobin.when(AnalyticsRobin.post('sessions_breakdown_products', '/engagement/sessions', {...options, split : 'product'})).catch( err => {
				notification.error({type: 'error', message: 'Could not load sessions breakdown data !', description: 'There was an error during the analytics api execution.'})
			}).finally( () => {
				this.setState({
					sessionsBreakdownLoader: false
				})
				AnalyticsRobin.when(AnalyticsRobin.post('sessions_breakdown', '/engagement/sessions', {...options, ...sessionsFilterOptions, split : queryString.parse(location.hash.split('?')[1]).sessionsbreakdown || 'product'})).catch( err => {
					notification.error({type: 'error', message: 'Could not load sessions breakdown data !', description: 'There was an error during the analytics api execution.'})
				})
			})
			// AnalyticsRobin.when(AnalyticsRobin.post('engagement_keymetrics', '/engagement/keymetrics', options)).catch( err => {
			// 	notification.error({type: 'error', message: 'Could not load engagement keymetrics data !', description: 'There was an error during the analytics api execution.'})
			// })
			// // AnalyticsNewRobin.when(AnalyticsNewRobin.post('engagement_trend', '/keymetrics', newAnalyticsParams)).catch( err => {
			// // 	// notification.error({type: 'error', message: 'Could not load engagement trend data !', description: 'There was an error during the analytics api execution.'})
			// // })
			// AnalyticsRobin.when(AnalyticsRobin.post('engagement_utilization', '/engagement/utilization', options)).catch( err => {
			// 	notification.error({type: 'error', message: 'Could not load engagement utilisation data !', description: 'There was an error during the analytics api execution.'})
			// })
			// AnalyticsRobin.when(AnalyticsRobin.post('utilization_breakdown', '/engagement/utilization', {...options, split : queryString.parse(location.hash.split('?')[1]).utilizationbreakdown || 'product'})).catch( err => {
			// 	notification.error({type: 'error', message: 'Could not load utilisation breakdown data !', description: 'There was an error during the analytics api execution.'})
			// })
			// AnalyticsRobin.when(AnalyticsRobin.post('sessions_breakdown', '/engagement/sessions', {...options, split : queryString.parse(location.hash.split('?')[1]).sessionsbreakdown || 'product'})).catch( err => {
			// 	notification.error({type: 'error', message: 'Could not load sessions breakdown data !', description: 'There was an error during the analytics api execution.'})
			// })
			// AnalyticsRobin.when(AnalyticsRobin.post('engagement_breakdown', '/engagement/keymetrics', {...options, split : queryString.parse(location.hash.split('?')[1]).keymetrics || 'service'})).catch( err => {
			// 	notification.error({type: 'error', message: 'Could not load engagement breakdown data !', description: 'There was an error during the analytics api execution.'})
			// })
			// AnalyticsRobin.when(AnalyticsRobin.post('group_breakdown', '/engagement/keymetrics', {...options, ...additionalFilters, split : queryString.parse(location.hash.split('?')[1]).groupKeymetrics || 'service'})).catch( err => {
			// 	notification.error({type: 'error', message: 'Could not load engagement breakdown data !', description: 'There was an error during the analytics api execution.'})
			// })
			// AnalyticsRobin.when(AnalyticsRobin.post('products_breakdown', '/engagement/keymetrics', {...options, split : 'product'})).catch( err => {
			// 	notification.error({type: 'error', message: 'Could not load products breakdown data !', description: 'There was an error during the analytics api execution.'})
			// })
			// AnalyticsRobin.when(AnalyticsRobin.post('problem_cluster_breakdown', '/engagement/keymetrics', {...options, split : 'problemcluster'})).catch( err => {
			// 	notification.error({type: 'error', message: 'Could not load problem cluster breakdown data !', description: 'There was an error during the analytics api execution.'})
			// })
			// AnalyticsRobin.when(AnalyticsRobin.post('online_access_breakdown', '/engagement/keymetrics', {...options, split : queryString.parse(location.hash.split('?')[1]).onlineAccessKeymetrics || 'service'})).catch( err => {
			// 	notification.error({type: 'error', message: 'Could not load engagement breakdown data !', description: 'There was an error during the analytics api execution.'})
			// })
	}
	componentWillUnmount(): void {
		if (this.unlisten)
			this.unlisten();
	}
	customBencharkOk = (e) => {
		this.setState({
			customBenchmark: false,
		});
		this.pageRedirect({
			benchmark: 'custom',
			benchmarkStartDate: moment(this.state.benchmarkStartDate).format('YYYY-MM-DD'),
			benchmarkEndDate: moment(this.state.benchmarkEndDate).format('YYYY-MM-DD')
		}, true)
	}


	sitelevelBencharkOk = (e) => {
		this.setState({
			sitelevelBenchmark: false,
		});
		this.pageRedirect({
			benchmark: 'clients'
		}, true)
	}
	customBencharkCancel = (e) => {
		this.setState({customBenchmark: false});
	}

	sitelevelBencharkCancel = (e) => {
		this.setState({sitelevelBenchmark: false});
	}
	pageRedirect(update: any, refrsh: boolean = false): void {
		
		const state = {
			...this.urlState(),
			...update}
		this.props.history.push(`${this.props.location.pathname}?${queryString.stringify(state)}`)
		if (refrsh)
			this.applyFilters()
	}
	ref = (React as any).createRef();
	urlState(): any {
		return queryString.parse(location.hash.split('?')[1])
	}

	handleTrend = () => {
		this.setState({engagementTrendModal: true})
	}

	render(): JSX.Element {
		const clientID = this.props.match.params.id
		const clientName = (ClientsRobin.getResult(`${clientID}`) || {name: ''}).Name
		// Query Strings
		let { dateRange, site, benchmarksitelevel,benchmark, onlineAccessKeymetrics, groupKeymetrics, keymetricsFilter, sessionsFilter, utilizationbreakdown, sessionsbreakdown,  keymetrics, comparison, tab, dependants} = defaults(this.urlState(), {
			dateRange : 'month',
			site: ['All'],
			benchmarksitelevel: ['All'],
			benchmark: 'Previous range',
			utilizationbreakdown: 'product',
			sessionsbreakdown: 'product',
			keymetrics: 'service',
			sessionsFilter: 'all',
			groupKeymetrics: 'service',
			keymetricsFilter: 'all',
			onlineAccessKeymetrics: 'service',
			comparison: 'absolute',
			tab: '0/.0'
		})
		const benchmarkFilter = Array.isArray(benchmarksitelevel) ? benchmarksitelevel.filter(item => item !== 'All') :  benchmarksitelevel.split()
		if(benchmarkFilter && benchmarkFilter.filter(item => item !== 'All').length) {
			benchmark = 'site'
		}
		const displayRelativeValues = comparison === 'relative'
		const includeDependants = dependants === 'true' || dependants === undefined
		// Robin results
		// Below variable is used for testing only. It has to be replaced when api is available for sitemapping
		const siteMapping = siteLevelsRobin.isLoading(siteLevelsRobin.ACTIONS.FIND_ONE) ? [] : (siteLevelsRobin.getModel() || []).map(s => `clients/${clientID}` === s.branchTo ? ({...s, branchTo: null}) : s )
		const treeData = ListToTree(siteMapping)

		const benchmarkMapping = {
			'Previous range': 'Previous period',
			'industry': 'Sector',
			'SegmentSize': 'Size',
			'clients': 'Company',
			'custom': 'Custom range',
			'site': 'Site levels'
		}
		const compareBenchmark = this.state.compareBenchmark && !displayRelativeValues
		// const includeDependants = this.state.includeDependants

		const engagementDashboard = (
			<DashboardContainer style={{padding: '1rem', position: 'relative'}}>
				{(clientSettings.isLoading(clientSettings.ACTIONS.FIND_ONE) || siteLevelsRobin.isLoading(siteLevelsRobin.ACTIONS.FIND_ONE) || clientSettings.isError(clientSettings.ACTIONS.FIND_ONE) || siteLevelsRobin.isError(siteLevelsRobin.ACTIONS.FIND_ONE)) ? <Loader error={clientSettings.isError(clientSettings.ACTIONS.FIND_ONE) || siteLevelsRobin.isError(siteLevelsRobin.ACTIONS.FIND_ONE)}/> : null}
				<div className='dashboard-top-bar'>
					<div style={{display: 'flex', padding: '1rem', alignItems:'flex-end'}}>
					<DashboardFilter
							width={200}
							type='tree'
							title='SITE'
							defaultValue={site}
							style={{marginRight: '1rem', display: 'flex', flexDirection: 'column'}}
							treeData={treeData}
							onChange={(selected) => this.pageRedirect({site: selected}, true)
						}/>
						<DateRangeFilter value={dateRange} customOption financialStartYear={moment(this.state.filter)} label='DATE RANGE' style={{minWidth: 130, marginRight: '1rem'}}
							onChange={(selected) => {
								notification.info({ type:'info', message: 'Reporting Period', description: <div><div>Start Date: {moment(selected.startDate).format('YYYY-MM-DD')}</div><div>End Date: {moment(selected.endDate).format('YYYY-MM-DD')}</div></div> })
									this.pageRedirect({
									dateRange: selected.value,
									startDate: moment(selected.startDate).format('YYYY-MM-DD'),
									endDate: moment(selected.endDate).format('YYYY-MM-DD')
								}, true)
							}
						}/>
						<DashboardFilter
							width={150}
							type='single'
							text='Compared to'
							title='BENCHMARK'
							defaultValue={benchmark}
							options={[{value: 'Previous range', text: 'Previous period'}, {value: 'industry', text: 'Sector'}, {value: 'SegmentSize', text: 'Size'}, {value: 'clients', text: 'Company'},{value: 'custom', text: 'Custom range'} ,{value: 'site', text: 'Site levels'}]}
							onChange={(selected) => {
								if (selected === 'custom') {
									this.pageRedirect({benchmarksitelevel: undefined}, false)
									this.setState({ customBenchmark: true });
								} else if (selected === 'site') {
									this.setState({ sitelevelBenchmark: true });
								} else {
									this.pageRedirect({benchmarksitelevel: undefined}, false)
									this.pageRedirect({ benchmark: selected }, true);
								}
							}}/>
						<DashboardFilter
							type='switch'
							text='Display Benchmark'
							checked={compareBenchmark}
							onChange={(selected) => this.setState({compareBenchmark: selected})
							}/>
						<DashboardFilter
							type='switch'
							text='Absolute'
							checked={comparison !== 'absolute'}
							onChange={(selected) => this.pageRedirect({comparison: selected ? 'relative' : 'absolute'}, true)
							}/>
						
						<div style={{alignSelf: 'flex-end', marginLeft: '6px', marginBottom: '5px', marginRight: '10px', fontSize: '1rem'}}>Relative</div>
						<DashboardFilter
							type='switch'
							text={`Include Dependants`}
							checked={includeDependants}
							onChange={(selected) => this.pageRedirect({dependants: selected ? 'true' : 'false'}, true)
							}/>
					</div>
				</div>
				<Modal
					className='si-date-picker-custom'
					title={<span style={{fontSize: '20px', fontWeight: 300}}>Custom range</span>}
					width={400}
					visible={this.state.customBenchmark}
					onOk={this.customBencharkOk}
					onCancel={this.customBencharkCancel}>
					<div className='si-date-picker-modal-body'>
						<div className='si-date-picker-row-from'>
							<span style={{width: 46}}>From: </span>
							<AntDatePicker allowClear={false} onChange={(date: any) => this.setState({benchmarkStartDate: date.startOf('day')})} />
						</div>
						<div className='si-date-picker-row-to'>
							<span style={{width: 46}}>To: </span>
							<AntDatePicker allowClear={false} onChange={(date: any) => this.setState({benchmarkEndDate: date.endOf('day')})}/>
						</div>
					</div>
				</Modal>
				<Modal
					className='si-date-picker-custom'
					title={<span style={{fontSize: '20px', fontWeight: 300}}>Site levels </span>}
					width={400}
					visible={this.state.sitelevelBenchmark}
					onOk={this.sitelevelBencharkOk}
					onCancel={this.sitelevelBencharkCancel}>
						<DashboardFilter
							width={300}
							type='tree'
							title='SITE'
							defaultValue={benchmarksitelevel}
							style={{marginRight: '1rem', display: 'flex', flexDirection: 'column'}}
							treeData={treeData}
							onChange={(selected) => this.pageRedirect({benchmarksitelevel: selected}, false)
						}/>
				</Modal>
				{/* <Modal
					title={<span style={{fontSize: '20px', fontWeight: 300}}>Engagement Trend</span>}
					visible={this.state.engagementTrendModal}
					footer={false}
					width={700}
					onCancel={() => this.setState({engagementTrendModal: false})}
					>
					<EngagementTrend client={clientName} displayRelativeValues={displayRelativeValues}/>
				</Modal> */}
				<Dashboard
				structure={{
					items : [2, 5, 7, 7],
					size : 7
				}}>
					<EngagementAdjustedKPI
						client={clientName}
						displayRelativeValues={displayRelativeValues}
						compareBenchmark={compareBenchmark}
						benchmarkMapping={benchmarkMapping}
						showDownload={true}/>
					<KeyMetricsKPI
						client={clientName}
						displayRelativeValues={displayRelativeValues}
						compareBenchmark={compareBenchmark}
						benchmarkMapping={benchmarkMapping}
						showDownload={true}
						handleTrend={this.handleTrend}/>

				<DashboardSection
					defaultActiveTab={tab}
					onTabChange={(selected) => this.pageRedirect({tab: selected})}
					section={{
						tabTitles : ['Utilisation', 'Individual Cases', 'Group Participants', 'Online Access', 'Sessions'],
						tabTooltips: [
							'Programme uptake by product or service',
							'Individual utilisation metrics drill-down',
							'Group utilisation metrics drill-down',
							'Digital platform utilisation metrics drill-down',
							'Face-to-face and targeted interventions drill-down'
						],
						title : 'Key metrics breakdown',
						tooltip : 'Key usage metrics unpacked'
					}}>
					<UtilisationBreakdown
						client={clientName}
						displayRelativeValues={displayRelativeValues}
						compareBenchmark={compareBenchmark}
						benchmark={benchmark}
						benchmarkMapping={benchmarkMapping}
						filter={utilizationbreakdown}
						splitFilter={(selected) => this.pageRedirect({utilizationbreakdown: selected, tab: '0/.0'}, true)} 
						loading={this.state.utilisationBreakdownLoader}
						showDownload={true}/>
					<IndividualCasesBreakdown
						client={clientName}
						displayRelativeValues={displayRelativeValues}
						compareBenchmark={compareBenchmark}
						benchmark={benchmark}
						benchmarkMapping={benchmarkMapping}
						filter={keymetrics}
						splitFilter={(selected) => this.pageRedirect({keymetrics: selected, tab: '1/.1'}, true)} 
						loading={this.state.individualBreakdownLoader}
						showDownload={true}/>
					<GroupParticipantsBreakdown
						client={clientName}
						displayRelativeValues={displayRelativeValues}
						compareBenchmark={compareBenchmark}
						benchmark={benchmark}
						benchmarkMapping={benchmarkMapping}
						filter={groupKeymetrics}
						filterValue={keymetricsFilter}
						splitFilter={(selected) => this.pageRedirect({groupKeymetrics: selected, keymetricsFilter: 'all', tab: '2/.2'}, true)}
						filterOn={(selected) => this.pageRedirect({keymetricsFilter: selected, tab: '2/.2'}, true)}
						loading={this.state.groupBreakdownLoader}
						showDownload={true}/>
					<OnlineAccessBreakdown
						client={clientName}
						displayRelativeValues={displayRelativeValues}
						compareBenchmark={compareBenchmark}
						benchmark={benchmark}
						benchmarkMapping={benchmarkMapping}
						filter={onlineAccessKeymetrics}
						splitFilter={(selected) => this.pageRedirect({onlineAccessKeymetrics: selected, tab: '3/.3'}, true)}
						loading={this.state.onlineAccessBreakdownLoader}
						showDownload={true}/>
					<SessionsBreakdown
						client={clientName}
						displayRelativeValues={displayRelativeValues}
						compareBenchmark={compareBenchmark}
						benchmark={benchmark}
						benchmarkMapping={benchmarkMapping}
						filter={sessionsbreakdown}
						filterValue={sessionsFilter}
						splitFilter={(selected) => this.pageRedirect({sessionsbreakdown: selected, sessionsFilter: 'all', tab: '4/.4'}, true)}
						filterOn={(selected) => this.pageRedirect({sessionsFilter: selected, tab: '4/.4'}, true)}
						loading={this.state.sessionsBreakdownLoader}
						showDownload={true}/>
					</DashboardSection>
				</Dashboard>
			</DashboardContainer>
		)
		return(
			<ErrorBoundary>
				<div className='dashboard-nav'>
					<Tabs activeKey="0"
					onChange={(current) => {
						switch (current) {
							case "1":
							this.props.history.push(`/client/${clientID}/dashboard/health`)
							break;
							case "2":
							this.props.history.push(`/client/${clientID}/dashboard/effectiveness`)
							break;
							case "3":
									this.props.history.push(`/client/${clientID}/dashboard/roi`)
							break;
							case "4":
									this.props.history.push(`/client/${clientID}/dashboard/quality`)
							break;
							// case 5:
							// this.props.history.push(`/client/${clientID}/dashboard/cost-to-serve`)
							// break;
						}
					}}
					>
						<TabPane tab={<span><Icon type="usergroup-add" />Engagement</span>} key="0">
							{hasPermission('/view/dashboard/engagement', PermissionsRobin.getResult('own-permissions')) ? engagementDashboard : noPermission(PermissionsRobin.isLoading('own-permissions'))}
						</TabPane>
						<TabPane tab={<span><Icon type="heart" />Health</span>} key="1" disabled={!hasPermission('/view/dashboard/health', PermissionsRobin.getResult('own-permissions'))}/>
						<TabPane tab={<span><Icon type="dashboard" />Effectiveness</span>} key="2" disabled={!hasPermission('/view/dashboard/effectiveness', PermissionsRobin.getResult('own-permissions'))}/>
						<TabPane tab={<span><Icon type="line-chart" />ROI</span>} key="3" disabled={!hasPermission('/view/dashboard/roi', PermissionsRobin.getResult('own-permissions'))}/>
						<TabPane tab={<span><Icon type="like" />Quality</span>} key="4" disabled={!hasPermission('/view/dashboard/quality', PermissionsRobin.getResult('own-permissions'))}/>
					</Tabs>
				</div>
			</ErrorBoundary>
		)
	}

}
export default Engagement;