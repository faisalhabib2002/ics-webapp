import * as React from 'react'
import {robins} from 'src/robins'
import {connectRobin} from '@simplus/robin-react'
import get from 'lodash/get'
import keys from 'lodash/keys'
import {
	capitalizeFirstLetter,
	Download
} from '../utils'
import {
	DashboardFilter,
	DashboardTab,
	BarChart
} from 'src/components/dashboard'
const {AnalyticsRobin} = robins

interface Props {
	client: string,
	displayRelativeValues: boolean,
	filter: string,
	compareBenchmark: boolean,
	benchmark: string,
	benchmarkMapping: any,
	splitFilter(filter: string): void,
	apiKey?: string,
	height?: number,
	showDownload?: boolean
	showLabel?: boolean
	downloadPreview?: boolean
	loading?: boolean
}
@connectRobin([AnalyticsRobin])
export class OnlineAccessBreakdown extends React.Component<Props> {
	render(): JSX.Element {
		const keymetrics_breakdown_result = AnalyticsRobin.getResult(this.props.apiKey || 'online_access_breakdown') || {data: []}
		const keymetrics_data_orig = get(keymetrics_breakdown_result, 'data', [])
		const keymetrics_data = keymetrics_data_orig.map( o => ({...o}))

		if (this.props.displayRelativeValues) {
			[keymetrics_data].forEach( o => {
				const os = Array.isArray(o) ? o : [o]
				os.forEach( e => {
					keys(e).filter( k => /relative$/.test(k)).map( k => k.replace(/relative$/, '')).forEach( k => {
						e[k] = e[`${k}relative`] * 100
					})
				})
			})
		}

		const online_access_excel_data = keymetrics_data_orig.sort((a: any, b: any) => b.onlineaccess - a.onlineaccess).map(item => {
			const output  = {
				client: this.props.client,
				period: `${keymetrics_breakdown_result.startdate} to ${keymetrics_breakdown_result.enddate}`,
				name: item.key,
				value: item[`onlineaccess`] || 0,
				benchmark: keymetrics_breakdown_result.benchmark ? this.props.benchmarkMapping[keymetrics_breakdown_result.benchmark[0]] || keymetrics_breakdown_result.benchmark[0] : `${keymetrics_breakdown_result.benchmarkstartdate} to ${keymetrics_breakdown_result.benchmarkenddate}`,
				benchmark_value: item[`onlineaccessbenchmark`] || 0
			}
			return output
		})

		const online_access_chart = <BarChart
			loading={AnalyticsRobin.isLoading(this.props.apiKey || 'online_access_breakdown')}
			error={AnalyticsRobin.isError(this.props.apiKey || 'online_access_breakdown') || this.props.loading}
			config={{showLegend: this.props.compareBenchmark}}
			showLabel={this.props.showLabel}
			bars={this.props.compareBenchmark ? ['Actual', `${this.props.benchmarkMapping[this.props.benchmark]} benchmark`] : ['Actual']}
			data={keymetrics_data.sort((a, b) => b.onlineaccess - a.onlineaccess).map( o => {
				return {
					x: o.key || 'Uncategorised',
					'Actual': o.onlineaccessrelative * 100,
					'Actual_absolute': o.onlineaccess,
					'Previous period benchmark':  o.onlineaccessbenchmarkrelative * 100,
					'Previous period benchmark_absolute': o.onlineaccessbenchmark,
					'Sector benchmark': o.onlineaccessbenchmarkrelative * 100,
					'Sector benchmark_absolute': o.onlineaccessbenchmark,
					'Company benchmark': o.onlineaccessbenchmarkrelative * 100,
					'Company benchmark_absolute': o.onlineaccessbenchmark,
					'Site levels benchmark': o.onlineaccessbenchmarkrelative * 100,
					'Site levels benchmark_absolute': o.onlineaccessbenchmark,
					'Size benchmark': o.onlineaccessbenchmarkrelative * 100,
					'Size benchmark_absolute': o.onlineaccessbenchmark,
					'Custom range benchmark': o.onlineaccessbenchmarkrelative * 100,
					'Custom range benchmark_absolute': o.onlineaccessbenchmark,
				}
			})}
			hideEmptyBars
			yaxisUnits={'%'}
			minHeight={this.props.height}
			relativeToTotal={!this.props.displayRelativeValues}
			/>
		// Keymetrics Section
		const keymetrics_excel_columns = [
			'Company',
			'Period',
			capitalizeFirstLetter(this.props.filter),
			'Online Access',
			'Benchmark',
			'Benchmark value'
		]

		return <DashboardTab>
			<div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
				<DashboardFilter
					width={120}
					type='single'
					text='Split by'
					defaultValue={this.props.filter}
					downloadPreview={this.props.downloadPreview}
					options={[
						{value: 'service', text: 'Service'},
						{value: 'product', text: 'Products'},
						{value: 'assesment_type', text: 'Assesment Type'}]}
					onChange={(selected) => this.props.splitFilter(selected)
					}/>
					{ this.props.showDownload ?<Download
						columns={keymetrics_excel_columns}
						data={online_access_excel_data}
						chart={online_access_chart}
						name={`${this.props.client} - Engagement - Online Access`}/>
						: null}
			</div>
			{online_access_chart}
		</DashboardTab>
	}
}