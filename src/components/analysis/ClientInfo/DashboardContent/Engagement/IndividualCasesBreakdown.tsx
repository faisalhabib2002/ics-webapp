import * as React from 'react'
import {robins} from 'src/robins'
import {connectRobin} from '@simplus/robin-react'
import get from 'lodash/get'
import keys from 'lodash/keys'
import {
	capitalizeFirstLetter,
	Download
} from '../utils'
import {
	DashboardFilter,
	DashboardTab,
	BarChart
} from 'src/components/dashboard'
const {AnalyticsRobin} = robins

interface Props {
	client: string,
	displayRelativeValues: boolean,
	filter: string,
	compareBenchmark: boolean,
	benchmark: string,
	benchmarkMapping: any,
	splitFilter(filter: string): void,
	apiKey ?: string,
	height?: number
	showDownload?: boolean
	showLabel?: boolean
	downloadPreview?: boolean
	loading?: boolean
}
@connectRobin([AnalyticsRobin])
export class IndividualCasesBreakdown extends React.Component<Props> {
	render(): JSX.Element {
		const keymetrics_breakdown_result = AnalyticsRobin.getResult(this.props.apiKey || 'engagement_breakdown') || {data: []}
		const keymetrics_data_orig = get(keymetrics_breakdown_result, 'data', [])
		const keymetrics_data = keymetrics_data_orig.map( o => ({...o}))

		if (this.props.displayRelativeValues) {
			[keymetrics_data].forEach( o => {
				const os = Array.isArray(o) ? o : [o]
				os.forEach( e => {
					keys(e).filter( k => /relative$/.test(k)).map( k => k.replace(/relative$/, '')).forEach( k => {
						e[k] = e[`${k}relative`] * 100
					})
				})
			})
		}

		const individual_cases_excel_data = keymetrics_data_orig.sort((a: any, b: any) => b.individualcases - a.individualcases).map(item => {
			const output  = {
				client: this.props.client,
				period: `${keymetrics_breakdown_result.startdate} to ${keymetrics_breakdown_result.enddate}`,
				name: item.key === 'AID' ? 'OP Solutions' : item.key,
				value: item[`individualcases`] || 0,
				benchmark: keymetrics_breakdown_result.benchmark ? this.props.benchmarkMapping[keymetrics_breakdown_result.benchmark[0]] || keymetrics_breakdown_result.benchmark[0] : `${keymetrics_breakdown_result.benchmarkstartdate} to ${keymetrics_breakdown_result.benchmarkenddate}`,
				benchmark_value: item[`individualcasesbenchmark`] || 0
			}
			return output
		})

		const individual_cases_chart = <BarChart
			loading={AnalyticsRobin.isLoading(this.props.apiKey || 'engagement_breakdown') || this.props.loading}
			error={AnalyticsRobin.isError(this.props.apiKey || 'engagement_breakdown')}
			bars={this.props.compareBenchmark ? ['Actual', `${this.props.benchmarkMapping[this.props.benchmark]} benchmark`] : ['Actual']}
			config={{showLegend: this.props.compareBenchmark}}
			xaxisDomain={[0, 'auto']}
			minHeight={this.props.height}
			downloadPreview={this.props.downloadPreview}
			showLabel={this.props.showLabel}
			data={keymetrics_data.sort((a, b) => b.individualcases - a.individualcases).map( o => {
				return {
					x: o.key === 'AID' ? 'OP Solutions' : o.key  || 'Uncategorised',
					'Actual': o.individualcasesrelative * 100,
					'Actual_absolute': o.individualcases,
					'Previous period benchmark':  o.individualcasesbenchmarkrelative * 100,
					'Previous period benchmark_absolute': o.individualcasesbenchmark,
					'Sector benchmark': o.individualcasesbenchmarkrelative * 100,
					'Sector benchmark_absolute': o.individualcasesbenchmark,
					'Company benchmark': o.individualcasesbenchmarkrelative * 100,
					'Company benchmark_absolute': o.individualcasesbenchmark,
					'Site levels benchmark': o.individualcasesbenchmarkrelative * 100,
					'Site levels benchmark_absolute': o.individualcasesbenchmark,				
					'Size benchmark': o.individualcasesbenchmarkrelative * 100,
					'Size benchmark_absolute': o.individualcasesbenchmark,
					'Custom range benchmark': o.individualcasesbenchmarkrelative * 100,
					'Custom range benchmark_absolute': o.individualcasesbenchmark,
				}
			})}
			hideEmptyBars
			yaxisUnits={'%'}
			relativeToTotal={!this.props.displayRelativeValues}
			/>
			// Keymetrics Section
			const keymetrics_excel_columns = [
				'Company',
				'Period',
				capitalizeFirstLetter(this.props.filter),
				'Individual Cases',
				'Benchmark',
				'Benchmark value'
			]

		return <DashboardTab>
		<div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
			<DashboardFilter
				width={140}
				type='single'
				text='Split by'
				defaultValue={this.props.filter}
				downloadPreview={this.props.downloadPreview}
				options={[
					{value: 'service', text: 'Service'},
					{value: 'product', text: 'Products'},
					{value: 'employement', text: 'Employment'},
					{value: 'problemcluster', text: 'Problem Cluster'},
					{value: 'problem', text: 'Problem'},
					{value: 'protocol', text: 'Protocol'},
					{value: 'origin', text: 'Origin'},
					{value: 'severity', text: 'Severity'}]}
					onChange={(selected) => this.props.splitFilter(selected)
				}/>
				{this.props.showDownload ? <Download
					columns={keymetrics_excel_columns}
					data={individual_cases_excel_data}
					chart={individual_cases_chart}
					name={`${this.props.client} - Engagement - Individual Cases`}/> : null}
		</div>
		{individual_cases_chart}
	</DashboardTab>
	}
}