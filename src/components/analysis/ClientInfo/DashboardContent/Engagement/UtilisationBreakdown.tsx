import * as React from 'react'
import {robins} from 'src/robins'
import {connectRobin} from '@simplus/robin-react'
import get from 'lodash/get'
import keys from 'lodash/keys'
import {
	capitalizeFirstLetter,
	Download
} from '../utils'
import {
	DashboardFilter,
	DashboardTab,
	BarChart
} from 'src/components/dashboard'
const {AnalyticsRobin} = robins

interface Props {
	client: string,
	displayRelativeValues: boolean,
	filter: string,
	compareBenchmark: boolean,
	benchmark: string,
	benchmarkMapping: any,
	splitFilter(filter: string): void
	apiKey?: string,
	height?: number
	showDownload?: boolean
	showLabel?: boolean
	downloadPreview?: boolean
	loading?: boolean
}
@connectRobin([AnalyticsRobin])
export class UtilisationBreakdown extends React.Component<Props> {
	render(): JSX.Element {
		const utilization_breakdown_result = AnalyticsRobin.getResult(this.props.apiKey || 'utilization_breakdown') || {data: []}
		const utilization_breakdown_orig = get(utilization_breakdown_result, 'data', [])
		const utilization_breakdown = utilization_breakdown_orig.map( o => ({...o}))

		if (this.props.displayRelativeValues) {
			utilization_breakdown.forEach( e => {
				keys(e).filter( k => /relative$/.test(k)).map( k => k.replace(/relative$/, '')).forEach( k => {
					e[k] = e[`${k}relative`] * 100
				})
			})
		}

		// Utilization section
		const utilization_chart = <BarChart
			loading={AnalyticsRobin.isLoading(this.props.apiKey || 'utilization_breakdown') || this.props.loading}
			error={AnalyticsRobin.isError(this.props.apiKey || 'utilization_breakdown')}
			bars={this.props.compareBenchmark ? ['Actual', `${this.props.benchmarkMapping[this.props.benchmark]} benchmark`] : ['Actual']}
			config={{showLegend: this.props.compareBenchmark}}
			downloadPreview={this.props.downloadPreview}
			showLabel={this.props.showLabel}
			data={utilization_breakdown.sort((a, b) => b.utilization - a.utilization).map( o => {
				return {
					x: o.key === 'AID' ? 'OP Solutions' : o.key  || 'Uncategorised',
					'Actual': o.utilization * (this.props.displayRelativeValues ? 1 : 100),
					'Previous period benchmark':  o.utilizationbenchmark * 100,
					'Sector benchmark': o.utilizationbenchmark * 100,
					'Company benchmark': o.utilizationbenchmark * 100,
					'Site levels benchmark': o.utilizationbenchmark * 100,
					'Size benchmark': o.utilizationbenchmark * 100,
					'Custom range benchmark': o.utilizationbenchmark * 100
				}
			})}
			minHeight={this.props.height}
			yaxisUnits={'%'}
			/>
			const utilization_excel_columns = [
				'Company',
				'Period',
				capitalizeFirstLetter(this.props.filter),
				'Utilisation',
				'Benchmark',
				'Benchmark value',
				'Site levels'
			]
			const utilization_excel_data = utilization_breakdown_orig.sort((a: any, b: any) => b.utilization - a.utilization).map(item => {
				const output  = {
					client: this.props.client,
					period: `${utilization_breakdown_result.startdate} to ${utilization_breakdown_result.enddate}`,
					name: item.key === 'AID' ? 'OP Solutions' : item.key,
					value: `${((item[`utilization`] || 0) * 100).toFixed(2)}%`,
					benchmark: utilization_breakdown_result.benchmark ? this.props.benchmarkMapping[utilization_breakdown_result.benchmark[0]] || utilization_breakdown_result.benchmark[0] : `${utilization_breakdown_result.benchmarkstartdate} to ${utilization_breakdown_result.benchmarkenddate}`,
					benchmark_value: `${((item[`utilizationbenchmark`] || 0) * 100).toFixed(2)}%`
				}
				return output
			})

		return <DashboardTab>
			<div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
				<DashboardFilter
					width={120}
					type='single'
					text='Split by'
					defaultValue={this.props.filter}
					downloadPreview={this.props.downloadPreview}
					options={[
						{value: 'product', text: 'Product'},
						{value: 'service', text: 'Service'}
					]}
					onChange={(selected) => this.props.splitFilter(selected)
				}/>
				{this.props.showDownload ?<Download
					columns={utilization_excel_columns}
					data={utilization_excel_data}
					chart={utilization_chart}
					name={`${this.props.client} - Engagement - Utilisation`}/>
					: null}
			</div>
			{utilization_chart}
		</DashboardTab>
	}
}