import * as React from 'react'
import {robins} from 'src/robins'
import {connectRobin} from '@simplus/robin-react'
import get from 'lodash/get'
import keys from 'lodash/keys'
import {
	Download
} from '../utils'
import {
	DashboardFilter,
	DashboardTab,
	BarChart
} from 'src/components/dashboard'
const {AnalyticsRobin} = robins

interface Props {
	client: string,
	displayRelativeValues: boolean,
	filter: string,
	filterValue: string,
	compareBenchmark: boolean,
	benchmark: string,
	benchmarkMapping: any,
	splitFilter(filter: string): void,
	filterOn(filter: string): void,
	apiKey?: string,
	height?: number,
	showDownload?: boolean
	showLabel?: boolean
	downloadPreview?: boolean
	loading?: boolean
}
@connectRobin([AnalyticsRobin])
export class SessionsBreakdown extends React.Component<Props> {
	render(): JSX.Element {
		const sessions_breakdown_result = AnalyticsRobin.getResult(this.props.apiKey || 'sessions_breakdown') || {data: []}
		const sessions_products_result = (AnalyticsRobin.getResult('sessions_breakdown_products') || {data: []}).data
		const sessions_breakdown_orig = get(sessions_breakdown_result, 'data', [])
		const sessions_breakdown = sessions_breakdown_orig.map( o => ({...o}))
		const filterList = [{value: 'all', text: 'All'}]

		if (this.props.displayRelativeValues) {
			[sessions_breakdown].forEach( o => {
				const os = Array.isArray(o) ? o : [o]
				os.forEach( e => {
					keys(e).filter( k => /relative$/.test(k)).map( k => k.replace(/relative$/, '')).forEach( k => {
						e[k] = e[`${k}relative`] * 100
					})
				})
			})
		}
		for (const item of sessions_products_result) {
			if (this.props.filter === 'event' && item.sessions > 0) {
			  filterList.push({value: item.key, text: item.key});
			}
		  }

		// Session section
		const sessions_excel_columns = [
			'Company',
			'Period',
			this.props.filter === 'product' ? 'Product' : 'Session Type',
			'Actual',
			'Benchmark',
			'Benchmark value'
		]

		const sessions_excel_data = sessions_breakdown_orig.sort((a: any, b: any) => b.sessions - a.sessions).map(item => {
			const output  = {
				client: this.props.client,
				period: `${sessions_breakdown_result.startdate} to ${sessions_breakdown_result.enddate}`,
				name: item.key,
				value: item[`sessions`] || 0,
				benchmark: sessions_breakdown_result.benchmark ? this.props.benchmarkMapping[sessions_breakdown_result.benchmark[0]] || sessions_breakdown_result.benchmark[0] : `${sessions_breakdown_result.benchmarkstartdate} to ${sessions_breakdown_result.benchmarkenddate}`,
				benchmark_value: item[`sessionsbenchmark`] || 0
			}
			return output
		})

		const session_chart = <BarChart
			loading={AnalyticsRobin.isLoading(this.props.apiKey || 'sessions_breakdown') || this.props.loading}
			error={AnalyticsRobin.isError(this.props.apiKey || 'sessions_breakdown')}
			config={{showLegend: this.props.compareBenchmark}}
			showLabel={this.props.showLabel}
			bars={this.props.compareBenchmark ? ['Actual', `${this.props.benchmarkMapping[this.props.benchmark]} benchmark`] : ['Actual']}
			data={sessions_breakdown.sort((a, b) => b.sessions - a.sessions).map( o => {
				return {
					x: o.key || 'Uncategorised',
					'Actual': o.sessionsrelative * 100,
					'Actual_absolute': o.sessions,
					'Previous period benchmark':  o.sessionsbenchmarkrelative * 100,
					'Previous period benchmark_absolute': o.sessionsbenchmark,
					'Sector benchmark': o.sessionsbenchmarkrelative * 100,
					'Sector benchmark_absolute': o.sessionsbenchmark,
					'Company benchmark': o.sessionsbenchmarkrelative * 100,
					'Company benchmark_absolute': o.sessionsbenchmark,
					'Site levels benchmark': o.sessionsbenchmarkrelative * 100,
					'Site levels benchmark_absolute': o.sessionsbenchmark,
					'Size benchmark': o.sessionsbenchmarkrelative * 100,
					'Size benchmark_absolute': o.sessionsbenchmark,
					'Custom range benchmark': o.sessionsbenchmarkrelative * 100,
					'Custom range benchmark_absolute': o.sessionsbenchmark,
				}
			})}
			hideEmptyBars
			yaxisUnits={'%'}
			minHeight={this.props.height}
			relativeToTotal={!this.props.displayRelativeValues}
			/>

		return <DashboardTab>
			<div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
				<div style={{display: 'flex'}}>
					<DashboardFilter
						width={120}
						type='single'
						text='Split by'
						downloadPreview={this.props.downloadPreview}
						defaultValue={this.props.filter}
						options={[
							{value: 'product', text: 'Product'},
							{value: 'service', text: 'Session Type'},
							{value: 'event', text: 'Session Topic'},
						]}
						onChange={(selected) => this.props.splitFilter(selected)
						}/>
						{(this.props.filter === 'event') ? 
						<DashboardFilter
							width={140}
							type='single'
							text={'Product'}
							defaultValue={this.props.filterValue}
							downloadPreview={this.props.downloadPreview}
							options={filterList}
							onChange={(selected) => this.props.filterOn(selected)}/>
					:null}
				</div>
				{this.props.showDownload ?<Download
					columns={sessions_excel_columns}
					data={sessions_excel_data}
					chart={session_chart}
					name={`${this.props.client} - Engagement - Sessions`}/>
					: null}
			</div>
			{session_chart}
		</DashboardTab>
	}
}