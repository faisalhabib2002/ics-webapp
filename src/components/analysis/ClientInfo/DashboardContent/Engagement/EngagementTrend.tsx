import * as React from 'react'
import {robins} from 'src/robins'
import {connectRobin} from '@simplus/robin-react'
import get from 'lodash/get'
import keys from 'lodash/keys'
import {
	LineChart
} from 'src/components/dashboard'
const {AnalyticsNewRobin} = robins

interface Props {
	client: string,
	displayRelativeValues: boolean,
	// filter: string,
	// compareBenchmark: boolean,
	// benchmark: string,
	// benchmarkMapping: any,
	// splitFilter(filter: string): void,
	apiKey?: string,
	height?: number,
	showDownload?: boolean
	showLabel?: boolean
	downloadPreview?: boolean
}
@connectRobin([AnalyticsNewRobin])
export class EngagementTrend extends React.Component<Props> {
	render(): JSX.Element {
		const keymetrics_breakdown_result = AnalyticsNewRobin.getResult(this.props.apiKey || 'engagement_trend') || {data: []}
		const keymetrics_data_orig = get(keymetrics_breakdown_result, 'data', [])
		const keymetrics_data = keymetrics_data_orig.map( o => ({...o}))
		console.log(keymetrics_data_orig)
		if (this.props.displayRelativeValues) {
			[keymetrics_data].forEach( o => {
				const os = Array.isArray(o) ? o : [o]
				os.forEach( e => {
					keys(e).filter( k => /relative$/.test(k)).map( k => k.replace(/relative$/, '')).forEach( k => {
						e[k] = parseFloat((e[`${k}relative`] * 100).toFixed(1))
					})
				})
			})
		}
		const engagement_trending_chart = <LineChart
			loading={AnalyticsNewRobin.isLoading(this.props.apiKey || 'engagement_trend')}
			error={AnalyticsNewRobin.isError(this.props.apiKey || 'engagement_trend')}
			data={keymetrics_data}
			lines={['individualcases', 'groupparticipants', 'onlineaccess']}
			/>
		// Keymetrics Section


		return <div style={{background: 'white'}}>
				{engagement_trending_chart}
			</div>
	}
}