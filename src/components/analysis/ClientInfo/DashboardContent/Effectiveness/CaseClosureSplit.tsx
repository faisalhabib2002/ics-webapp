import * as React from 'react'
import {robins} from 'src/robins'
import {connectRobin} from '@simplus/robin-react'
import get from 'lodash/get'
import keys from 'lodash/keys'
import {
	Download
} from '../utils'
import {
	DashboardSection,
	DashboardTab,
	BarChart
} from 'src/components/dashboard'
const {AnalyticsRobin} = robins

interface Props {
	client: string,
	displayRelativeValues: boolean,
	compareBenchmark: boolean,
	benchmark: string,
	benchmarkMapping: any
	apiKey?: string,
	showDownload?: boolean
	showLabel?: boolean

}
@connectRobin([AnalyticsRobin])
export class CaseClosureSplit extends React.Component<Props> {
	render(): JSX.Element {
		const result = AnalyticsRobin.getResult(this.props.apiKey || 'effectiveness_closure') || {data: []}
		const closure_orig = get(result, 'data' , [])
		const closure = closure_orig.map( o => ({...o}))

		if (this.props.displayRelativeValues) {
			[closure].forEach( o => {
				const os = Array.isArray(o) ? o : [o]
				os.forEach( e => {
					keys(e).filter( k => /relative$/.test(k)).map( k => k.replace(/relative$/, '')).forEach( k => {
						e[k] = e[`${k}relative`] * 100
					})
				})
			})
		}

		const chart = <BarChart
			config={{showLegend: this.props.compareBenchmark}}
			loading={AnalyticsRobin.isLoading(this.props.apiKey || 'effectiveness_closure')}
			error={AnalyticsRobin.isError(this.props.apiKey || 'effectiveness_closure')}
			yaxisUnits='%'
			showLabel={this.props.showLabel}
			bars={this.props.compareBenchmark ? ['Actual', `${this.props.benchmarkMapping[this.props.benchmark]} benchmark`] : ['Actual']}
			data={closure.sort((a, b) => b.closure - a.closure).map( c => {
				return {
					x: c.key,
					'Actual': c.closurerelative * 100,
					'Actual_absolute': c.closure,
					'Previous period benchmark':  c.closurebenchmarkrelative * 100,
					'Previous period benchmark_absolute': c.closurebenchmark,
					'Sector benchmark': c.closurebenchmarkrelative * 100,
					'Sector benchmark_absolute': c.closurebenchmark,
					'Size benchmark': c.closurebenchmarkrelative * 100,
					'Size benchmark_absolute': c.closurebenchmark,
					'Company benchmark': c.closurebenchmarkrelative * 100,
					'Company benchmark_absolute': c.closurebenchmark,
					'Site levels benchmark': c.closurebenchmarkrelative * 100,
					'Site levels benchmark_absolute': c.closurebenchmark,
					'Custom range benchmark': c.closurebenchmarkrelative * 100,
					'Custom range benchmark_absolute': c.closurebenchmark,
				}
			})}
			relativeToTotal
		/>
		const excel_columns = [
			'Company',
			'Period',
			'Closure Type',
			'Closure',
			'Benchmark',
			'Benchmark value'
		]

		const excel_data = closure_orig.sort((a: any, b: any) => b.closure - a.closure).map(item => {
			const output  = {
				client: this.props.client,
				period: `${result.startdate} to ${result.enddate}`,
				name: item.key,
				value: item[`closure`] || 0,
				benchmark: result.benchmark ? this.props.benchmarkMapping[result.benchmark[0]] || result.benchmark[0] : `${result.benchmarkstartdate} to ${result.benchmarkenddate}`,
				benchmark_value: item[`closurebenchmark`] || 0
			}
			return output
		})
		return <DashboardSection
			download={this.props.showDownload ?<Download
				columns={excel_columns}
				data={excel_data}
				chart={chart}
				chartWidth='70%'
				name={`${this.props.client} - Effectiveness - Closure`}/> : null}
			section={{
				title : 'Case closure split',
				tooltip : 'Face-to-face or targeted interventions post intervention improvement'
			}}>
				<DashboardTab>
					{chart}
				</DashboardTab>
			</DashboardSection>
	}
}