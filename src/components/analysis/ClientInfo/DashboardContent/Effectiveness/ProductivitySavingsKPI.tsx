import * as React from 'react'
import {robins} from 'src/robins'
import {connectRobin} from '@simplus/robin-react'
import get from 'lodash/get'
import keys from 'lodash/keys'
import {
	numberFormater,
	relative,
	Download
} from '../utils'
import {
	DashboardSection,
	DashboardTab,
	KpiChart
} from 'src/components/dashboard'
const {AnalyticsRobin} = robins

interface Props {
	client: string,
	displayRelativeValues: boolean,
	compareBenchmark: boolean,
	benchmarkMapping: any
	apiKey?: string,
	showDownload?: boolean
}
@connectRobin([AnalyticsRobin])
export class ProductivitySavingsKPI extends React.Component<Props> {
	render(): JSX.Element {
		const result = AnalyticsRobin.getResult(this.props.apiKey || 'effectiveness_keymetrics') || {data: {}}
		const kpis_orig = get(result, 'data', {})
		const kpis = {...kpis_orig}
		if (this.props.displayRelativeValues) {
			[kpis].forEach( o => {
				const os = Array.isArray(o) ? o : [o]
				os.forEach( e => {
					keys(e).filter( k => /relative$/.test(k)).map( k => k.replace(/relative$/, '')).forEach( k => {
						e[k] = e[`${k}relative`] * 100
					})
				})
			})
		}
		const chart = <KpiChart
			kpis={[{
				loading: AnalyticsRobin.isLoading(this.props.apiKey || 'effectiveness_keymetrics'),
				error: AnalyticsRobin.isError(this.props.apiKey || 'effectiveness_keymetrics'),
				data : numberFormater(kpis.productivitysavings * 100),
				footer: this.props.compareBenchmark ? kpis.productivitysavingsbenchmark ? `${relative(kpis.productivitysavings, kpis.productivitysavingsbenchmark)} Vs Benchmark (${numberFormater(kpis.productivitysavingsbenchmark * 100)})` : 'NO BENCHMARK' : ''
			}]}
		/>
		const excel_columns = [
			'Company',
			'Period',
			'Productivity savings',
			'Benchmark',
			'Benchmark value'
		]
		const excel_data = [{
				client: this.props.client,
				period: `${result.startdate} to ${result.enddate}`,
				value: numberFormater(kpis_orig[`productivitysavings`] * 100),
				benchmark: result.benchmark ? this.props.benchmarkMapping[result.benchmark[0]] || result.benchmark[0] : `${result.benchmarkstartdate} to ${result.benchmarkenddate}`,
				benchmark_value: numberFormater(kpis_orig[`productivitysavingsbenchmark`] * 100)
			}]
		return <DashboardSection
			bluetheme
			download={this.props.showDownload ? <Download
				columns={excel_columns}
				data={excel_data}
				chart={chart}
				chartWidth='30%'
				name={`${this.props.client} - Effectiveness - Productivity savings`}/> : null}
			section={{
				title : 'Productivity savings',
				tooltip : 'Average post work impact uplift in productivity due to clinical intervention'
			}}>
			<DashboardTab>
			{chart}
			</DashboardTab>
		</DashboardSection>
	}
}