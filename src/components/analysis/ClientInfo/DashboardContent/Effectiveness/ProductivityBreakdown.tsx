import * as React from 'react'
import {robins} from 'src/robins'
import {connectRobin} from '@simplus/robin-react'
import get from 'lodash/get'
import keys from 'lodash/keys'
import {
	Download
} from '../utils'
import {
	DashboardSection,
	DashboardTab,
	DashboardFilter,
	BarChart
} from 'src/components/dashboard'
const {AnalyticsRobin} = robins

interface Props {
	client: string,
	displayRelativeValues: boolean,
	compareBenchmark: boolean,
	benchmark: string,
	benchmarkMapping: any,
	splitBy: string,
	splitFilter(filter: string): void
	apiKey?: string,
	showDownload?: boolean,
	showLabel?: boolean,
	dod?: any
}
@connectRobin([AnalyticsRobin])
export class ProductivityBreakdown extends React.Component<Props> {
	render(): JSX.Element {
		const result = AnalyticsRobin.getResult(this.props.apiKey || 'effectiveness_productivity') || {data: []}
		const productivity_value_orig = get(result, 'data', [])
		const productivity_value = productivity_value_orig.map( o => ({...o}))
		if (this.props.displayRelativeValues) {
			[productivity_value].forEach( o => {
				const os = Array.isArray(o) ? o : [o]
				os.forEach( e => {
					keys(e).filter( k => /relative$/.test(k)).map( k => k.replace(/relative$/, '')).forEach( k => {
						e[k] = e[`${k}relative`] * 100
					})
				})
			})
		}

		const chart = <BarChart
			bars={this.props.compareBenchmark ? ['Actual', `${this.props.benchmarkMapping[this.props.benchmark]} benchmark`] : ['Actual']}
			loading={AnalyticsRobin.isLoading(this.props.apiKey || 'effectiveness_productivity')}
			error={AnalyticsRobin.isError(this.props.apiKey || 'effectiveness_productivity')}
			config={{showLegend: this.props.compareBenchmark}}
			showLabel={this.props.showLabel}
			data={productivity_value
				.sort((a, b) => b.productivitysavings - a.productivitysavings)
				.filter(a => Math.abs(a.productivitysavings * 100) >= 0.5)
				.map( c => {
				return {
					x: c.key,
					'Actual': c.productivitysavings * 100,
					'Previous period benchmark': c.productivitysavingsbenchmark * 100,
					'Sector benchmark': c.productivitysavingsbenchmark * 100,
					'Size benchmark': c.productivitysavingsbenchmark * 100,
					'Company benchmark': c.productivitysavingsbenchmark * 100,
					'Site levels benchmark': c.productivitysavingsbenchmark * 100,
					'Custom range benchmark': c.productivitysavingsbenchmark * 100,
				}
			})}
		/>
		const excel_columns = [
			'Site levels',
			'Company',
			'Split by',
			'Period',
			'Productivity savings',
			'Benchmark',
			'Benchmark value'
		]

		const excel_data = (productivity_value_orig).sort((a: any, b: any) => b.productivitysavings - a.productivitysavings).map(item => {
			const output  = {
				client: this.props.client,
				name: item.key,
				period: `${result.startdate} to ${result.enddate}`,
				value: ((item[`productivitysavings`] * 100) || 0).toFixed(2),
				benchmark: result.benchmark ? this.props.benchmarkMapping[result.benchmark[0]] || result.benchmark[0] : `${result.benchmarkstartdate} to ${result.benchmarkenddate}`,
				benchmark_value: ((item[`productivitysavingsbenchmark`] * 100) || 0).toFixed(2)
			}
			return output
		})
		return <DashboardSection
			download={this.props.showDownload ? <Download
				columns={excel_columns}
				data={excel_data}
				chart={chart}
				name={`${this.props.client} - Effectiveness - Productivity breakdown`}/>: null}
			section={{
				tabTitles : ['Productivity	 breakdown'],
				title : 'Productivity breakdown',
				tooltip : 'Productivity upliftment drill-down'
			}}
			dod={this.props.dod}>
				<DashboardTab>
					<div style={{display: 'flex'}} className='effectiveness-filter'>
						<DashboardFilter
							width={150}
							type='single'
							text='Split by'
							downloadPreview={this.props.dod && this.props.dod.downloadPreview}
							defaultValue={this.props.splitBy}
							options={[
								{value: 'service', text: 'Service'},
								{value: 'product', text: 'Product'},
								{value: 'employement', text: 'Employment'},
								{value: 'problemcluster', text: 'Problem Cluster'},
								{value: 'problem', text: 'Problem'},
								{value: 'protocol', text: 'Protocol'},
								{value: 'origin', text: 'Origin'},
								{value: 'severity', text: 'Severity'}]}
							onChange={(selected) => this.props.splitFilter(selected)
							}/>
					</div>
					{chart}
				</DashboardTab>
			</DashboardSection>
	}
}