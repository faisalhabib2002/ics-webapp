import * as React from 'react'
import {robins} from 'src/robins'
import {connectRobin} from '@simplus/robin-react'
import get from 'lodash/get'
import {
	Download,
	numberFormater,
	currency,
	percentage
} from '../utils'
import {
	DashboardTab,
	TableChart
} from 'src/components/dashboard'
const {AnalyticsRobin} = robins

interface Props {
	client: string,
	benchmark: string,
	benchmarkMapping: any,
	apiKey?: string
	showDownload?: boolean
}
@connectRobin([AnalyticsRobin])
export class ProblemPrioritisation extends React.Component<Props> {
	sortMethod = (a, b) => {
		if (a - b > 0) {
			return 1;
		} else if (a - b < 0) {
			return -1;
		}
		return 0;
	}

	render(): JSX.Element {
		const result = AnalyticsRobin.getResult(this.props.apiKey || 'health_prioritization_drilldown') || {data: []}
		const table = get(result, 'data', [])

		const chart = <TableChart sortable={true} columns={[
			{ Header: 'Problem type', accessor: 'key' },
			{
				Header: 'Risk Hours (benchmark)', accessor: 'riskHoursBenchmark', className: 'center',
				sortMethod: (a,b) => this.sortMethod(a,b)
			},
			{
				Header: 'Risk Hours (actuals)', accessor: 'riskHours', className: 'center',
				sortMethod: (a,b) => this.sortMethod(a,b)
			},
			{
				Header: 'Cost of health', accessor: 'costofhealth', className: 'center',
				sortMethod: (a, b) => {
					a = parseInt(a.split('R')[1]);
					b = parseInt(b.split('R')[1]);
					return this.sortMethod(a,b);
				}
			},
			{
				Header: 'Productivity', accessor: 'productivity', className: 'center',
				sortMethod: (a, b) => {
					a = parseFloat(a.split('%')[0]);
					b = parseFloat(b.split('%')[0])
					return this.sortMethod(a,b);
				}
			},
		]} data={(table || []).map((e, i) => ({
			key: e.key,
			riskHoursBenchmark: numberFormater(e.riskhoursbenchmark),
			riskHours: numberFormater(e.riskhours),
			costofhealth: currency(e.costofhealth),
			productivity: percentage(e.productivity * 100)
		}))} />
			const excel_columns = [
				'Company',
				'Period',
				'Problem type',
				'Risk hours',
				'Cost of health',
				'Prouctivity'
			]

			const excel_data = table.sort((a: any, b: any) => b.breakdown - a.breakdown).map(item => {
				const output  = {
					client: this.props.client,
					period: `${result.startdate} to ${result.enddate}`,
					name: item.key,
					risk_hours: numberFormater(item.riskhours),
					costofhealth: currency(item.costofhealth),
					productivity: percentage((item.productivity * 100) || 0)
				}
				return output
			})

		return <DashboardTab>
				<div style={{display: 'flex', justifyContent: 'flex-end', marginBottom: '0.5rem', alignItems: 'center'}}>
						{this.props.showDownload ?<Download
							columns={excel_columns}
							data={excel_data}
							chart={chart}
							name={`${this.props.client} - Health - Problem prioritization`}/>
							: null}
				</div>
				{chart}
			</DashboardTab>
	}
}