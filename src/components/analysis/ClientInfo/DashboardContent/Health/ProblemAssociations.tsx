import * as React from 'react'
import {robins} from 'src/robins'
import {connectRobin} from '@simplus/robin-react'
import keys from 'lodash/keys'
import get from 'lodash/get'
import {
	Download
} from '../utils'
import {
	DashboardFilter,
	DashboardTab,
	BarChart
} from 'src/components/dashboard'
const {AnalyticsRobin} = robins

interface Props {
	client: string,
	displayRelativeValues: boolean,
	splitBy: string,
	cases: string,
	casesMapping: any,
	changed?: string,
	associations: Array<{value: string, text: string}>,
	problembreakdown: string,
	splitFilter(filter: string): void,
	casesFilter(filter: string): void,
	associationsFilter(filter: string): void
	apiKey?: string,
	showDownload?: boolean
	showLabel?: boolean
	downloadPreview?: boolean
}
@connectRobin([AnalyticsRobin])
export class ProblemAssociations extends React.Component<Props> {
	render(): JSX.Element {
		const result = AnalyticsRobin.getResult(this.props.apiKey || 'health_association') || {data: []}
		const association_orig = get(result, 'data', [])
		const association = association_orig.map(k => ({ ...k }))

		if (this.props.displayRelativeValues) {
			[association].forEach( o => {
				const os = Array.isArray(o) ? o : [o]
				os.forEach( e => {
					keys(e).filter( k => /relative$/.test(k)).map( k => k.replace(/relative$/, '')).forEach( k => {
						e[k] = e[`${k}relative`] * 100
					})
				})
			})
		}

		const chart = <BarChart
			loading={AnalyticsRobin.isLoading(this.props.apiKey || 'health_association') || AnalyticsRobin.isLoading(this.props.apiKey || 'health_breakdown')}
			error={AnalyticsRobin.isError(this.props.apiKey || 'health_association') || AnalyticsRobin.isError(this.props.apiKey || 'health_breakdown')}
			bars={['Actual']}
			config={{showLegend: false}}
			showLabel={this.props.showLabel}
			data={association.sort((a, b) => b.value - a.value).map( a => ({
				x : a.key,
				'Actual': a.valuerelative * 100,
				'Actual_absolute': a.value
			}))}
			yaxisUnits={'%'}
			relativeToTotal={!this.props.displayRelativeValues}
		/>
			const excel_columns = [
				'Company',
				'Site levels',
				'Period',
				'Case Type',
				'Primary Problem',
				'Secondary Problem',
				'Occurance'
			]

			const excel_data = association_orig.sort((a: any, b: any) => b.value - a.value).map(item => {
				const output  = {
					client: this.props.client,
					period: `${result.startdate} to ${result.enddate}`,
					cases: this.props.casesMapping[result.casetype] || 'All',
					association: result.association,
					name: item.key,
					value: item[`value`] || 0
				}
				return output
			})

		return <DashboardTab>
			<div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
				<div style={{ display: 'flex' }}>
					<DashboardFilter
						width={120}
						type='single'
						text='Display problem proportion split by'
						defaultValue={this.props.splitBy}
						downloadPreview={this.props.downloadPreview}
						options={[
							{ value: 'problemcluster', text: 'Problem Cluster' },
							{ value: 'problem', text: 'Problem' }
						]}
						onChange={(selected) => this.props.splitFilter(selected)
						} />
					<DashboardFilter
						width={150}
						type='single'
						text='Filter on'
						defaultValue={this.props.cases || ''}
						downloadPreview={this.props.downloadPreview}
						options={[
							{ value: '', text: 'All Cases' },
							{ value: 'riskcases', text: 'Risk Cases' },
							{ value: 'formalreferals', text: 'Formal Referrals' },
							{ value: 'assistedreferals', text: 'Assisted Referrals' },
							{ value: 'conflictmediation', text: 'Conflict Mediation' },
							{ value: 'severecases', text: 'Severe Cases' },
							{ value: 'significantcases', text: 'Significant Cases' },
							{ value: 'aid', text: 'AID Cases'}
						]}
						onChange={(selected) => this.props.casesFilter(selected)
						} />
					<DashboardFilter
						width={200}
						type='single'
						text='associations for '
						options={this.props.associations}
						downloadPreview={this.props.downloadPreview}
						defaultValue={this.props.changed === 'problem' ? (this.props.associations[0] || { value: '' }).value : this.props.problembreakdown || (this.props.associations[0] || { value: '' }).value}
						onChange={(selected) => this.props.associationsFilter(selected)
						} />
				</div>
				{this.props.showDownload ?<Download
					columns={excel_columns}
					data={excel_data}
					chart={chart}
					name={`${this.props.client} - Health - Problem Associations`}/>
					: null}
			</div>
			{chart}
		</DashboardTab>
	}
}