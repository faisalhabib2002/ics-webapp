import * as React from 'react'
import {robins} from 'src/robins'
import {connectRobin} from '@simplus/robin-react'
import keys from 'lodash/keys'
import get from 'lodash/get'
import {
	numberFormater,
	shortNumber,
	percentage,
	relative,
	currency,
	Download
} from '../utils'
import {
	DashboardSection,
	DashboardTab,
	KpiChart
} from 'src/components/dashboard'
const {AnalyticsRobin} = robins

interface Props {
	client: string,
	displayRelativeValues: boolean,
	compareBenchmark: boolean,
	benchmarkMapping: any
	apiKey?: string
	showDownload?: boolean
}
@connectRobin([AnalyticsRobin])
export class WorkImpactScoreKPI extends React.Component<Props> {
	render(): JSX.Element {
		
		const result = AnalyticsRobin.getResult(this.props.apiKey || 'health_keymetrics') || {data: {}}
		const kpis_orig = get(result, 'data', {})
		const kpis = { ...kpis_orig }

		if (this.props.displayRelativeValues) {
			[kpis].forEach( o => {
				const os = Array.isArray(o) ? o : [o]
				os.forEach( e => {
					keys(e).filter( k => /relative$/.test(k)).map( k => k.replace(/relative$/, '')).forEach( k => {
						e[k] = e[`${k}relative`] * 100
					})
				})
			})
		}

		const excel_columns = [
			'Company',
			'Period',
			'Work impact score',
			'Working hours at risk',
			'Work-Days at risk',
			'Cost of health',
			'Benchmark',
			'Benchmark work impact score',
			'Benchmark working hours at risk',
			'Benchmark work-Days at risk',
			'Benchmark cost of health'
		]

		const excel_data = [{
			client: this.props.client,
			period: `${result.startdate} to ${result.enddate}`,
			workimpact: numberFormater(kpis_orig.workimpact),
			workinghours: shortNumber(kpis_orig.workingriskhours),
			workdays: numberFormater(kpis_orig.mandaysatrisk),
			costofhealth:  shortNumber(kpis_orig.costofhealth || 0, currency),
			benchmark: result.benchmark ? this.props.benchmarkMapping[result.benchmark[0]] || result.benchmark[0] : `${result.benchmarkstartdate} to ${result.benchmarkenddate}`,
			benchmark_workimpact: numberFormater(kpis_orig.workimpactbenchmark),
			benchmark_workinghourst: shortNumber(kpis_orig.workingriskhoursbenchmark),
			benchmark_workdays: numberFormater(kpis_orig.mandaysatriskbenchmark),
			benchamrk_costofhealth: shortNumber(kpis_orig.costofhealthbenchmark || 0, currency),
		}]

		const numberFormaterConditional = this.props.displayRelativeValues ? percentage : numberFormater
		const shortNumberFormaterConditional = this.props.displayRelativeValues ? percentage : shortNumber

		const chart = <KpiChart
			config={{}}
			kpis={[
				{
					loading: AnalyticsRobin.isLoading(this.props.apiKey || 'health_keymetrics'),
					error: AnalyticsRobin.isError(this.props.apiKey || 'health_keymetrics'),
					title: 'Work impact score',
					data: numberFormater(kpis.workimpact),
					footer: this.props.compareBenchmark ? kpis.workimpactbenchmark ? `${relative(kpis.workimpact, kpis.workimpactbenchmark)} Vs Benchmark (${numberFormater(kpis.workimpactbenchmark)})` : 'NO BENCHMARK' : ''
				},
				{
					loading: AnalyticsRobin.isLoading(this.props.apiKey || 'health_keymetrics'),
					error: AnalyticsRobin.isError(this.props.apiKey || 'health_keymetrics'),
					title: 'Working hours at risk',
					data: shortNumberFormaterConditional(kpis.workingriskhours),
					footer: this.props.compareBenchmark ? kpis.workingriskhoursbenchmark ? `${relative(kpis.workingriskhours, kpis.workingriskhoursbenchmark)} Vs Benchmark (${shortNumber(kpis.workingriskhoursbenchmark)})` : 'NO BENCHMARK' : ''
				},
				{
					loading: AnalyticsRobin.isLoading(this.props.apiKey || 'health_keymetrics'),
					error: AnalyticsRobin.isError(this.props.apiKey || 'health_keymetrics'),
					title: 'Work-Days at risk',
					data: numberFormaterConditional(kpis.mandaysatrisk),
					footer: this.props.compareBenchmark ? kpis.mandaysatriskbenchmark ? `${relative(kpis.mandaysatrisk, kpis.mandaysatriskbenchmark)} Vs Benchmark (${numberFormater(kpis.mandaysatriskbenchmark)})` : 'NO BENCHMARK' : ''
				},{
					loading: AnalyticsRobin.isLoading(this.props.apiKey || 'health_keymetrics'),
					error: AnalyticsRobin.isError(this.props.apiKey || 'health_keymetrics'),
					title: 'Costs of health',
					data: shortNumberFormaterConditional(kpis.costofhealth, this.props.displayRelativeValues ? undefined : currency),
					footer: this.props.compareBenchmark ? kpis.costofhealthbenchmark ? `${relative(kpis.costofhealth, kpis.costofhealthbenchmark)} Vs Benchmark (${shortNumber(kpis.costofhealthbenchmark, currency)})` : 'NO BENCHMARK' : ''
				}
			]}/>
		return <DashboardSection
			download={this.props.showDownload ?<Download
				columns={excel_columns}
				data={excel_data}
				chart={chart}
				chartWidth='50%'
				name={`${this.props.client} - Health - Work impact score`}/> : null}
			section={{
				title : 'Work impact score',
				tooltip : 'Aggregate indicator of compromised workplace and social functioning'
			}}>
				<DashboardTab>
					{chart}
				</DashboardTab>
		</DashboardSection>
	}
}