import * as React from 'react'
import {robins} from 'src/robins'
import {connectRobin} from '@simplus/robin-react'
import keys from 'lodash/keys'
import get from 'lodash/get'
import {
	Download
} from '../utils'
import {
	DashboardFilter,
	DashboardTab,
	BarChart
} from 'src/components/dashboard'
const {AnalyticsRobin} = robins

interface Props {
	client: string,
	displayRelativeValues: boolean,
	splitBy: string,
	cases: string,
	compareBenchmark: boolean,
	casesMapping: any,
	benchmark: string,
	benchmarkMapping: any,
	splitFilter(filter: string): void,
	casesFilter(filter: string): void
	apiKey?: string
	showDownload?: boolean
	showLabel?: boolean
	downloadPreview?: boolean
}
@connectRobin([AnalyticsRobin])
export class ProblemBreakdown extends React.Component<Props> {
	render(): JSX.Element {
		const result = AnalyticsRobin.getResult(this.props.apiKey || 'health_breakdown') || {data: []}
		const breakdown_orig = get(result, 'data', [])
		const breakdown = breakdown_orig.map(k => ({ ...k }))

		if (this.props.displayRelativeValues) {
			[breakdown].forEach( o => {
				const os = Array.isArray(o) ? o : [o]
				os.forEach( e => {
					keys(e).filter( k => /relative$/.test(k)).map( k => k.replace(/relative$/, '')).forEach( k => {
						e[k] = e[`${k}relative`] * 100
					})
				})
			})
		}

		const chart = <BarChart
			loading={AnalyticsRobin.isLoading(this.props.apiKey || 'health_breakdown')}
			error={AnalyticsRobin.isError(this.props.apiKey || 'health_breakdown')}
			bars={this.props.compareBenchmark ? ['Actual', `${this.props.benchmarkMapping[this.props.benchmark]} benchmark`] : ['Actual']}
			config={{ showLegend: this.props.compareBenchmark }}
			showLabel={this.props.showLabel}
			downloadPreview={this.props.downloadPreview}
			data={breakdown.sort((a, b) => b.breakdown - a.breakdown).map(k => {
				return {
					x: k.key,
					'Actual': k.breakdownrelative * 100,
					'Actual_absolute': k.breakdown,
					'Previous period benchmark':  k.breakdownbenchmarkrelative * 100,
					'Previous period benchmark_absolute': k.breakdownbenchmark,
					'Sector benchmark': k.breakdownbenchmarkrelative * 100,
					'Sector benchmark_absolute': k.breakdownbenchmark,
					'Size benchmark': k.breakdownbenchmarkrelative * 100,
					'Size benchmark_absolute': k.breakdownbenchmark,
					'Company benchmark': k.breakdownbenchmarkrelative * 100,
					'Company benchmark_absolute': k.breakdownbenchmark,
					'Site levels benchmark': k.breakdownbenchmarkrelative * 100,
					'Site levels benchmark_absolute': k.breakdownbenchmark,
					'Custom range benchmark': k.breakdownbenchmarkrelative * 100,
					'Custom range benchmark_absolute': k.breakdownbenchmark,
				}
			})}
			yaxisUnits={'%'}
			relativeToTotal={!this.props.displayRelativeValues}
		/>
			const excel_columns = [
				'Site levels',
				'Company',
				'Period',
				'Case Type',
				'Split by',
				'Occurance',
				'Benchmark',
				'Benchmark value'
			]

			const excel_data = breakdown_orig.sort((a: any, b: any) => b.breakdown - a.breakdown).map(item => {
				const output  = {
					client: this.props.client,
					period: `${result.startdate} to ${result.enddate}`,
					cases: this.props.casesMapping[result.casetype] || 'All',
					name: item.key,
					value: item[`breakdown`] || 0,
					benchmark: result.benchmark ? this.props.benchmarkMapping[result.benchmark[0]] || result.benchmark[0] : `${result.benchmarkstartdate} to ${result.benchmarkenddate}`,
					benchmark_value: item[`breakdownbenchmark`] || 0
				}
				return output
			})

		return <DashboardTab>
				<div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
					<div style={{display: 'flex'}}>
						<DashboardFilter
							width={120}
							type='single'
							text='Display problem proportion split by'
							downloadPreview={this.props.downloadPreview}
							defaultValue={this.props.splitBy}
							options={[
								{ value: 'problemcluster', text: 'Problem Cluster' },
								{ value: 'problem', text: 'Problem' }
							]}
							onChange={selected => this.props.splitFilter(selected)}
							// this.pageRedirect({ problemsplit selected, changed: 'problem' }, true)}
							/>
						<DashboardFilter
							width={150}
							type='single'
							text='Filter on'
							downloadPreview={this.props.downloadPreview}
							defaultValue={this.props.cases || ''}
							options={[
								{ value: '', text: 'All Cases' },
								{ value: 'riskcases', text: 'Risk Cases' },
								{ value: 'formalreferals', text: 'Formal Referrals' },
								{ value: 'assistedreferals', text: 'Assisted Referrals' },
								{ value: 'conflictmediation', text: 'Conflict Mediation' },
								{ value: 'severecases', text: 'Severe Cases' },
								{ value: 'significantcases', text: 'Significant Cases' },
								{ value: 'aid', text: 'AID Cases'}
							]}
							onChange={selected => this.props.casesFilter(selected)}
								// this.pageRedirect({ breakdownFilterOn: selected, changed: 'risk' }, true)
						/>
						</div>
						{this.props.showDownload ?<Download
							columns={excel_columns}
							data={excel_data}
							chart={chart}
							name={`${this.props.client} - Health - Problem breakdown`}/>
							: null}
				</div>
				{chart}
			</DashboardTab>
	}
}