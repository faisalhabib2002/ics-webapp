import * as React from 'react'
import {robins} from 'src/robins'
import {connectRobin} from '@simplus/robin-react'
import keys from 'lodash/keys'
import get from 'lodash/get'
import {
	numberFormater,
	shortNumber,
	percentage,
	relative,
	currency,
	Download
} from '../utils'
import {
	DashboardSection,
	DashboardTab,
	KpiChart
} from 'src/components/dashboard'
const {AnalyticsRobin} = robins

interface Props {
	client: string,
	displayRelativeValues: boolean,
	compareBenchmark: boolean,
	benchmarkMapping: any
	apiKey?: string,
	showDownload?: boolean
}
@connectRobin([AnalyticsRobin])
export class HighRiskFactorKPI extends React.Component<Props> {
	render(): JSX.Element {

		const result = AnalyticsRobin.getResult(this.props.apiKey || 'health_keymetrics') || {data: {}}
		const kpis_orig =  get(result, 'data',  {})
		const kpis = { ...kpis_orig }

		if (this.props.displayRelativeValues) {
			[kpis].forEach( o => {
				const os = Array.isArray(o) ? o : [o]
				os.forEach( e => {
					keys(e).filter( k => /relative$/.test(k)).map( k => k.replace(/relative$/, '')).forEach( k => {
						e[k] = e[`${k}relative`] * 100
					})
				})
			})
		}

		const excel_columns = [
			'Company',
			'Period',
			'Risk cases',
			'Formal referrals',
			'Assisted referrals',
			'Conflict mediation',
			'Benchmark',
			'Benchmark cost of health',
			'Benchmark risk cases',
			'Benchmark formal referrals',
			'Benchmark assisted referrals',
			'Benchmark conflict mediation'
		]

		const excel_data = [{
				client: this.props.client,
				period: `${result.startdate} to ${result.enddate}`,
				risk: numberFormater(kpis_orig.riskcases || 0),
				formal_referrals: numberFormater(kpis_orig.formalreferals || 0),
				assisted_referrals: numberFormater(kpis_orig.assistedreferals || 0),
				conflict_mediation:  numberFormater(kpis_orig.conflictmediation || 0),
				benchmark: result.benchmark ? this.props.benchmarkMapping[result.benchmark[0]] || result.benchmark[0] : `${result.benchmarkstartdate} to ${result.benchmarkenddate}`,
				risk_benchmark: numberFormater(kpis_orig.riskcasesbenchmark || 0),
				formal_referrals_benchmark: numberFormater(kpis_orig.formalreferalsbenchmark || 0),
				assisted_referrals_benchmark: numberFormater(kpis_orig.assistedreferalsbenchmark || 0),
				conflict_mediation_benchmark: numberFormater(kpis_orig.conflictmediationbenchmark || 0)
		}]

		const numberFormaterConditional = this.props.displayRelativeValues ? percentage : numberFormater

		const chart = <KpiChart
		config={{}}
		kpis={[
			{
				loading: AnalyticsRobin.isLoading(this.props.apiKey || 'health_keymetrics'),
				error: AnalyticsRobin.isError(this.props.apiKey || 'health_keymetrics'),
				title: 'Risk cases',
				data: numberFormaterConditional(kpis.riskcases),
				footer: this.props.compareBenchmark ? kpis.riskcasesbenchmark ? `${relative(kpis.riskcases, kpis.riskcasesbenchmark)} Vs Benchmark (${numberFormater(kpis.riskcasesbenchmark)})` : 'NO BENCHMARK' : ''
			},
			{
				loading: AnalyticsRobin.isLoading(this.props.apiKey || 'health_keymetrics'),
				error: AnalyticsRobin.isError(this.props.apiKey || 'health_keymetrics'),
				title: 'Formal referrals',
				data: numberFormaterConditional(kpis.formalreferals),
				footer: this.props.compareBenchmark ? kpis.formalreferalsbenchmark ? `${relative(kpis.formalreferals, kpis.formalreferalsbenchmark)} Vs Benchmark (${numberFormater(kpis.formalreferalsbenchmark)})` : 'NO BENCHMARK' : ''
			},
			{
				loading: AnalyticsRobin.isLoading(this.props.apiKey || 'health_keymetrics'),
				error: AnalyticsRobin.isError(this.props.apiKey || 'health_keymetrics'),
				title: 'Assisted referrals',
				data: numberFormaterConditional(kpis.assistedreferals),
				footer: this.props.compareBenchmark ? kpis.assistedreferalsbenchmark ? `${relative(kpis.assistedreferals, kpis.assistedreferalsbenchmark)} Vs Benchmark (${numberFormater(kpis.assistedreferalsbenchmark)})` : 'NO BENCHMARK' : ''
			},
			{
				loading: AnalyticsRobin.isLoading(this.props.apiKey || 'health_keymetrics'),
				error: AnalyticsRobin.isError(this.props.apiKey || 'health_keymetrics'),
				title: 'Conflict Mediation',
				data: numberFormaterConditional(kpis.conflictmediation),
				footer: this.props.compareBenchmark ? kpis.conflictmediationbenchmark ? `${relative(kpis.conflictmediation, kpis.conflictmediationbenchmark)} Vs Benchmark (${numberFormater(kpis.conflictmediationbenchmark)})` : 'NO BENCHMARK' : ''
			}
		]}
	/>
		return <DashboardSection
		download={this.props.showDownload ?<Download
			columns={excel_columns}
			data={excel_data}
			chart={chart}
			chartWidth='50%'
			name={`${this.props.client} - Employee risk factors`}/>: null}
		section={{
			title : 'Employee risk factors',
			tooltip : 'Employee risk and referral metrics'
		}}>
			<DashboardTab>
				{chart}
			</DashboardTab>
		</DashboardSection>
	}
}