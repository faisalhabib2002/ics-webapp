import * as React from 'react'
import {HashRouter as Router, Route, Switch, Redirect, RouteComponentProps} from 'react-router-dom'
import {Effectiveness, Health, Roi, Engagement, Quality} from './DashboardContent/index'
export class ClientDashboard extends React.Component<RouteComponentProps<{id: string}>> {

	render(): JSX.Element {
		return(
			<Router>
				<Switch>
					<Redirect exact from='/client/:id/dashboard' to={`/client/${this.props.match.params.id}/dashboard/engagement`}/>
					<Route exact path='/client/:id/dashboard/engagement' component={Engagement} />
					<Route path='/client/:id/dashboard/health' component={Health} />
					<Route path='/client/:id/dashboard/effectiveness' component={Effectiveness} />
					<Route path='/client/:id/dashboard/roi' component={Roi} />
					<Route path='/client/:id/dashboard/quality' component={Quality} />
					{/* <Route path='/client-prioritisation/:id/dashboard/cost-to-serve' component={CostToServe} /> */}
				</Switch>
			</Router>
		)
	}

}
export default ClientDashboard;