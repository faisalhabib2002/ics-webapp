import * as React from 'react';
import {Route, Switch, Redirect, RouteComponentProps} from 'react-router-dom'
import {ProgrammeInfo} from './ProgrammeInfo'
import {ClientDashboard} from './Dashboard'
import {IcasSummary} from './IcasSummary'
import {Prioritisation} from './Prioritisation'
// import {Notes} from './Notes'
import {DashboardOnDemand} from './DashboardOnDemand'
import { TopMenuLayout } from './layout/TopMenuLayout';

export class ClientInfo extends React.Component<RouteComponentProps<{id: string}>> {
	/**
	 *  Render method
	 */
	render(): JSX.Element {
		return(
			<TopMenuLayout>
				<Switch>
					<Redirect exact from='/client/:id' to={`/client/${this.props.match.params.id}/programmeinfo`}/>
					<Route path='/client/:id/summary' component={IcasSummary} />
					<Route path='/client/:id/dashboard' component={ClientDashboard} />
					<Route path='/client/:id/programmeinfo' component={ProgrammeInfo} />
					<Route path='/client/:id/prioritisation/:department?/:key?' component={Prioritisation} />
					{/* <Route path='/client/:id/notes' component={Notes} />  */}
					<Route path='/client/:id/dod/:key' component={DashboardOnDemand} />
					<Route path='/client/:id/dod' component={DashboardOnDemand} />
					<Redirect to='/' />
				</Switch>
		</TopMenuLayout>
		)
	}
}

export default ClientInfo;