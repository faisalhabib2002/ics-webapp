import * as React from 'react'
import {Select, Card} from '@simplus/siui'
import Divider from 'antd/lib/divider'
import Tooltip from 'antd/lib/tooltip'
import Icon from 'antd/lib/icon'
import {connectRobin} from '@simplus/robin-react'
import {robins} from '../../../robins'
import {RouteComponentProps } from 'react-router-dom';
// import {hasPermission} from '../../../utils'
import {ErrorBoundary} from '../../../utils/ErrorBoundary'
import defaults from 'lodash/defaults'
import {DateRangeFilter} from './DashboardContent/utils'
import * as moment from 'moment'
import * as queryString from 'query-string'
const Option = Select.Option;
const {PermissionsRobin, SummaryRobin, UsersRobin} = robins;

@connectRobin([PermissionsRobin, SummaryRobin, UsersRobin])
export class IcasSummary extends React.Component<RouteComponentProps<{id: string}>> {
	location = ''
	state = {filter: moment('2018-01-01')}
	unlisten?: () => void = undefined
	componentWillMount(): void {
		this.applyFilters()
		this.unlisten = this.props.history.listen((location, action) => {
			if (action === 'POP')
				this.applyFilters()
		});
	}
	applyFilters(): void {
		if ((!this.location) || (this.location !== this.props.location.search)) {
			const dateRange = queryString.parse(location.hash.split('?')[1]).dateRange || 'month'
			const startDate = queryString.parse(location.hash.split('?')[1]).startDate || moment().startOf('month').subtract(1, dateRange).format('YYYY-MM-DD');
			const endDate = queryString.parse(location.hash.split('?')[1]).endDate || moment().subtract(1, dateRange).endOf(dateRange).format('YYYY-MM-DD');
			const client = this.props.match.params.id
			const options: any = {
				startdate : startDate,
				enddate : endDate,
			}
			SummaryRobin.when(SummaryRobin.findOne(client, {...options})).then((data) => {
				const summary_data = SummaryRobin.getResult(data)
				const all_users: string[] = []
				summary_data.map( summary => {
					if (summary.type === 'change_of_custodian') {
						if (summary.body.old && (all_users.indexOf(summary.body.old as string) < 0))
							all_users.push(summary.body.old as string)
						if (summary.body.new && (all_users.indexOf(summary.body.new as string) < 0))
							all_users.push(summary.body.new)
					}
				})
				UsersRobin.get(`${this.props.match.params.id}-summary`, `/list/${JSON.stringify(all_users)}`)
			})
		}
	}
	componentWillUnmount(): void {
		if (this.unlisten)
			this.unlisten();
	}
	pageRedirect(update: any, refrsh: boolean = false): void {
		const state = {
			...this.urlState(),
			...update}
		this.props.history.push(`${this.props.location.pathname}?${queryString.stringify(state)}`)
		if (refrsh)
			this.applyFilters()
	}

	urlState(): any {
		return queryString.parse(location.hash.split('?')[1])
	}

	render(): JSX.Element {
		const clientID = this.props.match.params.id
		const usersList =  UsersRobin.getResult(`${clientID}-summary`);
		const { dateRange, risk} = defaults(this.urlState(), {
			dateRange : 'month',
			risk: '10'
		})
		let data = SummaryRobin.getModel() || []
		if (data.length > 0)
			data = data.sort((a, b) => b.date - a.date)
		const filtered_data = (risk === 'all') ? data : data.slice(0, parseInt(risk))

		return(
			<ErrorBoundary>
				<div className='summary'>
					<div className='summary-top-bar'>
						<h1 className='analysis-tab-title'>Summary</h1>
						{/* Might be added in future */}
						{/* {hasPermission('/view/summary/edit-summary', PermissionsRobin.getResult('own-permissions')) ?
						<Button className='new-risk-tag'>New risk tag</Button>
						: null} */}
					</div>
					<Divider/>
					<div className='summary-filters'>
						<DateRangeFilter value={dateRange} customOption financialStartYear={moment(this.state.filter)} label='DATE RANGE' style={{minWidth: 130, marginRight: '1rem'}}
							onChange={(selected) => this.pageRedirect({
								dateRange: selected.value,
								startDate: moment(selected.startDate).format('YYYY-MM-DD'),
								endDate: moment(selected.endDate).format('YYYY-MM-DD')
							}, true)
						}/>
						<Select
							value={risk}
							label='RISK TAG'
							style={{marginRight: '1rem', minWidth: 100}}
							onChange={(selected) => this.pageRedirect({
								risk: selected
							}, true)
							}>
							<Option value = '10'>Top 10 risk tags</Option>
							<Option value = '20'>Top 20 risk tags</Option>
							<Option value = 'all'>All risk tags</Option>
						</Select>
					</div>
					<div className='summary-content'>
					<Card padding style={{width: '70rem'}} loading={SummaryRobin.isLoading(SummaryRobin.ACTIONS.FIND_ONE)}>
					<div className='summary-content-title'>
							<span style={{marginRight: '0.5rem'}}>Risk factors</span>
							<Tooltip title='Tooltip...'>
								<Icon style={{color: '#909090'}} type='info-circle' />
							</Tooltip>
						</div>
						{
							filtered_data.map(item => {
								let title = ''
								let description = ''
								switch (item.type) {
									case 'change_of_custodian':
										title = 'Custodian changed'
										const previous_custodian = (usersList && usersList[item.body.old]) ? usersList[item.body.old] : {name: 'Unassigned'}
										const new_custodian = (usersList && usersList[item.body.new]) ? usersList[item.body.new] : {name: 'Unassigned'}
										description = `The custodian has been changed from ${previous_custodian.name} to ${new_custodian.name}`
										break;
									case 'low_referals':
										title = 'Low referals'
										description = `Formal referals is at ${item.body.formalreferals} which has fallen below benchmark of ${item.body.formalreferalsbenchmark}`
										break;
									case 'low_engagement':
										title = 'Low engagement'
										description = `Engagement utilisation is at ${(item.body.utilization * 100).toFixed(2)}% which has fallen below benchmark of ${(item.body.utilizationbenchmark * 100).toFixed(2)}%`
										break;
									default:
										description = 'Unknown event type'
								}
								return <div className='summary-item' key={item._id}>
										<div className='summary-item-date'>{moment(item.date, 'X').format('dddd, MMMM Do YYYY, h:mm a')}</div>
											<div className='summary-item-row'>
											<img src='/src/assets/img/summary.png'/>
											<div>
												<div className='summary-event'>{title}</div>
												<div className='summary-description'>{description}</div>
											</div>
										</div>
									</div>
								})
							}
						</Card>
					</div>
				</div>
			</ErrorBoundary>
		)
	}

}
export default IcasSummary;