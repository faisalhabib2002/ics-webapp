import * as React from 'react'
import {ErrorBoundary} from '../../utils/ErrorBoundary'
export interface DashboardContainerProps {
	style ?: React.CSSProperties
}

export class DashboardContainer extends React.Component<DashboardContainerProps> {
	render(): JSX.Element {
		return <ErrorBoundary>
			<div className='dashboard-container' style={this.props.style}>
				<table style = {{width: '100%', tableLayout: 'fixed'}}>
					<tbody>
						{React.Children.map(this.props.children, (item, index) => {
							return <tr key={index}>
								<td colSpan={1}>
										{item}
									</td>
							</tr>
						})}
					</tbody>
				</table>
			</div>
		</ErrorBoundary>
	}
}