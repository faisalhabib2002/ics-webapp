import * as React from "react";
import get from "lodash/get";
import values from "lodash/values";
import {
  BarChart as BC,
  XAxis,
  YAxis,
  Legend,
  Label,
  Bar,
  ResponsiveContainer,
  Tooltip,
  ReferenceLine,
  AxisDomain,
} from "recharts";
import { Loader, Button } from "@simplus/siui";
import { ErrorBoundary } from "../../../utils/ErrorBoundary";
import { Spin } from "antd";

const DEFAULT_COLORS = ["#66959d", "#aa6d8e", "#F6E9E8", "#B55C53"];

export interface BarChartProps {
  config?: {
    colors?: string;
    showLegend?: boolean;
  };
  bars: string[];
  data: Array<{
    x: string;
    [key: string]: string | number | number[];
  }>;
  showLabel?: boolean;
  minHeight?: number;
  yaxisWidth?: number;
  yaxisUnits?: string;
  yaxisLabel?: string;
  xaxisDomain?: AxisDomain[];
  barSize?: number;
  horizontal?: boolean;
  relativeToTotal?: boolean;
  hideEmptyBars?: boolean;
  loading?: boolean;
  error?: boolean;
  downloadPreview?: boolean;
  tooltip?: any;
}

export class BarChart extends React.Component<BarChartProps> {
  state = {
    shrink: true,
  };
  showchange = () => {
    this.setState({ shrink: !this.state.shrink });
  };
  render(): JSX.Element {
    const COLORS = get(this.props, "config.colors", DEFAULT_COLORS);
    const showLegend = get(this.props, "config.showLegend", false);
    let data = this.props.data;
    let unit = this.props.yaxisUnits;
    let max_label_length = 0;
    const emptyBars: string[] = [];

    if (this.props.relativeToTotal && data && data.length > 0) {
      unit = "%";

      // Commenting below code in case it is required again in future
      // 	const totals = mapValues(data[0], (v, k) => sum(data.map( o => o[k])))
      // 	data = data.map( e => {
      // 		return {...mapKeys(e, (k, v) => `___${v}`), ...mapValues(e, (v, k) => {
      // 				if (k !== 'x' && typeof v === 'number') {
      // 					const total = totals[k]
      // 					return (v / total) * 100
      // 				}
      // 				return v
      // 		})}
      // 	}) as any
    }
    const NO_DATA = (
      <div
        style={{ margin: "20px", display: "flex", justifyContent: "center" }}
      >
        <img src="/src/assets/img/no_data.png" width="70%" />
      </div>
    );
    if (this.props.loading)
      return (
        <div
          style={{
            height: 200,
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Spin />
        </div>
      );
    let negative = false;
    if (data) {
      data = data.filter((e) => {
        if (e.Actual === 0) return false;
        const numbers = values(e).filter((v) => typeof v === "number");
        const numbersAboveZero = numbers.filter(
          (v) => !isNaN(v as any) && Number(v).toFixed(4) !== "0.0000"
        ); // Treat numbers smaller than 0.0001 as 0
        if (numbersAboveZero.length === 0) {
          emptyBars.push(get(e, "x") as string);
          negative = true;
        }
        return numbers.length === 0 || numbersAboveZero.length > 0;
      });
      // const labels: string[] = []
      data.map((items) => {
        for (const key in this.props.bars) {
          const name = this.props.bars[key];
          items[name] = items[name] || 0;
        }
        // Replace _ with space to wrap long words - Faisal Nov 20, 2019
        items.x = items.x.split("_").join(" ");
        // Calculate a single word which is longest
        const name = items.x.split(" ");
        for (const i in name)
          if (name[i].length > max_label_length)
            max_label_length = name[i].length;
      });
      if (data.length === 0) return NO_DATA;
      const final_data =
        this.state.shrink && !this.props.horizontal
          ? data.filter((i, index) => index < 10)
          : data;
      const xaxisDomain = this.props.xaxisDomain
        ? this.props.xaxisDomain
        : negative
        ? ["auto", "auto"]
        : [0, "auto"];
      const leftMargin = this.props.horizontal ? 5 : 2 * max_label_length; //Made left margin dynamic to accomodate for long words
      return (
        <ErrorBoundary>
          <div
            style={{
              position: "relative",
              display: "flex",
              flexDirection: "column",
              alignItems: "flex-end",
              paddingBottom: "5px",
            }}
          >
            {this.props.loading ? <Loader /> : null}
            <ResponsiveContainer
              width="100%"
              height="100%"
              minHeight={
                this.props.minHeight ||
                (this.props.horizontal
                  ? 480
                  : (final_data || []).length * 70 + 200)
              }
              maxHeight={
                this.props.minHeight ||
                (this.props.horizontal
                  ? 480
                  : (final_data || []).length * 70 + 200)
              }
            >
              <BC
                data={final_data}
                layout={this.props.horizontal ? "horizontal" : "vertical"}
                margin={{ top: 20, right: 60, bottom: 20, left: leftMargin }}
              >
                <XAxis
                  tickFormatter={(value) =>
                    this.props.horizontal
                      ? value
                      : +parseFloat(value).toFixed(2)
                  }
                  unit={this.props.horizontal ? "" : this.props.yaxisUnits}
                  dataKey={this.props.horizontal ? "x" : undefined}
                  type={this.props.horizontal ? "category" : "number"}
                  domain={xaxisDomain}
                />
                <YAxis
                  unit={this.props.horizontal ? this.props.yaxisUnits : ""}
                  width={this.props.yaxisWidth || 120}
                  dataKey={this.props.horizontal ? undefined : "x"}
                  type={this.props.horizontal ? "number" : "category"}
                  domain={negative ? ["auto", "auto"] : [0, "auto"]}
                >
                  <Label position="insideLeft">{this.props.yaxisLabel}</Label>
                </YAxis>
                <ReferenceLine stroke="black" x={0} />
                <ReferenceLine stroke="black" y={0} />
                <Tooltip
                  content={(props) => {
                    if (props.active) {
                      return (
                        <div>
                          {props.payload.map((p, index) => {
                            return (
                              <div key={index}>
                                <h3>
                                  {p.payload.x} - {p.dataKey}
                                </h3>
                                {this.props.tooltip ? (
                                  this.props.tooltip(p.payload[p.dataKey])
                                ) : this.props.relativeToTotal ? (
                                  <p>
                                    <span>
                                      {(p.payload[p.dataKey] || 0).toFixed(2)}
                                      {unit} (
                                      {
                                        +parseFloat(
                                          p.payload[`${p.dataKey}_absolute`] ||
                                            0
                                        ).toFixed(2)
                                      }
                                      )
                                    </span>
                                  </p>
                                ) : (
                                  <p>
                                    {(p.payload[p.dataKey] || 0).toFixed(2)}
                                    {unit}
                                  </p>
                                )}
                                {/* Commenting below code in case it is required again in future
														<p ><span style={{fontWeight: 700}}>{(p.payload[`___${p.dataKey}`] === undefined) ? '' : (p.payload[`___${p.dataKey}`] === null) ? '0' : +parseFloat(p.payload[`___${p.dataKey}`]).toFixed(2)} ({(p.payload[`${p.dataKey}_relative`] || 0).toFixed(2)}{unit})</span></p> 
														*/}
                              </div>
                            );
                          })}
                        </div>
                      );
                    }
                    return null;
                  }}
                />
                {showLegend === true && (
                  <Legend
                    wrapperStyle={{ top: -10 }}
                    verticalAlign="top"
                    height={36}
                    width={200}
                    align="right"
                    iconType="circle"
                    layout="vertical"
                  />
                )}
                {this.props.bars.map((b: string, index) => {
                  // Custom label function
                  const customLabel = (props) => {
                    const { x, y, width, height, value, index } = props;
                    const absoluteValue = final_data[index][`${b}_absolute`];
                    const isHorizontal = this.props.horizontal;

                    // Adjust position based on layout
                    const labelX = isHorizontal
                      ? x + width / 2
                      : x + width + 50;
                    const labelY = isHorizontal ? y - 5 : y + height / 2;

                    return (
                      <text
                        x={labelX}
                        y={labelY}
                        textAnchor="middle"
                        dominantBaseline="middle"
                        fill="#000"
                      >
                        {`${
                          isNaN(value)
                            ? value
                            : this.props.tooltip
                            ? this.props.tooltip(value)
                            : (+parseFloat(value)).toFixed(2)
                        }${unit || ""} ${
                          absoluteValue
                            ? "(" +
                              parseFloat((absoluteValue as any) || 0) +
                              ")"
                            : ""
                        }`}
                        {/* {`${isNaN(value) ? value : (+parseFloat(value)).toFixed(2)}${unit || ''} 
											${absoluteValue ? '('+parseFloat(absoluteValue as any || 0)+')' : ''}
											`} */}
                      </text>
                    );
                  };
                  //
                  return (
                    <Bar
                      isAnimationActive={false}
                      key={`${b} - ${index}`}
                      dataKey={b}
                      barSize={this.props.barSize || 30}
                      fill={COLORS[index]}
                      label={this.props.showLabel && customLabel}
                    >
                      {/* <Label position="top" >{b}</Label> */}
                    </Bar>
                  );
                })}
              </BC>
            </ResponsiveContainer>
            {!this.props.downloadPreview && data.length > 10 ? (
              <Button
                style={{ marginRight: "8px", padding: "10px" }}
                rounded
                onClick={this.showchange}
              >
                {this.state.shrink ? "Show all items" : "Show top 10 items"}
              </Button>
            ) : null}
          </div>
        </ErrorBoundary>
      );
    }

    return NO_DATA;
  }
}
