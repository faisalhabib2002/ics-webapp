import * as React from 'react'
import get from 'lodash/get'
import {Loader} from '@simplus/siui'
import {ErrorBoundary} from '../../../utils/ErrorBoundary'
const DEFAULT_COLORS = ['#333'];
const DEFAULT_BACKGROUND = ['#FFF'];

export interface KpiChartProps {
	config?: {
		background?: string
		color?: string
		unit?: string
	}
	kpis: Array<{
		title?: string
		data: any
		footer?: any
		loading?: boolean
		error?: boolean
	}>
}

export class KpiChart extends React.Component<KpiChartProps> {
	render(): JSX.Element {
		const BACKGROUND = get(this.props, 'config.background', DEFAULT_BACKGROUND)
		const COLOR = get(this.props, 'config.color', DEFAULT_COLORS)
		const FOOTER_COLOR = get(this.props, 'config.color', ['#909090'])
		const unit = get(this.props, 'config.unit', '')
		return <ErrorBoundary>
			<div className='kpi-charts'>
				{
					this.props.kpis.map((kpi, index) => {
						const border = index < this.props.kpis.length - 1 ? '1px solid #999' : '';
						return <div
							key={index}
							className='kpi'
							style={{
								backgroundColor: BACKGROUND,
								color: COLOR
								}
							}>
								<div className='kpi-title'>{kpi.title}</div>
								<div className='kpi-data' style={{borderRight: border, position: 'relative'}}>
									{(kpi.loading || kpi.error) ? <div style={{color: 'transparent'}}><Loader error={kpi.error}/>-</div> : kpi.data}
									{unit}
								</div>
								<div className='kpi-footer' style={{position: 'relative', color: FOOTER_COLOR}}>{kpi.loading ? null : kpi.footer}</div>
						</div>
					}
				)}
			</div>
		</ErrorBoundary>
	}
}