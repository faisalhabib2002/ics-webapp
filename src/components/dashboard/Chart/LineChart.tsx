import * as React from 'react'
import {LineChart as LC, Line, XAxis, YAxis, Tooltip, Legend, ResponsiveContainer} from 'recharts'
import { Loader} from '@simplus/siui'
import {ErrorBoundary} from '../../../utils/ErrorBoundary'
export interface LineChartProps {
	lines: string[]
	data: any
	loading?: boolean
	error?: boolean
	relativeToTotal?: boolean
}
export class LineChart extends React.Component<LineChartProps>{

	render(): JSX.Element {
		const data = this.props.data
		const lines = this.props.lines
		let unit = ''
		if (this.props.relativeToTotal && data && data.length > 0)
			unit = '%'
		const DEFAULT_COLORS = ['#66959d', '#aa6d8e', '#82ca9d', '#B55C53'];
		return <ErrorBoundary>
			<div style={{position : 'relative', display: 'flex', flexDirection: 'column', alignItems: 'flex-end', paddingBottom: '5px'}}>
				{this.props.loading ? <Loader/> : null}
				<ResponsiveContainer width='100%' height='100%' minHeight={300} >
					<LC data={data}  margin={{
						top: 20,
						right: 40,
						left: 0,
						bottom: 20,
					}}> 
						<XAxis dataKey="date" />
						<YAxis unit={unit} type='number' domain={[0, 'auto']}/>
						<Tooltip />
						<Legend verticalAlign="top" height={36}/>
						{lines.map((line: string, index) => <Line type="monotone" strokeWidth={3} activeDot={{ r: 8 }} key={`${line} - ${index}`} dataKey={line} stroke={DEFAULT_COLORS[index]} />)}
					</LC>
					</ResponsiveContainer>
			</div>
		</ErrorBoundary>
	}
	
}