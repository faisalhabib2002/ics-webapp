import * as React from 'react'
// import randomColor from 'randomcolor'
import ReactWordCloud from 'react-wordcloud';
import {ErrorBoundary} from '../../../utils/ErrorBoundary'
export interface WordCloudProps {
	words: Array<{
		word: string,
		count: number,
		score?: number //added score because it was coming from api response
	}>
}

export class WordCloud extends React.Component<WordCloudProps> {
	componentDidMount(): void {
		window.addEventListener('resize', () => {
			this.setState({
				resize: Math.random()
			})
		})
	}
	componentWillUnmount(): void {
		window.removeEventListener('resize',() => {})
	}
	render(): JSX.Element {
		const data = this.props.words||[]
		if (!data.length)
			return <div style={{textAlign: 'center', margin: '20px', display: 'flex', justifyContent: 'center', alignItems: 'center', position: 'relative', height: 200}}>No data</div>
		const converted_words = data.map(item => {
			return {text: item.word, value: item.count}
		})
		return <div style={{ width: '100%', height: 400 }}>
			<ErrorBoundary>
				<ReactWordCloud
					words={converted_words}
					options={{
						enableTooltip: true,
						deterministic: true,
						fontSizes: [24, 60],
						fontStyle: 'normal',
						fontWeight: 'normal',
						padding: 1,
						rotations: 3,
						rotationAngles: [0, 0],
						scale: 'sqrt',
						spiral: 'archimedean',
						transitionDuration: 0,
					}}
					/>
			</ErrorBoundary>
		</div>
	}
}