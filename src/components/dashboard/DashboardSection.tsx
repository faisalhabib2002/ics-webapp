import * as React from 'react'
import {Card} from '@simplus/siui'
import Tooltip from 'antd/lib/tooltip'
import Icon from 'antd/lib/icon'
import Tabs from 'antd/lib/tabs'
import {ErrorBoundary} from '../../utils/ErrorBoundary'
import { Button, Menu, Dropdown } from 'antd'
const {TabPane} = Tabs

export interface DashboardSectionModel {
	title?: string,
	tooltip?: string,
	tabTitles?: string[]
	tabTooltips?: string[]
}

export interface DashboardSectionProps {
	section: DashboardSectionModel
	download?: any,
	bluetheme?: boolean
	onTabChange?(tab: string): void
	loading?: boolean
	defaultActiveTab?: string
	dod?: any
}

export class DashboardSection extends React.Component<DashboardSectionProps>{
	onChange = (selected) => {
		if (this.props.onTabChange) {
			this.props.onTabChange(selected)
		}
	}
	render(): JSX.Element {
		const section_class = this.props.bluetheme ? 'blue-theme' : ''
		const tooltip_color = '#909090'
		const menu = (
			<Menu>
				{this.props.dod && this.props.dod.filter && 
					<Menu.Item>
						<span onClick={() => this.props.dod.addDuplicateGraphs(this.props.dod.filter)}>Duplicate</span>
					</Menu.Item>
				}
			  	{this.props.dod && this.props.dod.graphLength && this.props.dod.graphLength > 1 &&
				  	<Menu.Item>
						<span onClick={() => this.props.dod.removeDuplicateGraphs(this.props.dod.listIndex, this.props.dod.filter)}>Remove</span>
					</Menu.Item>
				}
			</Menu>
		  );
		return <Card className={section_class} margin padding loading={this.props.loading} style={{height: '100%', overflowY: 'scroll'}}>
		<ErrorBoundary>
			<div className='dashboard-section-header'>
				<div style={{display: 'flex', marginRight: '1rem', width: '100%'}}>
					{this.props.section.title ? <div style={{width: '100%'}} className='dashboard-section-title'>{this.props.section.title}</div> : null}
					{
						this.props.section.tooltip ?
							<Tooltip title={this.props.section.tooltip}>
								<Icon style={{color: tooltip_color, display: 'flex', alignItems: 'center'}} type='info-circle' />
							</Tooltip>
						: null
					}
				</div>
				<div>
					{this.props.download ? this.props.download : null}
					{this.props.dod && !this.props.dod.downloadPreview &&
					<Dropdown overlay={menu}>
						<Button style={{height: '2rem', width: '3rem', border: '0'}}>
							<Icon type='ellipsis' />
						</Button>
					</Dropdown>
					}
				</div>
			</div>
			{React.Children.count(this.props.children) > 1 ?
				<Tabs activeKey={this.props.defaultActiveTab}
					onChange={(selected)=> this.onChange(selected)}>
					{React.Children.map(this.props.children, (item, index) => {
						return <TabPane tab={this.props.section.tabTitles ?
							(this.props.section.tabTooltips ? <Tooltip title={this.props.section.tabTooltips[index]}> {this.props.section.tabTitles[index]} </Tooltip> : this.props.section.tabTitles[index])
							: 'Tab'} key={index}>
						{item}
					</TabPane>
					})}
				</Tabs>
				: this.props.children
			}
		</ErrorBoundary>
	</Card>
	}
}