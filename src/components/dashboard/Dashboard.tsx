import * as React from 'react'
import {BaseStructureHelper} from './BaseStructure'
import {ErrorBoundary} from '../../utils/ErrorBoundary'
import { Button, Menu, Dropdown, Icon } from 'antd'
export interface DashboardStrucutureItemSize {

}

export interface DashboardStrucuture{
	size : number
	items: number[]
}

export interface DashboardProps {
	name?: string;
	className?: any
	structure?: DashboardStrucuture
	addDuplicateGraphs?: any
	listIndex?: Number
	removeDuplicateGraphs?: any
	graphLength?: any
	filter?: string
	downloadPreview?: boolean
}

export class Dashboard extends React.Component<DashboardProps>{

	render(){
		const menu = (
			<Menu>
				{this.props.filter && 
					<Menu.Item>
						<span onClick={() => this.props.addDuplicateGraphs(this.props.filter)}>Duplicate</span>
					</Menu.Item>
				}
			  	{this.props.graphLength && this.props.graphLength > 1 &&
				  	<Menu.Item>
						<span onClick={() => this.props.removeDuplicateGraphs(this.props.listIndex, this.props.filter)}>Remove</span>
					</Menu.Item>
				}
			</Menu>
		  );
		return <ErrorBoundary>
			<div style={{height: '100%', overflowX: 'scroll'}} className={this.props.className}>
				<div style={{display: 'flex', justifyContent: 'space-between'}}>
					{this.props.name? <p style={{marginRight: '0.7rem', color: '#565656',fontSize: '16px', fontWeight: 500, width: '100%'}}>{this.props.name}</p> : null}
					{/* <DashboardFilter filters={this.props.filters}/> */}
					{this.props.filter && !this.props.downloadPreview &&
					<div>
						<Dropdown overlay={menu}>
							<Button style={{height: '2rem', width: '3rem'}}>
								<Icon type='ellipsis' />
							</Button>
						</Dropdown>
					</div>}
				</div>
				{this.props.structure ?
					<table  style = {{width: '100%', tableLayout: 'fixed'}}>
						<tbody>
						{BaseStructureHelper.genStructure(this.props.structure.items, this.props.structure.size).map((row, index) => {
							return <tr key={index}>
								{row.map(col => {
									return <td key={col.index} colSpan={col.size}>{React.Children.toArray(this.props.children)[col.index]}</td>
								})}
							</tr>
						})}
						</tbody>
					</table>
					: <div style={{marginTop: '1rem'}}>{this.props.children}</div>
				}
			</div>
		</ErrorBoundary>
	}
}