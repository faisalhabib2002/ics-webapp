import * as React from 'react';
import {Card} from '@simplus/siui';
import {FillLayout} from '@simplus/macaw-business'
import { Link } from 'react-router-dom';
import Icon from 'antd/lib/icon'
import {ErrorBoundary} from '../../utils/ErrorBoundary'

/**
 *  Welcome Message
 */
export function Welcome(): JSX.Element {
	return (
		<ErrorBoundary>
			<FillLayout style={{flexGrow: 1, backgroundColor: '#F9FBFF', backgroundImage: 'url(/src/assets/img/BG.jpg)', backgroundSize: 'cover', marginTop: '-70px'}}>
					<Card style={{display: 'flex', margin: '100px', flexDirection: 'column', justifyContent: 'center'}} padding rounded>
						<div style={{display: 'flex', alignItems: 'center'}}>
							<img src='/src/assets/img/icas-logo.png' alt='Icas Logo' width='500px' height='auto' style={{margin: '3rem'}} />
							<div style={{marginRight: '50px'}}>
									<h1 style={{color: '#002E67', fontSize: '36px'}}>Welcome to the Lyra SA Employee Health & Wellness Platform </h1>
									<div style={{color: '#999999', marginTop: '20px'}}>
										<h3>In an era of rapid digital transformation, data is a valuable resource for insight and action. Through the tailored application of our data intelligence, you are able to track your programme success/health indicators, gain insight into the health and wellness of your people and teams, track existing trends and monitor emerging trends.</h3>
									</div>
							</div>
						</div>
						<Link style={{alignSelf: 'flex-end', justifySelf: 'flex-end', fontSize: '24px', marginRight: '50px', color: '#0090cd'}} to='/analysis'>
							<div>Get started <Icon type='arrow-right' /></div>
						</Link>
					</Card>
			</FillLayout>
		</ErrorBoundary>
	)
}
