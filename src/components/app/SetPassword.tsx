import * as React from 'react';
import { connectRobin } from '@simplus/robin-react';
import Input from 'antd/lib/input'
import Form from 'antd/lib/form'
import Button from 'antd/lib/button'
import notification from 'antd/lib/notification'
import { FormComponentProps, } from 'antd/lib/form';
import { robins } from '../../robins'
import { withRouter, RouteComponentProps } from 'react-router-dom'
import { ErrorBoundary } from '../../utils/ErrorBoundary'
import * as owasp from 'owasp-password-strength-test'
import { Card } from 'antd';

const Item = Form.Item;

/**
 *  Login Page
 */
export const SetPassword = withRouter(connectRobin([robins.UsersRobin])(Form.create()(class extends React.Component<FormComponentProps & RouteComponentProps<any>, { sent: boolean }> {
	PASSWORD_LENGTH = process.env.PASSWORD_LENGTH || 14
	constructor(props) {
		super(props)
		this.state = {
			sent: false
		}
	}

	handleSubmit = (evt) => {
		evt.preventDefault();
		const password = this.props.form.getFieldValue('password')
		robins.UsersRobin.when(robins.UsersRobin.post('set-password', '/set-password/' + this.props.match.params.token, { password })).then(key => {
			notification.success({
				message: 'Password changed',
				description: 'Your password has been changed successfully.'
			})
			this.props.history.push('/')
		}).catch(err => {
			notification.error({
				message: 'Update password error',
				description: 'An error has occured while you changed your password.'
			})
		})
		this.setState({ sent: true })
	}

	render(): JSX.Element {
		const { getFieldDecorator, getFieldError, getFieldValue, isFieldTouched } = this.props.form;
		const passwordError = getFieldError('password')
		const password2Error = getFieldError('password-confirm')

		return <ErrorBoundary>
			<div className='login-screen-dimension'>
				<div className='login-screen-image-dimension'>

					<img src='/src/assets/img/Login-new.png' />

				</div>
				<div className='login-screen-text-dimension'>

					<Card className='login-screen-card'
						loading={robins.AuthRobin.isLoading('set-password')}

					>
						<div className='login-screen-logo-dimension'>
							<img src='/src/assets/img/.png' />
						</div>
						{/* <Alert
							message="Password recommendation"
							description={
								<div>
									<h3> This application accepts strong password patterns only. Below are recommendations on how to set your password:</h3>
									<ul>
										<li>Password must be at least {this.PASSWORD_LENGTH} characters long</li>
										<li>We recommend your password has at least one lowercase letter</li>
										<li>We recommend your password has at least one uppercase letter</li>
										<li>We recommend your password has at least one special letter</li>
										<li>We recommend your password has at least one numerical value</li>
									</ul>
								</div>
							}
							type="info"
						/> */}
						<div className='login-text-body'>
							<span className='heading'>Password Recommendation</span>

							<div className='new-password-sub-heading'>
								{/* <span> This application accepts strong password patterns only. Below are recommendations on how to set your password:</span> */}
								<ul>
									<li>Password must be at least {this.PASSWORD_LENGTH} characters long</li>
									<li>We recommend your password has at least one lowercase letter, one uppercase letter, one special character and one numeric value</li>
									{/* <li>We recommend your password has at least one uppercase letter</li>
									<li>We recommend your password has at least one special letter</li>
									<li>We recommend your password has at least one numerical value</li> */}
								</ul>
							</div>
							<Form onSubmit={this.handleSubmit} className='login-form'>
								<Item className='item'>
									<span className='input'>New password:</span>
									{getFieldDecorator('password', {
										rules: [
											{ required: true, message: 'Please input your Password!' },
											{
												validator: (rule, value, done) => {
													owasp.config({ minLength: this.PASSWORD_LENGTH })
													const res = owasp.test(value)
													if (res.strong) {
														done()
													} else {
														done(res.errors[0])
													}
												}
											}
										],
									})(
										<Input type='password' />
									)}
									<span className='input'>Confirm password:</span>
									{getFieldDecorator('password-confirm', {
										rules: [{ required: true, message: 'Please confirm your Password!' },
										{
											validator: (rule, value, cb) => {
												if (value === getFieldValue('password'))
													cb()
												else
													cb(rule || new Error('Passwords do not match!'))
											}, message: 'Passwords do not match!'
										}],
									})(
										<Input type='password' />
									)}
								</Item>
								<div>
									<Button disabled={(!!(password2Error || passwordError)) || !isFieldTouched('password') || !isFieldTouched('password-confirm')} type='primary' htmlType='submit' >
										Set password
									</Button>
								</div>
							</Form>
						</div>
					</Card>
				</div>
			</div>
		</ErrorBoundary>
	}
})))
