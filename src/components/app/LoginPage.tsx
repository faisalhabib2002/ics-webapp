import * as React from 'react';
import { Link } from 'react-router-dom'
import Input from 'antd/lib/input'
import Form from 'antd/lib/form'
import Button from 'antd/lib/button'
import message from 'antd/lib/message'
import Alert from 'antd/lib/alert'
import { FormComponentProps } from 'antd/lib/form';
import { robins } from '../../robins'
import { ErrorBoundary } from '../../utils/ErrorBoundary'
import * as queryString from 'query-string'
import { Card } from 'antd';
const Item = Form.Item;

/**
 *  Login Page
 */

export const LoginPage = Form.create()(class Login extends React.Component<FormComponentProps> {
	handleSubmit = (evt) => {
		evt.preventDefault();

		const email = this.props.form.getFieldValue('email').toLowerCase()
		const password = this.props.form.getFieldValue('password')
		robins.AuthRobin.when(robins.AuthRobin.login(email, password)).then(data => {
			const base_url = location.href.split('?')[0]
			location.replace(base_url)
			message.success('You are now logged in !')
			robins.PermissionsRobin.get('own-permissions', '/')
		}).catch(err => {
			if (err.status === 400)
				message.error('Account not activated or is blocked. Please contact insights@icas.co.za!')
			else
				message.error('Wrong username or password !')
		})
	}


	render(): JSX.Element {
		const query_params = queryString.parse(location.hash.split('?')[1]) || {};
		const { getFieldDecorator, getFieldError, isFieldTouched } = this.props.form;
		const passwordError = getFieldError('password')
		const emailError = getFieldError('email')

		return <ErrorBoundary>
			<div className='login-screen-dimension'>
				<div className='login-screen-text-dimension'>
					{query_params.session_expired ?
						<Alert
							message='Your session has expired.'
							type='info'
							showIcon
						/>
						: null
					}

					<Card className='login-screen-card'
						loading={robins.AuthRobin.isLoading(robins.AuthRobin.ACTIONS.LOGIN) || robins.PermissionsRobin.isLoading('own-permissions')}
					>

						<div className='login-screen-logo-dimension'>
							<img src='/src/assets/img/lyra-logo.png' />
						</div>
						<div className='login-text-body'>
							<span className='heading'>Sign In</span>
							<span className='sub-heading'>Healthy employees build healthy companies.</span>
							<Form onSubmit={this.handleSubmit} className='login-form'>
								<Item className='item'>
									<span className='input'>Email:</span>
									{getFieldDecorator('email', {
										rules: [
											{ required: true, message: 'Please input your email!' },
										],
									})(
										<Input type={'email'} />
									)}
								</Item>
								<Item className='item'>
									<span>Password:</span>
									{getFieldDecorator('password', {
										rules: [{ required: true, message: 'Please input your Password!' }],
									})(
										<Input type='password' />
									)}
								</Item>

								<div>
									<Button disabled={(!!(emailError || passwordError) || (!isFieldTouched('email')) || (!isFieldTouched('password')))} htmlType='submit' >
										Sign in
									</Button>
								</div>
								<div className='forgot-password-dimension'>
									<Link className='link' to='/reset-password'>Forgot your password</Link>
									{/* <Link  to='/login'>Forgot your password</Link> */}
									{/* <a className='link' href='#/reset-password'>Forgot your password</a> */}
								
								</div>
							</Form>
						</div>

					</Card>
				</div>
				<div className='login-screen-image-dimension'>

							<img src='/src/assets/img/Login-new.png' />

						</div>
			</div>

		</ErrorBoundary >
	}
})
