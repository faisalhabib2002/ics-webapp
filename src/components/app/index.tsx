import * as React from 'react';
import {HashRouter as Router, Route, Switch, Redirect} from 'react-router-dom'
import {AnalysisApp} from '../../components/analysis'
import {ClientSettingsApp} from '../../components/ClientSettings'
import {ProfileApp} from '../../components/usermanagement/pages/Profile'
import {UsermanagementApp} from '../../components/usermanagement'
import {LoginPage} from './LoginPage';
import {SetPassword} from './SetPassword';
import {ResetPassword} from './ResetPassword';
import {robins} from '../../robins'
import {connectRobin} from '@simplus/robin-react'
import {ErrorBoundary} from '../../utils/ErrorBoundary'
import {SessionValidation} from '../../utils/SessionValidation'
import CookieConsent, {resetCookieConsentValue} from "react-cookie-consent";
import DodReport from 'src/components/analysis/ClientInfo/DodReport'

import { Loader } from '@simplus/siui';
import axios from 'axios'
import { Alert } from 'antd';
const {AuthRobin, PermissionsRobin, ClientsRobin} = robins

axios.interceptors.response.use((response) => {
	return response;
	},
	(error) => {
		// Some places I am receiving error.status and some others as error.response.status
		if ((error.status === 401 || error.response.status === 401) && (AuthRobin.getUserInfo())) {
			const base_url = location.href.split('?')[0]
			AuthRobin.logout();
			resetCookieConsentValue()
			location.replace(`${base_url}?session_expired=true`);
			location.reload();
		}
		return Promise.reject(error.response);
	}
   );


setInterval(() => SessionValidation(), 1000)

@connectRobin([AuthRobin, PermissionsRobin, ClientsRobin])

export class App extends React.Component {
	componentWillMount(): void {
		robins.AuthRobin.when(robins.AuthRobin.loadUserInfo()).then( key => {
		})
	}
	componentDidMount(): void {
		
		PermissionsRobin.get('own-permissions', '/')

	}
	/**
	 *  Render method
	 */
	render(): JSX.Element {	
		
		// Widget Section
		// const HTML_Window = (window as any)
		// if(AuthRobin.getUserInfo())
		// {
		// 	// If user is logged in, display widget with all articles
		// 	if(HTML_Window.Beacon('info') && HTML_Window.Beacon('info').beaconId !=  'c34c841f-648b-4c5c-8bff-b1cc1ff97b78')
		// 	{
		// 		HTML_Window.Beacon('destroy');
		// 		HTML_Window.Beacon('init', 'c34c841f-648b-4c5c-8bff-b1cc1ff97b78');
		// 	}
		// }
		// else {
		// 	// If user is not logged in, display widget with all login issue
		// 	if(HTML_Window.Beacon('info') && HTML_Window.Beacon('info').beaconId !=  '86e64002-0295-447a-a12a-d2c880ff5a32')
		// 	{
		// 		HTML_Window.Beacon('destroy');
		// 		HTML_Window.Beacon('init', '86e64002-0295-447a-a12a-d2c880ff5a32');
		// 	}
		// }
		
		// Top level component with router
		return <div className='app-container' style={{position: 'relative'}}>
			<ErrorBoundary>
				{AuthRobin.isLoading(AuthRobin.ACTIONS.LOGOUT) ? <Loader /> : null}
				<Router>
					{AuthRobin.getUserInfo() ? 
					<Switch>
						<Redirect exact from='/login' to='/' />
						<Route exact path='/set-password/:token' component={SetPassword} />,
						<Route exact path='/reset-password' component={ResetPassword} />,
						<Route exact path='/usermanagement' component={UsermanagementApp} />
						<Route exact path='/dod/:id/report/:key/:startDate/:endDate' component={DodReport} />
						<Route exact path='/logout' component={LoginPage} />
						<Route path='/' component={AnalysisApp} />,
						{/* <Redirect to='/' /> */}
						{/* /dod/CG2810/report/105458631/2025-01-04/2025-01-20 */}
					</Switch>: (
						AuthRobin.isLoading(AuthRobin.ACTIONS.FETCH_USER_INFO) ? null :
						<Switch>
							<Redirect from='/logout' to='/'/>
							<Route exact path='/reset-password' component={ResetPassword} />,
							<Route exact path='/set-password/:token' component={SetPassword} />,
							<Route path='/' component={LoginPage} />
							
						</Switch>
					)}
				</Router>
			</ErrorBoundary>
			{window.location.host === 'insights.pp.simpluscloud.com' ? <Alert
					message="THIS IS TESTING ENVIRONMENT, DO NOT USE IT FOR CLIENT DEMOS"
					type="warning"
					closable
					style={{width: '100%', height: 40, position: 'absolute', zIndex: 1, textAlign: 'center'}}
					/> : null}
			<CookieConsent
				style={{backgroundColor: '#F8FBFF', color: '#202224', textAlign: 'center'}}
				buttonStyle={{backgroundColor: '#133D54', color: 'white', borderRadius: 5}}
				buttonText="I Accept"
			>
				This website uses cookies to enhance the user experience.
			</CookieConsent>
			<div className='main-footer'>Powered by<a href="http://www.simplusinnovation.com" target="_blank" style={{marginLeft: '4px'}}>Simplus Innovation</a></div>
		</div>
	}
}
export default App;