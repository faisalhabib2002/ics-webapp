import * as React from 'react';
import Input from 'antd/lib/input'
import Form from 'antd/lib/form'
import Button from 'antd/lib/button'
import notification from 'antd/lib/notification'
import { FormComponentProps, } from 'antd/lib/form';
import { robins } from '../../robins'
import { Link } from 'react-router-dom'
import { ErrorBoundary } from '../../utils/ErrorBoundary'
import { Card } from 'antd';
const Item = Form.Item;

/**
 *  Login Page
 */

export const ResetPassword = Form.create()(class extends React.Component<FormComponentProps> {
	handleSubmit = (evt) => {
		evt.preventDefault();

		const email = this.props.form.getFieldValue('email')
		robins.AuthRobin.when(robins.AuthRobin.get('reset-password', `/reset-password/${email}`)).then(data => {
			notification.success({ description: 'You should receive an email shortly', message: 'Success' })
			robins.PermissionsRobin.get('own-permissions', '/')
		}).catch(err => {
			notification.error({ message: 'Error', description: 'An error occured while trying to send you the reset email.' })
		})
	}

	render(): JSX.Element {
		const { getFieldDecorator, getFieldError, isFieldTouched } = this.props.form;
		const emailError = getFieldError('email')
		const formItemLayout = {
			wrapperCol: {
				xs: {
					span: 24,
					offset: 0,
				}
			}
		};
		return <ErrorBoundary>
			<div className='login-screen-dimension'>
				<div className='login-screen-text-dimension'>

					<Card className='login-screen-card'
						loading={robins.AuthRobin.isLoading('reset-password')}
					>
						<div className='login-screen-logo-dimension'>
							<img src='/src/assets/img/lyra-logo.png' />
						</div>
						<div className='login-text-body'>
							<span className='heading'>Next Steps</span>
							<span className='forgot-sub-heading'>Enter your email address to update your password.</span>
							<Form onSubmit={this.handleSubmit} className='login-form'>
								<Item className='item'  {...formItemLayout}>
									<span className='input'>Email:</span>
									{getFieldDecorator('email', {
										rules: [
											{ required: true, message: 'Please input your email!' },
										],
									})(
										<Input type={'email'} />
									)}
								</Item>

								<div>
									<Button disabled={(!!(emailError) || (!isFieldTouched('email')))} htmlType='submit'>
										Submit
									</Button>
								</div>

								<div className='forgot-password-dimension'>
									{/* <Link to='/login'>Back to login</Link> */}
									<a href='#/login'>Back to login</a>
								</div>
							</Form>
						</div>
					</Card>
				</div>
				<div className='login-screen-image-dimension'>

					<img src='/src/assets/img/Login-new.png' />

				</div>
			</div>
		</ErrorBoundary>
	}
})
