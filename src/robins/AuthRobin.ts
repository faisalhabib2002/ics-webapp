import {Robin, CoreAction, Status} from '@simplus/robin'
import * as update from 'immutability-helper'
import { RobinReducerState } from '@simplus/robin/build/core/RobinReducerState';

export interface AuthRobinState<Model> extends RobinReducerState<any> {
	loggedIn : boolean,
	user?: Model
}

export class AuthRobin<UserModel = any> extends Robin<AuthRobinState<UserModel>> {
	public ACTIONS = {
		LOGIN  : 'login',
		LOGOUT : 'logout',
		LOGGEDIN : 'loggedin',
		FETCH_USER_INFO : 'fetch_user_info'
	}

	getActions() {
		return this.ACTIONS
	}

	login(email: string, password: string){
		return this.post(this.ACTIONS.LOGIN, '/login', { email, password })
	}

	loadUserInfo() {
		return this.get(this.ACTIONS.FETCH_USER_INFO, '/user-info', {
			withCredentials: true
		})
	}

	getUserInfo() {
		return this.getResult(this.ACTIONS.FETCH_USER_INFO)
	}

	isLoggedIn() {
		return this.get(this.ACTIONS.LOGGEDIN, '/logged-in')
	}

	logout() {
		return this.get(this.ACTIONS.LOGOUT, '/logout')
	}

	reducer() {
		const superReducer = super.reducer()
		const superDefaultState = superReducer()

		return (state: AuthRobinState<UserModel> = {loggedIn : false}, action: CoreAction<UserModel>): AuthRobinState<UserModel> => {
			if(!action || !action.type)
				return state;
			
			const actionType = this.parseActionType(action.type)

			if(actionType.namespace !== (this.options as any).namespace)
				return state

			if(!(state as any).data || !(state as any).status || !(state as any).errors) {
				state = {...superDefaultState, ...state }
			}
			state = superReducer(state, action)
			
			if(actionType.status === Status.FETCHED){
				switch(actionType.action){
					case this.ACTIONS.LOGIN:
					this.loadUserInfo()
					return (update as any)(state, { loggedIn : { $set : true}})
					case this.ACTIONS.LOGOUT:
					// location.reload();
					return (update as any)(state, { loggedIn : { $set : false}})
					case this.ACTIONS.FETCH_USER_INFO:
					return (update as any)(state, { user : { $set : action.data}, loggedIn : { $set : true } })
				}
			}
			return state
		}
	}
}