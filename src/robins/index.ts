import {CollectionRobin, Robin} from '@simplus/robin'
import {ClientSettingsModel, ClientsModel, IndustryModel, ProductsModel, NotesModel, TiersModel, StoriesModel} from '../models'
import {UsersRobin} from './UsersRobin'
import {AuthRobin} from './AuthRobin'

export const robins = {
	clientSettings: new CollectionRobin<ClientSettingsModel>({
		baseUrl : '/api/v1/settings/collection'
	}),
	NotesRobin: new CollectionRobin<NotesModel>({
		baseUrl : '/api/v1/notes/collection'
	}),
	StoriesRobin: new CollectionRobin<StoriesModel>({
		baseUrl : '/api/v1/stories/collection'
	}),
	ClientsRobin: new CollectionRobin<ClientsModel>({
		baseUrl : '/api/v1/collections/clients',
		cache : {
			methods: ['get']
		}
	}),
	ProblemsRobin: new CollectionRobin<any> ({
		baseUrl : '/api/v1/collections/problems',
		cache : {
			methods: ['get']
		}
	}),
	ProblemClustersRobin: new CollectionRobin<any> ({
		baseUrl : '/api/v1/collections/problemclusters',
		cache : {
			methods: ['get']
		}
	}),
	IndustryRobin: new CollectionRobin<IndustryModel>({
		baseUrl : '/api/v1/collections/industries',
		cache : {
			methods: ['get']
		}
	}),
	siteLevelsRobin: new CollectionRobin<any>({
		baseUrl : '/api/v1/collections/site-levels',
		cache : {
			methods: ['get']
		}
	}),
	ProductsRobin: new CollectionRobin<ProductsModel>({
		baseUrl : '/api/v1/collections/products',
		cache : {
			methods: ['get']
		}
	}),
	TiersRobin: new CollectionRobin<TiersModel>({
		baseUrl : '/api/v1/collections/tiers',
		cache : {
			methods: ['get']
		}
	}),
	UsersRobin: new UsersRobin({
		baseUrl : '/users'
	}),
	AuthRobin : new AuthRobin({
		baseUrl : '/oauth',
	}),
	PermissionsRobin : new Robin({
		baseUrl: '/hornbill/own/permissions',
	}),
	AnalyticsNewRobin : new CollectionRobin<any>({
		baseUrl : '/api/v1/analytics-new',
		cache : true
	}),
	AnalyticsRobin : new CollectionRobin<any>({
		baseUrl : '/api/v1/analytics',
		cache : true
	}),
	SummaryRobin : new CollectionRobin<any>({
		baseUrl : '/api/v1/summary'
	}),
	DoDRobin : new CollectionRobin<any>({
		baseUrl : '/api/v1/collections/dod'
	})
}