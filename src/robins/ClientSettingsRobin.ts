import {CollectionRobin} from '@simplus/robin'
import {ClientSettingsModel} from '../models'
export class ClientSettingsRobin extends CollectionRobin<ClientSettingsModel> {
	/**
	 *  Robin to create new client settings
	 *  @param {string} id - ID of client
	 *  @param {string} img - Path for client logo
	 *  @param {number} contractValue - Contract value of client
	 *  @param {number} workingHours - Working hours of client
	 *  @param {number} avgEmployeeWage - Average employee wage of client
	 */
	createClientSettings(id: string, img: string, workingHours: number, avgEmployeeWage: number): void {
		this.post(
			'createClientSettings',
			`/`,
			{
				'client': id,
				'img': img,
				// 'contractValue': contractValue,
				'workingHours': workingHours,
				'avgEmployeeWage': avgEmployeeWage
			}
		);
	}
	/**
	 *  Robin to update client settings
	 *  @param {string} id - ID of client
	 *  @param {string} img - Path for client logo
	 *  @param {number} contractValue - Contract value of client
	 *  @param {number} workingHours - Working hours of client
	 *  @param {number} avgEmployeeWage - Average employee wage of client
	 */
	updateClientSettings(id: string, img: string, workingHours: number, avgEmployeeWage: number): void {
		this.put(
			'updateClientSettings',
			`/${id}`,
			{
				'img': img,
				// 'contractValue': contractValue,
				'workingHours': workingHours,
				'avgEmployeeWage': avgEmployeeWage
			}
		);
	}
	/**
	 *  Robin to delete a client settings
	 *  @param {string} id - ID of client
	 */
	deleteClientSettings(id: string): void {
		this.delete('deleteClientSettings', `${id}`)
	}
}
