export interface ClientsModel {
	_key: string
	Name: string
	Industry: string
	EmployeeCount: number
	SegmentSize: string
	Tier: String
	FinancialYearEnd: string
	ContractValue: number
	BranchTo: string
	active: boolean
	Label: string
	siteMappings: Array<{
		siteLevel: string
	}>
}