export interface ClientSettingsModel {
	_key?: string
	client: string
	img: string
	icasCustodian: string
	clientCustodian: string
	avgWorkingHours: number
	avgEmployeeWage: number
	// siteMapping: Array<{
	// 	name: string
	// 	alias: string
	// }>
	products: string[]
	// benchmarks: Array<{
	// 	name: string
	// 	category: string
	// }>
	// sla: {
	// 	callbacks: number
	// 	callAnswerRate: number
	// 	sessions: number
	// }
	reportingCycle: Date
}