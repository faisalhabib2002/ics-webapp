

export interface UsersModel {
	_id: string,
	name: string,
	password: string,
	picture: string,
	status: boolean,
	email: string,
	client: string,
	role ?: string,
}


export default UsersModel