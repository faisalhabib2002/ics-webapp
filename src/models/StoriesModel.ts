export interface StoriesModel {
	_key: string
	title: string
	text: string
	user: string
	date: Date
}