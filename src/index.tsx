import * as React from 'react';
import * as ReactDOM from 'react-dom'
import {Breadcrumbs} from 'src/components/breadcrumbs/index'
import {App} from 'src/components/app/'
import 'antd/dist/antd.less';
import './style/index.less';
require('./style/index.less');
import '@simplus/siui/style/index.less'
import '@simplus/macaw-business/style/index.less'
import {NavBarApp} from 'src/components/navigation'
import {robins} from 'src/robins'
import {RobinProvider} from '@simplus/robin'
import RobinReact from '@simplus/robin-react'

const provider = new RobinProvider(robins);


RobinReact.setProvider(provider);

// ReactDOM.render(<Breadcrumbs/>, document.getElementById('bread-crumbs'))
// ReactDOM.render(<NavBarApp/>, document.getElementById('nav'))
ReactDOM.render(<App/>, document.getElementById('root'))