#!/bin/sh

docker stop ics-webapp;
docker rm ics-webapp;
docker rmi ics-webapp;
docker build -t ics-webapp .;
docker run --name ics-webapp -d -p 8085:80 ics-webapp;


